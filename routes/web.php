<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::prefix('/ship')->group(function () {
	Route::get('/first', 'ShipController@getFirstMap')->name('first-map-search');
	Route::get('/second', 'ShipController@getSecondMap')->name('second-map-search');
	Route::get('/couriers', 'ShipController@getCouriers')->name('get-couriers');
	Route::get('/rates', 'ShipController@checkRate')->name('get-rates');
});

Route::get('/', 'publicController@produk');

Route::get('/chartpublic', 'publicController@chartpublic');

Route::get('/produk', 'publicController@produk')->name('produk');

Route::get('/hapus/chart/public/{id}', 'publicController@hapuschartpublic');

Route::get('/publicpayment', 'publicController@publicpayment');

Route::post('/publicuploasbuktitrf/{invoice}', 'publicController@publicuploasbuktitrf');

Route::post('/cekpoin', 'publicController@cekpoin');

Route::get('/publicaddtochart', 'publicController@publicaddtochart');

Route::POST('/prosespublicchart', 'publicController@prosespublicchart');

Route::get('/tambahchartmanualPublic', 'publicController@tambahchartmanualPublic');

Route::get('/gagalLogin', 'HomeController@gagalLogin');

Route::get('/printperlustok', 'AdminController@printperlustok');

Route::get('/printstokopname', 'AdminController@printstokopname');

Route::get('/home', 'HomeController@home')->name('dashboard');

Route::get('/dashboardcrm', 'Crmcontroller@dashboardcrm'); //crm  controller mulai dari sini

Route::get('/contentplanning', 'Contentcontroller@contentplanning'); //conten controller mulai dari sini

Route::get('/contentplanningday/{tanggal}', 'Contentcontroller@contentplanningday');

Route::post('/taskkontenbaru', 'Contentcontroller@taskkontenbaru');

Route::get('/todolist', 'Todocontroller@todolist'); //todo controller mulai dari sini

Route::get('/todolist/{filter}', 'Todocontroller@todolist');

Route::get('/detailtodo/{id}', 'Todocontroller@detailtodo');

Route::get('/rubah/minimum', 'ActController@rubahminimum');

Route::post('/rubah/password', 'ActController@rubahpassword');

Route::get('/historybelanja', 'AdminController@historyBelanja')->name('historybelanja');

Route::get('/bayartempoagen/{id}', 'AdminController@bayartempoagen');

Route::get('/prosesbayartempoagen/{id}', 'ActController@prosesbayartempoagen');

Route::get('/stok', 'AdminController@stok')->name('stok');

Route::get('/stokcategory', 'AdminController@stokcategory')->name('stokcategory');

Route::get('/stoksupplier', 'AdminController@stoksupplier')->name('stoksupplier');

Route::get('/stokpending', 'AdminController@stokpending')->name('stokpending');

Route::get('/stokkedatangan', 'AdminController@stokkedatangan')->name('stokkedatangan');

Route::post('/uploadfotoproduk/{id}', 'ActController@uploadfotoproduk');

Route::get('/hapusstokpending/{id}', 'ActController@hapusstokpending');

Route::get('/editproduk/{id}', 'AdminController@editproduk');

Route::get('/updatedetailbarang/{id}', 'ActController@updatedetailbarang');

Route::get('/tambah/barang', 'ActController@belanjaBarangProsses');

Route::get('/suksesTambahBarang', 'ActController@suksesTambahBarang');

Route::get('/print/qrcode/{id}', 'ActController@printqrcode');

Route::get('/print/thermal/qrcode/{id}', 'ActController@printthermalqrcode');

Route::get('/tambah/category', 'ActController@tambahcategory');

Route::get('/tambah/supplier', 'ActController@tambahsupplier');

Route::get('/ganti/namasup/{id}', 'ActController@gantinamasup');

Route::get('/ganti/alamatsup/{id}', 'ActController@gantialamatsup');

Route::get('/hapus/sup/{id}', 'ActController@hapussupplier');

Route::get('/hapus/category/{id}', 'ActController@hapuscategory');

Route::get('/ganti/hpsup/{id}', 'ActController@gantihpsup');

Route::get('/ganti/namastok/{id}', 'ActController@gantinamastok');

Route::get('/ganti/catstok/{id}', 'ActController@ganticatstok');

Route::get('/ganti/jmlstok/{id}', 'ActController@updatestok');

Route::get('/hapus/barang/{id}', 'ActController@hapusbarang');

Route::get('/caristok', 'AdminController@caristok');

Route::get('/perifstoksamapi/{id}', 'AdminController@perifstoksamapi');

Route::get('/stoksampai/{id}', 'ActController@stoksampai');

Route::post('/jualact', 'ActController@penjualanProses');

Route::post('/test', 'AdminController@test');

Route::get('/jadikanecer/{id}', 'ActController@jadikanecer');

Route::get('/pindahkanstok/{id}', 'AdminController@pindahkanstok');

Route::get('/prosespindahkanstok/{id}', 'ActController@prosespindahkanstok');

Route::get('/pesananagen', 'AdminController@pesananagen')->name('pesananagen');

Route::get('/pesanancs', 'AdminController@pesanancs')->name('pesanancs');

Route::get('/reseller', 'AdminController@reseller')->name('resellerchart');

Route::get('/resellerorder', 'AdminController@resellerorder')->name('resellerorder');

Route::get('/historypembayaranreseller', 'AdminController@historypembayaranreseller')->name('historypembayaranreseller');

Route::get('/detailorderreseller/{id}', 'AdminController@detailorderreseller');

Route::get('/reselleraddtochart', 'ActController@reselleraddtochart');

Route::get('/Resellerchart/manual', 'ActController@tambahchartmanualReseller');

Route::post('/prosesReseller/chart', 'ActController@proseschartReseller');

Route::get('/updateprosesreseller/{id}', 'ActController@updateprosesreseller');

Route::get('/updatekirimreseller/{id}', 'ActController@updatekirimreseller');

Route::post('/uploasbuktitrf/{id}', 'ActController@uploasbuktitrf');

Route::get('/historyreseller', 'AdminController@historyreseller')->name('historyreseller');

Route::get('/tolakpesananreseller/{id}', 'AdminController@tolakpesananreseller');

Route::post('/prosestolakpesananreseller', 'ActController@prosestolakpesananreseller');

Route::get('/shop', 'AdminController@shop')->name('shop');

Route::get('/shopselect', 'AdminController@shopselect')->name('shopselect');

Route::get('/tambah/chart', 'ActController@tambahchart');

Route::get('/tambah/chart/manual', 'ActController@tambahchartmanual');

Route::get('/hapus/chart/{id}', 'ActController@hapuschart');

Route::POST('/proses/chart', 'ActController@proseschart');

Route::get('/update/lunas', 'ActController@updatelunas');

Route::get('/pembatalanpesanan/{id}', 'AdminController@pembatalanpesanan');

Route::POST('/prosespembatalanpesanan', 'ActController@prosespembatalanpesanan');

Route::get('/invoicethermal/{id}', 'AdminController@invoicethermal');

Route::get('/invoicepaper/{id}', 'AdminController@invoicepaper');

Route::get('/invoice/thermalReseller/{id}', 'AdminController@invoicethermalReseller');

Route::get('/keuangan', 'AdminController@keuangan')->name('keuangan');

Route::get('/tambah/akun', 'ActController@tambahakun');

Route::get('/tambahrekening', 'ActController@tambahrekening');

Route::get('/hapus/akun/{id}', 'ActController@hapusakun');

Route::get('/dana/masuk', 'ActController@danamasuk');

Route::get('/dana/keluar', 'ActController@danakeluar');

Route::get('/LaporanTransaksi', 'AdminController@LaporanTransaksi');

Route::get('/user', 'AdminController@user')->name('user');

Route::get('/updateuserdetail/{id}', 'ActController@updateuserdetail');

Route::post('/tambah/user', 'ActController@tambahuser');

Route::get('/hapususer/{id}', 'ActController@hapususer');

Route::get('/detailuser/{id}', 'AdminController@detailuser');

Route::get('/laporan', 'AdminController@laporan');

Route::get('/yearReport', 'AdminController@yearReport');

Route::get('/inventaris', 'AdminController@inventaris');

Route::get('/analytic', 'AdminController@analytic');

Route::post('/kirimpoin', 'ActController@kirimpoin');

Route::get('/vocher', 'AdminController@vocher');

Route::get('/profile', 'AdminController@profile');

Route::get('/setings', 'AdminController@setings');

Route::post('/updateprofile/{id}', 'ActController@updateprofile');

Route::post('/uploadfotoprofile/{id}', 'ActController@uploadfotoprofile');

Route::get('/setingspass', 'AdminController@setingspass');

Route::get('/setingsstore', 'AdminController@setingsstore');

Route::post('/updatetokodetail', 'ActController@updatetokodetail');

Route::post('/gantipassword/{id}', 'ActController@gantipassword');

Route::get('/rencanabayar/{id}', 'ActController@rencanabayar');

Route::get('/hapusrencanabayar/{id}', 'ActController@hapusrencanabayar');

Route::post('/buatpembayaran', 'ActController@buatpembayaran');

Route::get('/pembayaran', 'AdminController@pembayaran')->name('pembayaran');

Route::get('/detailhistorybayar/{id}', 'AdminController@detailhistorybayar');

Route::post('/pembayaranditerima/{id}', 'ActController@pembayaranditerima');

Route::get('/pembayarangagal/{id}', 'ActController@pembayarangagal');

Route::get('/listcabang', 'AdminController@listcabang')->name('listcabang');

Route::post('/buatcabangbaru', 'ActController@buatcabangbaru');

Route::get('/hapuscabang/{id}', 'ActController@hapuscabang');

Route::get('/editcabang/{id}', 'AdminController@editcabang');

Route::get('/listpenerimaan', 'AdminController@listpenerimaan');

Route::post('/updatepencairanmanual', 'PenerimaanController@updatepencairanmanual');

Route::get('/keuanganadvance', 'AdminController@keuanganadvance')->name('keluaradvance');

Route::get('/dana/masukadvance', 'PenerimaanController@danamasukadvance');

Route::get('/dana/keluaradvance', 'PenerimaanController@danakeluaradvance');

Route::post('/uploaddatapencairan', 'PenerimaanController@uploaddatapencairan');

Route::get('/danaantarrekening', 'PenerimaanController@danaantarrekening');

Route::get('/role', 'AdminController@role')->name('role');

Route::post('/tambahrole', 'ActController@tambahrole');

Route::get('/hapusrole/{id}', 'ActController@hapusrole');

Route::get('/editrole/{id}', 'AdminController@editrole');

Route::get('/submiteditrole/{id}', 'ActController@submiteditrole');

Route::get('/listmarketplace', 'AdminController@listmarketplace')->name('marketplace');

Route::get('/tambahmarketplace', 'ActController@tambahmarketplace');

Route::get('/editmarketplace/{id}', 'AdminController@editmarketplace');

Route::get('/hapusmarketplace/{id}', 'ActController@hapusmarketplace');

Route::get('/updatemarketplace/{id}', 'ActController@updatemarketplace');
