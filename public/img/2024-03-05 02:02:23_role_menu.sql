-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 05, 2024 at 01:59 AM
-- Server version: 10.6.16-MariaDB-cll-lve
-- PHP Version: 8.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u9712563_bisnisgood`
--

-- --------------------------------------------------------

--
-- Table structure for table `role_menu`
--

CREATE TABLE `role_menu` (
  `id_menu` int(12) NOT NULL,
  `role_id` int(12) NOT NULL,
  `role_name` varchar(25) NOT NULL,
  `user_id_number` int(12) NOT NULL,
  `home_menu` varchar(60) NOT NULL,
  `dashboard` varchar(12) NOT NULL,
  `system_transaksi` varchar(12) NOT NULL,
  `inventory` varchar(12) NOT NULL,
  `list_cabang` varchar(12) NOT NULL,
  `pesanan_agen` varchar(12) NOT NULL,
  `pembayaran_agen` varchar(12) NOT NULL,
  `history_belanja` varchar(12) NOT NULL,
  `tagihan` varchar(12) NOT NULL,
  `keuangan` varchar(12) NOT NULL,
  `settings` varchar(12) NOT NULL,
  `reseller_chart` varchar(12) NOT NULL,
  `reseller_order` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `role_menu`
--

INSERT INTO `role_menu` (`id_menu`, `role_id`, `role_name`, `user_id_number`, `home_menu`, `dashboard`, `system_transaksi`, `inventory`, `list_cabang`, `pesanan_agen`, `pembayaran_agen`, `history_belanja`, `tagihan`, `keuangan`, `settings`, `reseller_chart`, `reseller_order`) VALUES
(4, 1, 'super admin', 16, 'dashboard', 'tampil', 'sembunyi', 'tampil', 'tampil', 'tampil', 'sembunyi', 'tampil', 'tampil', 'tampil', 'tampil', 'sembunyi', 'tampil'),
(11, 8, 'admin gudang', 1, 'pesananagen', 'sembunyi', 'tampil', 'tampil', 'sembunyi', 'tampil', 'sembunyi', 'sembunyi', 'tampil', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi'),
(12, 9, 'reseller', 3, 'resellerchart', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi', 'tampil', 'tampil'),
(13, 10, 'customor service', 3, 'dashboard', 'tampil', 'tampil', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi', 'sembunyi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `role_menu`
--
ALTER TABLE `role_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `role_menu`
--
ALTER TABLE `role_menu`
  MODIFY `id_menu` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
