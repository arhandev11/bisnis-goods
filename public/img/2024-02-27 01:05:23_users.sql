-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 03, 2024 at 11:58 AM
-- Server version: 10.6.16-MariaDB-cll-lve
-- PHP Version: 8.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u9459579_arsitekhijauapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `foto` varchar(250) DEFAULT NULL,
  `divisi` varchar(45) DEFAULT NULL,
  `manager` int(11) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `foto`, `divisi`, `manager`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(38, 'Rizki Al Ayubi', 'rizkyalayubi@rocketmail.com', 1, '1671514370_photo_2022-12-20 12.32.42.jpeg', 'Oprasional', 1, NULL, '$2y$10$eagViCjh6edVGQthiiH9e.yM8Przypg4muY23y/I9vVMf.DVe2n5m', NULL, '2022-12-19 17:00:00', NULL),
(39, 'Rahmatullah', 'rahmatullah99@gmail.com', 1, 'pekerja.png', 'Marketing', 1, NULL, '$2y$10$rPgQVg07p3jChf/e.FFgy.S7ZzsQR3HODiVbu/jcLpXfs7XsUazKi', NULL, '2022-12-19 17:00:00', NULL),
(40, 'Melati Agustin', 'melatiagustin99@gmail.com', 11, '1671528133_photo_6219793019423535651_y.jpg', 'Konten Kreator', 1, NULL, '$2y$10$hH.gcNzGUbNSoVWj1xnJcu6irZBIWOxgI3HM3jcmikuhHMva3eXO.', NULL, '2022-12-19 17:00:00', NULL),
(41, 'Rizky Ramadhan', 'rizky.workde@gmail.com', 13, '1671588730_IMG_8119.jpg', 'Konten Kreator', 0, NULL, '$2y$10$EeyKXAjDYXu9Nc3xmoLmdOM4ZNJHlk6DOmmv4sqj2SFNP8DG4nDQC', NULL, '2022-12-19 17:00:00', NULL),
(42, 'Nuri Yuliani', 'nuriyuliani99@gmail.com', 2, '1671849796_photo_6165504224780201769_c.jpg', 'Finance', 1, NULL, '$2y$10$R7SNSkzmBXlvjAWaYUkvVOFAd8qdlE17GISgMqIFpMQq9n2r86o02', NULL, '2022-12-19 17:00:00', NULL),
(43, 'akuntan', 'akuntan@gmail.com', 2, 'pekerja.png', 'Finance', 0, NULL, '$2y$10$GT4xlL1.PRdEQh0EK4fgCedIr8sbovZeL4gygDMY7YUpR8U0hoVte', NULL, '2022-12-19 17:00:00', NULL),
(44, 'M.Aulia Zikri', 'arch.muhammadzikri@gmail.com', 20, '1691832949_1asd.png', 'Oprasional', 1, NULL, '$2y$10$zTTHY1OGTFt5uehZVneoWuqb2eSXJ0/vnu2cVW7j0n6svJ3GkVa4C', NULL, '2022-12-19 17:00:00', NULL),
(45, 'Fatkhu', 'fatkhurohman530@gmail.com', 7, '1691834700_yuhu.jpg', 'Oprasional', 0, NULL, '$2y$10$HpvThnoVoSzFS5TKE03rxOx1pBnLXE4zCEAik9k0H0GQXx./49tcq', NULL, '2022-12-19 17:00:00', NULL),
(46, 'Muhamad Fiqri Fadilah, S.Psi', 'hrd@arsitekhijau.com', 4, '1673581285_WhatsApp Image 2022-03-02 at 13.00.13.jpeg', 'Hrga', 1, NULL, '$2y$10$jxsIu7NFlTEJ0gPLDfRahujswSgwm1gRq1NCyzQA4cKoMiuzV4WMy', NULL, '2023-01-11 17:00:00', NULL),
(47, 'M Tegar Supriyono', 'mtegarswork@gmail.com', 4, 'pekerja.png', 'Hrga', 1, NULL, '$2y$10$sjRO7xPg5vs60aEiFbmc0.5vpVyd00Ah4cwVlHUGnsK1S7uKLxiyC', NULL, '2023-01-11 17:00:00', NULL),
(48, 'Ario Darmoko, S.T., M.T', 'ariodarmoko84@gmail.com', 5, 'pekerja.png', 'Oprasional', 1, NULL, '$2y$10$0YibQjDyK9s5traPk2C/m.Zln/442Xv1lIUYZ12k25AwPv8n0FiZK', NULL, '2023-01-12 17:00:00', NULL),
(52, 'Amrul Fatih Thoriq, S.Ars', 'fatihamrul0@gmail.com', 5, 'pekerja.png', 'Oprasional', 0, NULL, '$2y$10$1V36jNBA1dC21I2X.BSmoO/gA/VBCtOjOrT6MPr/l6OFBtM2.T5S.', NULL, '2023-01-12 17:00:00', NULL),
(53, 'Asiddiqi', 'asiddiqigrass@gmail.com', 6, '1688952411_kucing lmaun.jpg', 'Oprasional', 0, NULL, '$2y$10$91MqulVRjKOG8jG3sDV81u4RxVQ415tUoYHi72ZPCs15l3tcEHrZK', NULL, '2023-01-12 17:00:00', NULL),
(54, 'Tedi Hermawan', 'tedihermawan2127@gmail.com', 6, 'pekerja.png', 'Oprasional', 0, NULL, '$2y$10$HkPGILIYvqEHxg2geezX6.005.Shb2m6kQR1epZpFdkZXtNpj7pXm', NULL, '2023-01-12 17:00:00', NULL),
(55, 'Akbar Prasetyo Pambudi', 'zainelshirazi@gmail.com', 6, 'pekerja.png', 'Oprasional', 0, NULL, '$2y$10$AtZ4IMWxnwARHswHjlrZuOvA.0NvfJnndeIUp8ZoL4FScfrgfGOTu', NULL, '2023-01-12 17:00:00', NULL),
(56, 'Dian Sindi Oktaviani, S.T', 'sindidian237@gmail.com', 9, 'pekerja.png', 'Oprasional', 0, NULL, '$2y$10$7Y83a6uUDBowv2hUNJf40.0XN4vOsdT8ru3sfN.5TMS2Fa6rwLi9y', NULL, '2023-01-12 17:00:00', NULL),
(57, 'Moh Rido Faizin', 'mridofaizin7@gmail.com', 6, 'pekerja.png', 'Oprasional', 0, NULL, '$2y$10$rPgQVg07p3jChf/e.FFgy.S7ZzsQR3HODiVbu/jcLpXfs7XsUazKi', NULL, '2023-01-12 17:00:00', NULL),
(58, 'Rachmi Pertiwi, S.Ars', 'rachmipertiwi@gmail.com', 5, '1692091939_photo_2023-04-11_15-52-35.jpg', 'Oprasional', 0, NULL, '$2y$10$7CFzX8OOgwUg3oFOy0Su2epPIPd0AagcC28cDHdLnyc/NK08Y1gfy', NULL, '2023-01-12 17:00:00', NULL),
(60, 'Yudha A Rangga Malela, S.T', 'yudhaagung.rm@gamil.com', 8, 'pekerja.png', 'Oprasional', 0, NULL, '$2y$10$gCrmrjMn/WiRrKD.TCw5tuWJZCc7JYAnAaruhRvW4nXsVjlzzJ1mG', NULL, '2023-01-12 17:00:00', NULL),
(61, 'M. Bachtiar Purnomo Aji', 'muh.bachtiar98@gmail.com', 5, 'pekerja.png', 'Oprasional', 0, NULL, '$2y$10$VigsWSpJne5Mzkbksv37aOf8ZA8.DhnpRdPXfNRiGRJZH13coQ/sq', NULL, '2023-01-29 17:00:00', NULL),
(64, 'Aisyah', 'tripu2898@gmail.com', 16, 'pekerja.png', 'Marketing', 0, NULL, '$2y$10$lyHm9voOf8i7N5w4V4oyMeMq.bJF90OG1medNfxcpAzF1nS29HRq.', NULL, '2023-02-21 17:00:00', NULL),
(65, 'Indah', 'sales1@yahoo.com', 16, 'pekerja.png', 'Marketing', 0, NULL, '$2y$10$YmyKC25OCj.VfJYiq7T2heV.gJtaRr.T.U88oPy5qit0DdWq7ONmm', NULL, '2023-02-21 17:00:00', NULL),
(66, 'Geri Daulan', 'geridaulan@gmail.com', 17, 'pekerja.png', 'Marketing', 1, NULL, '$2y$10$yo8MXe4.jV1bQEBQmVPgZOfjTHT0KlLpOmQyKyCfnMXSUq1tGcKXK', NULL, '2023-03-09 17:00:00', NULL),
(67, 'Riza Hasna Amelia', 'rizahasna98@gmail.com', 18, 'pekerja.png', 'Oprasional', 0, NULL, '$2y$10$clSxBOTghLnfjTu/ft/HA.TMbEge28FiDdQtjiz0wtemiX2pl/MIK', NULL, '2023-03-31 17:00:00', NULL),
(68, 'Apriliani Citra Rizki Faulida', 'aprilianic90@gmail.com', 8, 'pekerja.png', 'Oprasional', 0, NULL, '$2y$10$508HeYLrNTqCm2ZZstX1I.oLmBgrcaENjd8oYX9ll0bCKQd6OhGqS', NULL, '2023-05-08 17:00:00', NULL),
(69, 'Fransisca Septiana', 'achmadfransisca20@gmail.com', 15, 'pekerja.png', 'Marketing', 0, NULL, '$2y$10$KmRN291onRcbBNo1Iqdx8OQL9tKOyVI640R3PHq.FBXMbXce3.lOi', NULL, '2023-07-19 17:00:00', NULL),
(70, 'Desti', 'destiaajah29@gmail.com', 15, '1699411490_WhatsApp Image 2023-11-08 at 09.44.27.jpeg', 'Marketing', 0, NULL, '$2y$10$K5I2jOMsZ6vUGEDJ/zwWweuxSTFD45Oe77FDhUzL87Y.drUMUOS/i', NULL, '2023-09-04 17:00:00', NULL),
(71, 'Silvia', 'silviasptiani@gmail.com', 9, 'pekerja.png', 'Oprasional', NULL, NULL, '$2y$10$3Y4u2t7Gdb1JLa.rWiLUnuIHmOyJEo2MYC/C19ar94F9.eGLB.vh2', NULL, '2023-11-28 17:00:00', NULL),
(73, 'Tofana Eka Safitri', 'tofana1311@gmail.com', 6, 'pekerja.png', 'Oprasional', 0, NULL, '$2y$10$6ubEutMT/iBe9IUohrFnBu8oTSWyyDPtDBT3vFHRLXbl7StJr1B0a', NULL, '2023-11-30 17:00:00', NULL),
(74, 'Rahmat Fauzi', 'afaaji48@gmail.com', 19, 'pekerja.png', 'Hrga', 0, NULL, '$2y$10$uNH64sV2h1HemQQ/IqKPCeXXW61Awmt87NM5/ZXAMEXqVCcZbbRPm', NULL, '2023-12-07 17:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
