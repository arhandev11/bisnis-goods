<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('currency', function ($expression) {
            return "Rp. <?php echo number_format($expression, 0, ',', '.'); ?>";
        });

        //compose all the views....
        view()->composer('*', function ($view) 
        {

            $cekToko                = DB::table('users')
                                        ->where('id', Auth::id())
                                        ->first('toko');

            $cekRole                = DB::table('users')
                                        ->where('id', Auth::id())
                                        ->first('role');

            if (Auth::guest() == true) {
                return redirect('/');
            }

            //lupa di sini apaan----------------------------------------------------------------

            if($cekToko == null){
                $tokoUser = "";
            }else{
                $tokoUser = $cekToko->toko;
            };

            if($cekRole == null){
                $userrole = "";
            }else{
                $userrole = $cekRole->role;
            };
            
            if($userrole == 1){
                $notifikasiPesanan = DB::table('invoice')
                                            ->orderBy('id', 'Desc')
                                            ->limit('10')
                                            ->get();

                $view->with('notifikasiPesanan', $notifikasiPesanan );    

                $countNotifPesanan = DB::table('invoice')
                                            ->where('tempo', "new")
                                            ->count('id');

                $view->with('countNotifPesanan', $countNotifPesanan );  

                $countNotifReqBayar = DB::table('pembayaran')
                                            ->where('status_pembayaran', "new")
                                            ->count('id_pembayaran');

                $view->with('countNotifReqBayar', $countNotifReqBayar ); 

                $countNotifPesananagen = DB::table('invoice')
                                            ->whereNotIn('platform', ['ChartPublic'])
                                            ->where('tempo', "new")
                                            ->count('id');

                $view->with('countNotifPesananagen', $countNotifPesananagen ); 

                $countNotifPesanancs = DB::table('invoice')
                                            ->where('platform', 'ChartPublic')
                                            ->where('tempo', "new")
                                            ->count('id');

                $view->with('countNotifPesanancs', $countNotifPesanancs );

                $notifikasiPembayaran   = DB::table('pembayaran')
                                        ->join('invoice', 'invoice.bayar_id' ,'=', 'pembayaran.tiket_pembayaran')
                                        ->where('status_pembayaran', "new")
                                        ->get();

                $view->with('notifikasiPembayaran', $notifikasiPembayaran );

                $countPembayaran   = DB::table('pembayaran')
                                            ->where('status_pembayaran', "new")
                                            ->count('pembayaran_toko');

                $view->with('countPembayaran', $countPembayaran );
            }else{
                $notifikasiPesanan      = DB::table('invoice')
                                            ->where('invoice_toko', $tokoUser)
                                            ->orderBy('id', 'Desc')
                                            ->limit('10')
                                            ->get();
                
                //...with this variable
                $view->with('notifikasiPesanan', $notifikasiPesanan );    

                $countNotifPesanan      = DB::table('invoice')
                                            ->where('invoice_toko', $tokoUser)
                                            ->where('tempo', "new")
                                            ->count('id');

                $view->with('countNotifPesanan', $countNotifPesanan );

                $countNotifReqBayar = DB::table('pembayaran')
                                            ->where('status_pembayaran', "new")
                                            ->count('id_pembayaran');

                $view->with('countNotifReqBayar', $countNotifReqBayar ); 

                $countNotifPesananagen = DB::table('invoice')
                                            ->whereNotIn('platform', ['ChartPublic'])
                                            ->where('tempo', "new")
                                            ->count('id');

                $view->with('countNotifPesananagen', $countNotifPesananagen ); 

                $countNotifPesanancs = DB::table('invoice')
                                            ->where('platform', 'ChartPublic')
                                            ->where('tempo', "new")
                                            ->count('id');

                $view->with('countNotifPesanancs', $countNotifPesanancs );

                $notifikasiPembayaran   = DB::table('pembayaran')
                                            ->join('invoice', 'invoice.bayar_id' ,'=', 'pembayaran.tiket_pembayaran')
                                            ->where('status_pembayaran', "new")
                                            ->where('invoice_toko', $tokoUser)
                                            ->get();

                $view->with('notifikasiPembayaran', $notifikasiPembayaran );

                $countPembayaran   = DB::table('pembayaran')
                                            ->where('status_pembayaran', "new")
                                            ->where('pembayaran_toko', $tokoUser)
                                            ->count('pembayaran_toko');

                $view->with('countPembayaran', $countPembayaran );
            }


            //lupa di sini apaan----------------------------------------------------------------
            $menu   = DB::table('role_menu')
                            ->where('role_id', $userrole)
                            ->first();

            $view->with('menu', $menu );

            $userRoleName = DB::table('users')
                            ->where('id', Auth::id())
                            ->join('role', 'users.role' ,'=', 'role.id_role')
                            ->first('role_name');

            $view->with('userRoleName', $userRoleName );


            //seting pilihan menu chart toko----------------------------------------------------------------
            $chartToko = DB::table('app_settings')
                            ->where('settings_name', "jenis_chart")
                            ->first('settings_value');

            if ($chartToko->settings_value == "select") {
                $halamanChart = "shopselect";
            }else{
                $halamanChart = "shop";
            };

            $view->with('halamanChart', $halamanChart);

        });

    }
}
