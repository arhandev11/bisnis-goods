<?php

namespace App\Utilities;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class RequestUtilities
{
    private $shipAPI = "https://api.biteship.com/v1/";
    private $token = "biteship_test.eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiVGVzdGluZyIsInVzZXJJZCI6IjY2NmJlZjBiNDg4MTFmMDAxMjI4YjUxOSIsImlhdCI6MTcxODM0OTg0Nn0.MYAsa_Gr7uLiIjKqJyKdUYp2GeetnokEOkpq5PWIREs";
    private $originAreaId = "IDNP6IDNC149IDND851IDZ13510";
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => $this->shipAPI,
            'headers' => [
                "Authorization" => "Bearer {$this->token}",
                "Accept" => "application/json"
            ]
        ]);
    }
    public function getFirstSearchMap($search)
    {
        $response = $this->client->request('GET', "maps/areas", [
            "query" => [
                "countries" => "ID",
                "type" => "double",
                "input" => $search
            ]
        ]);

        $body = json_decode($response->getBody()->getContents(), true);
        $areas = $body['areas'];
        $lists = collect();
        foreach ($areas as $area) {
            $lists->push([
                "id" => $area['id'],
                "text" => $area['name'],
            ]);
        }

        return $lists->toArray();
    }

    public function getSecondSearchMap($areaId)
    {
        $response = $this->client->request('GET', "maps/areas/{$areaId}", []);

        $body = json_decode($response->getBody()->getContents(), true);
        $areas = $body['areas'];
        $lists = collect();
        foreach ($areas as $area) {
            $lists->push([
                "id" => $area['id'],
                "text" => $area['postal_code'],
            ]);
        }
        return $lists->toArray();
    }

    public function getCouriers()
    {
        $response = $this->client->request('GET', "couriers", []);

        $body = json_decode($response->getBody()->getContents(), true);
        $couriers = $body['couriers'];
        $lists = collect();
        foreach ($couriers as $courier) {
            $lists->push([
                "id" => $courier['courier_code'],
                "text" => $courier['courier_name'],
            ]);
        }
        $lists = $lists->unique('text')->values();
        return $lists->toArray();
    }

    public function checkRate(Request $request)
    {
        $itemRequests = collect();
        foreach ($request->items as $item) {
            $itemRequests->push([
                "name" => $item['nama'],
                "description" => $item['nama'],
                "weight" => 1000,
                "value" => $item['harga'],
                "quantity" => $item['jumlah'],
            ]);
        }
        $response = $this->client->request('POST', "rates/couriers", [
            "json" => [
                "origin_area_id" => $this->originAreaId,
                "destination_area_id" => $request->destination_area_id,
                "couriers" => $request->couriers,
                "items" => $request->items,
            ]
        ]);

        $body = json_decode($response->getBody()->getContents(), true);
        $pricings = $body['pricing'];
        $lists = collect();
        foreach ($pricings as $pricing) {
            $lists->push([
                "courier_service_name" => $pricing['courier_service_name'],
                "courier_service_code" => $pricing['courier_service_code'],
                "shipment_duration_range" => $pricing['shipment_duration_range'],
                "shipment_duration_unit" => $pricing['shipment_duration_unit'],
                "price" => $pricing['price'],
            ]);
        }
        return $lists->toArray();
    }
}
