<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invoice_bulk extends Model
{
    protected $table = "invoice_bulk";
    protected $fillable = ['invoice','id_barang','jumlah_barang','tanggal'];

}