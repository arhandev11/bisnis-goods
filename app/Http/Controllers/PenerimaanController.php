<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use App\penjualan;
use App\penjualan_batal;
use App\stok_barang;
use App\User;
use App\invoice;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use Carbon\Carbon;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\invoicecair;

class PenerimaanController extends Controller
{



    public function updatepencairanmanual(request $request)
    {
        $danaCair = $request->pencairan;
        $idinvoice = $request->id_invoice;
        $hariini = date('Y-m-d');

        $detailInvoice = DB::table('invoice')
                                ->where('id', $idinvoice)
                                ->first();

        

        $tagihan50      = $detailInvoice->tagihan * 0.5;

        // dd($tagihan50, $detailInvoice->tagihan, $danaCair, $idinvoice);

        if ($danaCair > $detailInvoice->tagihan) {
            return back()->with('pencairanover', 'detail pesan ada di footer');
        }if ($danaCair == 0) {
            return back()->with('pencairankosong', 'detail pesan ada di footer');
        }if ($danaCair <= $tagihan50) {
            return back()->with('pencairan50', 'detail pesan ada di footer');
        };

        DB::table('penjualan')
                ->where('id_invoice', $detailInvoice->id)
                ->update([
                    'status_dana' => "cair",
                ]);


        DB::table('invoice')
                ->where('id', $idinvoice)
                ->update([
                    'pencairan' => $danaCair,
                    'tempo' => "selesai",
                ]);
        
        //jika platform nya adalah ChartPublic maka post pada keuangan adalah rekening utama
        if($detailInvoice->platform = "ChartPublic"){

            $post = DB::table('rekening')
                ->where('id_rekening', 2)
                ->first('nomor_rekening');
                
            $valuepost = $post->nomor_rekening;
                
        }else{

            $post = DB::table('list_marketplace')
                ->where('nama_marketplace', $detailInvoice->platform)
                ->first('marketplace_utama');
                
            $valuepost = $post->nomor_rekening;
                
        }
                // dd($marketplace,$detailInvoice->platform, $idinvoice);

        DB::table('keuangan_advance')->insert([
            'tanggal' => $hariini,
            'nama_teransaksi' => "pencairan dana ".$detailInvoice->platform." invoice ".$detailInvoice->expedisi,
            'debit' => $danaCair,
            'kredit' => "0",
            'post' => $valuepost,
            'post_akun' => "pencairan dana",
        ]);
        return back()->with('berhasilcair', 'detail pesan ada di footer');
    }





    public function danamasukadvance(request $request)
    {
        DB::table('keuangan_advance')->insert([
            'tanggal' => $request->tanggal,
            'nama_teransaksi' => $request->nama_teransaksi,
            'debit' => $request->debit,
            'kredit' => "0",
            'post' => $request->post,
            'post_akun' => $request->post_keuangan,
        ]);
        return back()->with('status', 'Data keuangan Pemasukan Baru Berhasil Di Input');
    }





    public function danakeluaradvance(request $request)
    {
        DB::table('keuangan_advance')->insert([
            'tanggal' => $request->tanggal,
            'nama_teransaksi' => $request->nama_teransaksi,
            'debit' => "0",
            'kredit' => $request->debit,
            'post' => $request->post,
            'post_akun' => $request->post_keuangan,
        ]);
        return back()->with('status', 'Data keuangan Pengeluaran Baru Berhasil Di Input');
    }





    public function danaantarrekening(request $request)
    {
        DB::table('keuangan_advance')->insert([
            'tanggal' => $request->tanggal,
            'nama_teransaksi' => $request->nama_teransaksi,
            'debit' => "0",
            'kredit' => $request->nilai_teransaksi,
            'post_akun' => "pencairan dana",
            'post' => $request->dana_keluar,
        ]);

        DB::table('keuangan_advance')->insert([
            'tanggal' => $request->tanggal,
            'nama_teransaksi' => $request->nama_teransaksi,
            'debit' => $request->nilai_teransaksi,
            'kredit' => "0",
            'post_akun' => "pencairan dana",
            'post' => $request->dana_masuk,
        ]);

        return back()->with('status', 'Data keuangan Pengeluaran Baru Berhasil Di Input');
    }




    public function uploaddatapencairan(request $request)
    {
        $hariini = date('Y-m-d');
        //proses upload file single di mulai dari sini
        $requestFile = $request->file('file');
        $filesize = $request->file('file')->getSize();
        if( $filesize >= 1000000){
            return back()->with('fileoversize', 'detail di script');
        }else{
            // menyimpan data file yang diupload ke variabel $file
            $id_task = $request->id;   
            $time = Carbon::now();                 
            $nama_file = $time."_".$requestFile->getClientOriginalName();                        
            // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = public_path('img/');
            $requestFile->move($tujuan_upload,$nama_file);   
            //proses upload pdf berakhir di sini
        }

        //baca data yang baru saja di upload
        $data = Excel::import(new invoicecair, public_path('img/'.$nama_file));

        $dataInvoice    =   DB::table('invoice_pencairan')
                                ->get();

        foreach ($dataInvoice as $index => $invoicedata1) 
        {
            $invoice[]      = $invoicedata1->invoice ; 
            $pencairan[]    = $invoicedata1->pencairan ; 
            $marketplace[]  = $invoicedata1->marketplace ;  

            $ids[]          = DB::table('invoice')
                                    ->where('expedisi', $invoice[$index])
                                    ->first();
        };

        $ids = collect($ids)->pluck('id')->toArray();

        // dd($ids);

            DB::table('penjualan')
            ->whereIn('id_invoice', $ids)
            ->update([
                'status_dana' => "cair",
            ]);
        
        $rekeningbelanja = DB::table('app_settings')
            ->where('settings_name', "rekening_belanja")
            ->first('settings_value');

        foreach ($dataInvoice as $index => $invoicedata2) 
        {

            DB::table('invoice')
                ->where('expedisi', $invoice[$index])
                ->update([
                    'pencairan' => $pencairan[$index],
                    'tempo' => "selesai",
                ]);

            DB::table('keuangan_advance')
                ->insert([
                    'tanggal' => $hariini,
                    'nama_teransaksi' => "pencairan dana ".$marketplace[$index]." invoice ".$invoice[$index],
                    'debit' => $pencairan[$index],
                    'kredit' => '0',
                    'post' => $marketplace[$index],
                    'post_akun' => "pencairan dana",
                ]);


        };

        DB::table('invoice_pencairan')->delete();

        // dd($invoice, $pencairan, $marketplace);
        

        return back()->with('berhasilcair', 'Data keuangan Pengeluaran Baru Berhasil Di Input');

    }




}
