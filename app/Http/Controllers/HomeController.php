<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\app_settings;
use Illuminate\Support\Facades\Redirect;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home(request $request)
    {
        if(auth()->user()->role == 2){
            return redirect('/shop');
        }else{
        $mdy = date('m/d/Y');
        $day = date('Y-m-d');
        $bulanini = date('m');
        $tahunIni = date('Y');
        $date = Carbon::createFromFormat('m/d/Y', $mdy)->subMonth();
        $lastmont = $date->subMonths(0)->format('m');
        
        $listtoko               = DB::table('toko_cabang')
                                    ->get('nama_cabang');

        $minimumStok = app_settings::where('id', 4)
                        ->first('settings_value');
                        
        if($request->cabang == null){
            $selectedcabang = "gudang bekasi";
        }else{
            $selectedcabang = $request->cabang;
        }

                        

        $stokAlert = DB::table('penjualan')
                ->select(
                'penjualan.code_barang',
                'penjualan.title',
                'penjualan.toko',
                DB::raw('SUM(penjualan.jumlah) as total_terjual'),
                'stok_barang.stok'
                )
                ->join('stok_barang', 'penjualan.code_barang', '=', 'stok_barang.code_barang')
                ->where('penjualan.toko', $selectedcabang)
                ->where('stok_barang.tipe', $selectedcabang)
                ->where('penjualan.tanggal', '>=', Carbon::now()->subDays(14)->toDateString())
                ->where('penjualan.title', 'NOT LIKE', '%sachet%')
                ->groupBy('penjualan.code_barang', 'penjualan.title', 'stok_barang.stok')
                ->get();


                //     dd($stokAlert);



        $terlaris = DB::table('penjualan')
                        ->select('title', DB::raw('sum(jumlah) as totalTerjual'),DB::raw('sum(harga*jumlah) as totalHarga'))
                        ->groupBy('title')
                        ->orderBy('totalTerjual', 'desc')
                        ->paginate(10);

        $modalBulanIni = DB::table('invoice')
                        ->whereMonth('tanggal', '=', $bulanini)
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $brutoBulanIni = DB::table('invoice')
                        ->whereMonth('tanggal', '=', $bulanini)
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $expensBulanIni = DB::table('keuangan_advance')
                        ->whereMonth('tanggal', '=', $bulanini)
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->where('post_akun', "oprasional")
                        ->sum('kredit');
                        // dd($expensBulanIni);

        $marginBulanIni = $brutoBulanIni-$modalBulanIni;
        $profitBulanIni = $marginBulanIni - $expensBulanIni;

        $modalHariIni = DB::table('invoice')
                        ->where('tanggal', '=', $day)
                        ->sum('total_modal');

        $brutoHariIni = DB::table('invoice')
                        ->where('tanggal', '=', $day)
                        ->sum('tagihan');

        $marginHariIni = $brutoHariIni-$modalHariIni;

        $expensHariIni = DB::table('keuangan_advance')
                        ->where('tanggal', '=', $day)
                        ->sum('kredit');

        $pesananHariIni = DB::table('invoice')
                        ->where('tanggal', '=', $day)
                        ->count('id');

        $PesananTahunIni = DB::table('penjualan')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->count('id');

        $omsetTahunan = DB::table('invoice')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $presThisMont = DB::table('invoice')
                        ->whereMonth('tanggal', '=', $bulanini)
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');
                        
                      

        $presLastMont = DB::table('invoice')
                        ->whereMonth('tanggal', '=', $lastmont)
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

                        // dd($marginBulanIni, $brutoBulanIni, $bulanini, $tahunIni);

        if($marginBulanIni == 0 && $brutoBulanIni == 0){
                $presMarginBlnIni = 0;
                $presExpenseBlnIni = 0;
                $presProfitBlnIni = 0;
        }else{
                $presMarginBlnIni = $marginBulanIni / $brutoBulanIni * 100;
                $presExpenseBlnIni = $expensBulanIni / $marginBulanIni * 100;
                $presProfitBlnIni = $profitBulanIni / $marginBulanIni * 100;
        }
                        // dd($expensBulanIni, $profitBulanIni, $presExpenseBlnIni, $presProfitBlnIni);




        if($presThisMont == 0){
                $presResult = 0;
        }else{
                $presResult =  ($presThisMont - $presLastMont) / $presThisMont * 100;
        }

        // januari
        $janModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '01')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $janTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '01')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $janexpans = DB::table('keuangan_advance')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '01')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // februari         
        $febModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '02')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $febTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '02')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $febexpans = DB::table('keuangan_advance')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '02')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // maret         
        $marModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '03')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $marTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '03')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $marexpans = DB::table('keuangan_advance')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '03')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // april         
        $aprModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '04')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $aprTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '04')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $aprexpans = DB::table('keuangan_advance')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '04')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // mei         
        $meiModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '05')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $meiTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '05')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $meiexpans = DB::table('keuangan_advance')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '05')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // juni         
        $junModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '06')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $junTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '06')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $junexpans = DB::table('keuangan_advance')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '06')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // juli         
        $julModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '07')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $julTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '07')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $julexpans = DB::table('keuangan_advance')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '07')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // agustus
        $agsModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '08')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $agsTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '08')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $agsexpans = DB::table('keuangan_advance')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '08')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // september
        $sepModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '09')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $sepTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '09')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $sepexpans = DB::table('keuangan_advance')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '09')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // oktober
        $oktModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '10')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $oktTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '10')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $oktexpans = DB::table('keuangan_advance')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '10')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // november
        $novModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '11')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $novTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '11')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $novexpans = DB::table('keuangan_advance')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '11')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // desember
        $desModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '12')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $desTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '12')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $desexpans = DB::table('keuangan_advance')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '12')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');  

        $userActive     = Auth::user()->id;

        $userRole = DB::table('users')->where('id', $userActive)->first('role');

        $listToko       = DB::table('toko_cabang')
                                ->get('nama_cabang');

        $tokoutama      = DB::table('users')
                                ->where('role', 1)
                                ->first('toko');

        $chartStore     = DB::table('chart')
                                ->orderBy('id_barang', 'DESC')
                                ->limit('1')
                                ->first('id_barang');

        $cekchart       = DB::table('chart')
                                ->where('user', $userActive)
                                ->first('id_barang');

                                // dd($chartStore);
        if($cekchart == null){
                $codBrngStore   = $tokoutama;
                $clssChngToko   = "";
        }else{
                $codBrngStore   = $chartStore->id_barang;
                $clssChngToko   = "disabled";
                $productStore   = DB::table('stok_barang')
                                ->where('code_barang', $codBrngStore)
                                ->first('tipe');
        };

        // dd($chartStore, $productStore, $codBrngStore);

        if($cekchart != null){
                $selectedToko = $productStore->tipe;
        }elseif($request->nama_toko == null){
                $selectedToko = $tokoutama->toko;
        }else{
                $selectedToko   = $request->nama_toko;
        };

        $pilihBarang    = DB::table('stok_barang')
                                ->where('tipe', $selectedToko)
                                ->get();

        $stok           = DB::table('stok_barang')
                                ->where('tipe','Reseller')
                                ->paginate(5);

        $jual           = DB::table('stok_barang')
                                ->orderBy('id','desc')
                                ->get();

        $code           = DB::table('app_settings')
                                ->where('id',3)
                                ->get();

        $chart          = DB::table('chart')
                                ->where('user', $userActive)
                                ->join('stok_barang', 'chart.id_barang', '=', 'stok_barang.code_barang')
                                ->where('tipe', $selectedToko)
                                ->select('id_barang','nama_barang','harga_modal','harga_jual','stok','jml_barang','id_chart','total_harga','min_qty','agen_price','tr1','tr2','tr3','no_barang_id')
                                ->get();

        //data statistik pesanan bulanan
        $totThisYear    =       DB::table('invoice')
                                ->whereYear('tanggal', $tahunIni)
                                ->count('invoice');
        $totSalJan      =       DB::table('invoice')
                                ->whereMonth('tanggal', '01')
                                ->whereYear('tanggal', $tahunIni)
                                ->count('invoice');
        $totSalFeb      =       DB::table('invoice')
                                ->whereMonth('tanggal', '02')
                                ->whereYear('tanggal', $tahunIni)
                                ->count('invoice');
        $totSalMar      =       DB::table('invoice')
                                ->whereMonth('tanggal', '03')
                                ->whereYear('tanggal', $tahunIni)
                                ->count('invoice');
        $totSalApr      =       DB::table('invoice')
                                ->whereMonth('tanggal', '04')
                                ->whereYear('tanggal', $tahunIni)
                                ->count('invoice');
        $totSalMei      =       DB::table('invoice')
                                ->whereMonth('tanggal', '05')
                                ->whereYear('tanggal', $tahunIni)
                                ->count('invoice');
        $totSalJun      =       DB::table('invoice')
                                ->whereMonth('tanggal', '06')
                                ->whereYear('tanggal', $tahunIni)
                                ->count('invoice');
        $totSalJul      =       DB::table('invoice')
                                ->whereMonth('tanggal', '07')
                                ->whereYear('tanggal', $tahunIni)
                                ->count('invoice');
        $totSalAgs      =       DB::table('invoice')
                                ->whereMonth('tanggal', '08')
                                ->whereYear('tanggal', $tahunIni)
                                ->count('invoice');
        $totSalSep      =       DB::table('invoice')
                                ->whereMonth('tanggal', '09')
                                ->whereYear('tanggal', $tahunIni)
                                ->count('invoice');
        $totSalOkt      =       DB::table('invoice')
                                ->whereMonth('tanggal', '10')
                                ->whereYear('tanggal', $tahunIni)
                                ->count('invoice');
        $totSalNov      =       DB::table('invoice')
                                ->whereMonth('tanggal', '11')
                                ->whereYear('tanggal', $tahunIni)
                                ->count('invoice');
        $totSalDes      =       DB::table('invoice')
                                ->whereMonth('tanggal', '12')
                                ->whereYear('tanggal', $tahunIni)
                                ->count('invoice');


        $subtotal       = DB::table('chart')->where('user', $userActive)->sum('total_harga');
        $submodal       = DB::table('chart')->where('user', $userActive)->sum('total_modal');
        $totbarang      = DB::table('chart')->where('user', $userActive)->sum('jml_barang');
        $invoice        = DB::table('invoice')->where('user', $userActive)->orderBy('invoice','desc')->paginate(10);
        $ofline         = DB::table('invoice')->where('platform', 'ofline')->orderBy('invoice','desc')->paginate(5);
        $lunas          = DB::table('invoice')->where('platform', 'lunas')->where('user', $userActive)->orderBy('invoice','desc')->paginate(5);
        $hutang         = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->orderBy('id','desc')->paginate(10);
        $tagihan        = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->sum('tagihan');
        $jumlahtrx      = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->count('tagihan');
        $priceLevel     = DB::table('users')->where('id', $userActive)->first('tier');
        $getmenu        = DB::table('role_menu')->where('role_id', $userRole->role)->first('home_menu');
        // dd($getmenu->home_menu);

        if($getmenu->home_menu == "dashboard"){
                return view('admin.adminHome', compact('selectedcabang','stokAlert','marginHariIni','brutoHariIni','marginBulanIni','brutoBulanIni','expensBulanIni','pesananHariIni','omsetTahunan','janModal','janTagihan','janexpans','febModal','febTagihan','febexpans','marModal','marTagihan','marexpans','aprModal','aprTagihan','aprexpans','meiModal','meiTagihan','meiexpans','junModal','junTagihan','junexpans','julModal','julTagihan','julexpans','agsTagihan','agsModal','agsexpans','sepModal','sepTagihan','sepexpans','oktModal','oktTagihan','oktexpans','novModal','novTagihan','novexpans','desModal','desTagihan','desexpans','presResult','minimumStok','terlaris','expensHariIni','PesananTahunIni','presMarginBlnIni','presExpenseBlnIni','presProfitBlnIni','totThisYear','totSalJan','totSalFeb','totSalMar','totSalApr','totSalMei','totSalJun','totSalJul','totSalAgs','totSalSep','totSalOkt','totSalNov','totSalDes',
                    'profitBulanIni','listtoko'));
        }else {
            return redirect::route($getmenu->home_menu);
        }
        
        // penutup auth karyawan
        }
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function gagalLogin(){
        Session::flash('gagalLogin','user dan password tidak sesuai');
        return redirect('admin');
    }





}
