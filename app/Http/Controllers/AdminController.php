<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Facades\Auth;
use App\app_settings;
use Carbon\Carbon;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Soow the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function stok(request $request)
    {

        $toko = Auth::user()->toko;

        if ($request->stok == null) {
            $caristok = "";
        }else {
            $caristok = $request->stok;
        }

        if (Auth::user()->role == 1) {

            if ($request->cabang == null) {

                $selectedcabang     = "Pilih Cabang";
                $stok               = DB::table('stok_barang')
                                        ->orderBy('code_barang', 'desc')
                                        ->selectRaw('id, code_barang, nama_barang, harga_jual, harga_modal, tr1, tr2, tr3, tipe, stok, id_sachet, harga_modal * stok as total_modal_ready')
                                        ->groupBy('id', 'harga_modal', 'stok', 'code_barang', 'nama_barang', 'harga_jual', 'harga_modal', 'tr1', 'tr2', 'tr3', 'tipe', 'id_sachet')
                                        ->paginate(10);
            }else{

                $selectedcabang     = $request->cabang;
                $stok               = DB::table('stok_barang')
                                        ->where('tipe', $request->cabang)
                                        ->orderBy('code_barang', 'desc')
                                        ->selectRaw('id, code_barang, nama_barang, harga_jual, harga_modal, tr1, tr2, tr3, tipe, stok, id_sachet, harga_modal * stok as total_modal_ready')
                                        ->groupBy('id', 'harga_modal', 'stok', 'code_barang', 'nama_barang', 'harga_jual', 'harga_modal', 'tr1', 'tr2', 'tr3', 'tipe', 'id_sachet')
                                        ->paginate(10);
            };

            $p_stok                 = DB::table('stok_tertahan')
                                        ->orderBy('id','DESC')
                                        ->selectRaw('code_barang, nama_barang, category, stok, harga_modal, harga_jual, tipe, keterangan, harga_modal * stok as total_nilai_pending')
                                        ->groupBy('code_barang', 'nama_barang', 'category', 'stok', 'harga_modal', 'harga_jual', 'tipe', 'keterangan')
                                        ->paginate(100);

            $count_p_stok           = DB::table('stok_tertahan')
                                        ->sum('stok');

            $allstok                = DB::table('stok_barang')
                                        ->sum('stok');

            $pricestok              = DB::table('stok_barang')
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_modal'));

            $pricesale              = DB::table('stok_barang')
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_jual'));
        }else{

            if ($request->cabang == null) {
                $selectedcabang     = "Pilih Cabang";
            }else{
                $selectedcabang     = $request->cabang;
            };

            
            $stok                   = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->orderBy('code_barang', 'desc')
                                        ->selectRaw('id, code_barang, nama_barang, harga_jual, harga_modal, tr1, tr2, tr3, tipe, stok, id_sachet, harga_modal * stok as total_modal_ready')
                                        ->groupBy('id', 'harga_modal', 'stok', 'code_barang', 'nama_barang', 'harga_jual', 'harga_modal', 'tr1', 'tr2', 'tr3', 'tipe', 'id_sachet')
                                        ->paginate(10);

            $p_stok                 = DB::table('stok_tertahan')
                                        ->where('tipe', $toko)
                                        ->orderBy('id','DESC')
                                        ->selectRaw('code_barang, nama_barang, category, stok, harga_modal, harga_jual, tipe, keterangan, harga_modal * stok as total_nilai_pending')
                                        ->groupBy('code_barang', 'nama_barang', 'category', 'stok', 'harga_modal', 'harga_jual', 'tipe', 'keterangan')
                                        ->paginate(100);

            $count_p_stok           = DB::table('stok_tertahan')
                                        ->where('tipe', $toko)
                                        ->sum('stok');

            $allstok                = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum('stok');

            $pricestok              = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_modal'));

            $pricesale              = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_jual'));
        };


        $selectstok             = DB::table('stok_barang')
                                    ->get();

        $categrory              = DB::table('category_product')
                                    ->orderBy('id_category','desc')
                                    ->paginate(10);

        $supplier               = DB::table('data_supplier')
                                    ->orderBy('id_supplier','desc')
                                    ->get();

        $allcategory            = DB::table('category_product')
                                    ->count('id_category');

        $listrekening           = DB::table('rekening')
                                    ->get();

        $listtoko               = DB::table('toko_cabang')
                                    ->get('nama_cabang');

        $historybelanja               = DB::table('history_kedatangan')
                                            ->orderBy('id_history', 'desc')
                                            ->paginate(100);

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->inventory == "tampil"){
            return view('admin.stok', compact('caristok','stok','categrory','supplier','allstok','pricestok','pricesale','allcategory','selectstok','p_stok','count_p_stok','listrekening','listtoko','selectedcabang','historybelanja'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }


    /**
     * Soow the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function stokcategory(request $request)
    {

        $toko = Auth::user()->toko;

        if (Auth::user()->role == 1) {

            if ($request->cabang == null) {

                $selectedcabang     = "Pilih Cabang";
                $stok               = DB::table('stok_barang')
                                        ->orderBy('code_barang','desc')
                                        ->paginate(10);
            }else{

                $selectedcabang     = $request->cabang;
                $stok               = DB::table('stok_barang')
                                        ->where('tipe', $request->cabang)
                                        ->orderBy('code_barang','desc')
                                        ->paginate(10);
            };

            $p_stok                 = DB::table('stok_tertahan')
                                        ->orderBy('id','DESC')
                                        ->paginate(100);

            $count_p_stok           = DB::table('stok_tertahan')
                                        ->sum('stok');

            $allstok                = DB::table('stok_barang')
                                        ->sum('stok');

            $pricestok              = DB::table('stok_barang')
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_modal'));

            $pricesale              = DB::table('stok_barang')
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_jual'));
        }else{

            if ($request->cabang == null) {
                $selectedcabang     = "Pilih Cabang";
            }else{
                $selectedcabang     = $request->cabang;
            };

            
            $stok                   = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->orderBy('id','desc')
                                        ->paginate(10);

            $p_stok                 = DB::table('stok_tertahan')
                                        ->where('tipe', $toko)
                                        ->orderBy('id','DESC')
                                        ->paginate(100);

            $count_p_stok           = DB::table('stok_tertahan')
                                        ->where('tipe', $toko)
                                        ->sum('stok');

            $allstok                = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum('stok');

            $pricestok              = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_modal'));

            $pricesale              = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_jual'));
        };


        $selectstok             = DB::table('stok_barang')
                                    ->get();

        $categrory              = DB::table('category_product')
                                    ->orderBy('id_category','desc')
                                    ->paginate(10);

        $supplier               = DB::table('data_supplier')
                                    ->orderBy('id_supplier','desc')
                                    ->get();

        $allcategory            = DB::table('category_product')
                                    ->count('id_category');

        $listrekening           = DB::table('rekening')
                                    ->get();

        $listtoko               = DB::table('toko_cabang')
                                    ->get('nama_cabang');

        $historybelanja               = DB::table('history_kedatangan')
                                            ->orderBy('id_history', 'desc')
                                            ->paginate(100);

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->inventory == "tampil"){
            return view('admin.stokcategory', compact('stok','categrory','supplier','allstok','pricestok','pricesale','allcategory','selectstok','p_stok','count_p_stok','listrekening','listtoko','selectedcabang','historybelanja'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }


    /**
     * Soow the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function stoksupplier(request $request)
    {

        $toko = Auth::user()->toko;

        if (Auth::user()->role == 1) {

            if ($request->cabang == null) {

                $selectedcabang     = "Pilih Cabang";
                $stok               = DB::table('stok_barang')
                                        ->orderBy('code_barang','desc')
                                        ->paginate(10);
            }else{

                $selectedcabang     = $request->cabang;
                $stok               = DB::table('stok_barang')
                                        ->where('tipe', $request->cabang)
                                        ->orderBy('code_barang','desc')
                                        ->paginate(10);
            };

            $p_stok                 = DB::table('stok_tertahan')
                                        ->orderBy('id','DESC')
                                        ->paginate(100);

            $count_p_stok           = DB::table('stok_tertahan')
                                        ->sum('stok');

            $allstok                = DB::table('stok_barang')
                                        ->sum('stok');

            $pricestok              = DB::table('stok_barang')
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_modal'));

            $pricesale              = DB::table('stok_barang')
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_jual'));
        }else{

            if ($request->cabang == null) {
                $selectedcabang     = "Pilih Cabang";
            }else{
                $selectedcabang     = $request->cabang;
            };

            
            $stok                   = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->orderBy('id','desc')
                                        ->paginate(10);

            $p_stok                 = DB::table('stok_tertahan')
                                        ->where('tipe', $toko)
                                        ->orderBy('id','DESC')
                                        ->paginate(100);

            $count_p_stok           = DB::table('stok_tertahan')
                                        ->where('tipe', $toko)
                                        ->sum('stok');

            $allstok                = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum('stok');

            $pricestok              = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_modal'));

            $pricesale              = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_jual'));
        };


        $selectstok             = DB::table('stok_barang')
                                    ->get();

        $categrory              = DB::table('category_product')
                                    ->orderBy('id_category','desc')
                                    ->paginate(10);

        $supplier               = DB::table('data_supplier')
                                    ->orderBy('id_supplier','desc')
                                    ->get();

        $allcategory            = DB::table('category_product')
                                    ->count('id_category');

        $listrekening           = DB::table('rekening')
                                    ->get();

        $listtoko               = DB::table('toko_cabang')
                                    ->get('nama_cabang');

        $historybelanja               = DB::table('history_kedatangan')
                                            ->orderBy('id_history', 'desc')
                                            ->paginate(100);

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->inventory == "tampil"){
            return view('admin.stoksupplier', compact('stok','categrory','supplier','allstok','pricestok','pricesale','allcategory','selectstok','p_stok','count_p_stok','listrekening','listtoko','selectedcabang','historybelanja'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }


    /**
     * Soow the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function stokpending(request $request)
    {

        $toko = Auth::user()->toko;

        if (Auth::user()->role == 1) {

            if ($request->cabang == null) {

                $selectedcabang     = "Pilih Cabang";
                $stok               = DB::table('stok_barang')
                                        ->orderBy('code_barang','desc')
                                        ->paginate(10);
            }else{

                $selectedcabang     = $request->cabang;
                $stok               = DB::table('stok_barang')
                                        ->where('tipe', $request->cabang)
                                        ->orderBy('code_barang','desc')
                                        ->paginate(10);
            };

            $p_stok                 = DB::table('stok_tertahan')
                                        ->orderBy('id','DESC')
                                        ->selectRaw('id, code_barang, nama_barang, category, stok, harga_modal, harga_jual, tipe, keterangan, harga_modal * stok as total_nilai_pending')
                                        ->groupBy('id', 'code_barang', 'nama_barang', 'category', 'stok', 'harga_modal', 'harga_jual', 'tipe', 'keterangan')
                                        ->paginate(100);

            $count_p_stok           = DB::table('stok_tertahan')
                                        ->sum('stok');

            $allstok                = DB::table('stok_barang')
                                        ->sum('stok');

            $pricestok              = DB::table('stok_barang')
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_modal'));

            $pricesale              = DB::table('stok_barang')
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_jual'));
        }else{

            if ($request->cabang == null) {
                $selectedcabang     = "Pilih Cabang";
            }else{
                $selectedcabang     = $request->cabang;
            };

            
            $stok                   = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->orderBy('id','desc')
                                        ->paginate(10);

            $p_stok                 = DB::table('stok_tertahan')
                                        ->where('tipe', $toko)
                                        ->orderBy('id','DESC')
                                        ->selectRaw('id, code_barang, nama_barang, category, stok, harga_modal, harga_jual, tipe, keterangan, harga_modal * stok as total_nilai_pending')
                                        ->groupBy('id', 'code_barang', 'nama_barang', 'category', 'stok', 'harga_modal', 'harga_jual', 'tipe', 'keterangan')
                                        ->paginate(100);

            $count_p_stok           = DB::table('stok_tertahan')
                                        ->where('tipe', $toko)
                                        ->sum('stok');

            $allstok                = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum('stok');

            $pricestok              = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_modal'));

            $pricesale              = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_jual'));
        };


        $selectstok             = DB::table('stok_barang')
                                    ->get();

        $categrory              = DB::table('category_product')
                                    ->orderBy('id_category','desc')
                                    ->paginate(10);

        $supplier               = DB::table('data_supplier')
                                    ->orderBy('id_supplier','desc')
                                    ->get();

        $allcategory            = DB::table('category_product')
                                    ->count('id_category');

        $listrekening           = DB::table('rekening')
                                    ->get();

        $listtoko               = DB::table('toko_cabang')
                                    ->get('nama_cabang');

        $historybelanja               = DB::table('history_kedatangan')
                                            ->orderBy('id_history', 'desc')
                                            ->paginate(100);

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->inventory == "tampil"){
            return view('admin.stokpending', compact('stok','categrory','supplier','allstok','pricestok','pricesale','allcategory','selectstok','p_stok','count_p_stok','listrekening','listtoko','selectedcabang','historybelanja'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }


    /**
     * Soow the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function stokkedatangan(request $request)
    {

        $toko = Auth::user()->toko;

        if (Auth::user()->role == 1) {

            if ($request->cabang == null) {

                $selectedcabang     = "Pilih Cabang";
                $stok               = DB::table('stok_barang')
                                        ->orderBy('code_barang','desc')
                                        ->paginate(10);
            }else{

                $selectedcabang     = $request->cabang;
                $stok               = DB::table('stok_barang')
                                        ->where('tipe', $request->cabang)
                                        ->orderBy('code_barang','desc')
                                        ->paginate(10);
            };

            $p_stok                 = DB::table('stok_tertahan')
                                        ->orderBy('id','DESC')
                                        ->paginate(100);

            $count_p_stok           = DB::table('stok_tertahan')
                                        ->sum('stok');

            $allstok                = DB::table('stok_barang')
                                        ->sum('stok');

            $pricestok              = DB::table('stok_barang')
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_modal'));

            $pricesale              = DB::table('stok_barang')
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_jual'));
        }else{

            if ($request->cabang == null) {
                $selectedcabang     = "Pilih Cabang";
            }else{
                $selectedcabang     = $request->cabang;
            };

            
            $stok                   = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->orderBy('id','desc')
                                        ->paginate(10);

            $p_stok                 = DB::table('stok_tertahan')
                                        ->where('tipe', $toko)
                                        ->orderBy('id','DESC')
                                        ->paginate(100);

            $count_p_stok           = DB::table('stok_tertahan')
                                        ->where('tipe', $toko)
                                        ->sum('stok');

            $allstok                = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum('stok');

            $pricestok              = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_modal'));

            $pricesale              = DB::table('stok_barang')
                                        ->where('tipe', $toko)
                                        ->sum(DB::raw('stok_barang.stok * stok_barang.harga_jual'));
        };


        $selectstok             = DB::table('stok_barang')
                                    ->get();

        $categrory              = DB::table('category_product')
                                    ->orderBy('id_category','desc')
                                    ->paginate(10);

        $supplier               = DB::table('data_supplier')
                                    ->orderBy('id_supplier','desc')
                                    ->get();

        $allcategory            = DB::table('category_product')
                                    ->count('id_category');

        $listrekening           = DB::table('rekening')
                                    ->get();

        $listtoko               = DB::table('toko_cabang')
                                    ->get('nama_cabang');

        $historybelanja               = DB::table('history_kedatangan')
                                            ->orderBy('id_history', 'desc')
                                            ->paginate(100);

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->inventory == "tampil"){
            return view('admin.stokkedatangan', compact('stok','categrory','supplier','allstok','pricestok','pricesale','allcategory','selectstok','p_stok','count_p_stok','listrekening','listtoko','selectedcabang','historybelanja'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function editproduk(request $request)
    {
        $id = $request->id;
        $tokoActive     = Auth::user()->toko;
        $count_p_stok = DB::table('stok_tertahan')->sum('stok');
        $allstok = DB::table('stok_barang')->sum('stok');
        $pricestok = DB::table('stok_barang')->sum(DB::raw('stok_barang.stok * stok_barang.harga_modal'));
        $pricesale = DB::table('stok_barang')->sum(DB::raw('stok_barang.stok * stok_barang.harga_jual'));
        $allcategory = DB::table('category_product')->count('id_category');

        $detailBarang       = DB::table('stok_barang')
                                ->where('id', $request->id)
                                ->get();
        // dd($detailBarang);
        $getToko       = DB::table('stok_barang')
                                ->where('id', $request->id)
                                ->first();
                                
        $selectIdProduk     = DB::table('stok_barang')
                                ->where('tipe', $getToko->tipe)
                                ->get();

        $cariIdecer         = DB::table('stok_barang')
                                ->where('id_sachet', $request->id)
                                ->first();

            return view('admin.editproduk', compact('allstok','pricestok','pricesale','allcategory','count_p_stok','detailBarang','id','selectIdProduk','cariIdecer'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function pindahkanstok(request $request)
    {
        $id = $request->id;
        $tokoActive     = Auth::user()->toko;
        $count_p_stok = DB::table('stok_tertahan')->sum('stok');

        $detailBarang       = DB::table('stok_barang')
                                ->where('id', $request->id)
                                ->get();

        $getToko       = DB::table('stok_barang')
                                ->where('id', $request->id)
                                ->first();
                                
        $selectIdProduk     = DB::table('stok_barang')
                                ->where('tipe', $getToko->tipe)
                                ->get();

        $cariIdecer         = DB::table('stok_barang')
                                ->where('id_sachet', $request->id)
                                ->first();

        $tokoCabang         = DB::table('toko_cabang')
                                ->get();

            return view('admin.pindahkanstok', compact('count_p_stok','detailBarang','id','selectIdProduk','cariIdecer','tokoCabang'));
    }




    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function caristok(request $request)
    {

        $caristok = $request->stok;
            if ($request->cabang == "Pilih Cabang") {

                $selectedcabang     = "Pilih Cabang";
                $stok               = DB::table('stok_barang')
                                        ->where('nama_barang', 'LIKE', '%' . $caristok . '%')
                                        ->orderBy('code_barang', 'desc')
                                        ->selectRaw('id, code_barang, nama_barang, harga_jual, harga_modal, tr1, tr2, tr3, tipe, stok, id_sachet, harga_modal * stok as total_modal_ready')
                                        ->groupBy('id', 'harga_modal', 'stok', 'code_barang', 'nama_barang', 'harga_jual', 'harga_modal', 'tr1', 'tr2', 'tr3', 'tipe', 'id_sachet')
                                        ->paginate(10);
                                        
                                        // dd($stok);
            }else{

                $selectedcabang     = $request->cabang;
                $stok               = DB::table('stok_barang')
                                        ->where('tipe', $request->cabang)
                                        ->where('nama_barang', 'LIKE', '%' . $caristok . '%')
                                        ->orderBy('code_barang', 'desc')
                                        ->selectRaw('id, code_barang, nama_barang, harga_jual, harga_modal, tr1, tr2, tr3, tipe, stok, id_sachet, harga_modal * stok as total_modal_ready')
                                        ->groupBy('id', 'harga_modal', 'stok', 'code_barang', 'nama_barang', 'harga_jual', 'harga_modal', 'tr1', 'tr2', 'tr3', 'tipe', 'id_sachet')
                                        ->paginate(10);
            };
            // dd($stok);

        $p_stok = DB::table('stok_tertahan')->orderBy('id','DESC')->paginate(10);
        $count_p_stok = DB::table('stok_tertahan')->sum('stok');
        $selectstok = DB::table('stok_barang')->get();
        $categrory = DB::table('category_product')->orderBy('id_category','desc')->paginate(10);
        $supplier = DB::table('data_supplier')->orderBy('id_supplier','desc')->paginate(10);
        $allstok = DB::table('stok_barang')->sum('stok');

        $pricestok = DB::table('stok_barang')->sum(DB::raw('stok_barang.stok * stok_barang.harga_modal'));

        $pricesale = DB::table('stok_barang')->sum(DB::raw('stok_barang.stok * stok_barang.harga_jual'));
        $allcategory = DB::table('category_product')->count('id_category');

        $listrekening       = DB::table('rekening')
                                ->get();

        $listtoko           = DB::table('toko_cabang')
                                ->get('nama_cabang');

        $historybelanja               = DB::table('history_kedatangan')
                                            ->orderBy('id_history', 'desc')
                                            ->paginate(10);


        if(auth()->user()->role == 1){
            return view('admin.stok', compact('caristok','historybelanja','stok','categrory','supplier','allstok','pricestok','pricesale','allcategory','selectstok','p_stok','count_p_stok','listrekening','listtoko','selectedcabang'));
        }if(auth()->user()->role == 8){
            return view('admin.stok', compact('caristok','historybelanja','stok','categrory','supplier','allstok','pricestok','pricesale','allcategory','selectstok','p_stok','count_p_stok','listrekening','listtoko','selectedcabang'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function shop()
    {

        $userActive     = Auth::user()->id;
        $userToko       = Auth::user()->toko;

        $jual           = DB::table('stok_barang')
                            ->orderBy('id','desc')
                            ->get();

        $code           = DB::table('app_settings')
                            ->where('id',3)
                            ->get();

        $chart          = DB::table('chart')
                            ->where('user', $userActive)
                            ->join('stok_barang', 'chart.no_barang_id', '=', 'stok_barang.id')
                            ->select('id_barang','nama_barang','harga_modal','harga_jual','stok','jml_barang','id_chart','total_harga','min_qty','agen_price','tr1','tr2','tr3','no_barang_id')
                            ->get();

        $chartcount          = DB::table('chart')
                            ->where('user', $userActive)
                            ->join('stok_barang', 'chart.no_barang_id', '=', 'stok_barang.id')
                            ->select('id_barang','nama_barang','harga_modal','harga_jual','stok','jml_barang','id_chart','total_harga','min_qty','agen_price','tr1','tr2','tr3','no_barang_id')
                            ->count('nama_barang');

        if($chartcount == 0){
            $btnEmptyChart = "disabled";
        }else{
            $btnEmptyChart = "";
        };

        $subtotal       = DB::table('chart')
                            ->where('user', $userActive)
                            ->sum('total_harga');

        $submodal       = DB::table('chart')
                            ->where('user', $userActive)
                            ->sum('total_modal');

        $totbarang      = DB::table('chart')
                            ->where('user', $userActive)
                            ->sum('jml_barang');

        $invoice        = DB::table('invoice')
                            ->where('invoice_toko', $userToko)
                            ->whereNotIn('platform', ["pesanan dibatalkan"])
                            ->orderBy('id','desc')
                            ->paginate(10);

        $ofline         = DB::table('invoice')
                            ->where('platform', 'ofline')
                            ->where('invoice_toko', $userToko)
                            ->orderBy('invoice','desc')
                            ->paginate(10);

        $lunas          = DB::table('invoice')
                            ->where('platform', 'lunas')
                            ->where('invoice_toko', $userToko)
                            ->orderBy('invoice','desc')
                            ->paginate(10);

        $hutang         = DB::table('invoice')
                            ->where('platform', 'tempo')
                            ->where('invoice_toko', $userToko)
                            ->orderBy('invoice','desc')
                            ->paginate(10);

        $listMarketplace= DB::table('list_marketplace')
                            ->where('id_user_toko', $userActive)
                            ->get();


        $cancelpesanan  = DB::table('invoice_batal')->where('invoice_toko', $userToko)->orderBy('id', 'desc')->paginate(10);

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        $invoiceType    = DB::table('app_settings')
                                ->where('settings_name', "invoice_type")
                                ->first('settings_value');


        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->system_transaksi == "tampil"){
            return view('admin.penjualan', compact('invoiceType','btnEmptyChart','jual','code','chart','subtotal','invoice','totbarang','submodal','ofline','lunas','hutang','cancelpesanan','listMarketplace'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }






    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function shopselect(request $request)
    {

        $userActive     = Auth::user()->id;
        $userToko       = Auth::user()->toko;

        $jual           = DB::table('stok_barang')
                            ->orderBy('id','desc')
                            ->get();

        $code           = DB::table('app_settings')
                            ->where('id',3)
                            ->get();

        $chart          = DB::table('chart')
                            ->where('user', $userActive)
                            ->join('stok_barang', 'chart.no_barang_id', '=', 'stok_barang.id')
                            ->select('id_barang','nama_barang','harga_modal','harga_jual','stok','jml_barang','id_chart','total_harga','min_qty','agen_price','tr1','tr2','tr3','no_barang_id')
                            ->get();

        $chartcount          = DB::table('chart')
                            ->where('user', $userActive)
                            ->join('stok_barang', 'chart.no_barang_id', '=', 'stok_barang.id')
                            ->select('id_barang','nama_barang','harga_modal','harga_jual','stok','jml_barang','id_chart','total_harga','min_qty','agen_price','tr1','tr2','tr3','no_barang_id')
                            ->count('nama_barang');

        if($chartcount == 0){
            $btnEmptyChart = "disabled";
        }else{
            $btnEmptyChart = "";
        };

        $subtotal       = DB::table('chart')
                            ->where('user', $userActive)
                            ->sum('total_harga');

        $submodal       = DB::table('chart')
                            ->where('user', $userActive)
                            ->sum('total_modal');

        $totbarang      = DB::table('chart')
                            ->where('user', $userActive)
                            ->sum('jml_barang');

                            
        if($request->cari == null){

            $invoice        = DB::table('invoice')
                                ->where('invoice_toko', $userToko)
                                ->whereNotIn('platform', ["pesanan dibatalkan"])
                                ->orderBy('id','desc')
                                ->paginate(10);
            
        }else{
            
            if($request->dari == "Invoice"){

            $invoice        = DB::table('invoice')
                                ->where('expedisi', $request->cari)
                                ->whereNotIn('platform', ["pesanan dibatalkan"])
                                ->orderBy('id','desc')
                                ->paginate(10);
            
            }if($request->dari == "Resi"){

            $invoice        = DB::table('invoice')
                                ->where('code_booking', $request->cari)
                                ->whereNotIn('platform', ["pesanan dibatalkan"])
                                ->orderBy('id','desc')
                                ->paginate(10);
            
            }
            
        };

        $ofline         = DB::table('invoice')
                            ->where('platform', 'ofline')
                            ->where('invoice_toko', $userToko)
                            ->orderBy('invoice','desc')
                            ->paginate(10);

        $lunas          = DB::table('invoice')
                            ->where('platform', 'lunas')
                            ->where('invoice_toko', $userToko)
                            ->orderBy('invoice','desc')
                            ->paginate(10);

        $hutang         = DB::table('invoice')
                            ->where('platform', 'tempo')
                            ->where('invoice_toko', $userToko)
                            ->orderBy('invoice','desc')
                            ->paginate(10);

        $listMarketplace= DB::table('list_marketplace')
                            ->where('id_user_toko', $userActive)
                            ->get();


        $cancelpesanan  = DB::table('invoice_batal')->where('invoice_toko', $userToko)->orderBy('id', 'desc')->paginate(10);

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        $invoiceType    = DB::table('app_settings')
                                ->where('settings_name', "invoice_type")
                                ->first('settings_value');

        $pilihBarang    = DB::table('stok_barang')
                                ->where('tipe', $userToko)
                                ->get();


        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->system_transaksi == "tampil"){
            return view('admin.penjualanmodeselect', compact('pilihBarang','invoiceType','btnEmptyChart','jual','code','chart','subtotal','invoice','totbarang','submodal','ofline','lunas','hutang','cancelpesanan','listMarketplace'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }







    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function pesananagen(request $request)
    {

        $userActive         = Auth::user()->id;
        $userToko           = Auth::user()->toko;
        $lunas              = DB::table('invoice')
                                ->where('platform', 'lunas')
                                ->where('user', $userActive)
                                ->orderBy('invoice','desc')
                                ->paginate(50);

        $hutang             = DB::table('invoice')
                                ->where('platform', 'tempo')
                                ->where('user', $userActive)
                                ->orderBy('id','desc')
                                ->paginate(50);

        $tagihan            = DB::table('invoice')
                                ->where('platform', 'tempo')
                                ->where('user', $userActive)
                                ->sum('tagihan');

        $totaltrx           = DB::table('invoice')
                                ->where('user', $userActive)
                                ->sum('tagihan');

        $jumlahtrx          = DB::table('invoice')
                                ->where('platform', 'tempo')
                                ->where('user', $userActive)
                                ->count('tagihan');

        $totaljmltrx        = DB::table('invoice')
                                ->where('user', $userActive)
                                ->count('tagihan');
                                
        if($request->cari == null){

            $counttrxBaru       = DB::table('invoice')
                                    ->where('tempo', 'new')
                                    ->where('invoice_toko', $userToko)
                                    ->count('id');

            $trxBaru            = DB::table('invoice')
                                ->where('tempo', 'new')
                                ->where('invoice_toko', $userToko)
                                ->orderBy('invoice','desc')
                                ->paginate(50);
            
        }else{

            if($request->dari == "Invoice"){

                $counttrxBaru       = DB::table('invoice')
                                        ->where('expedisi', $request->cari)
                                        ->where('tempo', 'new')
                                        ->where('invoice_toko', $userToko)
                                        ->count('id');
            
                $trxBaru            = DB::table('invoice')
                                        ->where('expedisi', $request->cari)
                                        ->where('tempo', 'new')
                                        ->where('invoice_toko', $userToko)
                                        ->orderBy('invoice','desc')
                                        ->paginate(50);
    
            }if($request->dari == "Resi"){
    
    
                $counttrxBaru       = DB::table('invoice')
                                        ->where('code_booking', $request->cari)
                                        ->where('tempo', 'new')
                                        ->where('invoice_toko', $userToko)
                                        ->count('id');
            
                $trxBaru            = DB::table('invoice')
                                        ->where('code_booking', $request->cari)
                                        ->where('tempo', 'new')
                                        ->where('invoice_toko', $userToko)
                                        ->orderBy('invoice','desc')
                                        ->paginate(50);
            
            };
            
        };
                                
        if($request->cari == null){

            $counttrxproses     = DB::table('invoice')
                                    ->where('tempo', 'diproses')
                                    ->where('invoice_toko', $userToko)
                                    ->count('id');

            $trxproses          = DB::table('invoice')
                                    ->where('tempo', 'diproses')
                                    ->where('invoice_toko', $userToko)
                                    ->orderBy('invoice','desc')
                                ->paginate(50);
            
        }else{

            $counttrxproses     = DB::table('invoice')
                                    ->where('expedisi', $request->cari)
                                    ->where('tempo', 'diproses')
                                    ->where('invoice_toko', $userToko)
                                    ->count('id');

            $trxproses          = DB::table('invoice')
                                    ->where('expedisi', $request->cari)
                                    ->where('tempo', 'diproses')
                                    ->where('invoice_toko', $userToko)
                                    ->orderBy('invoice','desc')
                                ->paginate(50);
            
        };
                                
        if($request->cari == null){

            $counttrxpacking    = DB::table('invoice')
                                    ->where('tempo', 'dipacking')
                                    ->where('invoice_toko', $userToko)
                                    ->count('id');

            $trxpacking         = DB::table('invoice')
                                    ->where('tempo', 'dipacking')
                                    ->where('invoice_toko', $userToko)
                                    ->orderBy('invoice','desc')
                                ->paginate(50);
            
        }else{

            $counttrxpacking    = DB::table('invoice')
                                    ->where('expedisi', $request->cari)
                                    ->where('tempo', 'dipacking')
                                    ->where('invoice_toko', $userToko)
                                    ->count('id');

            $trxpacking         = DB::table('invoice')
                                    ->where('expedisi', $request->cari)
                                    ->where('tempo', 'dipacking')
                                    ->where('invoice_toko', $userToko)
                                    ->orderBy('invoice','desc')
                                ->paginate(50);
            
        };
                                
        if($request->cari == null){

            $counttrxkirim      = DB::table('invoice')
                                    ->where('tempo', 'dikirim')
                                    ->where('invoice_toko', $userToko)
                                    ->count('id');

            $trxkirim           = DB::table('invoice')
                                    ->where('tempo', 'dikirim')
                                    ->where('invoice_toko', $userToko)
                                    ->orderBy('invoice','desc')
                                ->paginate(50);
            
        }else{

            $counttrxkirim      = DB::table('invoice')
                                    ->where('expedisi', $request->cari)
                                    ->where('tempo', 'dikirim')
                                    ->where('invoice_toko', $userToko)
                                    ->count('id');

            $trxkirim           = DB::table('invoice')
                                    ->where('expedisi', $request->cari)
                                    ->where('tempo', 'dikirim')
                                    ->where('invoice_toko', $userToko)
                                    ->orderBy('invoice','desc')
                                ->paginate(50);
            
        };
        // dd($trxBaru);
        // data rencana pembayaran
        $dataRcb        = DB::table('rencana_pembayaran')
                            ->where('user_id', $userActive)
                            ->join('invoice', 'invoice.id' ,'=', 'rencana_pembayaran.id_inv')
                            ->get();
        $dataRcbtotal   = DB::table('rencana_pembayaran')
                            ->where('user_id', $userActive)
                            ->join('invoice', 'invoice.id' ,'=', 'rencana_pembayaran.id_inv')
                            ->sum('tagihan');
        $hitoryPembayaran = DB::table('pembayaran')
                            ->where('reseller_id', $userActive)
                            ->get();

        $cancelpesanan  = DB::table('invoice_batal')->where('user', $userActive)->orderBy('id', 'desc')->paginate(50);

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->pesanan_agen == "tampil"){
            return view('admin.pesananagen', compact('lunas','hutang','tagihan','jumlahtrx','totaltrx','totaljmltrx','trxBaru','counttrxBaru','trxproses','counttrxproses','trxpacking','counttrxpacking','trxkirim','counttrxkirim','dataRcb','dataRcbtotal','hitoryPembayaran','cancelpesanan'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }


        
    }







    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function pesanancs(request $request)
    {

        $userActive         = Auth::user()->id;
        $userToko           = Auth::user()->toko;
        $lunas              = DB::table('invoice')
                                ->where('platform', 'ChartPublic')
                                ->where('user', $userActive)
                                ->orderBy('invoice','desc')
                                ->paginate(50);

        $hutang             = DB::table('invoice')
                                ->where('platform', 'ChartPublic')
                                ->where('user', $userActive)
                                ->orderBy('id','desc')
                                ->paginate(50);

        $tagihan            = DB::table('invoice')
                                ->where('platform', 'ChartPublic')
                                ->where('user', $userActive)
                                ->sum('tagihan');

        $totaltrx           = DB::table('invoice')
                                ->where('user', $userActive)
                                ->sum('tagihan');

        $jumlahtrx          = DB::table('invoice')
                                ->where('platform', 'ChartPublic')
                                ->where('user', $userActive)
                                ->count('tagihan');

        $totaljmltrx        = DB::table('invoice')
                                ->where('platform', 'ChartPublic')
                                ->where('user', $userActive)
                                ->count('tagihan');
                                
        if($request->cari == null){

            $counttrxBaru       = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                    ->where('tempo', 'new')
                                    ->where('invoice_toko', $userToko)
                                    ->count('id');

            $trxBaru            = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                    ->where('tempo', 'new')
                                    ->where('invoice_toko', $userToko)
                                    ->orderBy('invoice','desc')
                                    ->paginate(50);
            
        }else{

            if($request->dari == "Invoice"){

                $counttrxBaru       = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                        ->where('expedisi', $request->cari)
                                        ->where('tempo', 'new')
                                        ->where('invoice_toko', $userToko)
                                        ->count('id');
            
                $trxBaru            = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                        ->where('expedisi', $request->cari)
                                        ->where('tempo', 'new')
                                        ->where('invoice_toko', $userToko)
                                        ->orderBy('invoice','desc')
                                        ->paginate(50);
    
            }if($request->dari == "Resi"){
    
    
                $counttrxBaru       = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                        ->where('code_booking', $request->cari)
                                        ->where('tempo', 'new')
                                        ->where('invoice_toko', $userToko)
                                        ->count('id');
            
                $trxBaru            = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                        ->where('code_booking', $request->cari)
                                        ->where('tempo', 'new')
                                        ->where('invoice_toko', $userToko)
                                        ->orderBy('invoice','desc')
                                        ->paginate(50);
            
            };
            
        };
                                
        if($request->cari == null){

            $counttrxproses     = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                    ->where('tempo', 'diproses')
                                    ->where('invoice_toko', $userToko)
                                    ->count('id');

            $trxproses          = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                    ->where('tempo', 'diproses')
                                    ->where('invoice_toko', $userToko)
                                    ->orderBy('invoice','desc')
                                ->paginate(50);
            
        }else{

            $counttrxproses     = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                    ->where('expedisi', $request->cari)
                                    ->where('tempo', 'diproses')
                                    ->where('invoice_toko', $userToko)
                                    ->count('id');

            $trxproses          = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                    ->where('expedisi', $request->cari)
                                    ->where('tempo', 'diproses')
                                    ->where('invoice_toko', $userToko)
                                    ->orderBy('invoice','desc')
                                ->paginate(50);
            
        };
                                
        if($request->cari == null){

            $counttrxpacking    = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                    ->where('tempo', 'dipacking')
                                    ->where('invoice_toko', $userToko)
                                    ->count('id');

            $trxpacking         = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                    ->where('tempo', 'dipacking')
                                    ->where('invoice_toko', $userToko)
                                    ->orderBy('invoice','desc')
                                    ->paginate(50);
            
        }else{

            $counttrxpacking    = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                    ->where('expedisi', $request->cari)
                                    ->where('tempo', 'dipacking')
                                    ->where('invoice_toko', $userToko)
                                    ->count('id');

            $trxpacking         = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                    ->where('expedisi', $request->cari)
                                    ->where('tempo', 'dipacking')
                                    ->where('invoice_toko', $userToko)
                                    ->orderBy('invoice','desc')
                                ->paginate(50);
            
        };
                                
        if($request->cari == null){

            $counttrxkirim      = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                    ->where('tempo', 'dikirim')
                                    ->where('invoice_toko', $userToko)
                                    ->count('id');

            $trxkirim           = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                    ->where('tempo', 'dikirim')
                                    ->where('invoice_toko', $userToko)
                                    ->orderBy('invoice','desc')
                                ->paginate(50);
            
        }else{

            $counttrxkirim      = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                    ->where('expedisi', $request->cari)
                                    ->where('tempo', 'dikirim')
                                    ->where('invoice_toko', $userToko)
                                    ->count('id');

            $trxkirim           = DB::table('invoice')
                                    ->where('platform', 'ChartPublic')
                                    ->where('expedisi', $request->cari)
                                    ->where('tempo', 'dikirim')
                                    ->where('invoice_toko', $userToko)
                                    ->orderBy('invoice','desc')
                                ->paginate(50);
            
        };
        // dd($trxBaru);
        // data rencana pembayaran
        $dataRcb        = DB::table('rencana_pembayaran')
                            ->where('user_id', $userActive)
                            ->join('invoice', 'invoice.id' ,'=', 'rencana_pembayaran.id_inv')
                            ->get();
        $dataRcbtotal   = DB::table('rencana_pembayaran')
                            ->where('user_id', $userActive)
                            ->join('invoice', 'invoice.id' ,'=', 'rencana_pembayaran.id_inv')
                            ->sum('tagihan');
        $hitoryPembayaran = DB::table('pembayaran')
                            ->where('reseller_id', $userActive)
                            ->get();

        $cancelpesanan  = DB::table('invoice_batal')->where('user', $userActive)->orderBy('id', 'desc')->paginate(50);

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->pesanan_agen == "tampil"){
            return view('admin.pesanancs', compact('lunas','hutang','tagihan','jumlahtrx','totaltrx','totaljmltrx','trxBaru','counttrxBaru','trxproses','counttrxproses','trxpacking','counttrxpacking','trxkirim','counttrxkirim','dataRcb','dataRcbtotal','hitoryPembayaran','cancelpesanan'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }


        
    }



    /**
     * Show the application dashboard.
     *
     *
     */
    public function detailorderreseller(request $request)
    {
        $id            = $request->id;
        $domain         = $request->getHost();

        $invoice = DB::table('invoice_penjualan')
                            ->where('id_invoice', $id)
                            ->join('invoice', 'invoice_penjualan.id_invoice', '=', 'invoice.id')
                            ->select('ongkir','invoice','tagihan','expedisi','code_booking','nama_pengirim','hp_pengirim','nama_pembeli','hp_pembeli','alamat','diskon')
                            ->first();

        $item = DB::table('penjualan')
                            ->where('id_invoice', $id)
                            ->get();

        $timeline       = DB::table('timeline_order')->where('invoice_page', $id)->get();
        $detailInvoice  = DB::table('invoice')->where('id', $id)->first();
        $userActive     = Auth::user()->id;
        $tagihan        = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->sum('tagihan');
        $totaltrx       = DB::table('invoice')->where('user', $userActive)->sum('tagihan');
        $jumlahtrx      = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->count('tagihan');
        $totaljmltrx    = DB::table('invoice')->where('user', $userActive)->count('tagihan');
        // dd($detailInvoice);

        return view('admin.detailorderreseller', compact('domain','tagihan','jumlahtrx','totaltrx','totaljmltrx','invoice','item','timeline','id','detailInvoice'));
    }





    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function historyBelanja()
    {
        $historyLunas   = DB::table('history_belanja')
                            ->where('status_belanja', "Lunas")
                            ->orderBy('id','desc')
                            ->paginate(5);

        $historyTempo   = DB::table('history_belanja')
                            ->where('status_belanja', "Tempo")
                            ->orderBy('id','desc')
                            ->paginate(5);

        $historyprice   = DB::table('history_belanja')
                             ->select('supplier', DB::raw('sum(total_belanja) as total'))
                             ->groupBy('supplier')
                             ->get();

        $historytempo   = DB::table('history_belanja')
                            ->join('users', 'users.id', '=', 'history_belanja.user')
                            ->select(DB::raw('sum(total_belanja) as totaltempo'))
                            ->selectRaw('name')
                            ->groupBy('name')
                            ->get();

        $listTempo   = DB::table('history_belanja')
                             ->select('supplier', DB::raw('sum(total_belanja) as totaltempo'))
                             ->where('status_belanja', "Tempo")
                             ->groupBy('supplier')
                             ->get();

        $listTempores      = DB::table('invoice')
                            ->join('users', 'users.id', '=', 'invoice.user')
                            ->select(DB::raw('sum(tagihan) as tagihanTempo'))
                            ->selectRaw('name')
                            ->where('platform', "tempo")
                            ->groupBy('name')
                            ->get();

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->history_belanja == "tampil"){
            return view('admin.historyBelanja', compact('historyLunas','historyprice','historyTempo','historytempo','listTempo'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function bayartempoagen(request $request)
    {
        $id             = $request->id;
        $detailbarang   = DB::table('history_belanja')
                            ->where('id', $id)
                            ->get();

        $rekening = DB::table('rekening')
                            ->get();

        return view('admin.detailhutangtempo', compact('rekening','detailbarang','id'));
    }









    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function LaporanTransaksi()
    {

        $invoice = DB::table('invoice')->paginate(10);


        if(auth()->user()->role == 1){
            return view('admin.LaporanTransaksi', compact('invoice'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }






    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function invoicepaper(request $request, $id){
        $invoice = DB::table('invoice_penjualan')
                ->where('id_invoice', $id)
                ->join('invoice', 'invoice_penjualan.id_invoice', '=', 'invoice.id')
                ->first();

                // dd($invoice);

        $item = DB::table('penjualan')
                ->where('id_invoice', $id)
                ->get();

        return view('admin.invoicepaper' ,compact('invoice','item'));
    }






    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function invoicethermal(request $request, $id){
        $invoice = DB::table('invoice_penjualan')
                ->where('id_invoice', $id)
                ->join('invoice', 'invoice_penjualan.id_invoice', '=', 'invoice.id')
                ->get();

                // dd($invoice);

        $item = DB::table('penjualan')
                ->where('id_invoice', $id)
                ->get();

        return view('admin.invoicethermal' ,compact('invoice','item'));
    }







    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function invoicethermalReseller(request $request, $id){
        $invoice = DB::table('invoice_penjualan')
                ->where('id_invoice', $id)
                ->join('invoice', 'invoice_penjualan.id_invoice', '=', 'invoice.id')
                ->select('invoice','tagihan','expedisi','code_booking','nama_pengirim','hp_pengirim','nama_pembeli','hp_pembeli','alamat')
                ->get();

        $item = DB::table('penjualan')
                ->where('id_invoice', $id)
                ->get();

        return view('admin.invoicethermalreseller' ,compact('invoice','item'));
    }





    public function keuangan(){
        $keuangan = DB::table('keuangan')->orderBy('id','desc')->paginate(10);
        $akun = DB::table('akun_keuangan')->orderBy('id_akun','desc')->paginate(10);
        $debit = DB::table('keuangan')->sum('debit');
        $kredit = DB::table('keuangan')->sum('kredit');
        $ballance = $debit-$kredit;
        $listKantongDana    =   DB::table('keuangan')
                                ->select('post',DB::raw('(sum(debit)) - (sum(kredit)) as total'))
                                ->groupBy('post')  
                                ->get();
                                // dd($listKantongDana);
        $listrekening       = DB::table('rekening')
                                ->get();


        return view('admin.keuangan', compact('keuangan','ballance','akun','listKantongDana','listrekening'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function user(request $request)
    {

        $user       = DB::table('users')
                        ->paginate(10);

        $toko       = DB::table('users')
                        ->get('toko');

        $sadmVal    = DB::table('users')
                        ->where('role', 1)
                        ->first('role');

        $selectrole = DB::table('role')
                        ->get();

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->settings == "tampil"){
            return view('admin.user', compact('user','toko','sadmVal','selectrole'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }




    public function laporan()
    {
        $readyYear = DB::table('keuangan')
                        ->selectRaw('YEAR(tanggal) year')
                        ->groupBy('year')
                        ->get();

                        // dd($readyYear);
                        
        $tahunIni = date('Y');
        
        $scty = date('Y');

        $omsetJan =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 1)
                            ->sum('debit');

        $opJan =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 1)
                            ->sum('kredit');

        $taJan =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 1)
                            ->sum('tagihan');

        $MoJan =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 1)
                            ->sum('total_modal');
        
        $omsetFeb =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 2)
                            ->sum('debit');

        $opFeb =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 2)
                            ->sum('kredit');

        $taFeb =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 2)
                            ->sum('tagihan');

        $MoFeb =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 2)
                            ->sum('total_modal');

                            
        $omsetMar =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 3)
                            ->sum('debit');

        $opMar =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 3)
                            ->sum('kredit');

        $taMar =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 3)
                            ->sum('tagihan');

        $MoMar =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 3)
                            ->sum('total_modal');

                            
        $omsetApr =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 4)
                            ->sum('debit');

        $opApr =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 4)
                            ->sum('kredit');

        $taApr =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 4)
                            ->sum('tagihan');

        $MoApr =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 4)
                            ->sum('total_modal');

                            
        $omsetMei =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 5)
                            ->sum('debit');

        $opMei =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 5)
                            ->sum('kredit');

        $taMei =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 5)
                            ->sum('tagihan');

        $MoMei =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 5)
                            ->sum('total_modal');

                            
        $omsetJun =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 6)
                            ->sum('debit');

        $opJun =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 6)
                            ->sum('kredit');

        $taJun =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 6)
                            ->sum('tagihan');

        $MoJun =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 6)
                            ->sum('total_modal');

                            
        $omsetJul =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 7)
                            ->sum('debit');

        $opJul =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 7)
                            ->sum('kredit');

        $taJul =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 7)
                            ->sum('tagihan');

        $MoJul =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 7)
                            ->sum('total_modal');

                            

        $omsetAgs =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 8)
                            ->sum('debit');

        $opAgs =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 8)
                            ->sum('kredit');

        $taAgs =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 8)
                            ->sum('tagihan');

        $MoAgs =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 8)
                            ->sum('total_modal');

                            
        $omsetSep =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 9)
                            ->sum('debit');

        $opSep =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 9)
                            ->sum('kredit');

        $taSep =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 9)
                            ->sum('tagihan');

        $MoSep =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 9)
                            ->sum('total_modal');

                            
        $omsetOkt =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 10)
                            ->sum('debit');

        $opOkt =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 10)
                            ->sum('kredit');

        $taOkt =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 10)
                            ->sum('tagihan');

        $MoOkt =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 10)
                            ->sum('total_modal');

                            
        $omsetNov =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 11)
                            ->sum('debit');

        $opNov =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 11)
                            ->sum('kredit');

        $taNov =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 11)
                            ->sum('tagihan');

        $MoNov =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 11)
                            ->sum('total_modal');

                            
        $omsetDes =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 12)
                            ->sum('debit');

        $opDes =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 12)
                            ->sum('kredit');

        $taDes =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 12)
                            ->sum('tagihan');

        $MoDes =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 12)
                            ->sum('total_modal');

        // hitungan total


        $opTot =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->sum('kredit');

        $taTot =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->sum('tagihan');

        $MoTot =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->sum('total_modal');

        $totalProfit = ($taTot - $MoTot) - $opTot;

                                // dd($omsetAgs);

        return view('admin.laporan', compact('omsetJan','omsetFeb','omsetMar','omsetApr','omsetMei','omsetJun','omsetJul','omsetAgs','omsetSep','omsetOkt',
        'omsetNov','omsetDes','opJan','opFeb','opMar','opApr','opMei','opJun','opJul','opAgs','opSep','opOkt','opNov','opDes','taJan','MoJan','taFeb','MoFeb',
        'taMar','MoMar','taApr','MoApr','taMei','MoMei','taJun','MoJun','taJul','MoJul','taAgs','MoAgs','taSep','MoSep','taOkt','MoOkt','taNov','MoNov','taDes',
        'MoDes','totalProfit','readyYear','scty'));

    }






    public function yearReport(request $request)
    {
        $readyYear = DB::table('keuangan')
                        ->selectRaw('YEAR(tanggal) year')
                        ->groupBy('year')
                        ->get();

                        // dd($readyYear);
                        
        $tahunIni = $request->selectyear;

        $scty = $request->selectyear;

        $omsetJan =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 1)
                            ->sum('debit');

        $opJan =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 1)
                            ->sum('kredit');

        $taJan =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 1)
                            ->sum('tagihan');

        $MoJan =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 1)
                            ->sum('total_modal');
        
        $omsetFeb =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 2)
                            ->sum('debit');

        $opFeb =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 2)
                            ->sum('kredit');

        $taFeb =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 2)
                            ->sum('tagihan');

        $MoFeb =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 2)
                            ->sum('total_modal');

                            
        $omsetMar =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 3)
                            ->sum('debit');

        $opMar =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 3)
                            ->sum('kredit');

        $taMar =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 3)
                            ->sum('tagihan');

        $MoMar =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 3)
                            ->sum('total_modal');

                            
        $omsetApr =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 4)
                            ->sum('debit');

        $opApr =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 4)
                            ->sum('kredit');

        $taApr =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 4)
                            ->sum('tagihan');

        $MoApr =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 4)
                            ->sum('total_modal');

                            
        $omsetMei =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 5)
                            ->sum('debit');

        $opMei =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 5)
                            ->sum('kredit');

        $taMei =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 5)
                            ->sum('tagihan');

        $MoMei =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 5)
                            ->sum('total_modal');

                            
        $omsetJun =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 6)
                            ->sum('debit');

        $opJun =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 6)
                            ->sum('kredit');

        $taJun =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 6)
                            ->sum('tagihan');

        $MoJun =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 6)
                            ->sum('total_modal');

                            
        $omsetJul =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 7)
                            ->sum('debit');

        $opJul =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 7)
                            ->sum('kredit');

        $taJul =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 7)
                            ->sum('tagihan');

        $MoJul =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 7)
                            ->sum('total_modal');

                            

        $omsetAgs =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 8)
                            ->sum('debit');

        $opAgs =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 8)
                            ->sum('kredit');

        $taAgs =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 8)
                            ->sum('tagihan');

        $MoAgs =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 8)
                            ->sum('total_modal');

                            
        $omsetSep =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 9)
                            ->sum('debit');

        $opSep =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 9)
                            ->sum('kredit');

        $taSep =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 9)
                            ->sum('tagihan');

        $MoSep =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 9)
                            ->sum('total_modal');

                            
        $omsetOkt =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 10)
                            ->sum('debit');

        $opOkt =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 10)
                            ->sum('kredit');

        $taOkt =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 10)
                            ->sum('tagihan');

        $MoOkt =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 10)
                            ->sum('total_modal');

                            
        $omsetNov =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 11)
                            ->sum('debit');

        $opNov =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 11)
                            ->sum('kredit');

        $taNov =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 11)
                            ->sum('tagihan');

        $MoNov =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 11)
                            ->sum('total_modal');

                            
        $omsetDes =   DB::table('keuangan')
                            ->where('post_akun', "pendapatan")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 12)
                            ->sum('debit');

        $opDes =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 12)
                            ->sum('kredit');

        $taDes =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 12)
                            ->sum('tagihan');

        $MoDes =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->whereMonth('tanggal', 12)
                            ->sum('total_modal');

        // hitungan total


        $opTot =   DB::table('keuangan')
                            ->where('post_akun', "oprasional")
                            ->whereYear('tanggal', $tahunIni)
                            ->sum('kredit');

        $taTot =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->sum('tagihan');

        $MoTot =   DB::table('invoice')
                            ->whereYear('tanggal', $tahunIni)
                            ->sum('total_modal');

        $totalProfit = ($taTot - $MoTot) - $opTot;

                                // dd($omsetAgs);

        return view('admin.laporan', compact('omsetJan','omsetFeb','omsetMar','omsetApr','omsetMei','omsetJun','omsetJul','omsetAgs','omsetSep','omsetOkt',
        'omsetNov','omsetDes','opJan','opFeb','opMar','opApr','opMei','opJun','opJul','opAgs','opSep','opOkt','opNov','opDes','taJan','MoJan','taFeb','MoFeb',
        'taMar','MoMar','taApr','MoApr','taMei','MoMei','taJun','MoJun','taJul','MoJul','taAgs','MoAgs','taSep','MoSep','taOkt','MoOkt','taNov','MoNov','taDes',
        'MoDes','totalProfit','readyYear','scty'));

    }




    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function inventaris()
    {
        $datainventaris =   DB::table('data_inventaris')
                            -> paginate('10');

        if(auth()->user()->role == 1){
            return view('admin.inventaris', compact('datainventaris'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }




    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function analytic()
    {
        $penjualanJanuari =   DB::table('penjualan')
                                 ->join('stok_barang', 'penjualan.code_barang', '=', 'stok_barang.code_barang')
                                 ->select('penjualan.code_barang', DB::raw('sum(jumlah) as total'))
                                 ->groupBy('penjualan.code_barang')
                                 ->whereMonth('tanggal', 1)
                                 ->whereYear('tanggal', 2022)
                                 ->get();


        if(auth()->user()->role == 1){
            return view('admin.analytic', compact('penjualanJanuari'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }




    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function vocher()
    {
        $vocher =   DB::table('rand_numbers')
                            ->groupBy('number')
                            -> paginate('600');

        if(auth()->user()->role == 1){
            return view('admin.vocher', compact('vocher'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }




    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function detailuser(request $request)
    {
        $id = $request->id;
        $userDetail =   DB::table('users')
                            ->where('id', $request->id)
                            ->first();

        $getRole = DB::table('users')
                            ->where('id', $request->id)
                            ->join('role', 'users.role' ,'=', 'role.id_role')
                            ->first();

        if ($getRole == null) {
            $roleSelected = "pilih role";
            $idRole       = "";
        }else{
            $roleSelected = $getRole->role_name;
            $idRole       = $getRole->id_role;
        }

        $listToko   =   DB::table('toko_cabang')
                            ->get();

        $role       =   DB::table('role')
                            ->get();

        if(auth()->user()->role == 1){
            return view('admin.detailuser', compact('userDetail','id','listToko','role','roleSelected','idRole'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }




    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profile(request $request)
    {
        $userActive =   Auth::user()->id;

        $userDetail =   DB::table('users')
                            ->where('id', $userActive)
                            ->first();

        $getRole = DB::table('users')
                            ->where('id', $userActive)
                            ->join('role', 'users.role' ,'=', 'role.id_role')
                            ->first();
                            


            return view('admin.profile', compact('userDetail','getRole'));
    }




    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function setings(request $request)
    {
        $userActive =   Auth::user()->id;
        $userDetail =   DB::table('users')
                            ->where('id', $userActive)
                            ->first();

            return view('admin.setings', compact('userDetail'));
    }




    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function setingspass(request $request)
    {
        $userActive =   Auth::user()->id;
        $userDetail =   DB::table('users')
                            ->where('id', $userActive)
                            ->first();

            return view('admin.setingspass', compact('userDetail'));
    }




    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function setingsstore(request $request)
    {
        $rekening =   DB::table('rekening')
                        ->get();

        $seting1   =   DB::table('app_settings')
                        ->where('settings_name', "rekening_tempo")
                        ->join('rekening', 'rekening.nomor_rekening' ,'=', 'app_settings.settings_value')
                        ->first();

        if ($seting1 == null) {
            $norekreseller = "-";
            $namerekreseller = "-"; 
        }else{
            $norekreseller = $seting1->settings_value;
            $namerekreseller = $seting1->pemilik_rekening; 
        }

        $setingINvoice   =   DB::table('app_settings')
                        ->where('settings_name', "invoice_type")
                        ->first('settings_value');

        if ($setingINvoice->settings_value == "invoicethermal") {
            $selectedthermal    = "selected";
            $selectedpaper      = ""; 
        }else{
            $selectedthermal    = "";
            $selectedpaper      = "selected"; 
        }

        $setingChart   =   DB::table('app_settings')
                        ->where('settings_name', "jenis_chart")
                        ->first('settings_value');

        if ($setingChart->settings_value == "select") {
            $selectchart    = "selected";
            $barcodechart      = ""; 
        }else{
            $selectchart    = "";
            $barcodechart      = "selected"; 
        }

            return view('admin.setingsstore', compact('rekening','norekreseller','namerekreseller','selectedthermal','selectedpaper','selectchart','barcodechart'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function pembayaran(request $request)
    {

        $userToko       = Auth::user()->toko;
        $listpembayaran   = DB::table('pembayaran')
                                        ->where('status_pembayaran', "new")
                                        ->where('pembayaran_toko', $userToko)
                                        ->orderBy('id_pembayaran', 'DESC')
                                        ->paginate(10);

        $listlunas   = DB::table('pembayaran')
                                        ->where('status_pembayaran', "diterima")
                                        ->where('pembayaran_toko', $userToko)
                                        ->orderBy('id_pembayaran', 'DESC')
                                        ->paginate(10);


        return view('admin.pembayaran', compact('listpembayaran','listlunas'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function detailhistorybayar(request $request)
    {

        $id             = $request->id;

        // dd($id);

        //data platform chart public
        $chartpublic    = DB::table('invoice')
                                        ->where('bayar_id', $id)
                                        ->first('platform');
                                        
        $totalbayar    = DB::table('invoice')
                                        ->where('bayar_id', $id)
                                        ->sum('tagihan');

        $UserRole = DB::table('pembayaran')
                            ->where('tiket_pembayaran', $id)
                            ->first('reseller_id');

        $Usercek = DB::table('users')
                            ->where('id', $UserRole->reseller_id)
                            ->first('id');
                            // dd($Usercek);
                            
        if ($Usercek == null) {
            
            $dataPembayaran = DB::table('pembayaran')
                                ->where('tiket_pembayaran', $id)
                                ->first();
                                // dd($dataPembayaran);
        }else{

            $dataPembayaran = DB::table('pembayaran')
                                ->join('users', 'users.id' ,'=', 'pembayaran.reseller_id')
                                ->where('tiket_pembayaran', $id)
                                ->first();
        }

        $buktiBayar     = DB::table('file_task')
                            ->where('id_task', $id)
                            ->first('file');
                            // dd($id, $buktiBayar);

        $invoice = DB::table('invoice')
                        ->where('bayar_id', $id)
                        ->first();

        $item = DB::table('penjualan')
                            ->where('id_invoice', $invoice->id)
                            ->get();

        $listInvoice    = DB::table('invoice')
                                        ->where('bayar_id', $id)
                                        ->get();

        return view('admin.detailhistorybayar', compact('invoice','item','listInvoice','dataPembayaran','id','totalbayar','buktiBayar'));
    }





    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function historyreseller()
    {
        $historyLunas   = DB::table('invoice')
                            ->where('platform', "Lunas")
                            ->orderBy('id','desc')
                            ->paginate(10);

        $historyTempo   = DB::table('invoice')
                            ->where('platform', "tempo")
                            ->orderBy('id','desc')
                            ->paginate(10);
                            // dd($historyLunas, $historyTempo);

        $historyprice   = DB::table('history_belanja')
                             ->select('supplier', DB::raw('sum(total_belanja) as total'))
                             ->groupBy('supplier')
                             ->get();

        $historytempo   = DB::table('history_belanja')
                            ->join('users', 'users.id', '=', 'history_belanja.user')
                            ->select(DB::raw('sum(total_belanja) as totaltempo'))
                            ->selectRaw('name')
                            ->groupBy('name')
                            ->get();

        $listTempo      = DB::table('invoice')
                            ->join('users', 'users.id', '=', 'invoice.user')
                            ->select(DB::raw('sum(tagihan) as tagihanTempo'))
                            ->selectRaw('name')
                            ->where('tempo', "new")
                            ->groupBy('name')
                            ->get();
                            // dd($listTempo);

        $listTempores      = DB::table('invoice')
                            ->join('users', 'users.id', '=', 'invoice.user')
                            ->select(DB::raw('sum(tagihan) as tagihanTempo'))
                            ->selectRaw('name')
                            ->where('platform', "tempo")
                            ->groupBy('name')
                            ->get();

        $jmlpembayaranagen     = DB::table('pembayaran')
                                    ->where('status_pembayaran', "new")
                                    ->count('status_pembayaran');

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->tagihan == "tampil"){
            return view('admin.historyreseller', compact('jmlpembayaranagen','historyLunas','historyprice','historyTempo','historytempo','listTempo'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function listcabang(request $request)
    {

        $cabang = DB::table('toko_cabang')
                    ->join('users', 'users.id' ,'=', 'toko_cabang.kepala_cabang')
                    ->paginate(10);

        $userlist = DB::table('users')
                    ->whereIn('role', [1,2])
                    ->get();

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->list_cabang == "tampil"){
            return view('admin.listcabang', compact('cabang','userlist'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }







    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function editcabang(request $request)
    {
        $id     = $request->id;
        $cabang = DB::table('toko_cabang')
                    ->join('users', 'users.id' ,'=', 'toko_cabang.kepala_cabang')
                    ->where('id_cabang', $request->id)
                    ->get();

        return view('admin.editcabang', compact('cabang','id'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function pembatalanpesanan(request $request)
    {
        $id                 = $request->id;
        $invoice = DB::table('invoice_penjualan')
                            ->where('id_invoice', $id)
                            ->join('invoice', 'invoice_penjualan.id_invoice', '=', 'invoice.id')
                            ->select('invoice','tagihan','expedisi','code_booking','nama_pengirim','hp_pengirim','nama_pembeli','hp_pembeli','alamat','diskon')
                            ->get();

        $item = DB::table('penjualan')
                            ->where('id_invoice', $id)
                            ->get();

        $detailInvoice  = DB::table('invoice')->where('id', $id)->first();

            return view('admin.pembatalanpesanan', compact('invoice','id','item','detailInvoice'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function tolakpesananreseller(request $request)
    {
        $id                 = $request->id;

        $invoice = DB::table('invoice')
                        ->where('id', $id)
                        ->first();

        if ($invoice == null) {

            $invoice = DB::table('invoice')
                        ->where('bayar_id', $id)
                        ->first('id');

            $id                 = $invoice->id;

        }else {

            $id                 = $request->id;

        }

        // dd($invoice);

        $invoice = DB::table('invoice_penjualan')
                            ->where('id_invoice', $id)
                            ->join('invoice', 'invoice_penjualan.id_invoice', '=', 'invoice.id')
                            ->select('invoice','tagihan','expedisi','code_booking','nama_pengirim','hp_pengirim','nama_pembeli','hp_pembeli','alamat','diskon')
                            ->get();

        $item = DB::table('penjualan')
                            ->where('id_invoice', $id)
                            ->get();

        $detailInvoice  = DB::table('invoice')->where('id', $id)->first();

            return view('admin.tolakpesananreseller', compact('invoice','id','item','detailInvoice'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function listpenerimaan(request $request)
    {


        $toko = Auth::user()->toko;

        if ($request->cabang == null) {
            if($request->cariinvoice != null){

                $selectedcabang     = "Pilih Cabang";

                $listPencairan      = DB::table('invoice')
                                        ->where('pencairan', null)
                                        ->where('expedisi', $request->cariinvoice)
                                        ->whereNotIn('platform', ["pesanan dibatalkan"])
                                        ->orderBy('id', 'asc')
                                        ->paginate(5);

            }else{

                $selectedcabang     = "Pilih Cabang";
                $cabang             = "";

                $listPencairan      = DB::table('invoice')
                                        ->where('pencairan', null)
                                        ->whereNotIn('platform', ["pesanan dibatalkan"])
                                        ->orderBy('id', 'asc')
                                        ->paginate(10);

            };
        }else{
            if($request->cariinvoice != null){

                $selectedcabang     = "Pilih Cabang";

                $listPencairan      = DB::table('invoice')
                                        ->where('pencairan', null)
                                        ->where('expedisi', $request->cariinvoice)
                                        ->whereNotIn('platform', ["pesanan dibatalkan"])
                                        ->orderBy('id', 'desc')
                                        ->paginate(5);

            }else{

                $selectedcabang     = $request->cabang;
                $cabang             = $request->cabang;

                $listPencairan      = DB::table('invoice')
                                        ->where('pencairan', null)
                                        ->where('invoice_toko', $cabang)
                                        ->whereNotIn('platform', ["pesanan dibatalkan"])
                                        ->orderBy('id', 'desc')
                                        ->paginate(10);
            };
        };

        $listtoko               = DB::table('toko_cabang')
                                    ->get('nama_cabang');

        $countinvpending        = DB::table('invoice')
                                    ->where('pencairan', null)
                                    ->whereNotIn('platform', ["pesanan dibatalkan"])
                                    ->count('invoice');

        $countinvcair           = DB::table('invoice')
                                    ->where('pencairan', '>', 1)
                                    ->whereNotIn('platform', ["pesanan dibatalkan"])
                                    ->count('invoice');

        $nilaiinvpending        = DB::table('invoice')
                                    ->where('pencairan', null)
                                    ->whereNotIn('platform', ["pesanan dibatalkan"])
                                    ->sum('tagihan');

        $nilaiinvcair           = DB::table('invoice')
                                    ->where('pencairan', '>', 1)
                                    ->whereNotIn('platform', ["pesanan dibatalkan"])
                                    ->sum('pencairan');

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->keuangan == "tampil"){
            return view('admin.listpencairan', compact('listPencairan','selectedcabang','listtoko','countinvpending','countinvcair','nilaiinvcair','nilaiinvpending'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }





    public function keuanganadvance(Request $request)
    {
        //menampilkan data debit dan kredit pada keuangan adcvance
        $keuangan           = DB::table('keuangan_advance')
                                    ->orderBy('id','desc')
                                    ->paginate(10);
        //menampilkan akun keuangan
        $akun               = DB::table('akun_keuangan')
                                    ->orderBy('id_akun','desc')
                                    ->paginate(10);
        //menghitung total dana masuk pada keuangan advance
        $debit              = DB::table('keuangan_advance')
                                    ->sum('debit');
        //menghitung total dana keluar pada keuangan advance
        $kredit             = DB::table('keuangan_advance')
                                    ->sum('kredit');
        //menampilkan ballance uang masuk dikurangi uang keluar
        $ballance           = $debit-$kredit;
        //menampilkan list kantong dana atau list akun dana yang ada di dalam data keuangan advance
        $listKantongDana    =   DB::table('keuangan_advance')
                                    ->select('post',DB::raw('(sum(debit)) - (sum(kredit)) as total'))
                                    ->groupBy('post')  
                                    ->get();
                                
                                    // dd($listKantongDana);

        //menampilkan list rekening
        $listrekening       = DB::table('rekening')
                                    ->get();
        //menampilkan list marketplace
        $listMarketplace    = DB::table('list_marketplace')
                                    ->get();
                        
        //rubah format dari d-m-Y jadi Y-m-d
        $dmyAwal            = $request->awal;
        $dmyAkhir           = $request->akhir;
        // rubah "d m y" ke format unix timestamp
        $timestampawal      = strtotime($dmyAwal);
        $timestampakhir     = strtotime($dmyAkhir);
        
        // rubah Unix timestamp ke "ymd" format
        $ymd_awal           = date("Y-m-d", $timestampawal);
        $ymd_akhir           = date("Y-m-d", $timestampakhir);

        //value between kalau null
        $nullawal           = date('Y-m-d');
        $nulakhir           = date('Y-m-d');
        
        if ($request->awal == null) {
            //menampilan barang yang cair dalam kurun waktu tertentu
            $penjualancair      = DB::table('penjualan')
                                        ->join('stok_barang', 'stok_barang.id', '=', 'penjualan.id_barang')
                                        ->whereBetween('penjualan.tanggal', [$nullawal, $nulakhir])
                                        ->where('penjualan.status_dana', 'cair')
                                        ->select('stok_barang.nama_barang')
                                        ->selectRaw('SUM(penjualan.jumlah) as total_terjual')
                                        ->selectRaw('SUM(penjualan.jumlah * stok_barang.harga_modal) as total_modal')
                                        ->selectRaw('SUM(stok_barang.harga_modal) as sum_total_modal')
                                        ->groupBy('stok_barang.nama_barang')
                                        ->get();

            $sumpenjualancair   = DB::table('penjualan')
                                        ->join('stok_barang', 'stok_barang.id', '=', 'penjualan.id_barang')
                                        ->whereBetween('penjualan.tanggal', [$nullawal, $nulakhir])
                                        ->where('penjualan.status_dana', 'cair')
                                        ->selectRaw('SUM(penjualan.jumlah * stok_barang.harga_modal) as total_modal')
                                        ->first('total_modal');
        }else {
            //menampilan barang yang cair dalam kurun waktu tertentu
            $penjualancair      = DB::table('penjualan')
                                        ->join('stok_barang', 'stok_barang.id', '=', 'penjualan.id_barang')
                                        ->whereBetween('penjualan.tanggal', [$ymd_awal, $ymd_akhir])
                                        ->where('penjualan.status_dana', 'cair')
                                        ->select('stok_barang.nama_barang')
                                        ->selectRaw('SUM(penjualan.jumlah) as total_terjual')
                                        ->selectRaw('SUM(penjualan.jumlah * stok_barang.harga_modal) as total_modal')
                                        ->selectRaw('SUM(stok_barang.harga_modal) as sum_total_modal')
                                        ->groupBy('stok_barang.nama_barang')
                                        ->get();


            $sumpenjualancair   = DB::table('penjualan')
                                        ->join('stok_barang', 'stok_barang.id', '=', 'penjualan.id_barang')
                                        ->whereBetween('penjualan.tanggal', [$ymd_awal, $ymd_akhir])
                                        ->where('penjualan.status_dana', 'cair')
                                        ->selectRaw('SUM(penjualan.jumlah * stok_barang.harga_modal) as total_modal')
                                        ->first('total_modal');
        }
                                    // dd($penjualancair);
        
        


        return view('admin.keuanganadvance', compact('sumpenjualancair','dmyAwal','dmyAkhir','penjualancair','keuangan','ballance','akun','listKantongDana','listrekening','listMarketplace'));
    }







    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function perifstoksamapi(request $request)
    {
        $id     = $request->id;
        $detailbelanja = DB::table('stok_tertahan')
                    ->where('id', $id)
                    ->get();

        return view('admin.perifstoksamapi', compact('detailbelanja','id'));
    }







    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function role(request $request)
    {
        $role = DB::table('role')
                    ->join('role_menu', 'role.id_role' ,'=', 'role_menu.role_id')
                    ->paginate(10);

                    // dd($role);

        return view('admin.role', compact('role'));
    }







    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function editrole(request $request)
    {
        $id   = $request->id;
        $role = DB::table('role_menu')
                    ->where('role_id', $id)
                    ->first();

                    // dd($role);

        return view('admin.editrole', compact('role','id'));
    }







    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function reseller(request $request)
    {
             

        $userActive     = Auth::user()->id;

        $tagihan            = DB::table('invoice')
                                ->whereNotIn('tempo', ["lunas","tempo","selesai"])
                                ->whereNotIn('platform', ["pesanan dibatalkan"])
                                ->where('user', $userActive)
                                ->sum('tagihan');   

        $limitBelanja       = DB::table('list_marketplace')
                                ->where('id_user_toko', $userActive)
                                ->sum('limit_belanja');   

        if ($limitBelanja == 0) {
            $disableproduklist  = "";
            $pesanReseller      = "BELUM ADA INFO APAPUN HARI INI";
            $colloerAllert      = "text-success";
        }elseif ($tagihan > $limitBelanja) {
            $disableproduklist  = "disabled";
            $pesanReseller      = "SAAT INI KAMU TIDAK DAPAT MELAKUKAN TERANSAKSI KARNA TAGIHAN TEMPO KAMU MELEBIHI LIMIT BELANJA";
            $colloerAllert      = "text-danger";           
        }else {
            $disableproduklist  = "";
            $pesanReseller      = "BELUM ADA INFO APAPUN HARI INI";
            $colloerAllert      = "text-success";
        };

        $userRole = DB::table('users')->where('id', $userActive)->first('role');


        $tokoutama      = DB::table('users')
                                ->where('role', 1)
                                ->first('toko');

        $chartStore     = DB::table('chart')
                                ->where('user', $userActive)
                                ->orderBy('id_barang', 'DESC')
                                ->limit('1')
                                ->first('no_barang_id');

        $cekchart       = DB::table('chart')
                                ->where('user', $userActive)
                                ->first('id_barang');

                                // dd($chartStore);
        if($cekchart == null){
                $codBrngStore   = $tokoutama;
                $clssChngToko   = "";
        }else{
                $codBrngStore   = $chartStore->no_barang_id;
                $clssChngToko   = "disabled";
                $productStore   = DB::table('stok_barang')
                                ->where('id', $codBrngStore)
                                ->first('tipe');
        };

        // dd($chartStore, $productStore, $codBrngStore);
        
        $listToko       = DB::table('toko_cabang')
                                ->get('nama_cabang');

        if($cekchart != null){
                $selectedToko = $productStore->tipe;
        }elseif($request->nama_toko == null){
                $selectedToko = $tokoutama->toko;
        }else{
                $selectedToko   = $request->nama_toko;
        };
        
        // dd($selectedToko);

        $pilihBarang    = DB::table('stok_barang')
                                ->where('tipe', $selectedToko)
                                ->get();

        $stok           = DB::table('stok_barang')
                                ->where('tipe','Reseller')
                                ->paginate(5);

        $jual           = DB::table('stok_barang')
                                ->orderBy('id','desc')
                                ->get();

        $code           = DB::table('app_settings')
                                ->where('id',3)
                                ->get();

        $chart          = DB::table('chart')
                                ->where('user', $userActive)
                                ->join('stok_barang', 'chart.id_barang', '=', 'stok_barang.code_barang')
                                ->where('tipe', $selectedToko)
                                ->select('id_barang','nama_barang','harga_modal','harga_jual','stok','jml_barang','id_chart','total_harga','min_qty','agen_price','tr1','tr2','tr3','no_barang_id')
                                ->get();

        $subtotal       = DB::table('chart')->where('user', $userActive)->sum('total_harga');
        $submodal       = DB::table('chart')->where('user', $userActive)->sum('total_modal');
        $totbarang      = DB::table('chart')->where('user', $userActive)->sum('jml_barang');
        $invoice        = DB::table('invoice')->where('user', $userActive)->orderBy('invoice','desc')->paginate(10);
        $ofline         = DB::table('invoice')->where('platform', 'ofline')->orderBy('invoice','desc')->paginate(5);
        $lunas          = DB::table('invoice')->where('platform', 'lunas')->where('user', $userActive)->orderBy('invoice','desc')->paginate(5);
        $hutang         = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->orderBy('id','desc')->paginate(10);
        $tagihan        = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->sum('tagihan');
        $jumlahtrx      = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->count('tagihan');
        $priceLevel     = DB::table('users')->where('id', $userActive)->first('tier');
        $getmenu        = DB::table('role_menu')->where('role_id', $userRole->role)->first('home_menu');

        if ($priceLevel->tier == null) {
            return redirect('resellerorder')->with('kesalahantier', 'detail pesan ada di footer');
        };


        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();
        

        $listMarketplace= DB::table('list_marketplace')
                            ->where('id_user_toko', $userActive)
                            ->get();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->reseller_chart == "tampil"){
            return view('admin.penjualanreseller', compact('colloerAllert','pesanReseller','disableproduklist','listMarketplace','jual','tagihan','jumlahtrx','stok','code','chart','subtotal','invoice','totbarang','submodal','ofline','lunas','hutang','pilihBarang','listToko','selectedToko','clssChngToko','priceLevel'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function resellerorder(request $request)
    {

        $userActive         = Auth::user()->id;
        $userToko           = Auth::user()->toko;
        $lunas              = DB::table('invoice')
                                ->where('platform', 'lunas')
                                ->where('user', $userActive)
                                ->orderBy('invoice','desc')
                                ->paginate(5);

                            
        if($request->cari == null){

            $hutang             = DB::table('invoice')
                                    ->whereNotIn('tempo', ["lunas","tempo","selesai"])
                                    ->whereNotIn('platform', ["pesanan dibatalkan"])
                                    ->where('user', $userActive)
                                    ->orderBy('id','desc')
                                    ->paginate(10);
            
        }else{
            
            if($request->dari == "Invoice"){

            $hutang         = DB::table('invoice')
                                    ->where('expedisi', $request->cari)
                                    ->where('user', $userActive)
                                    ->orderBy('id','desc')
                                    ->paginate(10);
            
            }if($request->dari == "Resi"){

                $hutang         = DB::table('invoice')
                                        ->where('code_booking', $request->cari)
                                        ->where('user', $userActive)
                                        ->orderBy('id','desc')
                                        ->paginate(10);
            
            }
            
        };


        if($request->cari == null){

            $invoice        = DB::table('invoice')
                                ->where('invoice_toko', $userToko)
                                ->whereNotIn('platform', ["pesanan dibatalkan"])
                                ->orderBy('id','desc')
                                ->paginate(10);
            
        }else{
            
            if($request->dari == "Invoice"){

            $invoice        = DB::table('invoice')
                                ->where('expedisi', $request->cari)
                                ->whereNotIn('platform', ["pesanan dibatalkan"])
                                ->orderBy('id','desc')
                                ->paginate(10);
            
            }if($request->dari == "Resi"){

            $invoice        = DB::table('invoice')
                                ->where('code_booking', $request->cari)
                                ->whereNotIn('platform', ["pesanan dibatalkan"])
                                ->orderBy('id','desc')
                                ->paginate(10);
            
            }
            
        };





        $totaltrx           = DB::table('invoice')
                                ->where('user', $userActive)
                                ->sum('tagihan');

        $totalbayar           = DB::table('pembayaran')
                                ->where('status_pembayaran', "Diterima")
                                ->where('reseller_id', $userActive)
                                ->sum('total_pembayaran');

        $tagihan            = $totaltrx - $totalbayar;




        $jumlahtrx          = DB::table('invoice')
                                ->whereNotIn('tempo', ["lunas","tempo","selesai"])
                                ->whereNotIn('platform', ["pesanan dibatalkan"])
                                ->where('user', $userActive)
                                ->count('tagihan');

        $totaljmltrx        = DB::table('invoice')
                                ->where('user', $userActive)
                                ->whereNotIn('platform', ["pesanan dibatalkan"])
                                ->count('tagihan');
        
        $trxBaru            = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'new')
                                ->where('invoice_toko', $userToko)
                                ->orderBy('invoice','desc')
                                ->paginate(10);

        $counttrxBaru       = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'new')
                                ->where('invoice_toko', $userToko)
                                ->count('id');

        $trxproses          = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'diproses')
                                ->where('invoice_toko', $userToko)
                                ->orderBy('invoice','desc')
                                ->paginate(10);

        $counttrxproses     = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'diproses')
                                ->where('invoice_toko', $userToko)
                                ->count('id');

        $trxpacking         = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'dipacking')
                                ->where('invoice_toko', $userToko)
                                ->orderBy('invoice','desc')
                                ->paginate(10);

        $counttrxpacking    = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'dipacking')
                                ->where('invoice_toko', $userToko)
                                ->count('id');

        $trxkirim           = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'dikirim')
                                ->where('invoice_toko', $userToko)
                                ->orderBy('invoice','desc')
                                ->paginate(5);

        $counttrxkirim      = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'dikirim')
                                ->where('invoice_toko', $userToko)
                                ->count('id');
        // dd($trxBaru);
        // data rencana pembayaran
        $dataRcb        = DB::table('rencana_pembayaran')
                            ->where('user_id', $userActive)
                            ->join('invoice', 'invoice.id' ,'=', 'rencana_pembayaran.id_inv')
                            ->get();
                            
        $dataRcbtotal   = DB::table('rencana_pembayaran')
                            ->where('user_id', $userActive)
                            ->join('invoice', 'invoice.id' ,'=', 'rencana_pembayaran.id_inv')
                            ->sum('tagihan');

        $hitoryPembayaran = DB::table('pembayaran')
                            ->where('reseller_id', $userActive)
                            ->get();

        $cancelpesanan  = DB::table('invoice_batal')->where('user', $userActive)->orderBy('id', 'desc')->paginate(10);

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->reseller_order == "tampil"){
            return view('admin.resellerorder', compact('totalbayar','lunas','hutang','tagihan','jumlahtrx','totaltrx','totaljmltrx','trxBaru','counttrxBaru','trxproses','counttrxproses','trxpacking','counttrxpacking','trxkirim','counttrxkirim','dataRcb','dataRcbtotal','hitoryPembayaran','cancelpesanan'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }


        
    }






    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function historypembayaranreseller(request $request)
    {

        $userActive         = Auth::user()->id;
        $userToko           = Auth::user()->toko;
        $lunas              = DB::table('invoice')
                                ->where('platform', 'lunas')
                                ->where('user', $userActive)
                                ->orderBy('invoice','desc')
                                ->paginate(5);

                            
        if($request->cari == null){

            $hutang             = DB::table('invoice')
                                    ->whereNotIn('tempo', ["lunas","tempo","selesai"])
                                    ->whereNotIn('platform', ["pesanan dibatalkan"])
                                    ->where('user', $userActive)
                                    ->orderBy('id','desc')
                                    ->paginate(10);
            
        }else{
            
            if($request->dari == "Invoice"){

            $hutang         = DB::table('invoice')
                                    ->where('expedisi', $request->cari)
                                    ->where('user', $userActive)
                                    ->orderBy('id','desc')
                                    ->paginate(10);
            
            }if($request->dari == "Resi"){

                $hutang         = DB::table('invoice')
                                        ->where('code_booking', $request->cari)
                                        ->where('user', $userActive)
                                        ->orderBy('id','desc')
                                        ->paginate(10);
            
            }
            
        };


        if($request->cari == null){

            $invoice        = DB::table('invoice')
                                ->where('invoice_toko', $userToko)
                                ->whereNotIn('platform', ["pesanan dibatalkan"])
                                ->orderBy('id','desc')
                                ->paginate(10);
            
        }else{
            
            if($request->dari == "Invoice"){

            $invoice        = DB::table('invoice')
                                ->where('expedisi', $request->cari)
                                ->whereNotIn('platform', ["pesanan dibatalkan"])
                                ->orderBy('id','desc')
                                ->paginate(10);
            
            }if($request->dari == "Resi"){

            $invoice        = DB::table('invoice')
                                ->where('code_booking', $request->cari)
                                ->whereNotIn('platform', ["pesanan dibatalkan"])
                                ->orderBy('id','desc')
                                ->paginate(10);
            
            }
            
        };



        $totaltrx           = DB::table('invoice')
                                ->where('user', $userActive)
                                ->sum('tagihan');

        $totalbayar           = DB::table('pembayaran')
                                ->where('status_pembayaran', "Diterima")
                                ->where('reseller_id', $userActive)
                                ->sum('total_pembayaran');

        $tagihan            = $totaltrx - $totalbayar;

        $jumlahtrx          = DB::table('invoice')
                                ->whereNotIn('tempo', ["lunas","tempo","selesai"])
                                ->whereNotIn('platform', ["pesanan dibatalkan"])
                                ->where('user', $userActive)
                                ->count('tagihan');

        $totaljmltrx        = DB::table('invoice')
                                ->where('user', $userActive)
                                ->whereNotIn('platform', ["pesanan dibatalkan"])
                                ->count('tagihan');
        
        $trxBaru            = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'new')
                                ->where('invoice_toko', $userToko)
                                ->orderBy('invoice','desc')
                                ->paginate(10);

        $counttrxBaru       = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'new')
                                ->where('invoice_toko', $userToko)
                                ->count('id');

        $trxproses          = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'diproses')
                                ->where('invoice_toko', $userToko)
                                ->orderBy('invoice','desc')
                                ->paginate(10);

        $counttrxproses     = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'diproses')
                                ->where('invoice_toko', $userToko)
                                ->count('id');

        $trxpacking         = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'dipacking')
                                ->where('invoice_toko', $userToko)
                                ->orderBy('invoice','desc')
                                ->paginate(10);

        $counttrxpacking    = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'dipacking')
                                ->where('invoice_toko', $userToko)
                                ->count('id');

        $trxkirim           = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'dikirim')
                                ->where('invoice_toko', $userToko)
                                ->orderBy('invoice','desc')
                                ->paginate(5);

        $counttrxkirim      = DB::table('invoice')
                                ->whereIn('platform', ['Tempo','lunas'])
                                ->where('tempo', 'dikirim')
                                ->where('invoice_toko', $userToko)
                                ->count('id');
        // dd($trxBaru);
        // data rencana pembayaran
        $dataRcb        = DB::table('rencana_pembayaran')
                            ->where('user_id', $userActive)
                            ->join('invoice', 'invoice.id' ,'=', 'rencana_pembayaran.id_inv')
                            ->get();
                            
        $dataRcbtotal   = DB::table('rencana_pembayaran')
                            ->where('user_id', $userActive)
                            ->join('invoice', 'invoice.id' ,'=', 'rencana_pembayaran.id_inv')
                            ->sum('tagihan');

        $hitoryPembayaran = DB::table('pembayaran')
                            ->where('reseller_id', $userActive)
                            ->get();

        $cancelpesanan  = DB::table('invoice_batal')->where('user', $userActive)->orderBy('id', 'desc')->paginate(10);

        $UserRole = Auth::user()->role;

        $getmenu        = DB::table('role_menu')->where('role_id', $UserRole)->first();

        if($getmenu == null){
            return abort(404,'tujuan tidak di temukan');
        }if($getmenu->reseller_order == "tampil"){
            return view('admin.historypembayaranreseller', compact('totalbayar','lunas','hutang','tagihan','jumlahtrx','totaltrx','totaljmltrx','trxBaru','counttrxBaru','trxproses','counttrxproses','trxpacking','counttrxpacking','trxkirim','counttrxkirim','dataRcb','dataRcbtotal','hitoryPembayaran','cancelpesanan'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }


        
    }







    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function listmarketplace(request $request)
    {
        $marketplace    = DB::table('list_marketplace')
                            ->join('users', 'list_marketplace.id_user_toko' ,'=', 'users.id')
                            ->get();

        $users          = DB::table('users')
                            ->get();

                    // dd($role);

        return view('admin.listmarketplace', compact('marketplace','users'));
    }







    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function editmarketplace(request $request)
    {
        $id                     = $request->id;

        $detailmarketplace      = DB::table('list_marketplace')
                                    ->where('id_marketplace', $id)
                                    ->join('users', 'list_marketplace.id_user_toko' ,'=', 'users.id')
                                    ->first();
                                    // dd($detailmarketplace);

        $users                  = DB::table('users')
                                    ->get();

                    // dd($role);

        return view('admin.editmarketplace', compact('detailmarketplace','users','id'));
    }







    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function printperlustok(request $request)
    {
        

        $minimumStok = app_settings::where('id', 4)
                        ->first('settings_value');
                        
        $toko = Auth::user()->toko;
                        
        if($request->cabang == "Pilih Cabang"){
            $selectedcabang = $toko;
        }else{
            $selectedcabang = $request->cabang;
        }

                        

        $stokAlert = DB::table('penjualan')
        ->select(
        'penjualan.code_barang',
        'penjualan.title',
        'penjualan.toko',
        DB::raw('SUM(penjualan.jumlah) as total_terjual'),
        'stok_barang.stok'
        )
        ->join('stok_barang', 'penjualan.code_barang', '=', 'stok_barang.code_barang')
        ->where('penjualan.toko', $selectedcabang)
        ->where('stok_barang.tipe', $selectedcabang)
        ->where('penjualan.tanggal', '>=', Carbon::now()->subDays(14)->toDateString())
        ->where('penjualan.title', 'NOT LIKE', '%sachet%')
        ->groupBy('penjualan.code_barang', 'penjualan.title', 'stok_barang.stok')
        ->get();


        return view('admin.printperlustok', compact('stokAlert'));
    }








    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function printstokopname(request $request)
    {
        

        $minimumStok = app_settings::where('id', 4)
                        ->first('settings_value');
                        
        $toko = Auth::user()->toko;
                        
        if($request->cabang == "Pilih Cabang"){
            $selectedcabang = $toko;
        }else{
            $selectedcabang = $request->cabang;
        }

                        

        $stokAlert = DB::table('penjualan')
                    ->select(
                    'penjualan.code_barang',
                    'penjualan.title',
                    'penjualan.toko',
                    DB::raw('SUM(penjualan.jumlah) as total_terjual'),
                    'stok_barang.stok'
                    )
                    ->join('stok_barang', 'penjualan.code_barang', '=', 'stok_barang.code_barang')
                    ->where('penjualan.toko', $selectedcabang)
                    ->where('stok_barang.tipe', $selectedcabang)
                    ->where('penjualan.tanggal', '>=', Carbon::now()->subDays(14)->toDateString())
                    ->groupBy('penjualan.code_barang', 'penjualan.title', 'stok_barang.stok')
                    ->get();


        return view('admin.printstokopname', compact('stokAlert'));
    }








}
