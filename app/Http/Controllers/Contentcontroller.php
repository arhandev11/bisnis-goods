<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\app_settings;


class Contentcontroller extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function contentplanning(request $request)
                    {

                            $activeuser = auth()->user()->id;

                            $userdivisi = DB::table('users')
                                            ->where('id', $activeuser)
                                            ->first('divisi');

                            $manincharge = DB::table('users')
                                            ->where('divisi', $userdivisi->divisi)
                                            ->get();

                            $purposelist = DB::table('data_task')
                                            ->where('media', "instagram")
                                            ->orwhere('media', "tiktok")
                                            ->orwhere('media', "youtube")
                                            ->get();
                                            // dd($purposelist);

                            $countpurpose = $purposelist->count();
                                            

                            $selectedDate = date('Y-m-d');
                            $hariini = date('Y-m-d');
                            if(isset($request->bulan) == null){
                                $bulan = date('m');
                            }else{
                                $bulan = $request->bulan;
                            };
                            if(isset($request->bulan) == null){
                                $tahun = date('Y');
                            }else{
                                $tahun = $request->tahun;
                            };

                            if($request->bulan == null){
                                $selectedBulan = date('m', strtotime($bulan));
                            }else{
                                $selectedBulan = $bulan;
                            };
                            if($request->tahun == null){
                                $selectedTahun = date('Y', strtotime($tahun));
                            }else{
                                $selectedTahun = $tahun;
                            };


                            $detailtaskIg = DB::table('data_task')
                                            ->where('tanggal_mulai', $selectedDate)
                                            ->where('media', "instagram")
                                            ->get();

                            $igtaskcount = DB::table('data_task')
                                            ->where('tanggal_mulai', $selectedDate)
                                            ->where('media', "instagram")
                                            ->count('media');

                            $detailtaskyt = DB::table('data_task')
                                            ->where('tanggal_mulai', $selectedDate)
                                            ->where('media', "youtube")
                                            ->get();

                            $yttaskcount = DB::table('data_task')
                                            ->where('tanggal_mulai', $selectedDate)
                                            ->where('media', "youtube")
                                            ->count('media');

                            $detailtasktk = DB::table('data_task')
                                            ->where('tanggal_mulai', $selectedDate)
                                            ->where('media', "tiktok")
                                            ->get();

                            $tktaskcount = DB::table('data_task')
                                            ->where('tanggal_mulai', $selectedDate)
                                            ->where('media', "tiktok")
                                            ->count('media');

                            // tanggal 1
                                            $instagram1 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 1)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg1 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 1)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg1->publish_date) == null){
                                                $displayCheckIg1 = "x-square";
                                            }else{
                                                $displayCheckIg1 = "check-square";
                                            }
                            // dd($checkIg1);
                
                                            $tiktok1 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 1)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk1 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 1)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk1->publish_date) == null){
                                                $displayCheckTk1 = "x-square";
                                            }else{
                                                $displayCheckTk1 = "check-square";
                                            }
                
                                            $youtube1 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 1)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt1 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 1)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt1->publish_date) == null){
                                                $displayCheckYt1 = "x-square";
                                            }else{
                                                $displayCheckYt1 = "check-square";
                                            }
                
                                            // tanggal 2
                                            $instagram2 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 2)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg2 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 2)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg2->publish_date) == null){
                                                $displayCheckIg2 = "x-square";
                                            }else{
                                                $displayCheckIg2 = "check-square";
                                            }
                
                                            $tiktok2 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 2)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk2 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 2)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk2->publish_date) == null){
                                                $displayCheckTk2 = "x-square";
                                            }else{
                                                $displayCheckTk2 = "check-square";
                                            }
                
                                            $youtube2 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 2)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt2 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 2)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt2->publish_date) == null){
                                                $displayCheckYt2 = "x-square";
                                            }else{
                                                $displayCheckYt2 = "check-square";
                                            }
                
                                            // tanggal 3
                                            $instagram3 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 3)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg3 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 3)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg3->publish_date) == null){
                                                $displayCheckIg3 = "x-square";
                                            }else{
                                                $displayCheckIg3 = "check-square";
                                            }
                
                                            $tiktok3 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 3)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk3 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 3)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk3->publish_date) == null){
                                                $displayCheckTk3 = "x-square";
                                            }else{
                                                $displayCheckTk3 = "check-square";
                                            }
                
                                            $youtube3 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 3)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt3 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 3)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt3->publish_date) == null){
                                                $displayCheckYt3 = "x-square";
                                            }else{
                                                $displayCheckYt3 = "check-square";
                                            }
                
                                            // tanggal 4
                                            $instagram4 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 4)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg4 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 4)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg4->publish_date) == null){
                                                $displayCheckIg4 = "x-square";
                                            }else{
                                                $displayCheckIg4 = "check-square";
                                            }
                
                                            $tiktok4 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 4)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk4 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 4)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk4->publish_date) == null){
                                                $displayCheckTk4 = "x-square";
                                            }else{
                                                $displayCheckTk4 = "check-square";
                                            }
                
                                            $youtube4 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 4)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt4 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 4)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt4->publish_date) == null){
                                                $displayCheckYt4 = "x-square";
                                            }else{
                                                $displayCheckYt4 = "check-square";
                                            }
                
                                            // tanggal 5
                                            $instagram5 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 5)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg5 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 5)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg5->publish_date) == null){
                                                $displayCheckIg5 = "x-square";
                                            }else{
                                                $displayCheckIg5 = "check-square";
                                            }
                
                                            $tiktok5 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 5)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk5 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 5)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk5->publish_date) == null){
                                                $displayCheckTk5 = "x-square";
                                            }else{
                                                $displayCheckTk5 = "check-square";
                                            }
                
                                            $youtube5 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 5)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt5 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 5)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt5->publish_date) == null){
                                                $displayCheckYt5 = "x-square";
                                            }else{
                                                $displayCheckYt5 = "check-square";
                                            }
                
                                            // tanggal 6
                                            $instagram6 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 6)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg6 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 6)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg6->publish_date) == null){
                                                $displayCheckIg6 = "x-square";
                                            }else{
                                                $displayCheckIg6 = "check-square";
                                            }
                
                                            $tiktok6 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 6)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk6 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 6)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk6->publish_date) == null){
                                                $displayCheckTk6 = "x-square";
                                            }else{
                                                $displayCheckTk6 = "check-square";
                                            }
                
                                            $youtube6 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 6)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt6 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 6)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt6->publish_date) == null){
                                                $displayCheckYt6 = "x-square";
                                            }else{
                                                $displayCheckYt6 = "check-square";
                                            }
                
                                            // tanggal 7
                                            $instagram7 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 7)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg7 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 7)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg7->publish_date) == null){
                                                $displayCheckIg7 = "x-square";
                                            }else{
                                                $displayCheckIg7 = "check-square";
                                            }
                
                                            $tiktok7 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 7)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk7 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 7)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk7->publish_date) == null){
                                                $displayCheckTk7 = "x-square";
                                            }else{
                                                $displayCheckTk7 = "check-square";
                                            }
                
                                            $youtube7 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 7)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt7 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 7)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt7->publish_date) == null){
                                                $displayCheckYt7 = "x-square";
                                            }else{
                                                $displayCheckYt7 = "check-square";
                                            }
                
                                            // tanggal 8
                                            $instagram8 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 8)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg8 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 8)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg8->publish_date) == null){
                                                $displayCheckIg8 = "x-square";
                                            }else{
                                                $displayCheckIg8 = "check-square";
                                            }
                
                                            $tiktok8 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 8)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk8 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 8)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk8->publish_date) == null){
                                                $displayCheckTk8 = "x-square";
                                            }else{
                                                $displayCheckTk8 = "check-square";
                                            }
                
                                            $youtube8 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 8)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt8 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 8)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt8->publish_date) == null){
                                                $displayCheckYt8 = "x-square";
                                            }else{
                                                $displayCheckYt8 = "check-square";
                                            }
                
                                            // tanggal 9
                                            $instagram9 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 9)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg9 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 9)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg9->publish_date) == null){
                                                $displayCheckIg9 = "x-square";
                                            }else{
                                                $displayCheckIg9 = "check-square";
                                            }
                
                                            $tiktok9 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 9)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk9 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 9)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk9->publish_date) == null){
                                                $displayCheckTk9 = "x-square";
                                            }else{
                                                $displayCheckTk9 = "check-square";
                                            }
                
                                            $youtube9 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 9)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt9 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 9)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt9->publish_date) == null){
                                                $displayCheckYt9 = "x-square";
                                            }else{
                                                $displayCheckYt9 = "check-square";
                                            }
                
                                            // tanggal 10
                                            $instagram10 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 10)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg10 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 10)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg10->publish_date) == null){
                                                $displayCheckIg10 = "x-square";
                                            }else{
                                                $displayCheckIg10 = "check-square";
                                            }
                
                                            $tiktok10 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 10)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk10 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 10)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk10->publish_date) == null){
                                                $displayCheckTk10 = "x-square";
                                            }else{
                                                $displayCheckTk10 = "check-square";
                                            }
                
                                            $youtube10 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 10)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt10 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 10)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt10->publish_date) == null){
                                                $displayCheckYt10 = "x-square";
                                            }else{
                                                $displayCheckYt10 = "check-square";
                                            }
                
                                            // tanggal 11
                                            $instagram11 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 11)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg11 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 11)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg11->publish_date) == null){
                                                $displayCheckIg11 = "x-square";
                                            }else{
                                                $displayCheckIg11 = "check-square";
                                            }
                
                                            $tiktok11 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 11)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk11 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 11)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk11->publish_date) == null){
                                                $displayCheckTk11 = "x-square";
                                            }else{
                                                $displayCheckTk11 = "check-square";
                                            }
                
                                            $youtube11 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 11)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt11 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 11)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt11->publish_date) == null){
                                                $displayCheckYt11 = "x-square";
                                            }else{
                                                $displayCheckYt11 = "check-square";
                                            }
                
                                            // tanggal 12
                                            $instagram12 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 12)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg12 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 12)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg12->publish_date) == null){
                                                $displayCheckIg12 = "x-square";
                                            }else{
                                                $displayCheckIg12 = "check-square";
                                            }
                
                                            $tiktok12 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 12)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk12 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 12)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk12->publish_date) == null){
                                                $displayCheckTk12 = "x-square";
                                            }else{
                                                $displayCheckTk12 = "check-square";
                                            }
                
                                            $youtube12 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 12)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt12 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 12)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt12->publish_date) == null){
                                                $displayCheckYt12 = "x-square";
                                            }else{
                                                $displayCheckYt12 = "check-square";
                                            }
                
                                            // tanggal 13
                                            $instagram13 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 13)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg13 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 13)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg13->publish_date) == null){
                                                $displayCheckIg13 = "x-square";
                                            }else{
                                                $displayCheckIg13 = "check-square";
                                            }
                
                                            $tiktok13 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 13)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk13 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 13)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk13->publish_date) == null){
                                                $displayCheckTk13 = "x-square";
                                            }else{
                                                $displayCheckTk13 = "check-square";
                                            }
                
                                            $youtube13 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 13)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt13 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 13)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt13->publish_date) == null){
                                                $displayCheckYt13 = "x-square";
                                            }else{
                                                $displayCheckYt13 = "check-square";
                                            }
                
                                            // tanggal 14
                                            $instagram14 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 14)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg14 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 14)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg14->publish_date) == null){
                                                $displayCheckIg14 = "x-square";
                                            }else{
                                                $displayCheckIg14 = "check-square";
                                            }
                
                                            $tiktok14 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 14)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk14 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 14)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk14->publish_date) == null){
                                                $displayCheckTk14 = "x-square";
                                            }else{
                                                $displayCheckTk14 = "check-square";
                                            }
                
                                            $youtube14 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 14)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt14 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 14)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt14->publish_date) == null){
                                                $displayCheckYt14 = "x-square";
                                            }else{
                                                $displayCheckYt14 = "check-square";
                                            }
                
                                            // tanggal 15
                                            $instagram15 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 15)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg15 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 15)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg15->publish_date) == null){
                                                $displayCheckIg15 = "x-square";
                                            }else{
                                                $displayCheckIg15 = "check-square";
                                            }
                
                                            $tiktok15 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 15)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk15 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 15)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk15->publish_date) == null){
                                                $displayCheckTk15 = "x-square";
                                            }else{
                                                $displayCheckTk15 = "check-square";
                                            }
                
                                            $youtube15 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 15)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt15 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 15)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt15->publish_date) == null){
                                                $displayCheckYt15 = "x-square";
                                            }else{
                                                $displayCheckYt15 = "check-square";
                                            }
                
                                            // tanggal 16
                                            $instagram16 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 16)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg16 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 16)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg16->publish_date) == null){
                                                $displayCheckIg16 = "x-square";
                                            }else{
                                                $displayCheckIg16 = "check-square";
                                            }
                
                                            $tiktok16 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 16)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk16 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 16)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk16->publish_date) == null){
                                                $displayCheckTk16 = "x-square";
                                            }else{
                                                $displayCheckTk16 = "check-square";
                                            }
                
                                            $youtube16 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 16)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt16 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 16)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt16->publish_date) == null){
                                                $displayCheckYt16 = "x-square";
                                            }else{
                                                $displayCheckYt16 = "check-square";
                                            }
                
                                            // tanggal 17
                                            $instagram17 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 17)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg17 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 17)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg17->publish_date) == null){
                                                $displayCheckIg17 = "x-square";
                                            }else{
                                                $displayCheckIg17 = "check-square";
                                            }
                
                                            $tiktok17 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 17)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk17 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 17)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk17->publish_date) == null){
                                                $displayCheckTk17 = "x-square";
                                            }else{
                                                $displayCheckTk17 = "check-square";
                                            }
                
                                            $youtube17 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 17)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt17 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 17)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt17->publish_date) == null){
                                                $displayCheckYt17 = "x-square";
                                            }else{
                                                $displayCheckYt17 = "check-square";
                                            }
                
                                            // tanggal 18
                                            $instagram18 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 18)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg18 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 18)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg18->publish_date) == null){
                                                $displayCheckIg18 = "x-square";
                                            }else{
                                                $displayCheckIg18 = "check-square";
                                            }
                
                                            $tiktok18 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 18)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk18 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 18)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk18->publish_date) == null){
                                                $displayCheckTk18 = "x-square";
                                            }else{
                                                $displayCheckTk18 = "check-square";
                                            }
                
                                            $youtube18 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 18)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt18 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 18)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt18->publish_date) == null){
                                                $displayCheckYt18 = "x-square";
                                            }else{
                                                $displayCheckYt18 = "check-square";
                                            }
                
                                            // tanggal 19
                                            $instagram19 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 19)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg19 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 19)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg19->publish_date) == null){
                                                $displayCheckIg19 = "x-square";
                                            }else{
                                                $displayCheckIg19 = "check-square";
                                            }
                
                                            $tiktok19 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 19)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk19 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 19)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk19->publish_date) == null){
                                                $displayCheckTk19 = "x-square";
                                            }else{
                                                $displayCheckTk19 = "check-square";
                                            }
                
                                            $youtube19 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 19)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt19 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 19)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt19->publish_date) == null){
                                                $displayCheckYt19 = "x-square";
                                            }else{
                                                $displayCheckYt19 = "check-square";
                                            }
                
                                            // tanggal 20
                                            $instagram20 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 20)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg20 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 20)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg20->publish_date) == null){
                                                $displayCheckIg20 = "x-square";
                                            }else{
                                                $displayCheckIg20 = "check-square";
                                            }
                
                                            $tiktok20 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 20)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk20 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 20)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk20->publish_date) == null){
                                                $displayCheckTk20 = "x-square";
                                            }else{
                                                $displayCheckTk20 = "check-square";
                                            }
                
                                            $youtube20 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 20)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt20 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 20)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt20->publish_date) == null){
                                                $displayCheckYt20 = "x-square";
                                            }else{
                                                $displayCheckYt20 = "check-square";
                                            }
                
                                            // tanggal 21
                                            $instagram21 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 21)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg21 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 21)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg21->publish_date) == null){
                                                $displayCheckIg21 = "x-square";
                                            }else{
                                                $displayCheckIg21 = "check-square";
                                            }
                
                                            $tiktok21 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 21)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk21 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 21)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk21->publish_date) == null){
                                                $displayCheckTk21 = "x-square";
                                            }else{
                                                $displayCheckTk21 = "check-square";
                                            }
                
                                            $youtube21 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 21)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt21 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 21)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt21->publish_date) == null){
                                                $displayCheckYt21 = "x-square";
                                            }else{
                                                $displayCheckYt21 = "check-square";
                                            }
                
                                            // tanggal 22
                                            $instagram22 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 22)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg22 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 22)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg22->publish_date) == null){
                                                $displayCheckIg22 = "x-square";
                                            }else{
                                                $displayCheckIg22 = "check-square";
                                            }
                
                                            $tiktok22 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 22)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk22 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 22)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk22->publish_date) == null){
                                                $displayCheckTk22 = "x-square";
                                            }else{
                                                $displayCheckTk22 = "check-square";
                                            }
                
                                            $youtube22 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 22)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt22 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 22)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt22->publish_date) == null){
                                                $displayCheckYt22 = "x-square";
                                            }else{
                                                $displayCheckYt22 = "check-square";
                                            }
                
                                            // tanggal 23
                                            $instagram23 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 23)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg23 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 23)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg23->publish_date) == null){
                                                $displayCheckIg23 = "x-square";
                                            }else{
                                                $displayCheckIg23 = "check-square";
                                            }
                
                                            $tiktok23 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 23)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk23 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 23)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk23->publish_date) == null){
                                                $displayCheckTk23 = "x-square";
                                            }else{
                                                $displayCheckTk23 = "check-square";
                                            }
                
                                            $youtube23 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 23)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt23 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 23)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt23->publish_date) == null){
                                                $displayCheckYt23 = "x-square";
                                            }else{
                                                $displayCheckYt23 = "check-square";
                                            }
                
                                            // tanggal 24
                                            $instagram24 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 24)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg24 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 24)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg24->publish_date) == null){
                                                $displayCheckIg24 = "x-square";
                                            }else{
                                                $displayCheckIg24 = "check-square";
                                            }
                
                                            $tiktok24 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 24)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk24 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 24)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk24->publish_date) == null){
                                                $displayCheckTk24 = "x-square";
                                            }else{
                                                $displayCheckTk24 = "check-square";
                                            }
                
                                            $youtube24 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 24)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt24 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 24)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt24->publish_date) == null){
                                                $displayCheckYt24 = "x-square";
                                            }else{
                                                $displayCheckYt24 = "check-square";
                                            }
                
                                            // tanggal 25
                                            $instagram25 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 25)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg25 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 25)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg25->publish_date) == null){
                                                $displayCheckIg25 = "x-square";
                                            }else{
                                                $displayCheckIg25 = "check-square";
                                            }
                
                                            $tiktok25 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 25)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk25 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 25)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk25->publish_date) == null){
                                                $displayCheckTk25 = "x-square";
                                            }else{
                                                $displayCheckTk25 = "check-square";
                                            }
                
                                            $youtube25 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 25)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt25 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 25)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt25->publish_date) == null){
                                                $displayCheckYt25 = "x-square";
                                            }else{
                                                $displayCheckYt25 = "check-square";
                                            }
                
                                            // tanggal 26
                                            $instagram26 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 26)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg26 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 26)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg26->publish_date) == null){
                                                $displayCheckIg26 = "x-square";
                                            }else{
                                                $displayCheckIg26 = "check-square";
                                            }
                
                                            $tiktok26 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 26)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk26 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 26)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk26->publish_date) == null){
                                                $displayCheckTk26 = "x-square";
                                            }else{
                                                $displayCheckTk26 = "check-square";
                                            }
                
                                            $youtube26 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 26)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt26 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 26)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt26->publish_date) == null){
                                                $displayCheckYt26 = "x-square";
                                            }else{
                                                $displayCheckYt26 = "check-square";
                                            }
                
                                            // tanggal 27
                                            $instagram27 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 27)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg27 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 27)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg27->publish_date) == null){
                                                $displayCheckIg27 = "x-square";
                                            }else{
                                                $displayCheckIg27 = "check-square";
                                            }
                
                                            $tiktok27 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 27)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk27 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 27)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk27->publish_date) == null){
                                                $displayCheckTk27 = "x-square";
                                            }else{
                                                $displayCheckTk27 = "check-square";
                                            }
                
                                            $youtube27 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 27)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt27 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 27)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt27->publish_date) == null){
                                                $displayCheckYt27 = "x-square";
                                            }else{
                                                $displayCheckYt27 = "check-square";
                                            }
                
                                            // tanggal 28
                                            $instagram28 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 28)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg28 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 28)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg28->publish_date) == null){
                                                $displayCheckIg28 = "x-square";
                                            }else{
                                                $displayCheckIg28 = "check-square";
                                            }
                
                                            $tiktok28 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 28)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk28 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 28)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk28->publish_date) == null){
                                                $displayCheckTk28 = "x-square";
                                            }else{
                                                $displayCheckTk28 = "check-square";
                                            }
                
                                            $youtube28 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 28)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt28 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 28)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt28->publish_date) == null){
                                                $displayCheckYt28 = "x-square";
                                            }else{
                                                $displayCheckYt28 = "check-square";
                                            }
                
                                            // tanggal 29
                                            $instagram29 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 29)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg29 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 29)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg29->publish_date) == null){
                                                $displayCheckIg29 = "x-square";
                                            }else{
                                                $displayCheckIg29 = "check-square";
                                            }
                
                                            $tiktok29 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 29)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk29 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 29)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk29->publish_date) == null){
                                                $displayCheckTk29 = "x-square";
                                            }else{
                                                $displayCheckTk29 = "check-square";
                                            }
                
                                            $youtube29 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 29)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt29 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 29)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt29->publish_date) == null){
                                                $displayCheckYt29 = "x-square";
                                            }else{
                                                $displayCheckYt29 = "check-square";
                                            }
                
                                            // tanggal 30
                                            $instagram30 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 30)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg30 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 30)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg30->publish_date) == null){
                                                $displayCheckIg30 = "x-square";
                                            }else{
                                                $displayCheckIg30 = "check-square";
                                            }
                
                                            $tiktok30 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 30)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk30 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 30)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk30->publish_date) == null){
                                                $displayCheckTk30 = "x-square";
                                            }else{
                                                $displayCheckTk30 = "check-square";
                                            }
                
                                            $youtube30 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 30)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt30 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 30)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt30->publish_date) == null){
                                                $displayCheckYt30 = "x-square";
                                            }else{
                                                $displayCheckYt30 = "check-square";
                                            }
                
                                            // tanggal 31
                                            $instagram31 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 31)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg31 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 31)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg31->publish_date) == null){
                                                $displayCheckIg31 = "x-square";
                                            }else{
                                                $displayCheckIg31 = "check-square";
                                            }
                
                                            $tiktok31 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 31)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk31 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 31)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk31->publish_date) == null){
                                                $displayCheckTk31 = "x-square";
                                            }else{
                                                $displayCheckTk31 = "check-square";
                                            }
                
                                            $youtube31 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 31)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt31 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 31)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt31->publish_date) == null){
                                                $displayCheckYt31 = "x-square";
                                            }else{
                                                $displayCheckYt31 = "check-square";
                                            }



                            return view('content.contentplanning', compact('countpurpose','purposelist','manincharge',
                            'hariini','igtaskcount','yttaskcount','tktaskcount','selectedBulan','selectedTahun',
                            'detailtaskIg','detailtasktk','detailtaskyt','selectedDate','bulan','tahun',
                            'instagram1','tiktok1','youtube1','displayCheckYt1','displayCheckTk1','displayCheckIg1',
                            'instagram2','tiktok2','youtube2','displayCheckYt2','displayCheckTk2','displayCheckIg2',
                            'instagram3','tiktok3','youtube3','displayCheckYt3','displayCheckTk3','displayCheckIg3',
                            'instagram4','tiktok4','youtube4','displayCheckYt4','displayCheckTk4','displayCheckIg4',
                            'instagram5','tiktok5','youtube5','displayCheckYt5','displayCheckTk5','displayCheckIg5',
                            'instagram6','tiktok6','youtube6','displayCheckYt6','displayCheckTk6','displayCheckIg6',
                            'instagram7','tiktok7','youtube7','displayCheckYt7','displayCheckTk7','displayCheckIg7',
                            'instagram8','tiktok8','youtube8','displayCheckYt8','displayCheckTk8','displayCheckIg8',
                            'instagram9','tiktok9','youtube9','displayCheckYt9','displayCheckTk9','displayCheckIg9',
                            'instagram10','tiktok10','youtube10','displayCheckYt10','displayCheckTk10','displayCheckIg10',
                            'instagram11','tiktok11','youtube11','displayCheckYt11','displayCheckTk11','displayCheckIg11',
                            'instagram12','tiktok12','youtube12','displayCheckYt12','displayCheckTk12','displayCheckIg12',
                            'instagram13','tiktok13','youtube13','displayCheckYt13','displayCheckTk13','displayCheckIg13',
                            'instagram14','tiktok14','youtube14','displayCheckYt14','displayCheckTk14','displayCheckIg14',
                            'instagram15','tiktok15','youtube15','displayCheckYt15','displayCheckTk15','displayCheckIg15',
                            'instagram16','tiktok16','youtube16','displayCheckYt16','displayCheckTk16','displayCheckIg16',
                            'instagram17','tiktok17','youtube17','displayCheckYt17','displayCheckTk17','displayCheckIg17',
                            'instagram18','tiktok18','youtube18','displayCheckYt18','displayCheckTk18','displayCheckIg18',
                            'instagram19','tiktok19','youtube19','displayCheckYt19','displayCheckTk19','displayCheckIg19',
                            'instagram20','tiktok20','youtube20','displayCheckYt20','displayCheckTk20','displayCheckIg20',
                            'instagram21','tiktok21','youtube21','displayCheckYt21','displayCheckTk21','displayCheckIg21',
                            'instagram22','tiktok22','youtube22','displayCheckYt22','displayCheckTk22','displayCheckIg22',
                            'instagram23','tiktok23','youtube23','displayCheckYt23','displayCheckTk23','displayCheckIg23',
                            'instagram24','tiktok24','youtube24','displayCheckYt24','displayCheckTk24','displayCheckIg24',
                            'instagram25','tiktok25','youtube25','displayCheckYt25','displayCheckTk25','displayCheckIg25',
                            'instagram26','tiktok26','youtube26','displayCheckYt26','displayCheckTk26','displayCheckIg26',
                            'instagram27','tiktok27','youtube27','displayCheckYt27','displayCheckTk27','displayCheckIg27',
                            'instagram28','tiktok28','youtube28','displayCheckYt28','displayCheckTk28','displayCheckIg28',
                            'instagram29','tiktok29','youtube29','displayCheckYt29','displayCheckTk29','displayCheckIg29',
                            'instagram30','tiktok30','youtube30','displayCheckYt30','displayCheckTk30','displayCheckIg30',
                            'instagram31','tiktok31','youtube31','displayCheckYt31','displayCheckTk31','displayCheckIg31'));
                        }




                    public function contentplanningday($tanggal)
                    {

                            $activeuser = auth()->user()->id;

                            $userdivisi = DB::table('users')
                                            ->where('id', $activeuser)
                                            ->first('divisi');

                            $manincharge = DB::table('users')
                                            ->where('divisi', $userdivisi->divisi)
                                            ->get();

                            $purposelist = DB::table('data_task')
                                            ->where('media', "instagram")
                                            ->orwhere('media', "tiktok")
                                            ->orwhere('media', "youtube")
                                            ->get();

                            $countpurpose = $purposelist->count();

                            $selectedDate = $tanggal;
                            $hariini = $tanggal;
                                
                            $bulan = date('m', strtotime($tanggal));
                            $tahun = date('Y', strtotime($tanggal));

                            if($tanggal == null){
                                $selectedBulan = date('m', strtotime($tanggal));
                            }else{
                                $selectedBulan = date('m', strtotime($tanggal));
                            };
                            if($tanggal == null){
                                $selectedTahun = date('Y', strtotime($tanggal));
                            }else{
                                $selectedTahun = date('Y', strtotime($tanggal));
                            };

                            $detailtaskIg = DB::table('data_task')
                                            ->where('tanggal_mulai', $selectedDate)
                                            ->where('media', "instagram")
                                            ->get();

                            $igtaskcount = DB::table('data_task')
                                            ->where('tanggal_mulai', $selectedDate)
                                            ->where('media', "instagram")
                                            ->count('media');

                            $detailtaskyt = DB::table('data_task')
                                            ->where('tanggal_mulai', $selectedDate)
                                            ->where('media', "youtube")
                                            ->get();

                            $yttaskcount = DB::table('data_task')
                                            ->where('tanggal_mulai', $selectedDate)
                                            ->where('media', "youtube")
                                            ->count('media');

                            $detailtasktk = DB::table('data_task')
                                            ->where('tanggal_mulai', $selectedDate)
                                            ->where('media', "tiktok")
                                            ->get();

                            $tktaskcount = DB::table('data_task')
                                            ->where('tanggal_mulai', $selectedDate)
                                            ->where('media', "tiktok")
                                            ->count('media');

                                            // tanggal 1
                                            $instagram1 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 1)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg1 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 1)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg1->publish_date) == null){
                                                $displayCheckIg1 = "x-square";
                                            }else{
                                                $displayCheckIg1 = "check-square";
                                            }
                            // dd($checkIg1);
                
                                            $tiktok1 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 1)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk1 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 1)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk1->publish_date) == null){
                                                $displayCheckTk1 = "x-square";
                                            }else{
                                                $displayCheckTk1 = "check-square";
                                            }
                
                                            $youtube1 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 1)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt1 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 1)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt1->publish_date) == null){
                                                $displayCheckYt1 = "x-square";
                                            }else{
                                                $displayCheckYt1 = "check-square";
                                            }
                
                                            // tanggal 2
                                            $instagram2 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 2)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg2 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 2)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg2->publish_date) == null){
                                                $displayCheckIg2 = "x-square";
                                            }else{
                                                $displayCheckIg2 = "check-square";
                                            }
                
                                            $tiktok2 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 2)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk2 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 2)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk2->publish_date) == null){
                                                $displayCheckTk2 = "x-square";
                                            }else{
                                                $displayCheckTk2 = "check-square";
                                            }
                
                                            $youtube2 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 2)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt2 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 2)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt2->publish_date) == null){
                                                $displayCheckYt2 = "x-square";
                                            }else{
                                                $displayCheckYt2 = "check-square";
                                            }
                
                                            // tanggal 3
                                            $instagram3 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 3)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg3 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 3)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg3->publish_date) == null){
                                                $displayCheckIg3 = "x-square";
                                            }else{
                                                $displayCheckIg3 = "check-square";
                                            }
                
                                            $tiktok3 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 3)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk3 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 3)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk3->publish_date) == null){
                                                $displayCheckTk3 = "x-square";
                                            }else{
                                                $displayCheckTk3 = "check-square";
                                            }
                
                                            $youtube3 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 3)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt3 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 3)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt3->publish_date) == null){
                                                $displayCheckYt3 = "x-square";
                                            }else{
                                                $displayCheckYt3 = "check-square";
                                            }
                
                                            // tanggal 4
                                            $instagram4 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 4)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg4 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 4)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg4->publish_date) == null){
                                                $displayCheckIg4 = "x-square";
                                            }else{
                                                $displayCheckIg4 = "check-square";
                                            }
                
                                            $tiktok4 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 4)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk4 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 4)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk4->publish_date) == null){
                                                $displayCheckTk4 = "x-square";
                                            }else{
                                                $displayCheckTk4 = "check-square";
                                            }
                
                                            $youtube4 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 4)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt4 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 4)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt4->publish_date) == null){
                                                $displayCheckYt4 = "x-square";
                                            }else{
                                                $displayCheckYt4 = "check-square";
                                            }
                
                                            // tanggal 5
                                            $instagram5 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 5)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg5 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 5)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg5->publish_date) == null){
                                                $displayCheckIg5 = "x-square";
                                            }else{
                                                $displayCheckIg5 = "check-square";
                                            }
                
                                            $tiktok5 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 5)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk5 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 5)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk5->publish_date) == null){
                                                $displayCheckTk5 = "x-square";
                                            }else{
                                                $displayCheckTk5 = "check-square";
                                            }
                
                                            $youtube5 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 5)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt5 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 5)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt5->publish_date) == null){
                                                $displayCheckYt5 = "x-square";
                                            }else{
                                                $displayCheckYt5 = "check-square";
                                            }
                
                                            // tanggal 6
                                            $instagram6 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 6)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg6 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 6)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg6->publish_date) == null){
                                                $displayCheckIg6 = "x-square";
                                            }else{
                                                $displayCheckIg6 = "check-square";
                                            }
                
                                            $tiktok6 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 6)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk6 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 6)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk6->publish_date) == null){
                                                $displayCheckTk6 = "x-square";
                                            }else{
                                                $displayCheckTk6 = "check-square";
                                            }
                
                                            $youtube6 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 6)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt6 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 6)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt6->publish_date) == null){
                                                $displayCheckYt6 = "x-square";
                                            }else{
                                                $displayCheckYt6 = "check-square";
                                            }
                
                                            // tanggal 7
                                            $instagram7 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 7)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg7 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 7)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg7->publish_date) == null){
                                                $displayCheckIg7 = "x-square";
                                            }else{
                                                $displayCheckIg7 = "check-square";
                                            }
                
                                            $tiktok7 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 7)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk7 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 7)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk7->publish_date) == null){
                                                $displayCheckTk7 = "x-square";
                                            }else{
                                                $displayCheckTk7 = "check-square";
                                            }
                
                                            $youtube7 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 7)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt7 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 7)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt7->publish_date) == null){
                                                $displayCheckYt7 = "x-square";
                                            }else{
                                                $displayCheckYt7 = "check-square";
                                            }
                
                                            // tanggal 8
                                            $instagram8 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 8)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg8 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 8)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg8->publish_date) == null){
                                                $displayCheckIg8 = "x-square";
                                            }else{
                                                $displayCheckIg8 = "check-square";
                                            }
                
                                            $tiktok8 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 8)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk8 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 8)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk8->publish_date) == null){
                                                $displayCheckTk8 = "x-square";
                                            }else{
                                                $displayCheckTk8 = "check-square";
                                            }
                
                                            $youtube8 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 8)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt8 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 8)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt8->publish_date) == null){
                                                $displayCheckYt8 = "x-square";
                                            }else{
                                                $displayCheckYt8 = "check-square";
                                            }
                
                                            // tanggal 9
                                            $instagram9 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 9)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg9 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 9)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg9->publish_date) == null){
                                                $displayCheckIg9 = "x-square";
                                            }else{
                                                $displayCheckIg9 = "check-square";
                                            }
                
                                            $tiktok9 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 9)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk9 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 9)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk9->publish_date) == null){
                                                $displayCheckTk9 = "x-square";
                                            }else{
                                                $displayCheckTk9 = "check-square";
                                            }
                
                                            $youtube9 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 9)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt9 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 9)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt9->publish_date) == null){
                                                $displayCheckYt9 = "x-square";
                                            }else{
                                                $displayCheckYt9 = "check-square";
                                            }
                
                                            // tanggal 10
                                            $instagram10 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 10)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg10 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 10)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg10->publish_date) == null){
                                                $displayCheckIg10 = "x-square";
                                            }else{
                                                $displayCheckIg10 = "check-square";
                                            }
                
                                            $tiktok10 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 10)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk10 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 10)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk10->publish_date) == null){
                                                $displayCheckTk10 = "x-square";
                                            }else{
                                                $displayCheckTk10 = "check-square";
                                            }
                
                                            $youtube10 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 10)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt10 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 10)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt10->publish_date) == null){
                                                $displayCheckYt10 = "x-square";
                                            }else{
                                                $displayCheckYt10 = "check-square";
                                            }
                
                                            // tanggal 11
                                            $instagram11 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 11)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg11 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 11)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg11->publish_date) == null){
                                                $displayCheckIg11 = "x-square";
                                            }else{
                                                $displayCheckIg11 = "check-square";
                                            }
                
                                            $tiktok11 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 11)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk11 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 11)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk11->publish_date) == null){
                                                $displayCheckTk11 = "x-square";
                                            }else{
                                                $displayCheckTk11 = "check-square";
                                            }
                
                                            $youtube11 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 11)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt11 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 11)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt11->publish_date) == null){
                                                $displayCheckYt11 = "x-square";
                                            }else{
                                                $displayCheckYt11 = "check-square";
                                            }
                
                                            // tanggal 12
                                            $instagram12 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 12)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg12 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 12)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg12->publish_date) == null){
                                                $displayCheckIg12 = "x-square";
                                            }else{
                                                $displayCheckIg12 = "check-square";
                                            }
                
                                            $tiktok12 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 12)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk12 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 12)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk12->publish_date) == null){
                                                $displayCheckTk12 = "x-square";
                                            }else{
                                                $displayCheckTk12 = "check-square";
                                            }
                
                                            $youtube12 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 12)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt12 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 12)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt12->publish_date) == null){
                                                $displayCheckYt12 = "x-square";
                                            }else{
                                                $displayCheckYt12 = "check-square";
                                            }
                
                                            // tanggal 13
                                            $instagram13 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 13)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg13 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 13)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg13->publish_date) == null){
                                                $displayCheckIg13 = "x-square";
                                            }else{
                                                $displayCheckIg13 = "check-square";
                                            }
                
                                            $tiktok13 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 13)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk13 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 13)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk13->publish_date) == null){
                                                $displayCheckTk13 = "x-square";
                                            }else{
                                                $displayCheckTk13 = "check-square";
                                            }
                
                                            $youtube13 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 13)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt13 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 13)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt13->publish_date) == null){
                                                $displayCheckYt13 = "x-square";
                                            }else{
                                                $displayCheckYt13 = "check-square";
                                            }
                
                                            // tanggal 14
                                            $instagram14 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 14)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg14 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 14)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg14->publish_date) == null){
                                                $displayCheckIg14 = "x-square";
                                            }else{
                                                $displayCheckIg14 = "check-square";
                                            }
                
                                            $tiktok14 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 14)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk14 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 14)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk14->publish_date) == null){
                                                $displayCheckTk14 = "x-square";
                                            }else{
                                                $displayCheckTk14 = "check-square";
                                            }
                
                                            $youtube14 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 14)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt14 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 14)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt14->publish_date) == null){
                                                $displayCheckYt14 = "x-square";
                                            }else{
                                                $displayCheckYt14 = "check-square";
                                            }
                
                                            // tanggal 15
                                            $instagram15 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 15)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg15 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 15)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg15->publish_date) == null){
                                                $displayCheckIg15 = "x-square";
                                            }else{
                                                $displayCheckIg15 = "check-square";
                                            }
                
                                            $tiktok15 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 15)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk15 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 15)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk15->publish_date) == null){
                                                $displayCheckTk15 = "x-square";
                                            }else{
                                                $displayCheckTk15 = "check-square";
                                            }
                
                                            $youtube15 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 15)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt15 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 15)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt15->publish_date) == null){
                                                $displayCheckYt15 = "x-square";
                                            }else{
                                                $displayCheckYt15 = "check-square";
                                            }
                
                                            // tanggal 16
                                            $instagram16 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 16)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg16 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 16)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg16->publish_date) == null){
                                                $displayCheckIg16 = "x-square";
                                            }else{
                                                $displayCheckIg16 = "check-square";
                                            }
                
                                            $tiktok16 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 16)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk16 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 16)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk16->publish_date) == null){
                                                $displayCheckTk16 = "x-square";
                                            }else{
                                                $displayCheckTk16 = "check-square";
                                            }
                
                                            $youtube16 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 16)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt16 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 16)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt16->publish_date) == null){
                                                $displayCheckYt16 = "x-square";
                                            }else{
                                                $displayCheckYt16 = "check-square";
                                            }
                
                                            // tanggal 17
                                            $instagram17 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 17)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg17 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 17)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg17->publish_date) == null){
                                                $displayCheckIg17 = "x-square";
                                            }else{
                                                $displayCheckIg17 = "check-square";
                                            }
                
                                            $tiktok17 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 17)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk17 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 17)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk17->publish_date) == null){
                                                $displayCheckTk17 = "x-square";
                                            }else{
                                                $displayCheckTk17 = "check-square";
                                            }
                
                                            $youtube17 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 17)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt17 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 17)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt17->publish_date) == null){
                                                $displayCheckYt17 = "x-square";
                                            }else{
                                                $displayCheckYt17 = "check-square";
                                            }
                
                                            // tanggal 18
                                            $instagram18 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 18)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg18 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 18)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg18->publish_date) == null){
                                                $displayCheckIg18 = "x-square";
                                            }else{
                                                $displayCheckIg18 = "check-square";
                                            }
                
                                            $tiktok18 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 18)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk18 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 18)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk18->publish_date) == null){
                                                $displayCheckTk18 = "x-square";
                                            }else{
                                                $displayCheckTk18 = "check-square";
                                            }
                
                                            $youtube18 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 18)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt18 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 18)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt18->publish_date) == null){
                                                $displayCheckYt18 = "x-square";
                                            }else{
                                                $displayCheckYt18 = "check-square";
                                            }
                
                                            // tanggal 19
                                            $instagram19 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 19)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg19 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 19)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg19->publish_date) == null){
                                                $displayCheckIg19 = "x-square";
                                            }else{
                                                $displayCheckIg19 = "check-square";
                                            }
                
                                            $tiktok19 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 19)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk19 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 19)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk19->publish_date) == null){
                                                $displayCheckTk19 = "x-square";
                                            }else{
                                                $displayCheckTk19 = "check-square";
                                            }
                
                                            $youtube19 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 19)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt19 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 19)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt19->publish_date) == null){
                                                $displayCheckYt19 = "x-square";
                                            }else{
                                                $displayCheckYt19 = "check-square";
                                            }
                
                                            // tanggal 20
                                            $instagram20 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 20)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg20 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 20)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg20->publish_date) == null){
                                                $displayCheckIg20 = "x-square";
                                            }else{
                                                $displayCheckIg20 = "check-square";
                                            }
                
                                            $tiktok20 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 20)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk20 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 20)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk20->publish_date) == null){
                                                $displayCheckTk20 = "x-square";
                                            }else{
                                                $displayCheckTk20 = "check-square";
                                            }
                
                                            $youtube20 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 20)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt20 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 20)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt20->publish_date) == null){
                                                $displayCheckYt20 = "x-square";
                                            }else{
                                                $displayCheckYt20 = "check-square";
                                            }
                
                                            // tanggal 21
                                            $instagram21 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 21)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg21 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 21)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg21->publish_date) == null){
                                                $displayCheckIg21 = "x-square";
                                            }else{
                                                $displayCheckIg21 = "check-square";
                                            }
                
                                            $tiktok21 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 21)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk21 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 21)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk21->publish_date) == null){
                                                $displayCheckTk21 = "x-square";
                                            }else{
                                                $displayCheckTk21 = "check-square";
                                            }
                
                                            $youtube21 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 21)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt21 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 21)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt21->publish_date) == null){
                                                $displayCheckYt21 = "x-square";
                                            }else{
                                                $displayCheckYt21 = "check-square";
                                            }
                
                                            // tanggal 22
                                            $instagram22 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 22)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg22 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 22)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg22->publish_date) == null){
                                                $displayCheckIg22 = "x-square";
                                            }else{
                                                $displayCheckIg22 = "check-square";
                                            }
                
                                            $tiktok22 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 22)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk22 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 22)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk22->publish_date) == null){
                                                $displayCheckTk22 = "x-square";
                                            }else{
                                                $displayCheckTk22 = "check-square";
                                            }
                
                                            $youtube22 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 22)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt22 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 22)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt22->publish_date) == null){
                                                $displayCheckYt22 = "x-square";
                                            }else{
                                                $displayCheckYt22 = "check-square";
                                            }
                
                                            // tanggal 23
                                            $instagram23 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 23)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg23 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 23)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg23->publish_date) == null){
                                                $displayCheckIg23 = "x-square";
                                            }else{
                                                $displayCheckIg23 = "check-square";
                                            }
                
                                            $tiktok23 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 23)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk23 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 23)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk23->publish_date) == null){
                                                $displayCheckTk23 = "x-square";
                                            }else{
                                                $displayCheckTk23 = "check-square";
                                            }
                
                                            $youtube23 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 23)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt23 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 23)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt23->publish_date) == null){
                                                $displayCheckYt23 = "x-square";
                                            }else{
                                                $displayCheckYt23 = "check-square";
                                            }
                
                                            // tanggal 24
                                            $instagram24 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 24)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg24 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 24)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg24->publish_date) == null){
                                                $displayCheckIg24 = "x-square";
                                            }else{
                                                $displayCheckIg24 = "check-square";
                                            }
                
                                            $tiktok24 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 24)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk24 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 24)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk24->publish_date) == null){
                                                $displayCheckTk24 = "x-square";
                                            }else{
                                                $displayCheckTk24 = "check-square";
                                            }
                
                                            $youtube24 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 24)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt24 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 24)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt24->publish_date) == null){
                                                $displayCheckYt24 = "x-square";
                                            }else{
                                                $displayCheckYt24 = "check-square";
                                            }
                
                                            // tanggal 25
                                            $instagram25 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 25)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg25 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 25)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg25->publish_date) == null){
                                                $displayCheckIg25 = "x-square";
                                            }else{
                                                $displayCheckIg25 = "check-square";
                                            }
                
                                            $tiktok25 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 25)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk25 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 25)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk25->publish_date) == null){
                                                $displayCheckTk25 = "x-square";
                                            }else{
                                                $displayCheckTk25 = "check-square";
                                            }
                
                                            $youtube25 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 25)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt25 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 25)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt25->publish_date) == null){
                                                $displayCheckYt25 = "x-square";
                                            }else{
                                                $displayCheckYt25 = "check-square";
                                            }
                
                                            // tanggal 26
                                            $instagram26 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 26)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg26 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 26)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg26->publish_date) == null){
                                                $displayCheckIg26 = "x-square";
                                            }else{
                                                $displayCheckIg26 = "check-square";
                                            }
                
                                            $tiktok26 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 26)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk26 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 26)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk26->publish_date) == null){
                                                $displayCheckTk26 = "x-square";
                                            }else{
                                                $displayCheckTk26 = "check-square";
                                            }
                
                                            $youtube26 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 26)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt26 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 26)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt26->publish_date) == null){
                                                $displayCheckYt26 = "x-square";
                                            }else{
                                                $displayCheckYt26 = "check-square";
                                            }
                
                                            // tanggal 27
                                            $instagram27 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 27)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg27 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 27)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg27->publish_date) == null){
                                                $displayCheckIg27 = "x-square";
                                            }else{
                                                $displayCheckIg27 = "check-square";
                                            }
                
                                            $tiktok27 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 27)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk27 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 27)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk27->publish_date) == null){
                                                $displayCheckTk27 = "x-square";
                                            }else{
                                                $displayCheckTk27 = "check-square";
                                            }
                
                                            $youtube27 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 27)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt27 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 27)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt27->publish_date) == null){
                                                $displayCheckYt27 = "x-square";
                                            }else{
                                                $displayCheckYt27 = "check-square";
                                            }
                
                                            // tanggal 28
                                            $instagram28 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 28)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg28 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 28)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg28->publish_date) == null){
                                                $displayCheckIg28 = "x-square";
                                            }else{
                                                $displayCheckIg28 = "check-square";
                                            }
                
                                            $tiktok28 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 28)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk28 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 28)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk28->publish_date) == null){
                                                $displayCheckTk28 = "x-square";
                                            }else{
                                                $displayCheckTk28 = "check-square";
                                            }
                
                                            $youtube28 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 28)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt28 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 28)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt28->publish_date) == null){
                                                $displayCheckYt28 = "x-square";
                                            }else{
                                                $displayCheckYt28 = "check-square";
                                            }
                
                                            // tanggal 29
                                            $instagram29 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 29)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg29 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 29)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg29->publish_date) == null){
                                                $displayCheckIg29 = "x-square";
                                            }else{
                                                $displayCheckIg29 = "check-square";
                                            }
                
                                            $tiktok29 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 29)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk29 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 29)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk29->publish_date) == null){
                                                $displayCheckTk29 = "x-square";
                                            }else{
                                                $displayCheckTk29 = "check-square";
                                            }
                
                                            $youtube29 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 29)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt29 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 29)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt29->publish_date) == null){
                                                $displayCheckYt29 = "x-square";
                                            }else{
                                                $displayCheckYt29 = "check-square";
                                            }
                
                                            // tanggal 30
                                            $instagram30 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 30)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg30 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 30)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg30->publish_date) == null){
                                                $displayCheckIg30 = "x-square";
                                            }else{
                                                $displayCheckIg30 = "check-square";
                                            }
                
                                            $tiktok30 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 30)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk30 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 30)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk30->publish_date) == null){
                                                $displayCheckTk30 = "x-square";
                                            }else{
                                                $displayCheckTk30 = "check-square";
                                            }
                
                                            $youtube30 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 30)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt30 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 30)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt30->publish_date) == null){
                                                $displayCheckYt30 = "x-square";
                                            }else{
                                                $displayCheckYt30 = "check-square";
                                            }
                
                                            // tanggal 31
                                            $instagram31 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 31)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('media');
                
                                            $checkIg31 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 31)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "instagram")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkIg31->publish_date) == null){
                                                $displayCheckIg31 = "x-square";
                                            }else{
                                                $displayCheckIg31 = "check-square";
                                            }
                
                                            $tiktok31 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 31)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('media');
                
                                            $checkTk31 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 31)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "tiktok")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkTk31->publish_date) == null){
                                                $displayCheckTk31 = "x-square";
                                            }else{
                                                $displayCheckTk31 = "check-square";
                                            }
                
                                            $youtube31 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 31)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('media');
                
                                            $checkYt31 = DB::table('data_task')
                                                            ->whereDay('tanggal_mulai', 31)
                                                            ->whereMonth('tanggal_mulai', $bulan)
                                                            ->whereYear('tanggal_mulai', $tahun)
                                                            ->where('media', "youtube")
                                                            ->first('publish_date');
                                            
                                            if(isset($checkYt31->publish_date) == null){
                                                $displayCheckYt31 = "x-square";
                                            }else{
                                                $displayCheckYt31 = "check-square";
                                            }
                
                
                
                                            return view('content.contentplanning', compact('countpurpose','purposelist','manincharge',
                                            'hariini','igtaskcount','yttaskcount','tktaskcount',
                                            'detailtaskIg','detailtasktk','detailtaskyt','selectedDate','bulan','tahun','selectedBulan','selectedTahun',
                                            'instagram1','tiktok1','youtube1','displayCheckYt1','displayCheckTk1','displayCheckIg1',
                                            'instagram2','tiktok2','youtube2','displayCheckYt2','displayCheckTk2','displayCheckIg2',
                                            'instagram3','tiktok3','youtube3','displayCheckYt3','displayCheckTk3','displayCheckIg3',
                                            'instagram4','tiktok4','youtube4','displayCheckYt4','displayCheckTk4','displayCheckIg4',
                                            'instagram5','tiktok5','youtube5','displayCheckYt5','displayCheckTk5','displayCheckIg5',
                                            'instagram6','tiktok6','youtube6','displayCheckYt6','displayCheckTk6','displayCheckIg6',
                                            'instagram7','tiktok7','youtube7','displayCheckYt7','displayCheckTk7','displayCheckIg7',
                                            'instagram8','tiktok8','youtube8','displayCheckYt8','displayCheckTk8','displayCheckIg8',
                                            'instagram9','tiktok9','youtube9','displayCheckYt9','displayCheckTk9','displayCheckIg9',
                                            'instagram10','tiktok10','youtube10','displayCheckYt10','displayCheckTk10','displayCheckIg10',
                                            'instagram11','tiktok11','youtube11','displayCheckYt11','displayCheckTk11','displayCheckIg11',
                                            'instagram12','tiktok12','youtube12','displayCheckYt12','displayCheckTk12','displayCheckIg12',
                                            'instagram13','tiktok13','youtube13','displayCheckYt13','displayCheckTk13','displayCheckIg13',
                                            'instagram14','tiktok14','youtube14','displayCheckYt14','displayCheckTk14','displayCheckIg14',
                                            'instagram15','tiktok15','youtube15','displayCheckYt15','displayCheckTk15','displayCheckIg15',
                                            'instagram16','tiktok16','youtube16','displayCheckYt16','displayCheckTk16','displayCheckIg16',
                                            'instagram17','tiktok17','youtube17','displayCheckYt17','displayCheckTk17','displayCheckIg17',
                                            'instagram18','tiktok18','youtube18','displayCheckYt18','displayCheckTk18','displayCheckIg18',
                                            'instagram19','tiktok19','youtube19','displayCheckYt19','displayCheckTk19','displayCheckIg19',
                                            'instagram20','tiktok20','youtube20','displayCheckYt20','displayCheckTk20','displayCheckIg20',
                                            'instagram21','tiktok21','youtube21','displayCheckYt21','displayCheckTk21','displayCheckIg21',
                                            'instagram22','tiktok22','youtube22','displayCheckYt22','displayCheckTk22','displayCheckIg22',
                                            'instagram23','tiktok23','youtube23','displayCheckYt23','displayCheckTk23','displayCheckIg23',
                                            'instagram24','tiktok24','youtube24','displayCheckYt24','displayCheckTk24','displayCheckIg24',
                                            'instagram25','tiktok25','youtube25','displayCheckYt25','displayCheckTk25','displayCheckIg25',
                                            'instagram26','tiktok26','youtube26','displayCheckYt26','displayCheckTk26','displayCheckIg26',
                                            'instagram27','tiktok27','youtube27','displayCheckYt27','displayCheckTk27','displayCheckIg27',
                                            'instagram28','tiktok28','youtube28','displayCheckYt28','displayCheckTk28','displayCheckIg28',
                                            'instagram29','tiktok29','youtube29','displayCheckYt29','displayCheckTk29','displayCheckIg29',
                                            'instagram30','tiktok30','youtube30','displayCheckYt30','displayCheckTk30','displayCheckIg30',
                                            'instagram31','tiktok31','youtube31','displayCheckYt31','displayCheckTk31','displayCheckIg31'));
                                        }


                            public function taskkontenbaru(request $request)
                            {

                                if($request->file == null){

                                    $hariIni = date('Y-m-d');
                                    $time = Carbon::now();

                                    $activeuser = auth()->user()->id;

                                    $tgl_mulai = date('Y-m-d', strtotime($request->tanggal_mulai));

                                    $jam_mulai = date('H:i:s', strtotime($request->tanggal_mulai));

                                    $tgl_selesai = date('Y-m-d', strtotime($request->tanggal_selesai));

                                    $jam_selesai = date('H:i:s', strtotime($request->tanggal_selesai));


                                    if($tgl_selesai < $tgl_mulai){
                                        return back()->with('taskGagal1', 'gagal');
                                    }if($tgl_selesai < $hariIni){
                                        return back()->with('taskGagal2', 'gagal');
                                    }else{
                                        DB::table('data_task')->insertGetId([
                                            'judul' => $request->task,
                                            'tanggal_mulai' => $tgl_mulai,
                                            'tanggal_selesai' => $tgl_selesai,
                                            'jam_mulai' => $jam_mulai,
                                            'jam_selesai' => $jam_selesai,
                                            'detail' => $request->detail,
                                            'status' => $request->status,
                                            'media' => $request->media,
                                            'tujuan_konten' => $request->tujuan_konten,
                                            'user' => $request->user,
                                            'mic' => $request->manincharge,
                                        ]);

                                        return back()->with('suksesBuatTask', 'data berhasil di buat');
                                    }
                                }else{
                                    $requestFile = $request->file('file');
                                        foreach($requestFile as $imageData){
                                            $singleSize[] = $imageData->getSize();
                                        };
            
                                        $totalSizeImage = array_sum($singleSize);
            
                                    if( $totalSizeImage >= 50000000){
                
                                        return back()->with('fileTerlaluBesar', 'detail di script');
                                
                                }else{

                                    $hariIni = date('Y-m-d');

                                    $activeuser = auth()->user()->id;

                                    $tgl_mulai = date('Y-m-d', strtotime($request->tanggal_mulai));

                                    $jam_mulai = date('H:i:s', strtotime($request->tanggal_mulai));

                                    $tgl_selesai = date('Y-m-d', strtotime($request->tanggal_selesai));

                                    $jam_selesai = date('H:i:s', strtotime($request->tanggal_selesai));


                                    if($tgl_selesai < $tgl_mulai){
                                        return back()->with('taskGagal1', 'gagal');
                                    }if($tgl_selesai < $hariIni){
                                        return back()->with('taskGagal2', 'gagal');
                                    }else{
                                        $id_task = DB::table('data_task')->insertGetId([
                                            'judul' => $request->task,
                                            'tanggal_mulai' => $tgl_mulai,
                                            'tanggal_selesai' => $tgl_selesai,
                                            'jam_mulai' => $jam_mulai,
                                            'jam_selesai' => $jam_selesai,
                                            'detail' => $request->detail,
                                            'status' => $request->status,
                                            'media' => $request->media,
                                            'tujuan_konten' => $request->tujuan_konten,
                                            'user' => $request->user,
                                            'mic' => $request->manincharge,
                                        ]);

                                        
                                        $time = Carbon::now();
                                        $fileYangDiKirim = $request->file('file');
                                        
                                        // membuat data yang di butuhkan untuk input ke data keuangan
                                        foreach($fileYangDiKirim as $fileBanyak){
                                            $nama_file[] = $time."_".$fileBanyak->getClientOriginalName();
                                                
                                            $fileBanyak->move(public_path('/img'), end($nama_file));
                                            $data[] = [
                                                'id_task' => $id_task,
                                                'file' => $time."_".$fileBanyak->getClientOriginalName(),
                                                'tag' => "refrensi",
                                                ];
                                        };
                                                
                                                        // dd($fileYangDiKirim,$nama_file,$data,$fileBanyak->getClientOriginalName());
                                                        
                                        DB::table('file_task')->insert($data);


                                        return back()->with('suksesBuatTask', 'data berhasil di buat');
                                    }
                                }
                            }

                    }




}
