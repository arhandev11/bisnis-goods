<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\app_settings;
use Illuminate\Support\Arr;


class Todocontroller extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function todolist(request $request)
                    {
                        $BulanIni = date('m');
                        $hariIni = date('Y-m-d');
                        $yesterday = Carbon::yesterday();
                        $dl7 = Carbon::now()->subDays(7);
                        $dl6 = Carbon::now()->subDays(6);
                        $dl5 = Carbon::now()->subDays(5);
                        $dl4 = Carbon::now()->subDays(4);
                        $dl3 = Carbon::now()->subDays(3);
                        $dl2 = Carbon::now()->subDays(2);
                        
                        $kemarin = date('Y-m-d', strtotime($yesterday));
                        $svnDl = date('Y-m-d', strtotime($dl7));
                        $sixDl = date('Y-m-d', strtotime($dl6));
                        $fiveDl = date('Y-m-d', strtotime($dl5));
                        $fourDl = date('Y-m-d', strtotime($dl4));
                        $treeDl = date('Y-m-d', strtotime($dl3));
                        $twoDl = date('Y-m-d', strtotime($dl2));

                        $managerTeam = DB::table('users')
                                        ->where('manager', 1)
                                        ->get();
                                        // dd($managerTeam);

                        if($request->id == null){
                            $activeuser = auth()->user()->id;
                        }else{
                            $activeuser = $request->id;
                        }
                        
                        $selectedManager = DB::table('users')
                                        ->where('id', $activeuser)
                                        ->first('name');

                        $getfilter = $request->filter;

                        $userdivisi = DB::table('users')
                                            ->where('id', $activeuser)
                                            ->first('divisi');

                        $manincharge = DB::table('users')
                                            ->where('divisi', $userdivisi->divisi)
                                            ->get();

                        $teamlist = DB::table('users')
                                            ->where('divisi', $userdivisi->divisi)
                                            ->whereNotIn('id', [$activeuser])
                                            ->get('id');
                        
                        foreach ($teamlist as $index => $d) {
                            $ids[$index] = $d->id; 
                        }


                        for($i = 0; $i < count($teamlist); $i++){
                            $data[] = [
                                'id' => $ids[$i],
                                ];
                            }

                        // dd($userdivisi->divisi, $manincharge, $teamlist, $data);

                        $idTeam = Arr::flatten($data);
                        
                        $task8count = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "Selesai")
                                                        ->where('tanggal_penyelesaian', $svnDl)
                                                        ->count();

                        $task7count = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "Selesai")
                                                        ->where('tanggal_penyelesaian', $sixDl)
                                                        ->count();

                        $task6count = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "Selesai")
                                                        ->where('tanggal_penyelesaian', $fiveDl)
                                                        ->count();

                        $task5count = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "Selesai")
                                                        ->where('tanggal_penyelesaian', $fourDl)
                                                        ->count();

                        $task4count = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "Selesai")
                                                        ->where('tanggal_penyelesaian', $treeDl)
                                                        ->count();

                        $task3count = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "Selesai")
                                                        ->where('tanggal_penyelesaian', $twoDl)
                                                        ->count();

                        $task2count = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "Selesai")
                                                        ->where('tanggal_penyelesaian', $kemarin)
                                                        ->count();

                        $task1count = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "Selesai")
                                                        ->where('tanggal_penyelesaian', $hariIni)
                                                        ->count();

                        $taskWeekcount = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "Selesai")
                                                        ->whereBetween('tanggal_penyelesaian', [$sixDl, $hariIni])
                                                        ->count();

                        $taskMonthcount = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "Selesai")
                                                        ->whereMonth('tanggal_penyelesaian', $BulanIni)
                                                        ->count();

                        $taskNextcount = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('tanggal_mulai', '>', $hariIni)
                                                        ->whereNotIn('status', ["selesai","cancel"])
                                                        ->count();


                        if($task7count > $task8count){
                            $arw7 = "arrow-top";
                        }else{
                            $arw7 = "arrow-bottom";
                        };
                        if($task6count > $task7count){
                            $arw6 = "arrow-top";
                        }else{
                            $arw6 = "arrow-bottom";
                        };
                        if($task5count > $task6count){
                            $arw5 = "arrow-top";
                        }else{
                            $arw5 = "arrow-bottom";
                        };
                        if($task4count > $task5count){
                            $arw4 = "arrow-top";
                        }else{
                            $arw4 = "arrow-bottom";
                        };
                        if($task3count > $task4count){
                            $arw3 = "arrow-top";
                        }else{
                            $arw3 = "arrow-bottom";
                        };
                        if($task2count > $task3count){
                            $arw2 = "arrow-top";
                        }else{
                            $arw2 = "arrow-bottom";
                        };
                        if($task1count > $task2count){
                            $arw1 = "arrow-top";
                        }else{
                            $arw1 = "arrow-bottom";
                        };

                        // dd($task3count,$task2count,$arw3,$arw2);


                        // data category task di sini
                        $taskDeadlineCount = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('tanggal_selesai', '<=', $kemarin)
                                                        ->whereNotIn('status', ["selesai","cancel"])
                                                        ->where('tanggal_penyelesaian', null)
                                                        ->count('status');

                        $selesaiCount = DB::table('data_task')
                                                        ->WhereNotNull('tanggal_penyelesaian')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "selesai")
                                                        ->count('status');

                        $pendingCount = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->whereNotIn('status', ["selesai","on progress","new task","cancel","hariini"])
                                                        ->where('status', "pending")
                                                        ->count('status');

                        $taskHariIniCount = DB::table('data_task')
                                                        ->where('tanggal_kerja', $hariIni)
                                                        ->where('mic', $activeuser)
                                                        ->whereNotIn('status', ["selesai","pending","cancel"])
                                                        ->count('judul');

                        $taskonprogresscount = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('tanggal_mulai', '<=', $hariIni)
                                                        ->where('tanggal_selesai', '>=', $hariIni)
                                                        ->whereNotIn('status', ["selesai","pending","cancel"])
                                                        ->where('tanggal_penyelesaian', null)
                                                        ->count('judul');

                        $cancelcount = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "cancel")
                                                        ->where('tanggal_penyelesaian', null)
                                                        ->count('judul');

                        $Reviewcount = DB::table('data_task')
                                                        ->where('user', $activeuser)
                                                        ->where('status', "review")
                                                        ->count('judul');

                        $ReviewEdcount = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "reviewed")
                                                        ->count('judul');


                        // data yang di tampilkan di list task dini sini
                        if($getfilter == null){

                                    $dataTask = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('tanggal_mulai', '<=', $hariIni)
                                                        ->where('tanggal_selesai', '>=', $hariIni)
                                                        ->whereNotIn('status', ["selesai","pending","cancel"])
                                                        ->where('tanggal_penyelesaian', null)
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();

                                    $teamDataTask = DB::table('data_task')
                                                        ->whereIn('mic', $idTeam)
                                                        ->where('tanggal_mulai', '<=', $hariIni)
                                                        ->where('tanggal_selesai', '>=', $hariIni)
                                                        ->whereNotIn('status', ["selesai","pending","cancel"])
                                                        ->where('tanggal_penyelesaian', null)
                                                        ->join('users', 'users.id', '=', 'data_task.mic')
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();    

                        }if($getfilter == 'hariini'){
                            
                                    $dataTask = DB::table('data_task')
                                                        ->where('tanggal_kerja', $hariIni)
                                                        ->where('mic', $activeuser)
                                                        ->whereNotIn('status', ["selesai","pending","cancel"])
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();    

                                    $teamDataTask = DB::table('data_task')
                                                        ->where('tanggal_kerja', $hariIni)
                                                        ->where('mic', $idTeam)
                                                        ->whereNotIn('status', ["selesai","pending","cancel"])
                                                        ->join('users', 'users.id', '=', 'data_task.mic')
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();    
                        }if($getfilter == 'selesai'){

                                    $dataTask = DB::table('data_task')
                                                        ->WhereNotNull('tanggal_penyelesaian')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "selesai")
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();

                                    $teamDataTask = DB::table('data_task')
                                                        ->WhereNotNull('tanggal_penyelesaian')
                                                        ->where('mic', $idTeam)
                                                        ->where('status', "selesai")
                                                        ->join('users', 'users.id', '=', 'data_task.mic')
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();    
                        }if($getfilter == 'pending'){
                            
                                    $dataTask = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->whereNotIn('status', ["selesai","on progress","new task","cancel","hariini"])
                                                        ->where('status', "pending")
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();

                                    $teamDataTask = DB::table('data_task')
                                                        ->where('mic', $idTeam)
                                                        ->whereNotIn('status', ["selesai","on progress","new task","cancel","hariini"])
                                                        ->where('status', "pending")
                                                        ->join('users', 'users.id', '=', 'data_task.mic')
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();    
                        }if($getfilter == 'deadline'){
                            
                                    $dataTask = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('tanggal_selesai', '<=', $kemarin)
                                                        ->whereNotIn('status', ["selesai","cancel"])
                                                        ->where('tanggal_penyelesaian', null)
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();    
                            
                                    $teamDataTask = DB::table('data_task')
                                                        ->where('mic', $idTeam)
                                                        ->where('tanggal_selesai', '<=', $kemarin)
                                                        ->whereNotIn('status', ["selesai","cancel"])
                                                        ->where('tanggal_penyelesaian', null)
                                                        ->join('users', 'users.id', '=', 'data_task.mic')
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();    
                        }if($getfilter == 'cancel'){
                            
                                    $dataTask = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "cancel")
                                                        ->where('tanggal_penyelesaian', null)
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();
                            
                                    $teamDataTask = DB::table('data_task')
                                                        ->where('mic', $idTeam)
                                                        ->where('status', "cancel")
                                                        ->where('tanggal_penyelesaian', null)
                                                        ->join('users', 'users.id', '=', 'data_task.mic')
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();    
                        }if($getfilter == 'review'){
                            
                            $dataTask = DB::table('data_task')
                                                        ->where('user', $activeuser)
                                                        ->where('status', "review")
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();
                    
                            $teamDataTask = DB::table('data_task')
                                                        ->where('mic', $idTeam)
                                                        ->where('status', "review")
                                                        ->where('tanggal_penyelesaian', null)
                                                        ->join('users', 'users.id', '=', 'data_task.mic')
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();    
                        }if($getfilter == 'reviewed'){
                                    
                            $dataTask = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('status', "reviewed")
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();
                    
                            $teamDataTask = DB::table('data_task')
                                                        ->where('mic', $idTeam)
                                                        ->where('status', "reviewed")
                                                        ->where('tanggal_penyelesaian', null)
                                                        ->join('users', 'users.id', '=', 'data_task.mic')
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();    
                        }if($getfilter == 'selanjutnya'){
                                    
                            $dataTask = DB::table('data_task')
                                                        ->where('mic', $activeuser)
                                                        ->where('tanggal_mulai', '>', $hariIni)
                                                        ->whereNotIn('status', ["selesai","cancel"])
                                                        ->join('users', 'users.id', '=', 'data_task.mic')
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();
                    
                            $teamDataTask = DB::table('data_task')
                                                        ->where('mic', $idTeam)
                                                        ->where('tanggal_mulai', '>', $hariIni)
                                                        ->whereNotIn('status', ["selesai","cancel"])
                                                        ->join('users', 'users.id', '=', 'data_task.mic')
                                                        ->orderBy('tanggal_mulai', 'Asc')
                                                        ->get();

                                                        // dd($dataTask, $teamDataTask);
                        }
                        


                        return view('todo.todolist', compact('selectedManager','managerTeam','taskNextcount','Reviewcount','ReviewEdcount','getfilter','taskWeekcount','taskMonthcount','arw7','arw6','arw5','arw4','arw3','arw2','arw1','task1count','task2count','task3count','task4count','task5count','task6count','task7count','kemarin','hariIni','sixDl','fiveDl','fourDl','treeDl','twoDl','cancelcount','teamDataTask','kemarin','getfilter','taskonprogresscount','hariIni','manincharge','pendingCount','dataTask',
                        'taskHariIniCount','selesaiCount','taskDeadlineCount','activeuser'));
                    }









                    public function detailtodo(request $request)
                    {
                            $id = $request->id;

                            $detailTask = DB::table('data_task')
                                        ->where('id_task', $id)
                                        ->join('users', 'users.id' ,'=', 'data_task.user')
                                        ->get();

                            $detailTeam = DB::table('data_task')
                                        ->where('id_task', $id)
                                        ->join('users', 'users.id' ,'=', 'data_task.mic')
                                        ->first();

                            $FirstTask = DB::table('data_task')
                                        ->where('id_task', $id)
                                        ->join('users', 'users.id' ,'=', 'data_task.user')
                                        ->first();

                            $valTask = DB::table('data_task')
                                        ->where('id_task', $id)
                                        ->join('users', 'users.id' ,'=', 'data_task.user')
                                        ->first('konten_link');
                                        
                            $selecMic = DB::table('users')
                                        ->where('role', $detailTeam->role)
                                        ->get();


                            $activeuserManager = auth()->user()->manager;
                            $activeuserDivisi = auth()->user()->divisi;

                            if($activeuserManager == 1 && $activeuserDivisi == "Konten Kreator" && $FirstTask->status == "Selesai"){
                                if($FirstTask->media == "instagram"){
                                    $btnSocialUpload = "block";
                                };
                                if($FirstTask->media == "tiktok"){
                                    $btnSocialUpload = "block";
                                };
                                if($FirstTask->media == "youtube"){
                                    $btnSocialUpload = "block";
                                };
                                if($FirstTask->media == null){
                                    $btnSocialUpload = "none";
                                };
                            }else{
                                $btnSocialUpload = "none";
                            };

                            $img_ref = DB::table('file_task')
                                        ->where('id_task', $id)
                                        ->where('tag', "refrensi")
                                        ->get();

                            $count_img = DB::table('file_task')
                                        ->where('id_task', $id)
                                        ->where('tag', "refrensi")
                                        ->count('tag');

                            $logdata = DB::table('data_log')
                                        ->where('id_link', $id)
                                        ->join('users', 'users.id' ,'=', 'data_log.id_user')
                                        ->orderBy('id_log', 'Desc')
                                        ->get();

                        
                            return view('todo.detailtodo', compact('selecMic','detailTeam','valTask','btnSocialUpload','logdata','count_img','id','img_ref','detailTask'));
                    }





}
