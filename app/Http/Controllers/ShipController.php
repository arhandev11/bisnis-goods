<?php

namespace App\Http\Controllers;

use App\Utilities\RequestUtilities;
use Illuminate\Http\Request;

class ShipController extends Controller
{
    public function getFirstMap(Request $request)
    {
        if ($request->ajax()) {
            $reqUtilities = new RequestUtilities();
            $results = $reqUtilities->getFirstSearchMap($request->search);
            $data = [
                "message" => "Berhasil mengambil data peta",
                "data" => $results
            ];
            return response()->json($data);
        }
        abort(404);
    }

    public function getSecondMap(Request $request)
    {
        if ($request->ajax()) {

            $reqUtilities = new RequestUtilities();
            $results = $reqUtilities->getSecondSearchMap($request->area_id);
            $data = [
                "message" => "Berhasil mengambil data peta",
                "data" => $results
            ];
            return response()->json($data);
        }
        abort(404);
    }

    public function getCouriers(Request $request)
    {
        if ($request->ajax()) {
            $reqUtilities = new RequestUtilities();
            $results = $reqUtilities->getCouriers();
            $data = [
                "message" => "Berhasil mengambil data kurir",
                "data" => $results
            ];
            return response()->json($data);
        }
        abort(404);
    }

    public function checkRate(Request $request)
    {
        // $request->validate([]);
        $reqUtilities = new RequestUtilities();
        $results = $reqUtilities->checkRate($request);
        $data = [
            "message" => "Berhasil mengambil data kurir",
            "data" => $results
        ];
        return response()->json($data);

        // return $results;
    }
}
