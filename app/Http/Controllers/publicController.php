<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use App\penjualan;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use App\penjualan_batal;
use App\stok_barang;

class publicController extends Controller
{

    public function halamanluar()
    {
        return view('test');
    }


    public function chartpublic(request $request)

    {
        $userActive     = request()->ip();

        $tagihan            = DB::table('invoice')
            ->whereNotIn('tempo', ["lunas", "tempo", "selesai"])
            ->whereNotIn('platform', ["pesanan dibatalkan"])
            ->where('user', $userActive)
            ->sum('tagihan');

        $limitBelanja       = DB::table('list_marketplace')
            ->where('id_user_toko', $userActive)
            ->sum('limit_belanja');



        $tokoutama      = DB::table('users')
            ->where('role', 1)
            ->first('toko');

        $chartStore     = DB::table('chart')
            ->where('user', $userActive)
            ->orderBy('id_barang', 'DESC')
            ->limit('1')
            ->first('no_barang_id');

        $cekchart       = DB::table('chart')
            ->where('user', $userActive)
            ->first('id_barang');

        // dd($chartStore);
        if ($cekchart == null) {
            $codBrngStore   = $tokoutama;
            $clssChngToko   = "";
        } else {
            $codBrngStore   = $chartStore->no_barang_id;
            $clssChngToko   = "disabled";
            $productStore   = DB::table('stok_barang')
                ->where('id', $codBrngStore)
                ->first('tipe');
        };

        // dd($chartStore, $productStore, $codBrngStore);

        $listToko       = DB::table('toko_cabang')
            ->get('nama_cabang');

        if ($request->nama_toko == null) {
            $selectedToko = $tokoutama->toko;
        } else {
            $selectedToko   = $request->nama_toko;
        };

        // dd($selectedToko);



        $pilihBarang = DB::table('stok_barang')
            ->where('tipe', $selectedToko)
            ->where('tampil_public', "tampil")
            ->get();
        // dd($pilihBarang);




        $stok           = DB::table('stok_barang')
            ->where('tipe', 'Reseller')
            ->paginate(5);

        $jual           = DB::table('stok_barang')
            ->orderBy('id', 'desc')
            ->get();

        $code           = DB::table('app_settings')
            ->where('id', 3)
            ->get();

        $chart          = DB::table('chart')
            ->where('user', $userActive)
            ->join('stok_barang', 'chart.id_barang', '=', 'stok_barang.code_barang')
            ->where('tipe', $selectedToko)
            ->select('produk_picture', 'id_barang', 'nama_barang', 'harga_modal', 'harga_jual', 'stok', 'jml_barang', 'id_chart', 'total_harga', 'min_qty', 'agen_price', 'tr1', 'tr2', 'tr3', 'no_barang_id')
            ->get();



        $chartcount          = DB::table('chart')
            ->where('user', $userActive)
            ->join('stok_barang', 'chart.no_barang_id', '=', 'stok_barang.id')
            ->select('id_barang', 'nama_barang', 'harga_modal', 'harga_jual', 'stok', 'jml_barang', 'id_chart', 'total_harga', 'min_qty', 'agen_price', 'tr1', 'tr2', 'tr3', 'no_barang_id')
            ->count('nama_barang');

        $subtotal       = DB::table('chart')->where('user', $userActive)->sum('total_harga');
        $submodal       = DB::table('chart')->where('user', $userActive)->sum('total_modal');
        $totbarang      = DB::table('chart')->where('user', $userActive)->sum('jml_barang');
        $invoice        = DB::table('invoice')->where('user', $userActive)->orderBy('invoice', 'desc')->paginate(10);
        $ofline         = DB::table('invoice')->where('platform', 'ofline')->orderBy('invoice', 'desc')->paginate(5);
        $lunas          = DB::table('invoice')->where('platform', 'lunas')->where('user', $userActive)->orderBy('invoice', 'desc')->paginate(5);
        $hutang         = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->orderBy('id', 'desc')->paginate(10);
        $tagihan        = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->sum('tagihan');
        $jumlahtrx      = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->count('tagihan');



        $vocher             = DB::table('vocher')
            ->where('id_vocher', 1)
            ->first();

        $nilaivocher        = $vocher->potongan_vocher;

        // dd($nilaivocher);

        $listMarketplace = DB::table('list_marketplace')
            ->where('id_user_toko', $userActive)
            ->get();

        if ($chartcount == 0) {
            $btnEmptyChart = "emptychart";
        } else {
            $btnEmptyChart = "";
        };

        $poinmingguan = DB::table('poin_cs')
            ->select(DB::raw('count(*) as poinm, no_hp'))
            ->where('mingguan', NULL)
            ->groupBy('no_hp')
            ->orderBy('poinm', 'Desc')
            ->paginate(10);
        // dd($poinmingguan);


        $poinbulanan = DB::table('poin_cs')
            ->select(DB::raw('count(*) as poinm, no_hp'))
            ->where('bulanan', NULL)
            ->groupBy('no_hp')
            ->orderBy('poinm', 'Desc')
            ->paginate(10);
        // dd($poinmingguan);


        $doorprize = DB::table('poin_cs')
            ->select(DB::raw('count(*) as poinm, no_hp'))
            ->groupBy('no_hp')
            ->orderBy('poinm', 'Desc')
            ->paginate(10);
        // dd($poinmingguan);


        $totalbarangdichart  = DB::table('chart')
            ->where('user', $userActive)
            ->sum('jml_barang');



        return view('chartpublic', compact('nilaivocher', 'totalbarangdichart', 'userActive', 'poinmingguan', 'poinbulanan', 'doorprize', 'listMarketplace', 'jual', 'tagihan', 'jumlahtrx', 'stok', 'code', 'chart', 'subtotal', 'invoice', 'totbarang', 'submodal', 'ofline', 'lunas', 'hutang', 'pilihBarang', 'listToko', 'selectedToko', 'clssChngToko', 'btnEmptyChart'));
    }




    public function publicaddtochart(request $request)
    {
        $item = $request->item;

        $c_barang       = DB::table('stok_barang')
            ->where('id', $item)
            ->first();

        $activeuser     = request()->ip();

        $cariItem       = DB::table('chart')
            ->where('user', $activeuser)
            ->where('id_barang', $c_barang->code_barang)
            ->count();

        $jmlitem        = DB::table('chart')
            ->where('user', $activeuser)
            ->where('id_barang', $c_barang->code_barang)
            ->sum('jml_barang');

        $stok           = DB::table('stok_barang')
            ->where('id', $item)
            ->sum('stok');

        $priceLevel     = $c_barang->harga_jual;



        $min_qty        = DB::table('stok_barang')
            ->where('id', $item)
            ->sum('min_qty');

        $hargaAgen          = DB::table('stok_barang')
            ->where('id', $item)
            ->first('agen_price');

        $harga = $c_barang->harga_jual;


        $modal          = DB::table('stok_barang')
            ->where('id', $item)
            ->sum('harga_modal');
        $jml        = 1;

        $tambahitem = $jmlitem + 1;

        $subtotal1 = $harga * $jml;
        $modal1 = $modal * $jml;
        $subtotal2 = $harga * $tambahitem;
        $modal2 = $modal * $tambahitem;
        $sisastok = $stok - 1;
        // dd($stok, $tambahitem);

        if ($stok == 0) {

            $codebox            = DB::table('stok_barang')
                ->where('id_sachet', $item)
                ->sum('id');

            $currentbox         = DB::table('stok_barang')
                ->where('id', $codebox)
                ->sum('stok');

            $jmlsachetperbox    = DB::table('stok_barang')
                ->where('id_sachet', $item)
                ->sum('qty_sachet');

            $currentsachet      = DB::table('stok_barang')
                ->where('id', $item)
                ->sum('stok');

            $currentchart       = DB::table('chart')
                ->where('id_barang', $c_barang->code_barang)
                ->where('user', $activeuser)
                ->sum('jml_barang');

            //jika stok kosong tapi adalah saachet maka sistem akan mengecek
            //apakah ada item box yang dapat di konfersi menjadi sachet

            if ($tambahitem > $stok) {

                return back()->with('stokProdukKosong', 'Stok barang yang anda tambahkan tidak cukup');
            }
            if ($currentbox > 0) {

                $updatesachet = $currentsachet + $jmlsachetperbox + $currentchart - $jml;

                $updatebox = $currentbox - 1;

                DB::table('stok_barang')->where('id', $item)->update([
                    'stok' => $updatesachet,
                ]);

                DB::table('stok_barang')->where('id', $codebox)->update([
                    'stok' => $updatebox,
                ]);

                $data = [
                    'no_barang_id' => $c_barang->id,
                    'id_barang' => $c_barang->code_barang,
                    'jml_barang' => $jml,
                    'total_harga' => $subtotal1,
                    'total_modal' => $modal1,
                    'user' => $activeuser,
                ];

                DB::table('chart')
                    ->where('user', $activeuser)
                    ->insert($data);

                return back()->with('sukses', 'sukses');
            } else {

                return back()->with('stokProdukKosong', 'Stok barang yang anda tambahkan tidak cukup');
            }
        } elseif ($harga == null) {

            return back()->with('noresellerprice', 'tidak di jual untuk reseller');
        } elseif ($cariItem == 0) {

            if ($tambahitem > $stok) {
                // dd($currentchart);

                return back()->with('stokProdukKosong', 'Stok barang yang anda tambahkan tidak cukup');
            }

            $data = [
                'no_barang_id' => $c_barang->id,
                'id_barang' => $c_barang->code_barang,
                'jml_barang' => $jml,
                'total_harga' => $subtotal1,
                'total_modal' => $modal1,
                'user' => $activeuser,
            ];

            DB::table('chart')
                ->insert($data);

            //untuk chart public tidak mengurangi stok langsung ketika dimasukan ke chart
            //mereka harus upload bukti trf dulu, lalu dikonfirmasi oleh admin baru stok bisa berkurang
            // DB::table('stok_barang')
            //     ->where('id',$item)
            //     ->update([
            //     'stok' => $sisastok,
            // ]);

            // dd($data);

        } elseif ($cariItem >= 0) {

            if ($tambahitem > $stok) {
                // dd($currentchart);

                return back()->with('stokProdukKosong', 'Stok barang yang anda tambahkan tidak cukup');
            }

            DB::table('chart')
                ->where('id_barang', $c_barang->code_barang)
                ->where('user', $activeuser)
                ->update([
                    'jml_barang' => $tambahitem,
                    'total_harga' => $subtotal2,
                    'total_modal' => $modal2,
                    'user' => $activeuser,
                ]);

            //untuk chart public tidak mengurangi stok langsung ketika dimasukan ke chart
            //mereka harus upload bukti trf dulu, lalu dikonfirmasi oleh admin baru stok bisa berkurang
            // stok_barang::where('id',$item)->update([
            //     'stok' => $sisastok,
            // ]);
        };

        return back()->with('berhasiltambahbarang', 'chart tidak boleh kosong');
    }




    public function tambahchartmanualPublic(request $request)
    {

        $item               = $request->id_barang;

        $activeuser         = request()->ip();

        $c_barang           = DB::table('stok_barang')
            ->where('id', $item)
            ->first();

        $jml                = $request->jml;

        $jmlitem            = DB::table('chart')
            ->where('user', $activeuser)
            ->where('id_barang', $c_barang->code_barang)
            ->sum('jml_barang');

        $stok               = DB::table('stok_barang')
            ->where('id', $item)
            ->sum('stok');

        $modal              = DB::table('stok_barang')
            ->where('id', $item)
            ->sum('harga_modal');

        $harga = $c_barang->harga_jual;;

        // dd($harga, $priceLevel->tier, $hargaLevel->tr1, $jml);


        $stokplusItem = $stok + $jmlitem;
        $subtotal1 = $harga * $stokplusItem;
        $subtotal2 = $harga * $jml;
        $submodal1 = $modal * $stokplusItem;
        $submodal2 = $modal * $jml;
        $sisastok = $stok + $jmlitem - $jml;
        $mincon = $jmlitem + $stok - $jml;


        if ($jml > $stok) {

            //harga total modal stok tersedia
            $submodalallstok = $stok * $modal;
            //harga total jual stok tersedia
            $subtotalallstok = $stok * $harga;

            //update chart sejumlah total maksimal, karna permintaan lebih besar dari stok tersedia
            DB::table('chart')
                ->where('id_barang', $c_barang->code_barang)
                ->where('user', $activeuser)
                ->update([
                    'jml_barang' => $stok,
                    'total_harga' => $subtotalallstok,
                    'total_modal' => $submodalallstok,
                ]);

            return back()->with('stokProdukKosong', 'Stok barang yang anda tambahkan tidak cukup');
        } elseif ($jml <= $jmlitem) {

            DB::table('chart')
                ->where('id_barang', $c_barang->code_barang)
                ->where('user', $activeuser)
                ->update([
                    'jml_barang' => $jml,
                    'total_harga' => $subtotal2,
                    'total_modal' => $submodal2,
                ]);

            //untuk chart public tidak mengurangi stok langsung ketika dimasukan ke chart
            //mereka harus upload bukti trf dulu, lalu dikonfirmasi oleh admin baru stok bisa berkurang
            // DB::table('stok_barang')->where('id',$item)->update([
            //     'stok' => $mincon,
            // ]);

            return back()->with('warning', 'chart tidak boleh kosong');
        } elseif ($jml >= $stokplusItem) {

            //mengecek id sachet pada item box
            $ceksachetataubox = DB::table('stok_barang')
                ->where('id', $item)
                ->sum('id_sachet');

            //mengecek apakah sachet nya nol atau tidak, jika nol maka sachet akan di tambah sejumlah sachet dalam satu box jika tersedia box
            if ($ceksachetataubox == 0) {

                $jmlsachetperbox    = DB::table('stok_barang')
                    ->where('id_sachet', $item)
                    ->sum('qty_sachet');

                $codebox            = DB::table('stok_barang')
                    ->where('id_sachet', $item)
                    ->sum('id');

                $currentsachet      = DB::table('stok_barang')
                    ->where('id', $item)
                    ->sum('stok');

                $currentbox         = DB::table('stok_barang')
                    ->where('id', $codebox)
                    ->sum('stok');

                $currentchart       = DB::table('chart')
                    ->where('id_barang', $c_barang->code_barang)
                    ->where('user', $activeuser)
                    ->sum('jml_barang');

                //request sachet dikurang jumlah chart saat ini
                $req_sachet_minus_crnt_chart   = $jml - $currentchart;

                //kalau jumlah request sachet di kurang current sachet saat ini lebih besar dari sachet per box maka harus di conversi
                if ($req_sachet_minus_crnt_chart > $jmlsachetperbox) {

                    return back()->with('permintaansachetkebanyakan', 'detail msg ada di footer');
                } else {
                    //lupa ini buat apa               
                    $updatesachet = $currentsachet + $jmlsachetperbox + $currentchart - $jml;
                }

                //di bawah ini adalah kalau stok box nya sudah nol jadi sachet tidak bisa di tambahkan lagi
                if ($currentbox == 0) {

                    $currentchart = DB::table('chart')
                        ->where('id_barang', $c_barang->code_barang)
                        ->where('user', $activeuser)
                        ->sum('jml_barang');

                    $updatechartmaxsachet = $currentchart + $currentsachet;

                    $subtotal3 = $harga * $updatechartmaxsachet;
                    $submodal3 = $modal * $updatechartmaxsachet;

                    DB::table('chart')
                        ->where('id_barang', $c_barang->code_barang)
                        ->where('user', $activeuser)
                        ->update([
                            'jml_barang' => $updatechartmaxsachet,
                            'total_harga' => $subtotal3,
                            'total_modal' => $submodal3,
                        ]);

                    return back()->with('sachetdanboxmax', 'box sudah kosong jadi tidak bisa di rubah ke sachet');
                };

                DB::table('chart')
                    ->where('id_barang', $c_barang->code_barang)
                    ->where('user', $activeuser)
                    ->update([
                        'jml_barang' => $jml,
                        'total_harga' => $subtotal2,
                        'total_modal' => $submodal2,
                    ]);

                //ini lupa fungsinya gimana
            } elseif ($ceksachetataubox > 0) {

                DB::table('chart')
                    ->where('id_barang', $c_barang->code_barang)
                    ->where('user', $activeuser)
                    ->update([
                        'jml_barang' => $stokplusItem,
                        'total_harga' => $subtotal1,
                        'total_modal' => $submodal1,
                    ]);

                return back()->with('stokmaksimal', 'chart tidak boleh kosong');
            }
        } elseif ($jml <= $stokplusItem) {
            DB::table('chart')
                ->where('id_barang', $c_barang->code_barang)
                ->where('user', $activeuser)
                ->update([
                    'jml_barang' => $jml,
                    'total_harga' => $subtotal2,
                    'total_modal' => $submodal2,
                ]);

            //untuk chart public tidak mengurangi stok langsung ketika dimasukan ke chart
            //mereka harus upload bukti trf dulu, lalu dikonfirmasi oleh admin baru stok bisa berkurang
            // stok_barang::where('id',$item)->update([
            //     'stok' => $sisastok,
            // ]);
            return back()->with('warning', 'chart tidak boleh kosong');
        };

        return back()->with('berhasiltambahbarang', 'chart tidak boleh kosong');
    }






    public function prosespublicchart(Request $request)
    {
        $nama = $request->nama_pembeli;
        $temporesult = "tempo " . $nama;
        $userActive = request()->ip();
        $chartStore     = DB::table('chart')
            ->where('user', $userActive)
            ->orderBy('id_barang', 'DESC')
            ->limit('1')
            ->first('no_barang_id');
        // dd($chartStore);

        $storeName       = DB::table('stok_barang')
            ->where('id', $chartStore->no_barang_id)
            ->first('tipe');
        $toko       = $storeName->tipe;

        $cekexistensiinv  = DB::table('invoice')
            ->count('invoice');

        if ($cekexistensiinv == null) {
            $invoiceno = "Inv000000001";
        } else {
            $inv = DB::table('invoice')->orderBy('id', 'DESC')->first('invoice');
            $urutan = (int) substr($inv->invoice, 3, 13);
            $urutan++;
            $text = "Inv";
            $invoiceno = $text . sprintf("%09s", $urutan);
        }

        $cekinvmarketplace  = DB::table('invoice')
            ->where('expedisi', $request->inv_marketplace)
            ->count('invoice');

        // dd($request->inv_marketplace, $cekinvmarketplace);

        // if($cekinvmarketplace != null){
        //     return back()->with('invexist', 'detail pesan ada di footer');
        // };

        $tipeToko   = DB::table('list_marketplace')
            ->where('nama_marketplace', $request->platform)
            ->first('diatur_marketplace');


        if ($tipeToko == null) {
            $tipeTokoValue = "tidak";
        } else {
            $tipeTokoValue = $tipeToko->diatur_marketplace;
        }

        if ($tipeTokoValue == "ya") {
            $statusPesanan = "dikirim";
        } else {
            $statusPesanan = "new";
        };

        if ($request->inv_marketplace == null) {
            $expedisi   =   $invoiceno;
        } else {
            $expedisi   =   $request->inv_marketplace;
        };

        $totalTagihan = $request->tagihan;

        $cekawalanhp = (int) substr($request->hp_pembeli, 0, 1);
        $cekakhiranhp = (int) substr($request->hp_pembeli, 1, 12);
        if ($cekawalanhp == 0) {
            $nohp = "62" . $cekakhiranhp;
        } else {
            $nohp = $request->hp_pembeli;
        }

        // dd($nohp, $cekakhiranhp);

        $insertedIdInvoice = DB::table('invoice')->insertGetId([
            'invoice' => $invoiceno,
            'tanggal' => $request->tanggal_order,
            'jumlah_barang' => $request->jumlah_barang,
            'tagihan' => $totalTagihan,
            'platform' => $request->platform,
            'tempo' => $request->tanggal_tempo,
            'nama_pembeli' => $request->nama_pembeli,
            'hp_pembeli' => $nohp,
            'total_modal' => $request->total_modal,
            'user' => $userActive,
            'invoice_toko' => $toko,
            'expedisi' => $expedisi,
            'code_booking' => $request->code_booking,
            'alamat' => $request->alamat,
            'tempo' => $statusPesanan,
            'ongkir' => $request->ongkir,
            'diskon' => $request->vocher,
        ]);

        $sisaitem = $request->sisaitem;
        $code = $request->codeItem;
        $idChart = $request->idChart;
        $title = $request->namaItem;
        $harga = $request->hargaSatuan;
        $jumlah = $request->jmlsbt;
        $tanggal = $request->tanggal_orderar;
        $idBarang = $request->idbarang;
        for ($i = 0; $i < count($idChart); $i++) {
            $data[] = [
                'id_invoice' => $insertedIdInvoice,
                'id_barang' => $idBarang[$i],
                'code_barang' => $code[$i],
                'title' => $title[$i],
                'harga' => $harga[$i],
                'jumlah' => $jumlah[$i],
                'tanggal' => $tanggal[$i],
                'toko' => $toko[$i],
            ];
        }

        penjualan::insert($data);

        for ($i = 0; $i < count($idChart); $i++) {
            $data2[] = [
                'id_chart' => $idChart[$i],
            ];
        }

        DB::table('chart')->whereIn('id_chart', $data2)->delete();

        DB::table('invoice_penjualan')->insert([
            'id_invoice' => $insertedIdInvoice,
            'id_penjualan' => $insertedIdInvoice,
        ]);

        $postAkun = "penjualan" . " " . $request->platform . " " . $invoiceno;

        if ($request->platform == "Tempo") {
            //do nothing
        }
        if ($request->platform == "PO") {
            //do nothing
        } else {
            DB::table('keuangan')->insert([
                'tanggal' => $request->tanggal_order,
                'nama_teransaksi' => $postAkun,
                'debit' => $request->tagihan,
                'kredit' => "0",
                'post' => $request->platform,
                'post_akun' => "pendapatan",
            ]);
        }

        return back()->with('berhasilInputOrder', 'detail pesan ada di footer');
    }






    public function publicpayment(request $request)

    {
        $id            = $request->id;
        $idinv = (int) substr($id, 0, 6);
        $invoiceStart = strpos($id, 'Inv');
        $invoiceno = substr($id, $invoiceStart);

        $invoice = DB::table('invoice_penjualan')
            ->where('id_invoice', $idinv)
            ->join('invoice', 'invoice_penjualan.id_invoice', '=', 'invoice.id')
            ->select('ongkir', 'invoice', 'tagihan', 'expedisi', 'code_booking', 'nama_pengirim', 'hp_pengirim', 'nama_pembeli', 'hp_pembeli', 'alamat', 'diskon')
            ->get();
        // dd($invoice, $id);

        $item = DB::table('penjualan')
            ->where('id_invoice', $idinv)
            ->get();

        $noinvinpembayaran = DB::table('pembayaran')
            ->where('status_pembayaran', "new")
            ->where('invoice_pesanan', $invoiceno)
            ->first('invoice_pesanan');

        // dd($id, $idinv, $invoiceno, $noinvinpembayaran);

        $timeline       = DB::table('timeline_order')->where('invoice_page', $idinv)->get();
        $detailInvoice  = DB::table('invoice')->where('id', $idinv)->where('invoice', $invoiceno)->first();
        $userActive     = request()->ip();
        $tagihan        = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->sum('tagihan');
        $totaltrx       = DB::table('invoice')->where('user', $userActive)->sum('tagihan');
        $jumlahtrx      = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->count('tagihan');
        $totaljmltrx    = DB::table('invoice')->where('user', $userActive)->count('tagihan');
        // dd($detailInvoice);

        $totalbarangdichart  = DB::table('chart')
            ->where('user', $userActive)
            ->sum('jml_barang');

        if ($idinv == null) {
            return redirect('chartpublic')->with('tagihanTidakValid', 'tagihan tidak valid');
        }
        if ($noinvinpembayaran != null) {
            return redirect('chartpublic')->with('tagihansudahdibayar', 'tagihan tidak valid');
        }
        if ($invoiceno == null) {
            return redirect('chartpublic')->with('tagihanTidakValid', 'tagihan tidak valid');
        }
        if ($detailInvoice == null) {
            return redirect('chartpublic')->with('tagihanTidakValid', 'tagihan tidak valid');
        };

        return view('publicpayment', compact('totalbarangdichart', 'invoiceno', 'tagihan', 'jumlahtrx', 'totaltrx', 'totaljmltrx', 'invoice', 'item', 'timeline', 'id', 'detailInvoice'));
    }



    public function publicuploasbuktitrf(request $request)
    {
        //proses upload pdf single di mulai dari sini
        $requestFile = $request->file('file');
        $filesize = $request->file('file')->getSize();
        // Start measuring the time
        $start = microtime(true);

        // Your code that may take some time
        // For example, processing a large file
        // Replace this with your actual logic
        sleep(1); // Simulate a long process (70 seconds)

        // End time measurement
        $end = microtime(true);

        // Calculate the time difference
        $loadingTime = $end - $start;

        if ($filesize >= 1000000) {
            return back()->with('fileoversize', 'detail di script');
        }
        if ($loadingTime > 10) {
            return back()->with('fileoversize', 'The file processing took too long. Please try again.');
        } else {
            // menyimpan data file yang diupload ke variabel $file
            $id_task = $request->id;
            $time = Carbon::now();
            $nama_file = $time . "_" . $requestFile->getClientOriginalName();
            // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'img';
            $requestFile->move($tujuan_upload, $nama_file);
            //proses upload pdf berakhir di sini

            $tanggal   = date('Y-m-d');
            $userActive =   request()->ip();
            $invoiceno     = $request->invoice;
            // dd($invoiceno, $loadingTime);

            $tiketLast = DB::table('pembayaran')->max('tiket_pembayaran');
            $urutan = (int) substr($tiketLast, 9, 13);
            $urutan++;
            $text = "LB";
            $hs   = date('His');
            $notiket = $text . $hs . sprintf("%05s", $urutan);

            // dd($tiketLast, $notiket);


            $dataRcbtotal           = DB::table('invoice')
                ->where('invoice', $invoiceno)
                ->first();

            $totaltagihan = $dataRcbtotal->tagihan + $dataRcbtotal->ongkir - $dataRcbtotal->diskon;

            $kirimtagihan           = DB::table('users')
                ->where('role', 1)
                ->first('toko');

            $idTiket = DB::table('pembayaran')->insertGetId([
                'tanggal_pembayaran' => $tanggal,
                'tiket_pembayaran' => $notiket,
                'status_pembayaran' => "new",
                'reseller_id' => $userActive,
                'total_pembayaran' => $totaltagihan,
                'pembayaran_toko' => $kirimtagihan->toko,
                'nama_pembeli' => $dataRcbtotal->nama_pembeli,
                'invoice_pesanan' => $dataRcbtotal->invoice,
            ]);
            //     dd($idTiket);

            DB::table('invoice')->where('invoice', $invoiceno)->update([
                'bayar_id' => $notiket, //perubahan status jadi diproses ada di actcontroller ketika pembayaran di rubah statusnya ke diterima
            ]);


            $data3 = [
                'id_task' => $notiket,
                'file' => $nama_file,
                'tag' => "buktibayartempo",
            ];

            DB::table('file_task')->insert($data3);

            return redirect('chartpublic')->with('berhasilmelakukanpembayaran', 'tagihan tidak valid');
        }
    }




    public function hapuschartpublic(request $request, $id)
    {
        $jmlitem        = DB::table('chart')->where('no_barang_id', $id)->sum('jml_barang');
        $stok           = DB::table('stok_barang')->where('id', $id)->sum('stok');
        //tidak perlu update sisa stok, karna setelah pembayaran baru mengurangi stok untuk public chart
        $sisastok       = $jmlitem + $stok;

        DB::table('chart')->where('no_barang_id', $id)->delete();

        return back()->with('hapusitemchartsukses', 'Chart Berhasil Di Hapus');
    }




    public function produk(request $request)

    {
        $userActive     = request()->ip();


        $tokoutama      = DB::table('users')
            ->where('role', 1)
            ->first('toko');

        $chartStore     = DB::table('chart')
            ->where('user', $userActive)
            ->orderBy('id_barang', 'DESC')
            ->limit('1')
            ->first('no_barang_id');

        $cekchart       = DB::table('chart')
            ->where('user', $userActive)
            ->first('id_barang');

        // dd($chartStore, $productStore, $codBrngStore);

        $listToko       = DB::table('toko_cabang')
            ->get('nama_cabang');

        if ($request->nama_toko == null) {
            $selectedToko = $tokoutama->toko;
        } else {
            $selectedToko   = $request->nama_toko;
        };

        // dd($selectedToko);

        if ($request->caribarang == null) {

            if ($request->category == null) {
                $selectedcategory = "";
                $pilihBarang = DB::table('stok_barang')
                    ->where('tampil_public', "tampil")
                    ->get();
            } else {
                $selectedcategory = $request->category;
                $pilihBarang = DB::table('stok_barang')
                    ->where('tampil_public', "tampil")
                    ->where('category', $selectedcategory)
                    ->get();
            }
        } else {

            if ($request->category == null) {
                $selectedcategory = "";
                $pilihBarang = DB::table('stok_barang')
                    ->where('nama_barang', 'LIKE', '%' . $request->caribarang . '%')
                    ->where('tampil_public', "tampil")
                    ->get();
            } else {
                $selectedcategory = $request->category;
                $pilihBarang = DB::table('stok_barang')
                    ->where('tampil_public', "tampil")
                    ->where('category', $selectedcategory)
                    ->get();
            }
        }

        $listcategory = DB::table('stok_barang')
            ->where('tipe', $selectedToko)
            ->where('tampil_public', "tampil")
            ->groupBy('category')
            ->get();

        $stok           = DB::table('stok_barang')
            ->where('tipe', 'Reseller')
            ->paginate(5);

        $jual           = DB::table('stok_barang')
            ->orderBy('id', 'desc')
            ->get();

        $code           = DB::table('app_settings')
            ->where('id', 3)
            ->get();

        $chart          = DB::table('chart')
            ->where('user', $userActive)
            ->join('stok_barang', 'chart.id_barang', '=', 'stok_barang.code_barang')
            ->where('tipe', $selectedToko)
            ->select('produk_picture', 'id_barang', 'nama_barang', 'harga_modal', 'harga_jual', 'stok', 'jml_barang', 'id_chart', 'total_harga', 'min_qty', 'agen_price', 'tr1', 'tr2', 'tr3', 'no_barang_id')
            ->get();

        $chartcount          = DB::table('chart')
            ->where('user', $userActive)
            ->join('stok_barang', 'chart.no_barang_id', '=', 'stok_barang.id')
            ->select('id_barang', 'nama_barang', 'harga_modal', 'harga_jual', 'stok', 'jml_barang', 'id_chart', 'total_harga', 'min_qty', 'agen_price', 'tr1', 'tr2', 'tr3', 'no_barang_id')
            ->count('nama_barang');

        $totalbarangdichart  = DB::table('chart')
            ->where('user', $userActive)
            ->sum('jml_barang');

        $subtotal       = DB::table('chart')->where('user', $userActive)->sum('total_harga');
        $submodal       = DB::table('chart')->where('user', $userActive)->sum('total_modal');
        $totbarang      = DB::table('chart')->where('user', $userActive)->sum('jml_barang');
        $invoice        = DB::table('invoice')->where('user', $userActive)->orderBy('invoice', 'desc')->paginate(10);
        $ofline         = DB::table('invoice')->where('platform', 'ofline')->orderBy('invoice', 'desc')->paginate(5);
        $lunas          = DB::table('invoice')->where('platform', 'lunas')->where('user', $userActive)->orderBy('invoice', 'desc')->paginate(5);
        $hutang         = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->orderBy('id', 'desc')->paginate(10);
        $tagihan        = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->sum('tagihan');
        $jumlahtrx      = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->count('tagihan');





        return view('produk', compact('selectedcategory', 'listcategory', 'totalbarangdichart', 'userActive', 'jual', 'jumlahtrx', 'stok', 'code', 'chart', 'subtotal', 'invoice', 'totbarang', 'submodal', 'ofline', 'lunas', 'hutang', 'pilihBarang', 'listToko', 'selectedToko'));
    }
}
