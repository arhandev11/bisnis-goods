<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\app_settings;


class Crmcontroller extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboardcrm()
    {
        if(auth()->user()->role == 2){
            return redirect('/shop');
        }else{
        $mdy = date('m/d/Y');
        $day = date('Y-m-d');
        $bulanini = date('m');
        $tahunIni = date('Y');
        $date = Carbon::createFromFormat('m/d/Y', $mdy)->subMonth();
        $lastmont = $date->subMonths(0)->format('m');

        $minimumStok = app_settings::where('id', 4)
                        ->sum('settings_value');

        $stokAlert = DB::table('stok_barang')
                        ->whereBetween('stok', [0, $minimumStok])
                        ->orderBy('stok','asc')
                        ->paginate(10);

        $terlaris = DB::table('penjualan')
                        ->select('title', DB::raw('sum(jumlah) as totalTerjual'),DB::raw('sum(harga*jumlah) as totalHarga'))
                        ->groupBy('title')
                        ->orderBy('totalTerjual', 'desc')
                        ->paginate(10);

        $modalBulanIni = DB::table('invoice')
                        ->whereMonth('tanggal', '=', $bulanini)
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $brutoBulanIni = DB::table('invoice')
                        ->whereMonth('tanggal', '=', $bulanini)
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $marginBulanIni = $brutoBulanIni-$modalBulanIni;

        $modalHariIni = DB::table('invoice')
                        ->where('tanggal', '=', $day)
                        ->sum('total_modal');

        $brutoHariIni = DB::table('invoice')
                        ->where('tanggal', '=', $day)
                        ->sum('tagihan');

        $marginHariIni = $brutoHariIni-$modalHariIni;

        $expensBulanIni = DB::table('keuangan')
                        ->whereMonth('tanggal', '=', $bulanini)
                        ->where('post_akun', "oprasional")
                        ->sum('kredit');

        $expensHariIni = DB::table('keuangan')
                        ->where('tanggal', '=', $day)
                        ->sum('kredit');

        $pesananHariIni = DB::table('penjualan')
                        ->where('tanggal', '=', $day)
                        ->count('id');

        $PesananTahunIni = DB::table('penjualan')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->count('id');

        $omsetTahunan = DB::table('invoice')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $presThisMont = DB::table('invoice')
                        ->whereMonth('tanggal', '=', $bulanini)
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');
                        
                      

        $presLastMont = DB::table('invoice')
                        ->whereMonth('tanggal', '=', $lastmont)
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $presMarginBlnIni = $marginBulanIni / $brutoBulanIni * 100;

        $presExpenseBlnIni = $expensBulanIni / $brutoBulanIni * 100;

        $profitBulanIni = $marginBulanIni - $expensBulanIni;

        $presProfitBlnIni = $profitBulanIni / $brutoBulanIni * 100;

        if($presThisMont == 0){
                $presResult = 0;
        }else{
                $presResult =  ($presThisMont - $presLastMont) / $presThisMont * 100;
        }

        // januari
        $janModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '01')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $janTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '01')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $janexpans = DB::table('keuangan')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '01')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // februari         
        $febModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '02')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $febTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '02')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $febexpans = DB::table('keuangan')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '02')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // maret         
        $marModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '03')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $marTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '03')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $marexpans = DB::table('keuangan')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '03')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // april         
        $aprModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '04')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $aprTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '04')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $aprexpans = DB::table('keuangan')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '04')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // mei         
        $meiModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '05')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $meiTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '05')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $meiexpans = DB::table('keuangan')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '05')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // juni         
        $junModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '06')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $junTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '06')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $junexpans = DB::table('keuangan')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '06')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // juli         
        $julModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '07')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $julTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '07')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $julexpans = DB::table('keuangan')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '07')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // agustus
        $agsModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '08')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $agsTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '08')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $agsexpans = DB::table('keuangan')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '08')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // september
        $sepModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '09')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $sepTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '09')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $sepexpans = DB::table('keuangan')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '09')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // oktober
        $oktModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '10')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $oktTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '10')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $oktexpans = DB::table('keuangan')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '10')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // november
        $novModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '11')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $novTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '11')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $novexpans = DB::table('keuangan')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '11')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');
        // desember
        $desModal = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '12')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('total_modal');

        $desTagihan = DB::table('invoice')
                        ->whereMonth('tanggal', '=', '12')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('tagihan');

        $desexpans = DB::table('keuangan')
                        ->where('post_akun', '=', "oprasional")
                        ->whereMonth('tanggal', '=', '12')
                        ->whereYear('tanggal', '=', $tahunIni)
                        ->sum('kredit');  
                        
                        $userActive = Auth::user()->id;
        $stok = DB::table('stok_barang')->where('tipe','Reseller')->paginate(5);
        $jual = DB::table('stok_barang')->orderBy('id','desc')->get();
        $code = DB::table('app_settings')->where('id',3)->get();
        $chart = DB::table('chart')
                ->where('user', $userActive)
                ->join('stok_barang', 'chart.id_barang', '=', 'stok_barang.code_barang')
                ->select('id_barang','nama_barang','harga_modal','harga_jual','stok','jml_barang','id_chart','total_harga')
                ->get();
        $subtotal = DB::table('chart')->where('user', $userActive)->sum('total_harga');
        $submodal = DB::table('chart')->where('user', $userActive)->sum('total_modal');
        $totbarang = DB::table('chart')->where('user', $userActive)->sum('jml_barang');
        $invoice = DB::table('invoice')->where('user', $userActive)->orderBy('invoice','desc')->paginate(10);
        $ofline = DB::table('invoice')->where('platform', 'ofline')->orderBy('invoice','desc')->paginate(5);
        $lunas = DB::table('invoice')->where('platform', 'lunas')->where('user', $userActive)->orderBy('invoice','desc')->paginate(5);
        $hutang = DB::table('invoice')->where('platform', 'tempo')->where('user', $userActive)->orderBy('invoice','desc')->paginate(10);
        $tagihan = DB::table('invoice')->where('user', $userActive)->sum('tagihan');
        $jumlahtrx = DB::table('invoice')->where('user', $userActive)->count('tagihan');

        if(auth()->user()->role == 1){
            return view('admin.crmdashboard', compact('stokAlert','marginHariIni','brutoHariIni','marginBulanIni','brutoBulanIni','expensBulanIni','pesananHariIni','omsetTahunan','janModal','janTagihan','janexpans','febModal','febTagihan','febexpans','marModal','marTagihan','marexpans','aprModal','aprTagihan','aprexpans','meiModal','meiTagihan','meiexpans','junModal','junTagihan','junexpans','julModal','julTagihan','julexpans','agsTagihan','agsModal','agsexpans','sepModal','sepTagihan','sepexpans','oktModal','oktTagihan','oktexpans','novModal','novTagihan','novexpans','desModal','desTagihan','desexpans','presResult','minimumStok','terlaris','expensHariIni','PesananTahunIni','presMarginBlnIni','presExpenseBlnIni','presProfitBlnIni'));
        }else if(auth()->user()->role == 2){
            return view('admin.penjualan2');
        }else if(auth()->user()->role == 3){
            return view('admin.penjualan', compact('jual','tagihan','jumlahtrx','stok','code','chart','subtotal','invoice','totbarang','submodal','ofline','lunas','hutang'));
        }else if(auth()->user()->role == 4){
            return view('admin.adminHome', compact('stokAlert','marginHariIni','brutoHariIni','marginBulanIni','brutoBulanIni','expensBulanIni','pesananHariIni','omsetTahunan','janModal','janTagihan','janexpans','febModal','febTagihan','febexpans','marModal','marTagihan','marexpans','aprModal','aprTagihan','aprexpans','meiModal','meiTagihan','meiexpans','junModal','junTagihan','junexpans','julModal','julTagihan','julexpans','agsTagihan','agsModal','agsexpans','sepModal','sepTagihan','sepexpans','oktModal','oktTagihan','oktexpans','novModal','novTagihan','novexpans','desModal','desTagihan','desexpans','presResult','minimumStok','terlaris','expensHariIni','PesananTahunIni','presMarginBlnIni','presExpenseBlnIni','presProfitBlnIni'));
        }else {
            return abort(403,'Anda tidak punya akses');
        }
        
        // penutup auth karyawan
        }
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function gagalLogin(){
        Session::flash('gagalLogin','user dan password tidak sesuai');
        return redirect('admin');
    }





}
