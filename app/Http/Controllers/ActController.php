<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\penjualan;
use App\penjualan_batal;
use App\stok_barang;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
class ActController extends Controller
{




    
    
     public function belanjaBarangProsses(Request $request)
    {

    $cbr = DB::table('stok_barang')->max('code_barang');
    $urutan = (int) substr($cbr, 0, 5);
    $urutan++;
    $nmbr = "00";
    $reqcdbrng = $request->code_barang;
    if($reqcdbrng == null){
        $kodeBarang = $nmbr . sprintf("%03s", $urutan);    
    }else{
        $kodeBarang = $reqcdbrng;
    };
    $jumlahStok = DB::table('stok_barang')->where('code_barang',$kodeBarang)->sum('stok');
    $updateStok = $jumlahStok+$request->jumlah;
    $akunKeuangan = $request->akun_keuangan;
    $activeuser = auth()->user()->id;

    $detailBarang   = DB::table('stok_barang')
                        ->where('code_barang', $kodeBarang)
                        ->first();

    $cekStokCabang  = DB::table('stok_barang')
                        ->where('code_barang', $kodeBarang)
                        ->where('tipe', $request->tipe)
                        ->first('code_barang');

                        // dd($cbr, $urutan, $request->code_barang, $reqcdbrng, $request->tipe, $kodeBarang, $cekStokCabang);

    $selectedStore = $request->tipe;
    if ($detailBarang == null) {
        $productSelectedStore = $request->tipe;
    }else{
        $productSelectedStore = $detailBarang->tipe;
    };

    if($productSelectedStore != $selectedStore){
            if($akunKeuangan == "Belanja Tempo"){
        if($cekStokCabang == null){

            DB::table('stok_barang')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $detailBarang->nama_barang,
                'category' => $detailBarang->category,
                'stok' => "0",
                'harga_modal' => $detailBarang->harga_modal,
                'harga_jual' => $detailBarang->harga_jual,
                'tipe' => $selectedStore,
            ]);

            DB::table('stok_tertahan')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $detailBarang->nama_barang,
                'category' => $detailBarang->category,
                'stok' => $request->jumlah,
                'harga_modal' => $detailBarang->harga_modal,
                'harga_jual' => $detailBarang->harga_jual,
                'tipe' => $selectedStore,
            ]);   

            $totalBelanja = $request->jumlah * $request->harga_modal;

        
            DB::table('history_belanja')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $detailBarang->nama_barang,
                'jumlah' => $request->jumlah,
                'harga_modal' => $detailBarang->harga_modal,
                'total_belanja' => $totalBelanja,
                'supplier' => $request->supplier,
                'tgl_belanja' => $request->tanggal,
                'status_belanja' => "Tempo",
                'user' => $activeuser,
            ]);
            // belanja barang inventory dengan pembayaran tempo, berarti dia tidak masuk pengeluaran, barang masuk stok tertahan, status history belanja hutang tempo 
        }elseif($cekStokCabang != null){

            $totalBelanja = $request->jumlah * $detailBarang->harga_modal;

            DB::table('stok_tertahan')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $detailBarang->nama_barang,
                'category' => $detailBarang->category,
                'stok' => $request->jumlah,
                'harga_modal' => $detailBarang->harga_modal,
                'harga_jual' => $detailBarang->harga_jual,
                'tipe' => $selectedStore,
            ]);  
        
            DB::table('history_belanja')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $detailBarang->nama_barang,
                'jumlah' => $request->jumlah,
                'harga_modal' => $detailBarang->harga_modal,
                'total_belanja' => $totalBelanja,
                'supplier' => $request->supplier,
                'tgl_belanja' => $request->tanggal,
                'status_belanja' => "Tempo",
                'user' => $activeuser,
            ]);            
        }
    // belanja barang baru dengan pembayaran cash, masuk stok_barang sebagai barang baru, stok_tertahan, history belanja lunas dan keuangan.  
    }else if($akunKeuangan != "Belanja Tempo"){
        if($cekStokCabang == null){

            DB::table('stok_barang')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $detailBarang->nama_barang,
                'category' => $detailBarang->category,
                'stok' => "0",
                'harga_modal' => $detailBarang->harga_modal,
                'harga_jual' => $detailBarang->harga_jual,
                'tipe' => $selectedStore,
            ]);

            DB::table('stok_tertahan')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $detailBarang->nama_barang,
                'category' => $detailBarang->category,
                'stok' => $request->jumlah,
                'harga_modal' => $detailBarang->harga_modal,
                'harga_jual' => $detailBarang->harga_jual,
                'tipe' => $selectedStore,
            ]);   

            $totalBelanja = $request->jumlah * $request->harga_modal;

        
            DB::table('history_belanja')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $detailBarang->nama_barang,
                'jumlah' => $request->jumlah,
                'harga_modal' => $detailBarang->harga_modal,
                'total_belanja' => $totalBelanja,
                'supplier' => $request->supplier,
                'tgl_belanja' => $request->tanggal,
                'status_belanja' => "Lunas",
                'user' => $activeuser,
            ]);

            $namaTrx = "Belanja"." ".$request->nama_barang." Cabang".$request->tipe; 

            DB::table('keuangan_advance')->insert([
                'tanggal' => $request->tanggal,
                'nama_teransaksi' => $namaTrx,
                'kredit' => $totalBelanja,
                'debit' => "0",
                'post' => $request->akun_keuangan,
                'post_akun' => "Belanja Barang",
            ]);

        // belanja barang invoice dengan pembayaran cash, masuk stok_tertahan, history belanja lunas dan keuangan.
        }else if($cekStokCabang != null){

            $totalBelanja = $request->jumlah * $detailBarang->harga_modal;

            DB::table('stok_tertahan')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $detailBarang->nama_barang,
                'category' => $detailBarang->category,
                'stok' => $request->jumlah,
                'harga_modal' => $detailBarang->harga_modal,
                'harga_jual' => $detailBarang->harga_jual,
                'tipe' => $selectedStore,
            ]);  
        
            DB::table('history_belanja')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $detailBarang->nama_barang,
                'jumlah' => $request->jumlah,
                'harga_modal' => $detailBarang->harga_modal,
                'total_belanja' => $totalBelanja,
                'supplier' => $request->supplier,
                'tgl_belanja' => $request->tanggal,
                'status_belanja' => "Lunas",
                'user' => $activeuser,
            ]);

            $namaTrx = "Belanja"." ".$detailBarang->nama_barang." Cabang".$request->tipe; 

            DB::table('keuangan_advance')->insert([
                'tanggal' => $request->tanggal,
                'nama_teransaksi' => $namaTrx,
                'kredit' => $totalBelanja,
                'debit' => "0",
                'post' => $request->akun_keuangan,
                'post_akun' => "Belanja Barang"
            ]);
        }
    }
    return back()->with('berhasiltambahbarang', 'detail ada di script footer');
    };
    
//di bawah ini kalau input produk baru dan belanja barang yang di gudangnya sudah tersedia stok nya
    if($akunKeuangan == "Belanja Tempo"){
        if($cekStokCabang == null){

            DB::table('stok_barang')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $request->nama_barang,
                'category' => $request->category,
                'stok' => "0",
                'harga_modal' => $request->harga_modal,
                'harga_jual' => $request->harga_jual,
                'tipe' => $request->tipe,
            ]);

            DB::table('stok_tertahan')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $request->nama_barang,
                'category' => $request->category,
                'stok' => $request->jumlah,
                'harga_modal' => $request->harga_modal,
                'harga_jual' => $request->harga_jual,
                'tipe' => $request->tipe,
            ]);   

            $totalBelanja = $request->jumlah * $request->harga_modal;

        
            DB::table('history_belanja')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $request->nama_barang,
                'jumlah' => $request->jumlah,
                'harga_modal' => $request->harga_modal,
                'total_belanja' => $totalBelanja,
                'supplier' => $request->supplier,
                'tgl_belanja' => $request->tanggal,
                'status_belanja' => "Tempo",
                'user' => $activeuser,
            ]);
            // belanja barang inventory dengan pembayaran tempo, berarti dia tidak masuk pengeluaran, barang masuk stok tertahan, status history belanja hutang tempo 
        }elseif($cekStokCabang != null){

            $totalBelanja = $request->jumlah * $detailBarang->harga_modal;

            DB::table('stok_tertahan')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $detailBarang->nama_barang,
                'category' => $detailBarang->category,
                'stok' => $request->jumlah,
                'harga_modal' => $detailBarang->harga_modal,
                'harga_jual' => $detailBarang->harga_jual,
                'tipe' => $request->tipe,
            ]);  
        
            DB::table('history_belanja')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $detailBarang->nama_barang,
                'jumlah' => $request->jumlah,
                'harga_modal' => $detailBarang->harga_modal,
                'total_belanja' => $totalBelanja,
                'supplier' => $request->supplier,
                'tgl_belanja' => $request->tanggal,
                'status_belanja' => "Tempo",
                'user' => $activeuser,
            ]);            
        }
    // belanja barang baru dengan pembayaran cash, masuk stok_barang sebagai barang baru, stok_tertahan, history belanja lunas dan keuangan.  
    }else if($akunKeuangan != "Belanja Tempo"){
        if($cekStokCabang == null){

            DB::table('stok_barang')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $request->nama_barang,
                'category' => $request->category,
                'stok' => "0",
                'harga_modal' => $request->harga_modal,
                'harga_jual' => $request->harga_jual,
                'tipe' => $request->tipe,
            ]);

            DB::table('stok_tertahan')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $request->nama_barang,
                'category' => $request->category,
                'stok' => $request->jumlah,
                'harga_modal' => $request->harga_modal,
                'harga_jual' => $request->harga_jual,
                'tipe' => $request->tipe,
            ]);   

            $totalBelanja = $request->jumlah * $request->harga_modal;

        
            DB::table('history_belanja')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $request->nama_barang,
                'jumlah' => $request->jumlah,
                'harga_modal' => $request->harga_modal,
                'total_belanja' => $totalBelanja,
                'supplier' => $request->supplier,
                'tgl_belanja' => $request->tanggal,
                'status_belanja' => "Lunas",
                'user' => $activeuser,
            ]);

            $namaTrx = "Belanja"." ".$request->nama_barang." Cabang".$request->tipe; 

            DB::table('keuangan_advance')->insert([
                'tanggal' => $request->tanggal,
                'nama_teransaksi' => $namaTrx,
                'kredit' => $totalBelanja,
                'debit' => "0",
                'post' => $request->akun_keuangan,
                'post_akun' => "Belanja Barang",
            ]);

        // belanja barang invoice dengan pembayaran cash, masuk stok_tertahan, history belanja lunas dan keuangan.
        }else if($cekStokCabang != null){

            $totalBelanja = $request->jumlah * $detailBarang->harga_modal;

            DB::table('stok_tertahan')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $detailBarang->nama_barang,
                'category' => $detailBarang->category,
                'stok' => $request->jumlah,
                'harga_modal' => $detailBarang->harga_modal,
                'harga_jual' => $detailBarang->harga_jual,
                'tipe' => $request->tipe,
            ]);  
        
            DB::table('history_belanja')->insert([
                'code_barang' => $kodeBarang,
                'nama_barang' => $detailBarang->nama_barang,
                'jumlah' => $request->jumlah,
                'harga_modal' => $detailBarang->harga_modal,
                'total_belanja' => $totalBelanja,
                'supplier' => $request->supplier,
                'tgl_belanja' => $request->tanggal,
                'status_belanja' => "Lunas",
                'user' => $activeuser,
            ]);

            $namaTrx = "Belanja"." ".$detailBarang->nama_barang." Cabang".$request->tipe; 

            DB::table('keuangan_advance')->insert([
                'tanggal' => $request->tanggal,
                'nama_teransaksi' => $namaTrx,
                'kredit' => $totalBelanja,
                'debit' => "0",
                'post' => $request->akun_keuangan,
                'post_akun' => "Belanja Barang"
            ]);
        }
    }
    return back()->with('berhasiltambahbarang', 'detail ada di script footer');
    }

    public function printqrcode($id){
        $qrcode = DB::table('stok_barang')->where('id',$id)->get();
        return view('admin.printqr', compact('qrcode'));
    }

    public function printthermalqrcode($id){
        $qrcode = DB::table('stok_barang')->where('id',$id)->get();
        return view('admin.printqr', compact('qrcode'));
    }

    public function suksesInputPenjualan(){
        Session::flash('suksesInputPenjualan','Penjualan Barang Berhasil Di Lakukan');

        return redirect('/LaporanTransaksi');
    }

    public function tambahcategory(request $request){
        DB::table('category_product')->insert([
            'nama_category' => $request->category,
        ]);
        return back()->with('status', 'Category Baru Berhasil Ditambahkan');
    }

    public function tambahsupplier(request $request){
        DB::table('data_supplier')->insert([
            'nama_supplier' => $request->nama_supplier,
            'alamat_supplier' => $request->alamat_supplier,
            'hp_supplier' => $request->hp_supplier,
        ]);
        return back()->with('status', 'Data Supplier Berhasil Ditambahkan');
    }

    public function gantinamasup(request $request, $id){
        DB::table('data_supplier')->where('id_supplier', $id)->update([
            'nama_supplier' => $request->nama_supplier,
        ]);
        return back()->with('status', 'Data Supplier Berhasil Di Rubah');
    }

     public function gantialamatsup(request $request, $id){
        DB::table('data_supplier')->where('id_supplier', $id)->update([
            'alamat_supplier' => $request->alamat_supplier,
        ]);
        return back()->with('status', 'Data Supplier Berhasil Di Rubah');
    }

     public function gantihpsup(request $request, $id){
        DB::table('data_supplier')->where('id_supplier', $id)->update([
            'hp_supplier' => $request->hp_supplier,
        ]);
        return back()->with('status', 'Data Supplier Berhasil Di Rubah');
    }

    public function hapussupplier(request $request, $id){
        DB::table('data_supplier')->where('id_supplier', $id)->delete();
        return back()->with('status', 'Satu Data Supplier Berhasil Di Hapus');
    }

    public function hapusstokpending(request $request, $id){
        DB::table('stok_tertahan')->where('id', $id)->delete();
        return back()->with('berhasilhapusdata', 'sukses hapus data');
    }

    public function hapuscategory(request $request,$id){
        DB::table('category_product')->where('id_category', $id)->delete();
        return back()->with('status', 'Category Berhasil Do Hapus');
    }

    public function gantinamastok(request $request, $id){
         DB::table('stok_barang')->where('id', $id)->update([
            'nama_barang' => $request->nama_barang,
        ]);
        return back()->with('status', 'Data Barang Berhasil Di Rubah');
    }

    public function ganticatstok(request $request, $id){
        DB::table('stok_barang')->where('id', $id)->update([
            'category' => $request->category_stok,
        ]);
        return back()->with('status', 'Data Barang Berhasil Di Rubah');
    }

    public function stoksampai(request $request, $id)
    {
        $hariini = date('Y-m-d');
        $jmlstokpending = DB::table('stok_tertahan')
                            ->where('id', $id)
                            ->first('stok');

        $idbarang = DB::table('stok_tertahan')
                            ->where('id', $id)
                            ->first('code_barang');

        $tokoCabang = DB::table('stok_tertahan')
                            ->where('id', $id)
                            ->first('tipe');

        $jmlstokreal = DB::table('stok_barang')
                            ->where('code_barang', $idbarang->code_barang)
                            ->where('tipe', $tokoCabang->tipe)
                            ->first('stok');
        
        if ($request->stok_datang > $jmlstokpending->stok) {
            
            return redirect::route('stok')->with('stokdatangterlalubanyak', 'Data Barang Berhasil Di Rubah');
            
        }elseif ($jmlstokpending->stok > $request->stok_datang) {

        // dd($request->stok_datang, $jmlstokpending->stok);

            $newstoktertahan = $jmlstokpending->stok - $request->stok_datang;

            DB::table('stok_tertahan')
                            ->where('id', $id)
                            ->update([
                                'stok' => $newstoktertahan
                            ]);

            $newstokreal = $jmlstokreal->stok + $request->stok_datang;

            DB::table('stok_barang')
                            ->where('code_barang', $idbarang->code_barang)
                            ->where('tipe', $tokoCabang->tipe)
                            ->update([
                                'stok' => $newstokreal,
                            ]);

            DB::table('history_kedatangan')
                            ->insert([
                                'id_tertahan' => $id,
                                'nama_barang' => $request->nama_barang,
                                'code_barang' => $request->code_barang,
                                'toko_cabang' => $request->toko_cabang,
                                'stok_dibeli' => $request->stok_dibeli,
                                'stok_datang' => $request->stok_datang,
                                'keterangan' => $request->keterangan,
                                'tanggal' => $hariini,
                            ]);
            

            return redirect::route('stok')->with('stokdatangtidaksesuai', 'Data Barang Berhasil Di Rubah');
        }elseif($jmlstokpending->stok = $request->stok_datang){

            $newstok = $jmlstokreal->stok + $jmlstokpending->stok;
            
            DB::table('stok_barang')
                                ->where('code_barang', $idbarang->code_barang)
                                ->where('tipe', $tokoCabang->tipe)
                                ->update([
                'stok' => $newstok,
            ]);

            DB::table('stok_tertahan')->where('id', $id)->delete();

            DB::table('history_kedatangan')
                            ->insert([
                                'id_tertahan' => $id,
                                'nama_barang' => $request->nama_barang,
                                'code_barang' => $request->code_barang,
                                'toko_cabang' => $request->toko_cabang,
                                'stok_dibeli' => $request->stok_dibeli,
                                'stok_datang' => $request->stok_datang,
                                'keterangan' => $request->keterangan,
                                'tanggal' => $hariini,
                            ]);

            return redirect::route('stok')->with('stokberhasildipindah', 'Data Barang Berhasil Di Rubah');

        };
    }

    public function updatestok(request $request, $id)
    {
        DB::table('stok_barang')->where('id', $id)->update([
            'stok' => $request->jml_stok,
        ]);
        return back()->with('status', 'Data Stok Berhasil Di Rubah');
    }





    public function hapusbarang(request $request, $id)
    {
        $idbarang_tertahan =    DB::table('stok_barang')
                                            ->where('id', $id)
                                            ->first('code_barang');
                                            
        if($idbarang_tertahan == null){
            $idbarang = "";
        }else{
            $idbarang = $idbarang_tertahan->code_barang;
        }
        
                                DB::table('stok_barang')
                                            ->where('id', $id)
                                            ->delete();
                                            
                                DB::table('stok_tertahan')
                                            ->where('code_barang', $idbarang)
                                            ->delete();
                                            
        return back()->with('berhasilhapusdata', 'detail pesan ada di footer');
    }





    public function tambahchart(request $request)
    {

        $activetoko     = auth()->user()->toko;
        $idBarang           = DB::table('stok_barang')
                                ->where('tipe', $activetoko)
                                ->where('code_barang', $request->id_barang)
                                ->first('id');

                                // dd($idBarang);

        if ($idBarang == null) {
            return back()->with('itemnotfound', 'detail pesan ada di footer');
        }else{
            $item           = $idBarang->id;
        };

        $c_barang       = DB::table('stok_barang')
                            ->where('id', $item)
                            ->first();

        $activeuser     = auth()->user()->id;

        $cariItem       = DB::table('chart')
                            ->where('user', $activeuser)
                            ->where('id_barang', $c_barang->code_barang)
                            ->count();

        $jmlitem        = DB::table('chart')
                            ->where('user', $activeuser)
                            ->where('id_barang', $c_barang->code_barang)
                            ->sum('jml_barang');

        $stok           = DB::table('stok_barang')
                            ->where('id',$item)
                            ->sum('stok');

        $priceLevel     = DB::table('users')
                            ->where('id', $activeuser)
                            ->first('tier');

        $hargaLevel     = DB::table('stok_barang')
                            ->where('id',$item)
                            ->first();

        $min_qty        = DB::table('stok_barang')
                            ->where('id', $item)
                            ->first('min_qty');

        $harga = $hargaLevel->harga_jual;


        $modal          = DB::table('stok_barang')
                            ->where('id',$item)
                            ->sum('harga_modal');
        $jml        = 1;

        $tambahitem = $jmlitem+1;

        $subtotal1 = $harga*$jml;
        $modal1 = $modal*$jml;
        $modal2 = $modal*$tambahitem;
        $sisastok = $stok-1;

        // // code yang menghitung jika ada vocher yang terdaftar untuk chart ketika baru menambahkan
        if($c_barang->id == 43){
            if($jml > 0) {
                $subtotal2 = $harga*$jml;
            }if($jml > 2) {
                $subtotal2 = 33000*$jml;
            }
        }elseif($c_barang->id == 77){
            if($jml > 0) {
                $subtotal2 = $harga*$jml;
            }if($jml > 2) {
                $subtotal2 = 33000*$jml;
            }
        }else{
                $subtotal2 = $harga*$jml;
        };

        if($stok == 0){
                                        
            $codebox            = DB::table('stok_barang')
                                        ->where('id_sachet',$item)
                                        ->sum('id');
                                            
            $currentbox         = DB::table('stok_barang')
                                        ->where('id',$codebox)
                                        ->sum('stok');

            $jmlsachetperbox    = DB::table('stok_barang')
                                        ->where('id_sachet',$item)
                                        ->sum('qty_sachet');
                                            
            $currentsachet      = DB::table('stok_barang')
                                        ->where('id',$item)
                                        ->sum('stok');
                                            
            $currentchart       = DB::table('chart')
                                        ->where('id_barang',$c_barang->code_barang)
                                        ->where('user', $activeuser)
                                        ->sum('jml_barang');
                                        
            //jika stok kosong tapi adalah saachet maka sistem akan mengecek
            //apakah ada item box yang dapat di konfersi menjadi sachet

            if($currentbox > 0 ){
                                            
                $updatesachet = $currentsachet + $jmlsachetperbox + $currentchart - $jml;

                $updatebox = $currentbox - 1 ;
                
                DB::table('stok_barang')->where('id',$item)->update([
                    'stok' => $updatesachet,
                ]);
                
                DB::table('stok_barang')->where('id',$codebox)->update([
                    'stok' => $updatebox,
                ]);
        
                $data = [
                    'no_barang_id' => $c_barang->id,
                    'id_barang' => $c_barang->code_barang,
                    'jml_barang' => $jml,
                    'total_harga' => $subtotal1,
                    'total_modal' => $modal1,
                    'user' => $activeuser,
                ];

                DB::table('chart')
                    ->where('user', $activeuser)
                    ->insert($data);

                return back()->with('sukses', 'sukses');

            }else {

                return back()->with('stokProdukKosong', 'Stok barang yang anda tambahkan tidak cukup');

            }
        }elseif($harga == null){
            back()->with('noresellerprice', 'tidak di jual untuk reseller');
        }elseif($cariItem == 0){

            $data = [
                'no_barang_id' => $c_barang->id,
                'id_barang' => $c_barang->code_barang,
                'jml_barang' => $jml,
                'total_harga' => $subtotal1,
                'total_modal' => $modal1,
                'user' => $activeuser,
            ];

            // dd($c_barang->code_barang, $jml, $subtotal1, $modal1, $c_barang, $data);

            DB::table('chart')->insert($data);

            DB::table('stok_barang')
                ->where('id',$item)
                ->update([
                'stok' => $sisastok,
            ]);
        }elseif($cariItem >= 0){
            DB::table('chart')
                ->where('user', $activeuser)
                ->where('id_barang',$c_barang->code_barang)
                ->update([
                    'jml_barang' => $tambahitem,
                    'total_harga' => $subtotal2,
                    'total_modal' => $modal2,
                    'user' => $activeuser,
            ]);
            stok_barang::where('id',$item)->update([
                'stok' => $sisastok,
            ]);
        };

        $jenischart = DB::table('app_settings')
                        ->where('settings_name', "jenis_chart")
                        ->first('settings_value');

        if ($jenischart->settings_value == "select") {
            return redirect('/shopselect');
        }else{
            return redirect('/shop');
        }
    }



    public function tambahchartmanual(request $request)
    {

        $activetoko     = auth()->user()->toko;
        $idBarang           = DB::table('stok_barang')
                            ->where('tipe', $activetoko)
                            ->where('code_barang', $request->id_barang)
                            ->first('id');

        $item               = $idBarang->id;

        $activeuser         = auth()->user()->id;

        $c_barang           = DB::table('stok_barang')
                                ->where('id', $item)
                                ->first();

        $jml                = $request->jml;

        $jmlitem            = DB::table('chart')
                                ->where('user', $activeuser)
                                ->where('id_barang', $c_barang->code_barang)
                                ->sum('jml_barang');

        $stok               = DB::table('stok_barang')
                                ->where('id',$item)
                                ->sum('stok');

        $modal              = DB::table('stok_barang')
                                ->where('id',$item)
                                ->sum('harga_modal');

        $priceLevel         = DB::table('users')
                                ->where('id', $activeuser)
                                ->first('tier');

                                // dd($priceLevel->tier);

        if ($priceLevel->tier == null) {
            return back()->with('kesalahantier', 'Stok barang yang anda tambahkan tidak cukup');
        };

        $hargaLevel         = DB::table('stok_barang')
                                ->where('id',$item)
                                ->first($priceLevel->tier);

        $hargaJual         = DB::table('stok_barang')
                                ->where('id',$item)
                                ->first('harga_jual');

        $hargaAgen          = DB::table('stok_barang')
                                ->where('id',$item)
                                ->first('agen_price');

        $min_qty            = DB::table('stok_barang')
                                ->where('id', $item)
                                ->sum('min_qty');

        // dd($jml, $min_qty);
        $harga = $hargaJual->harga_jual;
        

        $stokplusItem = $stok+$jmlitem;
        $subtotal1 = $harga*$stokplusItem;
        $submodal1 = $modal*$stokplusItem;
        $submodal2 = $modal*$jml;
        $sisastok = $stok+$jmlitem-$jml;
        $mincon = $jmlitem+$stok-$jml;

        // // code yang menghitung jika ada vocher yang terdaftar untuk chart ketika baru menambahkan
        if($c_barang->id == 43){
            if($jml > 0) {
                $subtotal2 = $harga*$jml;
            }if($jml > 2) {
                $subtotal2 = 33000*$jml;
            }
        }elseif($c_barang->id == 77){
            if($jml > 0) {
                $subtotal2 = $harga*$jml;
            }if($jml > 2) {
                $subtotal2 = 33000*$jml;
            }
        }else{
                $subtotal2 = $harga*$jml;
        };

        // dd($harga,$subtotal2, $submodal2);

        if($stokplusItem == 0){
            back()->with('warning', 'Stok barang yang anda tambahkan tidak cukup');
        }elseif($jml <= $jmlitem){
           DB::table('chart')
                ->where('id_barang',$c_barang->code_barang)
                ->where('user', $activeuser)
                ->update([
                    'jml_barang' => $jml,
                    'total_harga' => $subtotal2,
                    'total_modal' => $submodal2,
            ]);
            DB::table('stok_barang')->where('id',$item)->update([
                'stok' => $mincon,
            ]);
        }elseif($jml >= $stokplusItem){
           DB::table('chart')
                ->where('id_barang',$c_barang->code_barang)
                ->where('user', $activeuser)
                ->update([
                    'jml_barang' => $stokplusItem,
                    'total_harga' => $subtotal1,
                    'total_modal' => $submodal1,
            ]);
            DB::table('stok_barang')->where('id',$item)->update([
                'stok' => "0",
            ]);
            return back()->with('stokmaksimal', 'chart tidak boleh kosong');
        }elseif($jml <= $stokplusItem){
            DB::table('chart')
                ->where('id_barang',$c_barang->code_barang)
                ->where('user', $activeuser)
                ->update([
                'jml_barang' => $jml,
                'total_harga' => $subtotal2,
                'total_modal' => $submodal2,
            ]);
            stok_barang::where('id',$item)->update([
                'stok' => $sisastok,
            ]);
        };

        return redirect('/shop');
    }

    public function hapuschart(request $request,$id)
    {
        $jmlitem        = DB::table('chart')->where('no_barang_id', $id)->sum('jml_barang');
        $stok           = DB::table('stok_barang')->where('id',$id)->sum('stok');
        $sisastok       = $jmlitem + $stok;
        DB::table('chart')->where('no_barang_id', $id)->delete();
        stok_barang::where('id',$id)->update([
                'stok' => $sisastok,
            ]);
        return back()->with('hapusitemchartsukses', 'Chart Berhasil Di Hapus');
    }

    public function proseschart(Request $request)
    {
        $nama = $request->nama_pembeli;
        $temporesult = "tempo ".$nama;
        $userActive = Auth::user()->id;
        $toko       = Auth::user()->toko;

        $cekexistensiinv  = DB::table('invoice')
                                ->count('invoice');
        
        if($cekexistensiinv == null){
            $invoiceno = "Inv000000001";
        }else{
            $inv = DB::table('invoice')->orderBy('id', 'DESC')->first('invoice');
            $urutan = (int) substr($inv->invoice, 3, 13);
            $urutan++;
            $text = "Inv";
            $invoiceno = $text . sprintf("%09s", $urutan);
        }

        $cekinvmarketplace  = DB::table('invoice')
                                ->where('expedisi', $request->inv_marketplace)
                                ->count('invoice');

                                // dd($request->inv_marketplace, $cekinvmarketplace);

        if($cekinvmarketplace != null){
            return back()->with('invexist', 'detail pesan ada di footer');
        };

        $tipeToko   = DB::table('list_marketplace')
                        ->where('nama_marketplace', $request->platform)
                        ->first('diatur_marketplace');


        if ($tipeToko == null) {
            $tipeTokoValue = "tidak";
        }else{
            $tipeTokoValue = $tipeToko->diatur_marketplace;
        }

        if ($tipeTokoValue == "ya") {
            $statusPesanan = "dikirim";
        }else{
            $statusPesanan = "dikirim";
        };

        if ($request->inv_marketplace == null) {
            $expedisi   =   $invoiceno;
        }else{
            $expedisi   =   $request->inv_marketplace;
        };

        $totalTagihan = $request->tagihan + $request->ongkir;

        $insertedIdInvoice = DB::table('invoice')->insertGetId([
            'invoice' => $invoiceno,
            'tanggal' => $request->tanggal_order,
            'jumlah_barang' => $request->jumlah_barang,
            'tagihan' => $totalTagihan,
            'platform' => $request->platform,
            'tempo' => $request->tanggal_tempo,
            'nama_pembeli' => $request->nama_pembeli,
            'total_modal' => $request->total_modal,
            'user' => $userActive,
            'invoice_toko' => $toko,
            'expedisi' => $expedisi,
            'code_booking' => $request->code_booking,
            'alamat' => $request->alamat,
            'tempo' => $statusPesanan,
            'ongkir' => $request->ongkir,
        ]);

        $sisaitem = $request->sisaitem;
        $code = $request->codeItem;  
        $idChart = $request->idChart;
        $title = $request->namaItem;
        $harga = $request->hargaSatuan;
        $jumlah = $request->jmlsbt;
        $tanggal = $request->tanggal_orderar;
        $idBarang = $request->idbarang;
        for($i = 0; $i < count($idChart); $i++){
             $data[] = [
                'id_invoice' => $insertedIdInvoice,
                'id_barang' => $idBarang[$i],
                'code_barang' => $code[$i],
                'title' => $title[$i],
                'harga' => $harga[$i],
                'jumlah' => $jumlah[$i],
                'tanggal' => $tanggal[$i],
                'toko' => $toko[$i],
            ];
        }
        
        penjualan::insert($data);

        for($i = 0; $i < count($idChart); $i++){
             $data2[] = [
                'id_chart' => $idChart[$i],
            ];
        }

        DB::table('chart')->whereIn('id_chart', $data2)->delete();

        DB::table('invoice_penjualan')->insert([
            'id_invoice' => $insertedIdInvoice,
            'id_penjualan' => $insertedIdInvoice,
        ]);

        $postAkun = "penjualan"." ".$request->platform." ".$invoiceno;

        if($request->platform == "Tempo"){
            //do nothing
        }if($request->platform == "PO"){
            //do nothing
        }else{
            DB::table('keuangan')->insert([
            'tanggal' => $request->tanggal_order,
            'nama_teransaksi' => $postAkun,
            'debit' => $request->tagihan,
            'kredit' => "0",
            'post' => $request->platform,
            'post_akun' => "pendapatan",
            ]);    
        }

        return back()->with('berhasilInputOrder', 'detail pesan ada di footer');
    }

    public function tambahakun(request $request){
        DB::table('akun_keuangan')->insert([
            'nama_akun' => $request->akun,
        ]);
        return back()->with('status', 'Akun Keuangan Baru Berhasil Di Tambahkan');
    }

    public function hapusakun(request $request, $id){
        DB::table('akun_keuangan')->where('id_akun', $id)->delete();
        return back()->with('status', 'Akun Keuangan Berhasil Di Hapus');
    }

    public function tambahrekening(request $request){
        DB::table('rekening')->insert([
            'pemilik_rekening' => $request->pemilik_rekening,
            'nama_bank' => $request->nama_bank,
            'nomor_rekening' => $request->nomor_rekening,
        ]);
        return back()->with('berhasiltambahbank', 'detail msg box ada di footer');
    }

    public function danamasuk(request $request){
        DB::table('keuangan')->insert([
            'tanggal' => $request->tanggal,
            'nama_teransaksi' => $request->nama_teransaksi,
            'debit' => $request->debit,
            'kredit' => "0",
            'post' => $request->post,
            'post_akun' => $request->post_keuangan,
        ]);
        return back()->with('status', 'Data keuangan Pemasukan Baru Berhasil Di Input');
    }

    public function danakeluar(request $request){
        DB::table('keuangan')->insert([
            'tanggal' => $request->tanggal,
            'nama_teransaksi' => $request->nama_teransaksi,
            'debit' => "0",
            'kredit' => $request->debit,
            'post' => $request->post,
            'post_akun' => $request->post_keuangan,
        ]);
        return back()->with('status', 'Data keuangan Pengeluaran Baru Berhasil Di Input');
    }

    public function updatelunas(request $request){
        $nama = $request->nama_pembeli;
        $id = $request->id;
        $tanggal = date('Y-m-d');
        $temporesult = "tempo ".$nama;

        DB::table('invoice')->where('id', $id)->update([
            'platform' => "lunas",
        ]);

        DB::table('keuangan_advance')->insert([
            'tanggal' => $tanggal,
            'nama_teransaksi' => $temporesult,
            'debit' => $request->debit,
            'kredit' => "0",
            'post' => "Tempo",
            ]);

        return back()->with('status', 'Tempo Berhasil Di Lunasi');
    }

    public function rubahminimum(request $request){
        DB::table('app_settings')->where('id', 4)->update([
            'settings_value' => $request->minimum,
        ]);
        return back()->with('status', 'seting minimum berhasil di rubah');
    }

    public function rubahpassword(request $request)
    {
        $id = $request->id;
        User::where('id', $id)->update([
            'password' => Hash::make($request->pass2),
        ]);
        return back()->with('status', 'Password berhasil Di Rubah');
    }





    public function reselleraddtochart(request $request)
    {
        $item = $request->item;
        
        $c_barang       = DB::table('stok_barang')
                            ->where('id', $item)
                            ->first();

        $activeuser     = auth()->user()->id;

        $cariItem       = DB::table('chart')
                            ->where('user', $activeuser)
                            ->where('id_barang', $c_barang->code_barang)
                            ->count();

        $jmlitem        = DB::table('chart')
                            ->where('user', $activeuser)
                            ->where('id_barang', $c_barang->code_barang)
                            ->sum('jml_barang');

        $stok           = DB::table('stok_barang')
                            ->where('id',$item)
                            ->sum('stok');

        $priceLevel     = DB::table('users')
                            ->where('id', $activeuser)
                            ->first('tier');

        if ($priceLevel->tier == null) {
            return back()->with('notierlevel', 'detail pesan ada di footer');
        };

        if ($jmlitem >= $stok) {
            return back()->with('stokProdukKosong', 'detail pesan ada di footer');
        };

        $hargaLevel     = DB::table('stok_barang')
                            ->where('id',$item)
                            ->first($priceLevel->tier);

        $min_qty        = DB::table('stok_barang')
                            ->where('id', $item)
                            ->sum('min_qty');

        $hargaAgen          = DB::table('stok_barang')
                                ->where('id',$item)
                                ->first('agen_price');

        if($priceLevel->tier == "tr1"){
            $harga = $hargaLevel->tr1;
        }if($priceLevel->tier == "tr2") {
            $harga = $hargaLevel->tr2;
        }if($priceLevel->tier == "tr3") {
            $harga = $hargaLevel->tr3;
        }if($jmlitem + 1 >= $min_qty) {
            $harga = $hargaAgen->agen_price;
        };


        if($harga == null){
            return back()->with('noresellerprice', 'Stok barang yang anda tambahkan tidak cukup');
        };


        $modal          = DB::table('stok_barang')
                            ->where('id',$item)
                            ->sum('harga_modal');
        $jml        = 1;

        $tambahitem = $jmlitem+1;

        $subtotal1 = $harga*$jml;
        $modal1 = $modal*$jml;
        $subtotal2 = $harga*$tambahitem;
        $modal2 = $modal*$tambahitem;
        // jika ingin langsung mengurangi stok rubah - 1 jika tidak mau lansung kurangi stok - 0 saja
        $sisastok = $stok-0;

        if($stok == 0){
                                        
            $codebox            = DB::table('stok_barang')
                                        ->where('id_sachet',$item)
                                        ->sum('id');
                                            
            $currentbox         = DB::table('stok_barang')
                                        ->where('id',$codebox)
                                        ->sum('stok');

            $jmlsachetperbox    = DB::table('stok_barang')
                                        ->where('id_sachet',$item)
                                        ->sum('qty_sachet');
                                            
            $currentsachet      = DB::table('stok_barang')
                                        ->where('id',$item)
                                        ->sum('stok');
                                            
            $currentchart       = DB::table('chart')
                                        ->where('id_barang',$c_barang->code_barang)
                                        ->where('user', $activeuser)
                                        ->sum('jml_barang');
                                        
            //jika stok kosong tapi adalah saachet maka sistem akan mengecek
            //apakah ada item box yang dapat di konfersi menjadi sachet

            if($currentbox > 0 ){
                                            
                $updatesachet = $currentsachet + $jmlsachetperbox + $currentchart - $jml;

                // jika ingin langsung mengurangi stok rubah - 1 jika tidak mau lansung kurangi stok - 0 saja
                $updatebox = $currentbox - 0 ;
                
                DB::table('stok_barang')->where('id',$item)->update([
                    'stok' => $updatesachet,
                ]);
                
                DB::table('stok_barang')->where('id',$codebox)->update([
                    'stok' => $updatebox,
                ]);
        
                $data = [
                    'no_barang_id' => $c_barang->id,
                    'id_barang' => $c_barang->code_barang,
                    'jml_barang' => $jml,
                    'total_harga' => $subtotal1,
                    'total_modal' => $modal1,
                    'user' => $activeuser,
                ];

                DB::table('chart')
                    ->where('user', $activeuser)
                    ->insert($data);

                return back()->with('sukses', 'sukses');

            }else {

                return back()->with('stokProdukKosong', 'Stok barang yang anda tambahkan tidak cukup');

            }

        }elseif($harga == null){

            return back()->with('noresellerprice', 'tidak di jual untuk reseller');

        }elseif($cariItem == 0){

            $data = [
                'no_barang_id' => $c_barang->id,
                'id_barang' => $c_barang->code_barang,
                'jml_barang' => $jml,
                'total_harga' => $subtotal1,
                'total_modal' => $modal1,
                'user' => $activeuser,
            ];

            DB::table('chart')
                ->where('user', $activeuser)
                ->insert($data);

            DB::table('stok_barang')
                ->where('id',$item)
                ->update([
                'stok' => $sisastok,
            ]);

        }elseif($cariItem >= 0){
            DB::table('chart')
                ->where('id_barang',$c_barang->code_barang)
                ->where('user', $activeuser)
                ->update([
                    'jml_barang' => $tambahitem,
                    'total_harga' => $subtotal2,
                    'total_modal' => $modal2,
                    'user' => $activeuser,
            ]);
            stok_barang::where('id',$item)->update([
                'stok' => $sisastok,
            ]);
        };
        return redirect('/home');
    }


    public function tambahchartmanualReseller(request $request)
    {

        $item               = $request->id_barang;

        $activeuser         = auth()->user()->id;

        $c_barang           = DB::table('stok_barang')
                                ->where('id', $item)
                                ->first();

        $jml                = $request->jml;

        $jmlitem            = DB::table('chart')
                                ->where('user', $activeuser)
                                ->where('id_barang', $c_barang->code_barang)
                                ->sum('jml_barang');

        $stok               = DB::table('stok_barang')
                                ->where('id',$item)
                                ->sum('stok');

        $modal              = DB::table('stok_barang')
                                ->where('id',$item)
                                ->sum('harga_modal');

        $priceLevel         = DB::table('users')
                                ->where('id', $activeuser)
                                ->first('tier');

        $hargaLevel         = DB::table('stok_barang')
                                ->where('id',$item)
                                ->first($priceLevel->tier);

        $hargaAgen          = DB::table('stok_barang')
                                ->where('id',$item)
                                ->first('agen_price');

        $min_qty            = DB::table('stok_barang')
                                ->where('id', $item)
                                ->sum('min_qty');

        if ($jml > $stok) {
            return back()->with('stokProdukKosong', 'detail pesan ada di footer');
        };


        if($priceLevel->tier == "tr1"){
            $harga = $hargaLevel->tr1;
        }if($priceLevel->tier == "tr2") {
            $harga = $hargaLevel->tr2;
        }if($priceLevel->tier == "tr3") {
            $harga = $hargaLevel->tr3;
        }if($jml >= $min_qty){
            $harga = $hargaAgen->agen_price;
        };
        
        // dd($harga, $priceLevel->tier, $hargaLevel->tr1, $jml);
        

        $stokplusItem = $stok+$jmlitem;
        $subtotal1 = $harga*$stokplusItem;
        $subtotal2 = $harga*$jml;
        $submodal1 = $modal*$stokplusItem;
        $submodal2 = $modal*$jml;
        $sisastok = $stok+$jmlitem-$jml;
        $mincon = $jmlitem+$stok-$jml;


        if($stokplusItem == 0){

            back()->with('warning', 'Stok barang yang anda tambahkan tidak cukup');

        }elseif($jml <= $jmlitem){

            DB::table('chart')
                ->where('id_barang',$c_barang->code_barang)
                ->where('user', $activeuser)
                ->update([
                    'jml_barang' => $jml,
                    'total_harga' => $subtotal2,
                    'total_modal' => $submodal2,
            ]);

            return back()->with('warning', 'chart tidak boleh kosong');

        }elseif($jml >= $stokplusItem){
            
            $ceksachetataubox = DB::table('stok_barang')
                                        ->where('id',$item)
                                        ->sum('id_sachet');
                                        
            if($ceksachetataubox == 0 ){

                $jmlsachetperbox    = DB::table('stok_barang')
                                        ->where('id_sachet',$item)
                                        ->sum('qty_sachet');
                                        
                $codebox            = DB::table('stok_barang')
                                        ->where('id_sachet',$item)
                                        ->sum('id');
                                            
                $currentbox         = DB::table('stok_barang')
                                        ->where('id',$codebox)
                                        ->sum('stok');
                                            
                $currentsachet      = DB::table('stok_barang')
                                        ->where('id',$item)
                                        ->sum('stok');
                                            
                $currentbox         = DB::table('stok_barang')
                                        ->where('id',$codebox)
                                        ->sum('stok');
                                            
                $currentchart       = DB::table('chart')
                                        ->where('id_barang',$c_barang->code_barang)
                                        ->where('user', $activeuser)
                                        ->sum('jml_barang');

                //request sachet dikurang jumlah chart saat ini
                $req_sachet_minus_crnt_chart   = $jml - $currentchart;

                //kalau jumlah request sachet di kurang current sachet saat ini lebih besar dari sachet per box maka harus di conversi
                if ($req_sachet_minus_crnt_chart > $jmlsachetperbox) {

                    return back()->with('permintaansachetkebanyakan', 'detail msg ada di footer');

                }else {
                                            
                    $updatesachet = $currentsachet + $jmlsachetperbox + $currentchart - $jml;

                }
                
                        //di bawah ini adalah kalau stok box nya sudah nol jadi sachet tidak bisa di tambahkan lagi
                        if($currentbox == 0){
                            
                            $currentchart = DB::table('chart')
                                                    ->where('id_barang',$c_barang->code_barang)
                                                    ->where('user', $activeuser)
                                                    ->sum('jml_barang');
                            
                            $updatechartmaxsachet = $currentchart + $currentsachet;
                            
                            $subtotal3 = $harga*$updatechartmaxsachet;
                            $submodal3 = $modal*$updatechartmaxsachet;
                            
                            DB::table('chart')
                                ->where('id_barang',$c_barang->code_barang)
                                ->where('user', $activeuser)
                                ->update([
                                    'jml_barang' => $updatechartmaxsachet,
                                    'total_harga' => $subtotal3,
                                    'total_modal' => $submodal3,
                            ]);
                            
                            DB::table('stok_barang')->where('id',$item)->update([
                                'stok' => "0",
                            ]);
                            
                            return back()->with('sachetdanboxmax', 'box sudah kosong jadi tidak bisa di rubah ke sachet');
                            
                        };

                $updatebox = $currentbox - 0 ;
                
                DB::table('stok_barang')->where('id',$item)->update([
                    'stok' => $updatesachet,
                ]);
                
                DB::table('stok_barang')->where('id',$codebox)->update([
                    'stok' => $updatebox,
                ]);
        
                DB::table('chart')
                    ->where('id_barang',$c_barang->code_barang)
                    ->where('user', $activeuser)
                    ->update([
                        'jml_barang' => $jml,
                        'total_harga' => $subtotal2,
                        'total_modal' => $submodal2,
                ]);

            //ini lupa fungsinya gimana
            }elseif($ceksachetataubox > 0 ){

                DB::table('chart')
                    ->where('id_barang',$c_barang->code_barang)
                    ->where('user', $activeuser)
                    ->update([
                        'jml_barang' => $stokplusItem,
                        'total_harga' => $subtotal1,
                        'total_modal' => $submodal1,
                ]);
                
                DB::table('stok_barang')->where('id',$item)->update([
                    'stok' => "0",
                ]);
                
                return back()->with('stokmaksimal', 'chart tidak boleh kosong');
            }
            
            
            
            
            
        }elseif($jml <= $stokplusItem){
            DB::table('chart')
                ->where('id_barang',$c_barang->code_barang)
                ->where('user', $activeuser)
                ->update([
                    'jml_barang' => $jml,
                    'total_harga' => $subtotal2,
                    'total_modal' => $submodal2,
            ]);

            return back()->with('warning', 'chart tidak boleh kosong');
        };
        
        return back()->with('warning', 'chart tidak boleh kosong');
    }






    public function proseschartReseller(Request $request)
    {

        $cekinvmarketplace  = DB::table('invoice')
                                ->where('expedisi', $request->inv_marketplace)
                                ->count('invoice');

                                // dd($request->inv_marketplace, $cekinvmarketplace);

        if($cekinvmarketplace != null){
            return back()->with('invexist', 'detail pesan ada di footer');
        };
        
        $idChart        = $request->idChart;
        $nama           = $request->nama_pembeli;
        $temporesult    = "tempo ".$nama;
        $activeuser         = auth()->user()->id;

        $chartStore     = DB::table('chart')
                                ->where('user', $activeuser)
                                ->orderBy('id_barang', 'DESC')
                                ->limit('1')
                                ->first('no_barang_id');

        $cekToko        = DB::table('stok_barang')
                                ->where('id', $chartStore->no_barang_id)
                                ->first('tipe');

        $tokoutama      = DB::table('users')
                                ->where('role', 1)
                                ->first('toko');

        if($chartStore == null){
                $namaToko   = $tokoutama;
        }else{
                $namaToko   = $cekToko->tipe;
        };
        
        $cekexistensiinv  = DB::table('invoice')
                                ->count('invoice');
        
        if($cekexistensiinv == null){
            $invoiceno = "Inv000000001";
        }else{
            $inv = DB::table('invoice')->orderBy('id', 'DESC')->first('invoice');
            $urutan = (int) substr($inv->invoice, 3, 13);
            $urutan++;
            $text = "Inv";
            $invoiceno = $text . sprintf("%09s", $urutan);
        }

        //proses upload pdf single di mulai dari sini
        $requestFile = $request->file('pdf');
        $filesize = $request->file('pdf')->getSize();
        if( $filesize >= 1000000){
            return back()->with('fileoversize', 'detail di script');
        }else{
        // menyimpan data file yang diupload ke variabel $file
        $id_task = $request->id;   
        $time = Carbon::now();                 
        $nama_file = $time."_".$requestFile->getClientOriginalName();                        
        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'img';
        $requestFile->move($tujuan_upload,$nama_file);   
        //proses upload pdf berakhir di sini

        if($idChart == 0){
            return back()->with('warning', 'chart tidak boleh kosong');
        }else{
        $insertedIdInvoice = DB::table('invoice')->insertGetId([
            'invoice' => $invoiceno,
            'tanggal' => $request->tanggal_order,
            'jumlah_barang' => $request->jumlah_barang,
            'tagihan' => $request->tagihan,
            'platform' => $request->platform,
            'tempo' => $request->status_tempo,
            'nama_pembeli' => $request->nama_pembeli,
            'hp_pembeli' => $request->hp_pembeli,
            'nama_pengirim' => $request->nama_pengirim,
            'hp_pengirim' => $request->hp_pengirim,
            'alamat' => $request->alamat,
            'expedisi' => $request->inv_marketplace,
            'code_booking' => $request->code_booking,
            'total_modal' => $request->total_modal,
            'user' => $request->user,
            'invoice_toko' => $namaToko,
        ]);

        $data = [
            'id_task' => $insertedIdInvoice,
            'file' => $nama_file,
            'tag' => "resellerpdf",
        ];

        DB::table('file_task')->insert($data);

        $sisaitem = $request->sisaitem;
        $code = $request->codeItem;  
        $idChart = $request->idChart;
        $title = $request->namaItem;
        $harga = $request->hargaSatuan;
        $jumlah = $request->jmlsbt;
        $tanggal = $request->tanggal_orderar;
        $idBarang = $request->idbarang;
        for($i = 0; $i < count($idChart); $i++){
             $data1[] = [
                'id_invoice' => $insertedIdInvoice,
                'id_barang' => $idBarang[$i],
                'code_barang' => $code[$i],
                'title' => $title[$i],
                'harga' => $harga[$i],
                'jumlah' => $jumlah[$i],
                'tanggal' => $tanggal[$i],
                'toko' => $namaToko,
            ];
        }
        
        penjualan::insert($data1);

        $jam_order = date('H:i');

        DB::table('timeline_order')->insert([
            'invoice_page' => $insertedIdInvoice,
            'judul_timeline' => "Pesanan Di Terima",
            'tanggal_timeline' => $request->tanggal_order,
            'jam_timeline' => $jam_order,
            'deskripsi_timeline' => "pesanan baru masuk, klik di link di bawah untuk melihat file pesanan",
            'file_timeline' => $nama_file,
        ]);

        for($i = 0; $i < count($idChart); $i++){
             $data2[] = [
                'code_barang' => $idChart[$i],
            ];
        }

        DB::table('chart')->whereIn('id_chart', $data2)->delete();

        DB::table('invoice_penjualan')->insert([
            'id_invoice' => $insertedIdInvoice,
            'id_penjualan' => $insertedIdInvoice,
        ]);

        $postAkun = "penjualan"." ".$request->platform." ".$invoiceno;

        if($request->platform == "Tempo"){
            //do nothing
        }else{
            DB::table('keuangan')->insert([
            'tanggal' => $request->tanggal_order,
            'nama_teransaksi' => $postAkun,
            'debit' => $request->tagihan,
            'kredit' => "0",
            'post' => $request->platform,
            'post_akun' => "pendapatan",
            ]);    
        }

        }

        return back()->with('berhasilInputOrder', 'Penjualan Berhasil Di Lakukan');
        }
    }




    public function updateprosesreseller(request $request)
    {

        $id                 = $request->id;

        DB::table('invoice')->where('id', $id)->update([
            'tempo' => "diproses",
        ]);

        $jam_timeline       = date('H:i');
        $tanggal_timeline   = date('Y-m-d');

        DB::table('timeline_order')->insert([
            'invoice_page' => $id,
            'judul_timeline' => "Pesanan Di Proses Oleh Admin",
            'tanggal_timeline' => $tanggal_timeline,
            'jam_timeline' => $jam_timeline,
            'deskripsi_timeline' => "pesanan berhasil di proses",
            'file_timeline' => null,
        ]);

        return back()->with('berhasilupdatereseller', 'Penjualan Berhasil Di Lakukan');
    }




    public function updatekirimreseller(request $request)
    {

        $id                 = $request->id;

        DB::table('invoice')->where('id', $id)->update([
            'tempo' => "dikirim",
        ]);

        $jam_timeline       = date('H:i');
        $tanggal_timeline   = date('Y-m-d');

        DB::table('timeline_order')->insert([
            'invoice_page' => $id,
            'judul_timeline' => "Pesanan Di Kirim Oleh Admin",
            'tanggal_timeline' => $tanggal_timeline,
            'jam_timeline' => $jam_timeline,
            'deskripsi_timeline' => "pesanan berhasil di kirim",
            'file_timeline' => null,
        ]);

        return back()->with('berhasilupdatereseller', 'Penjualan Berhasil Di Lakukan');
    }




    public function uploasbuktitrf(request $request)
    {
        //proses upload pdf single di mulai dari sini
        $requestFile = $request->file('file');
        $filesize = $request->file('file')->getSize();
        if( $filesize >= 1000000){
            return back()->with('fileoversize', 'detail di script');
        }else{
        // menyimpan data file yang diupload ke variabel $file
        $id_task = $request->id;   
        $time = Carbon::now();                 
        $nama_file = $time."_".$requestFile->getClientOriginalName();                        
        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'img';
        $requestFile->move($tujuan_upload,$nama_file);   
        //proses upload pdf berakhir di sini

        $id                 = $request->id;
        DB::table('invoice')->where('id', $id)->update([
            'tempo' => "dipacking",
        ]);

        $jam_timeline       = date('H:i');
        $tanggal_timeline   = date('Y-m-d');

        DB::table('timeline_order')->insert([
            'invoice_page' => $id,
            'judul_timeline' => "Pesanan Sudah Dipacking",
            'tanggal_timeline' => $tanggal_timeline,
            'jam_timeline' => $jam_timeline,
            'deskripsi_timeline' => "Pesanan sudah di packing, silahkan klik link di bawah untuk detail",
            'file_timeline' => $nama_file,
        ]);

        return back()->with('berhasilupdatereseller', 'Penjualan Berhasil Di Lakukan');
        }
    }




    public function tambahuser(request $request){
        User::insert([
            'name' => $request->nama,
            'email' => $request->email,
            'role' => $request->role,
            'password' => Hash::make($request->password),
            'created_at' => $request->created_at,
            'toko' => $request->toko,
            'hp_toko'=> $request->hp,
        ]);
        return redirect('/user');
    }





    public function hapususer($id){
            
            $idRole     = DB::table('users')
                            ->where('id', $id)
                            ->first('role');

            if ($idRole->role == 1) {
                return back()->with('superadmin', 'detail ada di script footer');
            };

        User::where('id',$id)->delete();
        return back()->with('berhasilhapusdata', 'User berhasil di hapus');
    }


    public function kirimpoin(request $request){
        $cekdbcode  = DB::table('rand_numbers')
                    ->where('number',$request->code)
                    ->count('number');

        $cekpoincode = DB::table('poin_cs')
                    ->where('code',$request->code)
                    ->count('code');

        $tanggal = date('Y-m-d');

                    // dd($cekcode);

        if($cekdbcode == 0){
            return back()->with('tidakterdaftar', 'text ada di blade');
            }else{
                if($cekpoincode >= 1){
                return back()->with('sudahdigunakan', 'text ada di blade');
                    }else{
                        DB::table('poin_cs')->insert([
                            'no_hp' => $request->no_hp,
                            'code' => $request->code,
                            'tanggal' => $tanggal,
                        ]); 
                        DB::table('rand_numbers')
                            ->where('number', $request->code)
                            ->delete();
                        return back()->with('berhasilinputcode', 'Poin kamu berhasil di tambahkan');
                    }
        }


    }





    public function updatedetailbarang(request $request){
        DB::table('stok_barang')->where('id',$request->id)->update([
                'nama_barang' => $request->nama_barang,
                'category' => $request->category,
                'harga_modal' => $request->harga_modal,
                'harga_jual' => $request->harga_jual,
                'tr1' => $request->tr1,
                'tr2' => $request->tr2,
                'tr3' => $request->tr3,
                'agen_price' => $request->agen_price,
                'min_qty' => $request->min_qty,
                'qty_sachet' => $request->qty_sachet,
                'id_sachet' => $request->id_sachet,
                'tampil_public' => $request->tampil_public,
                'produk_deskripsi' => $request->produk_deskripsi,
            ]);
        return back()->with('updateProdukBerhasil', 'User berhasil di hapus');
    }




    public function uploadfotoprofile(request $request)
    {
        //proses upload pdf single di mulai dari sini
        $requestFile = $request->file('file');
        $filesize = $request->file('file')->getSize();
        if( $filesize >= 1000000){
            return back()->with('fileoversize', 'detail di script');
        }else{
        // menyimpan data file yang diupload ke variabel $file
        $id_task = $request->id;   
        $time = Carbon::now();                 
        $nama_file = $time."_".$requestFile->getClientOriginalName();                        
        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'img';
        $requestFile->move($tujuan_upload,$nama_file);   
        //proses upload pdf berakhir di sini

        $id                 = $request->id;
        DB::table('users')->where('id', $id)->update([
            'foto' => $nama_file,
        ]);

        return back()->with('berhasilupdateprofile', 'detail ada di script footer');
        }
    }




    public function uploadfotoproduk(request $request)
    {
        //proses upload pdf single di mulai dari sini
        $requestFile = $request->file('file');
        $filesize = $request->file('file')->getSize();
        if( $filesize >= 1000000){
            return back()->with('fileoversize', 'detail di script');
        }else{
        // menyimpan data file yang diupload ke variabel $file
        $id_task = $request->id;   
        $time = Carbon::now();                 
        $nama_file = $time."_".$requestFile->getClientOriginalName();                        
        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'img';
        $requestFile->move($tujuan_upload,$nama_file);   
        //proses upload pdf berakhir di sini

        $id                 = $request->id;
        DB::table('stok_barang')->where('id', $id)->update([
            'produk_picture' => $nama_file,
        ]);

        return back()->with('sukses', 'detail ada di script footer');
        }
    }




    public function updateprofile(request $request)
    {
        $id                 = $request->id;
        DB::table('users')->where('id', $id)->update([
            'name' => $request->nama,
            'hp_toko' => $request->hp_toko,
            'email' => $request->email,
        ]);

        return back()->with('berhasilupdateprofile', 'detail ada di script footer');
        
    }




    public function gantipassword(request $request)
    {

        $userActive =   Auth::user()->id;
        $userDetail =   DB::table('users')
                            ->where('id', $userActive)
                            ->first();

        $curPass            =   $userDetail->password;
        $reqCurPass         =   $request->password_saat_ini;
        $reqPass            =   $request->password_baru;
        $perifReqPass       =   $request->confirm_password_saat_ini;

        // dd($curPass, $reqCurPass, $reqPass, $perifReqPass);

        if($reqPass != $perifReqPass) {
            return back()->with('passwordincorrect', 'detail ada di script footer');
        }elseif(Hash::check($reqCurPass, $curPass)){
            $id                 = $request->id;
            DB::table('users')->where('id', $id)->update([
                'password' => Hash::make($perifReqPass)
            ]);
            return back()->with('passwordberhasildiupdate', 'detail ada di script footer');
        }else{
            return back()->with('passwordsalah', 'detail ada di script footer');
        }
        
    }




    public function rencanabayar(request $request)
    {
        $userActive =   Auth::user()->id;
        DB::table('rencana_pembayaran')->insert([
            'id_inv' => $request->id,
            'user_id'=> $userActive
        ]);

        DB::table('invoice')->where('id', $request->id)->update([
            'check_bayar' => "check"
        ]);

        
        return back()->with('berhasilrencanabayar', 'detail ada di script footer');
    }




    public function hapusrencanabayar(request $request)
    {
        $inv = DB::table('rencana_pembayaran')
            ->where('id_rencana', $request->id)
            ->first('id_inv');

        DB::table('rencana_pembayaran')
            ->where('id_rencana', $request->id)
            ->delete();

        DB::table('invoice')->where('id', $inv->id_inv)->update([
            'check_bayar' => "",
        ]);

        
        return back()->with('berhasilhapusrencana', 'detail ada di script footer');
    }




    public function buatpembayaran(request $request)
    {
        //proses upload pdf single di mulai dari sini
        $requestFile = $request->file('file');
        $filesize = $request->file('file')->getSize();
        if( $filesize >= 1000000){
            return back()->with('fileoversize', 'detail di script');
        }else{
        // menyimpan data file yang diupload ke variabel $file
        $id_task = $request->id;   
        $time = Carbon::now();                 
        $nama_file = $time."_".$requestFile->getClientOriginalName();                        
        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'img';
        $requestFile->move($tujuan_upload,$nama_file);   
        //proses upload pdf berakhir di sini

        $tanggal   = date('Y-m-d');
        $userActive =   Auth::user()->id;
        $dataRcbtotal   = DB::table('rencana_pembayaran')
                            ->where('user_id', $userActive)
                            ->join('invoice', 'invoice.id' ,'=', 'rencana_pembayaran.id_inv')
                            ->sum('tagihan');

        $tiketLast = DB::table('pembayaran')->max('tiket_pembayaran');
        $urutan = (int) substr($tiketLast, 9, 13);
        $urutan++;
        $text = "LB";
        $hs   = date('His');
        $notiket = $text.$hs.sprintf("%05s", $urutan);
        
        // dd($tiketLast, $notiket);


        $dataRcbtotal           = DB::table('rencana_pembayaran')
                                        ->where('user_id', $userActive)
                                        ->join('invoice', 'invoice.id' ,'=', 'rencana_pembayaran.id_inv')
                                        ->sum('tagihan');

        $kirimtagihan           = DB::table('users')
                                        ->where('role', 1)
                                        ->first('toko');
        
        $idTiket = DB::table('pembayaran')->insertGetId([
                'tanggal_pembayaran' => $tanggal,
                'tiket_pembayaran' =>$notiket,
                'status_pembayaran' => "new",
                'reseller_id' => $userActive,
                'total_pembayaran' => $dataRcbtotal,
                'pembayaran_toko' => $kirimtagihan->toko,
            ]);

        for($i = 0; $i < count($request->id_inv); $i++){
             $data2[] = [
                'id_inv' => $request->id_inv[$i],
            ];
        }

        $fta = Arr::flatten($data2);

        $hahaha = implode(",",$fta);

        $myArray = explode(',', $hahaha);

        DB::table('rencana_pembayaran')->whereIn('id_inv', $myArray)->delete();
        DB::table('invoice')->whereIn('id', $myArray)->update([
            'bayar_id' => $notiket,
        ]);


        $data3 = [
            'id_task' => $notiket,
            'file' => $nama_file,
            'tag' => "buktibayartempo",
        ];

        DB::table('file_task')->insert($data3);

        return back()->with('berhasilmelakukanpembayaran', 'detail ada di script footer');
        }
    }




    public function pembayaranditerima(request $request)
    {
            $id = $request->id;

            $idBarang   =   $request->idbarang;

            $getidinvoice = DB::table('invoice')
                        ->where('bayar_id', $id)
                        ->first('id');

            for($i = 0; $i < count($idBarang); $i++){

                $data[] = [
                    'id_barang' => $idBarang[$i],
                ];
                            
                $stok[] = DB::table('stok_barang')
                            ->where('id', $data[$i])
                            ->sum('stok');

                $penjualan[] = DB::table('penjualan')
                            ->where('id_invoice', $getidinvoice->id)
                            ->where('id_barang', $data[$i])
                            ->sum('jumlah');

                $dataUpdate[]  = $stok[$i] - $penjualan[$i];

                DB::table('stok_barang')
                            ->where('id', $data[$i])
                            ->update([
                                'stok' => $dataUpdate[$i],
                            ]);
            }

            // dd($dataUpdate);

            $rekTempo = DB::table('app_settings')
                        ->where('settings_name', "rekening_tempo")
                        ->first('settings_value');

            if($rekTempo->settings_value == ""){
                return back()->with('rekeningkosong', 'detail ada di script footer');
            };

            //karna sudah melalui perifikasi maka status pembayaran dirubah menjadi diterima
            DB::table('pembayaran')
                ->where('tiket_pembayaran', $id)
                ->update([
                        'status_pembayaran' => "Diterima",
                    ]);

            $detail = DB::table('pembayaran')
                        ->where('tiket_pembayaran', $id)
                        ->first();

            $platform = DB::table('invoice')
                            ->where('bayar_id', $id)
                            ->first('platform');
            
                DB::table('invoice')
                    ->where('bayar_id', $id)
                    ->update([
                        'pencairan' => $detail->total_pembayaran,
                            'tempo' => "diproses",
                        ]);

                $post = DB::table('rekening')
                    ->where('id_rekening', 2)
                    ->first('nomor_rekening');
                    
                $valuepost = $post->nomor_rekening;
                
                //keuangan advance menerima uang sejumlah yang dibayarkan
                DB::table('keuangan_advance')->insert([
                    'tanggal' => $detail->tanggal_pembayaran,
                    'nama_teransaksi' => "pembayaran".' '.$detail->tiket_pembayaran,
                    'debit' => $detail->total_pembayaran,
                    'kredit' => "0",
                    'post' => $valuepost,
                    'post_akun' => "pendapatan",
                ]);

                DB::table('keuangan')->insert([
                    'tanggal' => $detail->tanggal_pembayaran,
                    'nama_teransaksi' => "pembayaran".' '.$detail->tiket_pembayaran,
                    'debit' => $detail->total_pembayaran,
                    'kredit' => "0",
                    'post' => $valuepost,
                    'post_akun' => "pendapatan",
                ]);


            //keuangan advance mengeluarkan senilai potongan harga diskon atau gratis ongkir jika terdapat promo (belum)
            //keuangan menerima uang sejumlah yang dibayarkan
            //keuangan mengeluarkan senilai potongan harga diskon atau gratis ongkir jika terdapat promo (belum)

            
            

            

        
        return back()->with('pembayarandiupdate', 'detail ada di script footer');
    }




    public function updatetokodetail(request $request)
    {

            DB::table('app_settings')
                ->where('settings_name', "rekening_tempo")->update([
                'settings_value' => $request->rekeningreseller,
            ]);

            DB::table('app_settings')
                ->where('settings_name', "invoice_type")->update([
                'settings_value' => $request->invoice_type,
            ]);

            DB::table('app_settings')
                ->where('settings_name', "jenis_chart")->update([
                'settings_value' => $request->jenis_chart,
            ]);

        
        return back()->with('berhasilupdateprofile', 'detail ada di script footer');
    }




    public function updateuserdetail(request $request)
    {

            $id         = $request->id;

            $idRole     = DB::table('users')
                            ->where('id', $id)
                            ->first('role');
            if ($idRole->role == 1) {
                return back()->with('superadmin', 'detail ada di script footer');
            };

            DB::table('users')->where('id', $id)->update([
                'name' => $request->nama,
                'email' => $request->email,
                'hp_toko' => $request->hp,
                'tier' => $request->tier,
                'role' => $request->role,
                'manager' => $request->manager,
                'toko' => $request->toko,
            ]);

        
        return back()->with('berhasilupdateprofile', 'detail ada di script footer');
    }




    public function buatcabangbaru(request $request)
    {

            DB::table('toko_cabang')->insert([
                'nama_cabang' => $request->nama_cabang,
                'alamat_cabang' => $request->alamat_cabang,
                'hp_cabang' => $request->hp_cabang,
                'kepala_cabang' => $request->kepala_cabang,
            ]);

        
        return back()->with('berhasiltambahcabang', 'detail ada di script footer');
    }




    public function hapuscabang(request $request)
    {

            $id = $request->id;

            $toko = DB::table('toko_cabang')
                ->where('id_cabang', $id)
                ->first('nama_cabang');

            DB::table('toko_cabang')
                ->where('id_cabang', $id)
                ->delete();

            DB::table('users')
                ->where('toko', $toko->nama_cabang)
                ->update([
                    'toko' => "",
                ]);

        
        return back()->with('berhasilhapuscabang', 'detail ada di script footer');
    }




    public function jadikanecer(request $request)
    {

            $id = $request->id;

            $idsachet       =   DB::table('stok_barang')
                                    ->where('id', $id)
                                    ->first('id_sachet');

            $jumlahecer     =   DB::table('stok_barang')
                                    ->where('id', $id)
                                    ->first('qty_sachet');

            $stokEcerRn     =   DB::table('stok_barang')
                                    ->where('id', $idsachet->id_sachet)
                                    ->first('stok');

            $stokSekarang   =   DB::table('stok_barang')
                                    ->where('id', $id)
                                    ->first('stok');

            $sisaStok       =   $stokSekarang->stok - 1;
            $totalEcer      =   $jumlahecer->qty_sachet + $stokEcerRn->stok;

            // dd($idsachet->id_sachet, $jumlahecer->qty_sachet, $stokSekarang->stok, $sisaStok, $qtySachet);

            DB::table('stok_barang')
                ->where('id', $id)
                ->update([
                    'stok' => $sisaStok,
                ]);

            DB::table('stok_barang')
                ->where('id', $idsachet->id_sachet)
                ->update([
                    'stok' => $totalEcer,
                ]);

        
        return back()->with('berhasilecer', 'detail ada di script footer');
    }




    public function prosespembatalanpesanan(request $request)
    {

            $id = $request->id;
            $hariini = date('Y-m-d');

            $invoice = DB::table('invoice')
                            ->where('id', $id)
                            ->first();

            // dd($invoice->id);

            $idBarang   =   $request->idbarang;

            for($i = 0; $i < count($idBarang); $i++){
                $data[] = [
                    'id_barang' => $idBarang[$i],
                ];
                            
                $stok[] = DB::table('stok_barang')
                            ->where('id', $data[$i])
                            ->sum('stok');

                $penjualan[] = DB::table('penjualan')
                            ->where('id_invoice', $invoice->id)
                            ->where('id_barang', $data[$i])
                            ->sum('jumlah');

                $dataUpdate[]  = $stok[$i] + $penjualan[$i];

                DB::table('stok_barang')
                            ->where('id', $data[$i])
                            ->update([
                                'stok' => $dataUpdate[$i],
                            ]);
            }

            $insertedIdInvoice = DB::table('invoice_batal')->insertGetId([
                'id_invoice' => $invoice->id,
                'invoice' => $invoice->invoice,
                'tanggal' => $hariini,
                'jumlah_barang' => $invoice->jumlah_barang,
                'tagihan' => $invoice->tagihan,
                'platform' => $invoice->platform,
                'nama_pembeli' => $invoice->nama_pembeli,
                'total_modal' => $invoice->total_modal,
                'user' => $invoice->user,
                'invoice_toko' => $invoice->invoice_toko,
                'jenis_pembatalan' => $request->jenis_pembatalan,
                'alasan_pembatalan' => $request->alasan_pembatalan, 
            ]);


            $idBarang       = $request->idbarang;
            $codeBarang     = $request->codebarang;
            $namaItem       = $request->namaItem;
            $hargaSatuan    = $request->hargaSatuan;
            $jumlah         = $request->jumlah;

            for($i = 0; $i < count($idBarang); $i++){
                 $data1[] = [
                    'id_invoice' => $insertedIdInvoice,
                    'id_barang' => $idBarang[$i],
                    'code_barang' => $codeBarang[$i],
                    'title' => $namaItem[$i],
                    'harga' => $hargaSatuan[$i],
                    'jumlah' => $jumlah[$i],
                    'tanggal' => $request->tanggal_batal[$i],
                ];
            }

            DB::table('keuangan')->insert([
            'tanggal' => $hariini,
            'nama_teransaksi' => "pembatalan pesanan invoice ".$invoice->invoice,
            'debit' => "0",
            'kredit' => $invoice->tagihan,
            'post' => $invoice->platform,
            'post_akun' => "ordercancel",
            ]);    
            
            penjualan_batal::insert($data1);

            DB::table('invoice')
                    ->where('id', $invoice->id)
                    ->delete();

        
        return redirect('/shop')->with('berhasilbatalkanpesanan', 'detail ada di script footer');
    }




    public function prosestolakpesananreseller(request $request)
    {

            $id = $request->id;

            $hariini = date('Y-m-d');

            $invoice = DB::table('invoice')
                            ->where('id', $id)
                            ->first();

            // dd($id, $invoice->bayar_id);

                    $insertedIdInvoice = DB::table('invoice_batal')->insertGetId([
                        'id_invoice' => $invoice->id,
                        'invoice' => $invoice->invoice,
                        'tanggal' => $hariini,
                        'jumlah_barang' => $invoice->jumlah_barang,
                        'tagihan' => $invoice->tagihan,
                        'platform' => $invoice->platform,
                        'nama_pembeli' => $invoice->nama_pembeli,
                        'total_modal' => $invoice->total_modal,
                        'user' => $invoice->user,
                        'invoice_toko' => $invoice->invoice_toko,
                        'jenis_pembatalan' => $request->jenis_pembatalan,
                        'alasan_pembatalan' => $request->alasan_pembatalan, 
                    ]);


                    $idBarang       = $request->idbarang;
                    $codeBarang     = $request->codebarang;
                    $namaItem       = $request->namaItem;
                    $hargaSatuan    = $request->hargaSatuan;
                    $jumlah         = $request->jumlah;

                    for($i = 0; $i < count($idBarang); $i++){
                        $data1[] = [
                            'id_invoice' => $insertedIdInvoice,
                            'id_barang' => $idBarang[$i],
                            'code_barang' => $codeBarang[$i],
                            'title' => $namaItem[$i],
                            'harga' => $hargaSatuan[$i],
                            'jumlah' => $jumlah[$i],
                            'tanggal' => $request->tanggal_batal[$i],
                        ];
                    }

                    DB::table('invoice')
                            ->where('id', $invoice->id)
                            ->update([
                                'tagihan' => 0,
                                'total_modal' => 0,
                                'platform' => "pesanan dibatalkan",
                                'tempo' => "",
                                'expedisi' => "",
                            ]);

                    DB::table('pembayaran')
                            ->where('tiket_pembayaran', $invoice->bayar_id)
                            ->update([
                                'status_pembayaran' => "Ditolak",
                            ]);
               
            
    

        
        return redirect('/pesananagen')->with('berhasilbatalkanpesanan', 'detail ada di script footer');
    }




    public function prosesbayartempoagen(request $request)
    {

            $id = $request->id;

            $rekeningbelanja = $request->rekening;

            if($rekeningbelanja == ""){
                return back()->with('rekeningkosong', 'detail ada di script footer');
            };

            $hariini = date('Y-m-d');

            $detailHistory = DB::table('history_belanja')
                                ->where('id', $id)
                                ->first();

            DB::table('history_belanja')
                    ->where('id', $id)
                    ->update([
                        'status_belanja' => "Lunas",
                    ]);

            DB::table('keuangan_advance')->insert([
            'tanggal' => $hariini,
            'nama_teransaksi' => "bayar tempo agen ".$detailHistory->supplier.' '.$detailHistory->nama_barang.' '.$detailHistory->jumlah." Pcs",
            'debit' => "0",
            'kredit' => $detailHistory->total_belanja,
            'post' => $rekeningbelanja,
            'post_akun' => "bayar tempo agen",
            ]);    

        
        return redirect('historybelanja')->with('tempoberhasil', 'detail ada di script footer');
    }




    public function tambahrole(request $request)
    {
            $userActive =   Auth::user()->id;

            $roleid = DB::table('role')->insertGetId([
                'role_name' => $request->nama_role,
            ]);    

            DB::table('role_menu')->insert([
                'role_id' => $roleid,
                'role_name' => $request->nama_role,
                'home_menu' => $request->home_menu,
                'user_id_number' => $userActive,
                'dashboard' => $request->dashboard,
                'system_transaksi' => $request->system_transaksi,
                'inventory' => $request->system_inventory,
                'list_cabang' => $request->list_cabang,
                'pesanan_agen' => $request->pesanan_agen,
                'pembayaran_agen' => $request->pembayaran_agen,
                'history_belanja' => $request->history_belanja,
                'tagihan' => $request->tagihan,
                'keuangan' => $request->keuangan,
                'settings' => $request->settings,
                'reseller_chart' => $request->resellerchart,
                'reseller_order' => $request->resellerorder,
            ]);    

        
        return back()->with('rolebaru', 'detail ada di script footer');
    }




    public function submiteditrole(request $request)
    {
            $id =   $request->id;  

            DB::table('role_menu')->where('role_id', $id)->update([
                'role_name' => $request->nama_role,
                'home_menu' => $request->home_menu,
                'dashboard' => $request->dashboard,
                'system_transaksi' => $request->system_transaksi,
                'inventory' => $request->system_inventory,
                'list_cabang' => $request->list_cabang,
                'pesanan_agen' => $request->pesanan_agen,
                'pembayaran_agen' => $request->pembayaran_agen,
                'history_belanja' => $request->history_belanja,
                'tagihan' => $request->tagihan,
                'keuangan' => $request->keuangan,
                'settings' => $request->settings,
                'reseller_chart' => $request->resellerchart,
                'reseller_order' => $request->resellerorder,
            ]);    

        
        return redirect::route('role')->with('updaterole', 'detail ada di script footer');
    }




    public function hapusrole(request $request)
    {

            $id = $request->id;

            if ($id == 1) {
                return back()->with('superadmin', 'detail ada di script footer');
            };

            DB::table('role')
                ->where('id_role', $id)
                ->delete();

            DB::table('role_menu')
                ->where('role_id', $id)
                ->delete();

        
        return back()->with('berhasilhapusrole', 'detail ada di script footer');
    }




    public function tambahmarketplace(request $request)
    {
            $idUser         =   $request->user;

            $userToko       =   DB::table('users')
                                    ->where('id', $idUser)
                                    ->first('toko');

            $marketplace    =   $request->marketplace;

            if ($request->diatur_marketplace == "ya") {
                $diaturCabang   = "cabang";
            }else{
                $diaturCabang   = "reguler";
            };

            $namaMarketplace=   $request->nama_toko.' '.$marketplace.' '.$diaturCabang;

            $cekMarketplace =   DB::table("list_marketplace")
                                    ->where('nama_marketplace', $namaMarketplace)
                                    ->first('nama_marketplace');

            if ($cekMarketplace != null) {
                    return back()->with('marketplaceterdaftar', 'detail ada di script footer');
            };

            DB::table('list_marketplace')
                ->insert([
                    'id_user_toko' => $idUser,
                    'id_toko_cabang' => $userToko->toko,
                    'nama_marketplace' => $namaMarketplace,
                    'marketplace_utama' => $marketplace.' '.$request->nama_toko,
                    'diatur_marketplace' => $request->diatur_marketplace,
                ]);

        
        return back()->with('berhasilbuatmarketplace', 'detail ada di script footer');
    }




    public function hapusmarketplace(request $request)
    {
            $id     =   $request->id;

            DB::table('list_marketplace')
                ->where('id_marketplace', $id)
                ->delete();

        
        return back()->with('berhasilhapusdata', 'detail ada di script footer');
    }




    public function updatemarketplace(request $request)
    {
            $id         =   $request->id;
            $idUser     =   $request->user;

            $userToko   =   DB::table('users')
                                ->where('id', $idUser)
                                ->first('toko');

            DB::table('list_marketplace')
                ->where('id_marketplace', $id)
                ->update([
                    'id_user_toko' => $idUser,
                    'limit_belanja' => $request->limit_belanja,
                ]);

        
        return redirect::route('marketplace')->with('berhasilupdatedata', 'detail ada di script footer');
    }




    public function prosespindahkanstok(request $request)
    {
            $id                 =   $request->id;
            $codeBarang         =   $request->code_barang;
            $namaBarang         =   $request->nama_barang;
            $jumlahStokAsal     =   $request->jumlah_stok_asal;
            $jumlahDipindah     =   $request->jumlah_dipindah;
            $lokasiAsal         =   $request->tipe;
            $lokasiTujuan       =   $request->tipe_tujuan;


            $stokTujuan         =   DB::table('stok_barang')
                                    ->where('code_barang', $codeBarang)
                                    ->where('tipe', $lokasiTujuan)
                                    ->sum('stok');

            $detailBarang       =   DB::table('stok_barang')
                                    ->where('id', $id)
                                    ->first();

            $cekProdukTujuan    =   DB::table('stok_barang')
                                    ->where('code_barang', $codeBarang)
                                    ->where('tipe', $lokasiTujuan)
                                    ->first('code_barang');

                                    // dd($jumlahStokAsal, $stokTujuan);

            $stokUpdateAsal     =   $jumlahStokAsal - $jumlahDipindah;

            if ($jumlahDipindah > $jumlahStokAsal) {
                return back()->with('jumlahdipindahover', 'detail ada di script footer');
            }if($cekProdukTujuan == null){
                return back()->with('barangkosong', 'detail ada di script footer');
            }if ($lokasiAsal == $lokasiTujuan) {
                return back()->with('lokasisalah', 'detail ada di script footer');
            }

                                DB::table('stok_barang')
                                    ->where('code_barang', $codeBarang)
                                    ->where('tipe', $lokasiAsal)
                                    ->update([
                                        'stok' => $stokUpdateAsal,
                                    ]);

                                DB::table('stok_tertahan')
                                    ->insert([
                                        'code_barang' => $detailBarang->code_barang,
                                        'nama_barang' => $detailBarang->nama_barang,
                                        'category' => $detailBarang->category,
                                        'stok' => $jumlahDipindah,
                                        'harga_modal' => $detailBarang->harga_modal,
                                        'harga_jual' => $detailBarang->harga_jual,
                                        'tipe' => $lokasiTujuan,
                                        'keterangan' => "Pemindahan stok dari ".$lokasiAsal,
                                    ]);


        
        return redirect::route('stok')->with('stokberhasildipindah', 'detail ada di script footer');
    }




            











}
