<?php

 namespace App\Imports;

 use App\invoice_pencairan;
 use Maatwebsite\Excel\Concerns\ToModel;
 use Maatwebsite\Excel\Concerns\WithHeadingRow;


 class invoicecair implements ToModel
 {
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new invoice_pencairan([
            'invoice' => $row[0],
            'pencairan' => $row[1], 
            'marketplace' => $row[2], 
        ]);
    }
  }
