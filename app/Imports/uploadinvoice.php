<?php

 namespace App\Imports;

 use App\invoice_bulk;
 use Maatwebsite\Excel\Concerns\ToModel;
 use Maatwebsite\Excel\Concerns\WithHeadingRow;


 class uploadInvoice implements ToModel
 {
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new invoice_pencairan([
            'invoice' => $row[0],
            'id_barang' => $row[1], 
            'jumlah_barang' => $row[2], 
            'tanggal' => $row[3], 
        ]);
    }
  }
