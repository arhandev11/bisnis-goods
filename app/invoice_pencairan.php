<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invoice_pencairan extends Model
{
    protected $table = "invoice_pencairan";
    protected $fillable = ['invoice','pencairan','marketplace'];

}