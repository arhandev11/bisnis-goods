@extends('layouts.publicapp')
<?php $page = "Demo Toko Online Inventory System" ?>
<title>{{$page}}</title>
@section('content')
<body class=" layout-top-nav">


    
  <div class="bg-gray-50">
    <main class="py-8 md:py-12 mx-auto max-w-7xl px-2 md:px-8">
      <section
        class="flex flex-col sm:flex-row gap-3 sm:gap-8 items-start relative"
      >
      
        <div
            class="w-full sm:w-64 flex-shrink-0 sm:sticky top-16 flex flex-col"
          >
            <div class="relative sm:mb-6">
              <form action="/produk">
                <input
                  name="caribarang"
                  type="text"
                  placeholder="Cari Produk"
                  class="border border-gray-400 shadow-sm px-3 py-2 rounded-md w-full"
                />
              </form>
              <img
                src="../../dist/img/search-line-icon.svg"
                class="absolute w-5 right-3 top-3"
                alt=""
              />
            </div>
            <div class="hidden sm:flex border border-gray-300 bg-white shadow-md rounded-lg flex-col py-4 px-2 gap-1">
              <h1 class="font-bold text-xl mb-3 mx-3">Kategori</h1>
              @foreach($listcategory as $listcategory)
              <a href="/produk?category={{$listcategory->category}}" class="
                        <?php 
                          if($listcategory->category == $selectedcategory)
                            {echo "py-2 px-3 rounded-md bg-blue-600 text-white font-bold";}
                          else {
                            {echo "py-2 px-3 rounded-md text-blue-600 hover:bg-blue-100 hover:font-bold cursor-pointer";}
                          }
                        ?>">
                {{$listcategory->category}}
              </a>
              @endforeach
            </div>
          </div>
          <div
            class="grid min-[300px]:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 flex-grow w-full sm:w-auto gap-4"
          >
          
            @foreach($pilihBarang as $st)
            <div
              class="border border-gray-300 bg-white shadow-md rounded-lg overflow-hidden flex flex-col"
            >
              <div class="bg-green-500 h-50">
                <img
                  src="../../img/{{$st->produk_picture}}"
                  alt="tutor image 1"
              /></div>
              <div class="flex flex-col justify-between px-4 py-2 grow">
                <div class="flex flex-col gap-1">
                  <h1 class="leading-5 mb-1">{{$st->nama_barang}}</h1>
                  <h1 class="text-xl font-semibold">@currency($st->harga_jual)</h1>
                  <div class="flex gap-2 items-start">
                    <h1 class="text-base text-gray-400 line-through">
                      Rp. 30.000
                    </h1>
                    <h2
                      class="text-green-500 text-xs font-bold bg-green-100 text-green-500 p-0.5"
                    >
                      50%
                    </h2>
                  </div>
                  <h1 class="text-sm text-gray-500 mt-2">{{$st->tipe}}</h1>
                </div>
                <form action="/publicaddtochart">
                  <input class="hidden" name="item" value="{{$st->id}}">
                  <button
                  class="mt-3 mb-1 border border-blue-600 py-1 px-5 rounded-lg bg-blue-600 text-white text-sm"
                >
                  + Keranjang
                </button>
                </form>
                
              </div>
            </div>
            @endforeach
          </div>
        </section>
        <section class="grid grid-cols-2 my-6 gap-6">
          <div
            class="bg-white rounded-lg border border-gray-300 p-4 flex flex-col gap-5 items-start shadow-md"
          >
            <h1 class="text-blue-600 text-lg">Mau Jadi Reseller</h1>
            <h2>
              Dapat penghasilan dari rumah berjual produk yang sudah pasti laku
            </h2>
            <button
              class="border border-blue-600 py-1.5 px-5 text-sm rounded-lg bg-blue-600 text-white mt-6"
            >
              Hubungi Kami
            </button>
          </div>
          <div
            class="bg-white rounded-lg border border-gray-300 p-4 flex flex-col gap-5 items-start shadow-md"
          >
            <h1 class="text-red-500 text-lg">Atau Mau Jadi Agen</h1>
            <h2>
              Keuntungan lebih besar dari reseller dan nyetok banyak produk
            </h2>
            <button
              class="border border-red-500 py-1.5 px-5 text-sm rounded-lg bg-red-500 text-white mt-6"
            >
              Hubungi Kami
            </button>
          </div>
        </section>
      </main>
    </div>
  </div>
      <!-- / Content -->
@endsection
