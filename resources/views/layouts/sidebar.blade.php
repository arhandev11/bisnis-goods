<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="../../dist/img/ego.jpg"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Gayub App</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        @if ((auth()->user()->role == 4))
          <li class="nav-item">
            <a href="/home" class="nav-link <?php if($page == "dashboard"){echo 'active';}else{echo '';} ?>">
              <i class="nav-icon fas fa-columns"></i>
              <p>
                Dashboard
              </p>
            </a>
            @endif
          @if ((auth()->user()->role == 2))
          
          <li class="nav-item">
            <a href="shop" class="nav-link <?php if($page == "System Transaksi"){echo 'active';}else{echo '';} ?>">
              <i class="nav-icon fas fa-dolly-flatbed"></i>
              <p>
                System Transaksi
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="stok" class="nav-link <?php if($page == "stok"){echo 'active';}else{echo '';} ?>">
              <i class="nav-icon fas fa-pallet"></i> 
              <p>
                System Inventory
              </p>
            </a>
          </li>
            @endif
          </li>@if ((auth()->user()->role == 1))
          <li class="nav-item">
            <a href="/home" class="nav-link <?php if($page == "dashboard"){echo 'active';}else{echo '';} ?>">
              <i class="nav-icon fas fa-columns"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="shop" class="nav-link <?php if($page == "System Transaksi"){echo 'active';}else{echo '';} ?>">
              <i class="nav-icon fas fa-dolly-flatbed"></i>
              <p>
                System Transaksi
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="stok" class="nav-link <?php if($page == "stok"){echo 'active';}else{echo '';} ?>">
              <i class="nav-icon fas fa-pallet"></i> 
              <p>
                System Inventory
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/historybelanja" class="nav-link <?php if($page == "History Belanja"){echo 'active';}else{echo '';} ?>">
              <i class="nav-icon fas fa-business-time"></i>
              <p>
                History Belanja
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/keuangan" class="nav-link <?php if($page == "Laporan Keuangan"){echo 'active';}else{echo '';} ?>">
              <i class="nav-icon fas fa-money-check-alt"></i>
              <p>
                Laporan Keuangan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/user" class="nav-link <?php if($page == "user"){echo 'active';}else{echo '';} ?>">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Pengguna
              </p>
            </a>
          </li>
          @endif
          @if ((auth()->user()->role == 3))
          <li class="nav-item">
            <a href="/Reseller" class="nav-link <?php if($page == "Order Reseller"){echo 'active';}else{echo '';} ?>">
              <i class="nav-icon fas fa-dolly-flatbed"></i>
              <p>
                Order Reseller
              </p>
            </a>
          </li>
          @endif
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
