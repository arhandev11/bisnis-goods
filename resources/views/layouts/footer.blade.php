


    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('app-assets/vendors/js/charts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/moment.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap5.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> <!-- wajib kalau mau pake select 2 -->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('app-assets/js/core/app.js') }}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('app-assets/js/scripts/pages/app-invoice-list.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/extensions/ext-component-sweet-alerts.js') }}"></script>
    <!-- END: Page JS-->


    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('app-assets/vendors/js/charts/chart.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/select2/select2.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/bootstrap-select/bootstrap-select.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/typeahead-js/typeahead.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/tagify/tagify.js') }}"></script>
    <script src="{{ asset('app-assets/js/forms-selects.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <script type="text/javascript">
        // Stand-in for something that takes time
        function doSomethingThatTakesTime() {
            return new Promise(resolve => setTimeout(resolve, 2000));
        }
    
        // Code triggered by the button
        let running = 0;
        function doOperation() {
            if (running > 0) {
                console.log("Call ignored, already running");
                return;
            }
            ++running;
            updateUI();
            console.log("Started running");
            doSomethingThatTakesTime()
            .finally(() => {
                console.log("Done running");
                --running;
                updateUI();
                document.getElementById("formChart").submit(); // Submit the form
            });
        }
    
        // Update the UI to match our current state
        function updateUI() {
            document.getElementById("cegah-double-klik").disabled = running > 0;
        }
    
        // Handle clicks on the button
        document.getElementById("cegah-double-klik").addEventListener("click", function(event) {
            event.preventDefault(); // Prevent the default form submission
            doOperation(); // Perform your operation
        });
    </script>
    

    <script type="text/javascript">
        // Stand-in for something that takes time
        function doSomethingThatTakesTime() {
            return new Promise(resolve => setTimeout(resolve, 2000));
        }
    
        // Handle clicks on the button "submittedformditerima"
        document.getElementById("submittedformditerima").addEventListener("click", async function(event) {
            event.preventDefault(); // Prevent the default form submission
            
            // Show a warning message
            const confirmed = await Swal.fire({
                title: 'PERHATIAN!!',
                text: "pastikan bukti transfer benar dan uang sudah masuk ke rekening tujuan",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'proses'
            });
    
            // If user confirmed, proceed with form submission
            if (confirmed.isConfirmed) {
                // Perform something that takes time
                console.log("Started running");
                await doSomethingThatTakesTime();
                console.log("Done running");
    
                // Submit the form
                document.getElementById("formditerima").submit();
            }
        });
    </script>
    
    
    
  

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>


<script type="text/javascript">
    $('.buatpesananreseller').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "Pastikan Pesanan Sudah Sesuai",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'proses'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = url;
      }
    });
});
</script>


<script type="text/javascript">
    $('.akanProsesPesanan').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "yakin mau proses pesanan??",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'proses'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = url;
      }
    });
});
</script>


<script type="text/javascript">
    $('.perifpembayaran').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "pastikan bukti transfer benar dan uang sudah masuk ke rekening tujuan",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'proses'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = url;
      }
    });
});
</script>

<script type="text/javascript">
    $('.bayartempo').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "kamu yakin akan membayar tempo hutang ini",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'proses'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = url;
      }
    });
});
</script>

<script type="text/javascript">
    $('.tolakpesanan').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "kamu yakin akan menolak pesanan ini?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'tolak'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = url;
      }
    });
});
</script>

<script type="text/javascript">
    $('.pembatalanwarning').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "yakin mau membatalkan pesanan ini ??",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'proses'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = url;
      }
    });
});
</script>

<!-- atribut menggunakan form -->
<script type="text/javascript">
    $('.pembatalanperif').on('click', function (event) {
    event.preventDefault();
    const url = $('#formpembatalan').attr('action');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "dengan meng klik proses, pesanan akan di batalkan, stok akan di kembalikan ke inventory sesuai dengan jumlah pesanan dan system akan memballance kan akun keuangan di menu keuangan",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'proses'
    }).then((result) => {
      if (result.isConfirmed) {
        var form = document.getElementById("formpembatalan");
        form.method = "POST";
        form.action = "/prosespembatalanpesanan";
        form.submit();
      }
    });
});
</script>


<script type="text/javascript">
    $('.pembatalanreseller').on('click', function (event) {
    event.preventDefault();
    const url = $('#formpembatalanreseller').attr('action');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "dengan meng klik proses, pesanan akan di batalkan, stok akan di kembalikan ke inventory sesuai dengan jumlah pesanan",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'proses'
    }).then((result) => {
      if (result.isConfirmed) {
        var form = document.getElementById("formpembatalanreseller");
        form.method = "POST";
        form.action = "/prosestolakpesananreseller";
        form.submit();
      }
    });
});
</script>

<!-- hanya tombol oke tanpa cancel -->
<script type="text/javascript">
    $('.jadikanecer').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    Swal.fire(
        'PERHATIAN!!',
        'barang ini tidak memiliki id sachet, edit barang dan berikan id sachet terlebih dahulu sebelum merubahnya menjadi sachet',
        'warning'
    );
});
</script>

<script type="text/javascript">
    $('.emptychart').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    Swal.fire(
        'PERHATIAN!!',
        'chart tidak boleh kosong',
        'warning'
    );
});
</script>

<script type="text/javascript">
    $('.akanKirimPesanan').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "pastikan paket yang kamu kirim adalah paket yang benar",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'proses'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = url;
      }
    });
});
</script>

<script type="text/javascript">
    $('.hapusdata').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "data yang dihapus tidak dapat di kembalikan",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'hapus'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = url;
      }
    });
});
</script>

<script type="text/javascript">
    $('.hapustokocabang').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "jika kamu menghapus toko cabang, maka seluruh user yang berhubungan dengan toko cabang yang kamu hapus akan kehilangan toko",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'hapus'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = url;
      }
    });
});
</script>

<script type="text/javascript">
    $('.yakindatang').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "pastikan stok yang sampai sudah sesuai",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ok'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = url;
      }
    });
});
</script>

<script type="text/javascript">
    $('.tanpafiturkeuangan').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "fitur ini akan menghapus stok pending dan mengembalikan uang ke kantung dana yang digunakan untuk belanja {feature not ready yet}",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ok'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = url;
      }
    });
});
</script>


@if(Session::has('berhasilhapusdata'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'SUKSES',
                    'data berhasil dihapus',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif


@if(Session::has('noresellerprice'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'WARNING!!',
                    'produk ini tidak di jual untuk reseller',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif

@if(Session::has('fileoversize'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'WARNING!!',
                    'file terlalu besar maksimal 10mb',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif

@if(Session::has('berhasilInputOrder'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'SUKSES!!',
                    'Pesanan Kamu Berhasil Di Buat',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif

@if(Session::has('berhasilupdatereseller'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'SUKSES!!',
                    'kamu berhasil update status pesanan ini',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif

@if(Session::has('updateProdukBerhasil'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'SUKSES!!',
                    'kamu berhasil update produk ini',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif

@if(Session::has('stokProdukKosong'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'MAAF',
                    'mohon maaf stok sedang kosong atau kamu telah memasukan jumlah maksimal silahkan hubungi admin kami',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif

@if(Session::has('berhasilupdateprofile'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'BERHASIL',
                    'data profile kamu berhasil di rubah',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif

@if(Session::has('passwordsalah'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'MAAF',
                    'pastikan pasword yang kamu masukan benar',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif

@if(Session::has('passwordincorrect'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'MAAF',
                    'pastikan pasword baru dan confirm password baru sesuai',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif

@if(Session::has('stokmaksimal'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'MAAF',
                    'mohon maap kamu sudah memasukan maksimal stok ke dalam chart',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif

@if(Session::has('passwordberhasildiupdate'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'SUKSES',
                    'pasword kamu berhasil di rubah',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('berhasilrencanabayar'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'SUKSES',
                    'inv berhasil di masukan ke rencana pembayaran',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('berhasilhapusrencana'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'SUKSES',
                    'inv berhasil di hapus dari rencana pembayaran',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('berhasilmelakukanpembayaran'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'SUKSES',
                    'pembayaran berhasil di kirim, silahkan tunggu review dari admin',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('pembayarandiupdate'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'SUKSES',
                    'status pembayaran berhasil di update',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('berhasiltambahbank'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'SUKSES',
                    'rekening baru berhasil di tambahkan',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('berhasiltambahbarang'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'SUKSES',
                    'barang berhasil di tambahkan',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('berhasiltambahcabang'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'SUKSES',
                    'barang berhasil di tambahkan',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('berhasilhapuscabang'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'SUKSES',
                    'cabang berhasil di hapus',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('hapusitemchartsukses'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'SUKSES',
                    'item di chart berhasil di hapus',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('itemnotfound'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'code barang tersebut tidak di temukan di dalam database',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('berhasilecer'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'BERHASIL',
                    'stok 1 box berhasil di rubah ke stok eceran',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('berhasilbatalkanpesanan'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'BERHASIL',
                    'pesanan berhasil di batalkan dan stok berhasil di kembalikan ke inventory',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('notierlevel'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'akun kamu belum memiliki level tier, harap hubungi super admin untuk mendapatkan tier level',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('tempoberhasil'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'BERHASIL',
                    'tempo berhasil di bayarkan',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('rekeningkosong'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'pilih rekening tujuan',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('barangkosong'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'lokasi toko tidak sesuai dengan lokasi barang yang di pilih atau barang yang kamu input tidak ada di inventory cabang tujuan',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('invexist'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'invoice marketplace yang kamu masukan sudah ada di database, pastikan kamu memasukan inv marketplace yang benar',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('berhasilcair'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'nilai pencairan berhasil di input',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('pencairanover'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'nilai yang kamu input lebih besar dari nilai pembelian coba cek kembali dana yang cair dari marketplace',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('pencairankosong'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'nilai pencairan dana tidak boleh kosong',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('stokdatangtidaksesuai'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'stok yang di terima tidak sesuai stok sampai sudah di masukan ke stok cabang dan masi ada stok yang di tunggu segera hubungi purchasing',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('stokdatangterlalubanyak'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'STOK DATANG LEBIH DARI YANG DI PESAN HARAP SEGERA HUBUNGI PURCHASING',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('stokberhasildipindah'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'stok berhasil di pindah ke toko cabang',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('pencairan50'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'pencairan tidak boleh lebih kecil dari 50% nilai teransaksi',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif       

@if(Session::has('rolebaru'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'role baru berhasil ditambahkan',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('berhasilhapusrole'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'role berhasil di hapus',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('superadmin'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'role super admin tidak bisa di hapus dan di rubah',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif       

@if(Session::has('updaterole'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'role berhasil di update',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('berhasilbuatmarketplace'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'kamu berhasil menambahkan list marketplace ke dalam database',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('berhasilupdatedata'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'kamu berhasil meng update data',
                    'success'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('jumlahdipindahover'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'jumah stok yang di pindah tidak bisa lebih besar dari jumlah stok asal',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('lokasisalah'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'lokasi yang kamu tuju salah, coba lokasi lain',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('kesalahantier'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'user tidak memiliki tier, harap berikan tier kepada user untuk menggunakan fitur chart',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('marketplaceterdaftar'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'marketplace yang kamu masukan sudah teraftar',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('sachetdanboxmax'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'kamu memasukan jumlah maksimal karna produk dalam bentuk box habis maka produk sachet tidak dapat di tambahkan, kamu hanya bisa memesan sejumlah yang tersedia',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('pilihcabangdulu'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'pilih cabang yang mau di print dulu ya guys',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    

@if(Session::has('permintaansachetkebanyakan'))
        <script type="text/javascript">

        function massge() {
        Swal.fire(
                    'PERHATIAN',
                    'kamu merequest sachet lebih dari satu box, hubungi admin untuk kebutuhan kamu',
                    'warning'
                );
        }

        window.onload = massge;
        </script>
@endif    
  