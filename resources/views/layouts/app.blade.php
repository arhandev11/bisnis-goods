<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title><?php echo $page ?></title>
    <link rel="apple-touch-icon" href="{{ asset('app-assets/images/ico/ego.jpg') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('app-assets/images/ico/ego.jpg') }}">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- select2 -->
    <script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/libs/select2/select2.css') }}" />

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/animate/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/libs/bootstrap-select/bootstrap-select.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/libs/typeahead-js/typeahead.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/libs/tagify/tagify.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/libs/perfect-scrollbar/perfect-scrollbar.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/libs/node-waves/node-waves.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/libs/apex-charts/apex-charts.css') }}" />
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/bordered-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/semi-dark-layout.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/form-validation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/authentication.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pages/page-profile.css') }}" />
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <!-- END: Custom CSS-->



    <!-- Helpers -->
    <script src="{{ asset('app-assets/vendors/js/helpers.js') }}"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="{{ asset('app-assets/assets/js/config.js') }}"></script>



</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu-modern  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="">

    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow container-xxl">
        <div class="navbar-container d-flex content">
            <div class="bookmark-wrapper d-flex align-items-center">
                <ul class="nav navbar-nav d-xl-none">
                    <li class="nav-item"><a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i></a></li>
                </ul>
            </div>
            <ul class="nav navbar-nav align-items-center ms-auto">
                <li class="nav-item d-none d-lg-block">
                    <a class="nav-link nav-link-style">
                        <i class="ficon" data-feather="moon"></i>
                    </a>
                </li>
                <li style="display:none" class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon" data-feather="search"></i></a>
                    <div class="search-input">
                        <div class="search-input-icon"><i data-feather="search"></i></div>
                        <input class="form-control input" type="text" placeholder="Explore Vuexy..." tabindex="-1" data-search="search">
                        <div class="search-input-close"><i data-feather="x"></i></div>
                        <ul class="search-list search-list-main"></ul>
                    </div>
                </li>
                <li class="nav-item dropdown dropdown-notification me-25"><a class="nav-link" href="#" data-bs-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span class="badge rounded-pill bg-danger badge-up">0</span></a>
                    <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                        <li class="dropdown-menu-header">
                            <div class="dropdown-header d-flex">
                                <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                                <div class="badge rounded-pill badge-light-primary">0 New</div>
                            </div>
                        </li>
                        <li class="scrollable-container media-list">
                            @foreach($notifikasiPesanan as $notifikasiPesanan)
                            <a class="d-flex" href="/detailorderreseller/{{$notifikasiPesanan->id}}">
                                <div class="list-item d-flex align-items-start
                                    <?php if ($notifikasiPesanan->tempo == "new") {
                                        echo "bg-light-success";
                                    }else{
                                        echo "";
                                    } ?>">
                                    <div class="me-1">
                                        <i data-feather="box" class="ti-xl"></i>
                                    </div>
                                    <div class="list-item-body flex-grow-1">
                                        <p class="media-heading"><span class="fw-bolder">Pesanan Baru Dari {{$notifikasiPesanan->nama_pengirim}}</p><small class="notification-text">{{$notifikasiPesanan->jumlah_barang}} Item Senilai @currency($notifikasiPesanan->tagihan)</small>
                                    </div>
                                </div>
                            </a>
                            @endforeach
                            <div class="list-item d-flex align-items-center">
                                <h6 class="fw-bolder me-auto mb-0">System Notifications</h6>
                                <div class="form-check form-check-primary form-switch">
                                    <input class="form-check-input" id="systemNotification" type="checkbox" checked="">
                                    <label class="form-check-label" for="systemNotification"></label>
                                </div>
                            </div>
                            <a class="d-flex" href="#">
                                <div class="list-item d-flex align-items-start">
                                    <div class="me-1">
                                        <div class="avatar bg-light-success">
                                            <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                        </div>
                                    </div>
                                    <div class="list-item-body flex-grow-1">
                                        <p class="media-heading"><span class="fw-bolder">System </span>&nbsp;Baik</p><small class="notification-text"> Tidak ada kendala</small>
                                    </div>
                                </div>
                            </a>
                            <a class="d-flex" href="#">
                                <div class="list-item d-flex align-items-start">
                                    <div class="me-1">
                                        <div class="avatar bg-light-success">
                                            <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                        </div>
                                    </div>
                                    <div class="list-item-body flex-grow-1">
                                        <p class="media-heading"><span class="fw-bolder">Gratis</span>&nbsp;Penggunaan 6 bulan</p><small class="notification-text"> sisa 6 bulan</small>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-menu-footer"><a class="btn btn-primary w-100" href="#">Read all notifications</a></li>
                    </ul>
                </li>
                @guest
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                  </li>
                    @if (Route::has('register'))
                    @endif
                @else
                <li class="nav-item dropdown dropdown-user">
                  <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="user-nav d-sm-flex d-none">
                          <span class="user-name fw-bolder">{{ Auth::user()->name }}</span>
                          <span class="user-status">{{$userRoleName->role_name}}</span>
                        </div>
                          <span class="avatar"><img class="round" src="../../img/{{ Auth::user()->foto }}" alt="avatar" height="40" width="40"><span class="avatar-status-online"></span></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                      <a class="dropdown-item" href="/profile"><i class="me-50" data-feather="user"></i> Profile</a>
                      <a style="display:none" class="dropdown-item" href="app-email.html"><i class="me-50" data-feather="mail"></i> Inbox</a>
                      <a style="display:none" class="dropdown-item" href="app-todo.html"><i class="me-50" data-feather="check-square"></i> Task</a>
                      <a style="display:none" class="dropdown-item" href="app-chat.html"><i class="me-50" data-feather="message-square"></i> Chats</a>
                    <div class="dropdown-divider"></div>
                      <a style="display:" class="dropdown-item" href="/setings"><i class="me-50" data-feather="settings"></i> Settings</a>
                      <a style="display:none" class="dropdown-item" href="page-pricing.html"><i class="me-50" data-feather="credit-card"></i> Pricing</a>
                      <a style="display:none" class="dropdown-item" href="page-faq.html"><i class="me-50" data-feather="help-circle"></i> FAQ</a>
                      <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="me-50" data-feather="power"></i> Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                          @csrf
                        </form>
                    </div>
                </li>
            </ul>
          @endguest
        </div>
    </nav>

    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header mb-1">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item me-auto">
                  <a class="navbar-brand" >
                            <span class="brand-logo">
                              <img class="round" src="../../dist/img/ego.jpg">
                            </span>
                        <h2 class="brand-text">BisnisGood</h2>
                    </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li style="display:<?php if($menu->dashboard == "tampil"){echo "block";}else{echo "none";} ?>" class="<?php if($page == "dashboard"){echo 'active';}else{echo '';} ?> nav-item">
                  <a class="d-flex align-items-center" href="/home">
                    <i data-feather="home"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboards">Dashboards</span>
                  </a>
                </li>
                <li style="display:<?php if($menu->system_transaksi == "tampil"){echo "block";}else{echo "none";} ?>" class="<?php if($page == "shop"){echo 'active';}else{echo '';} ?> nav-item">
                  <a class="d-flex align-items-center" href="/{{$halamanChart}}">
                    <i data-feather="shopping-cart"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboards">System Transaksi</span>
                  </a>
                </li>
                <li style="display:<?php if($menu->inventory == "tampil"){echo "block";}else{echo "none";} ?>" class="<?php if($page == "stok"){echo 'active';}else{echo '';} ?> nav-item">
                  <a class="d-flex align-items-center" href="/stok">
                    <i data-feather="archive"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboards">System Inventory</span>
                  </a>
                </li>
                <li style="display:<?php if($menu->list_cabang == "tampil"){echo "block";}else{echo "none";} ?>" class="<?php if($page == "cabang"){echo 'active';}else{echo '';} ?> nav-item">
                  <a class="d-flex align-items-center" href="/listcabang">
                    <i data-feather="truck"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboards">List Toko Cabang</span>
                  </a>
                </li>
                <li style="display:<?php if($menu->pesanan_agen == "tampil"){echo "block";}else{echo "none";} ?>" class="nav-item">
                  <a class="d-flex align-items-center" href="/home">
                    <i data-feather="shopping-bag"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboards">List Pesanan</span>
                    <span class="badge badge-light-success text-danger badge-center ms-1">{{$countNotifPesanan}}</span>
                  </a>
                  <ul class="menu-sub">
                    <li class="<?php if($page == "pesananagen"){echo 'active';}else{echo '';} ?> nav-item">
                      <a href="/pesananagen" class="d-flex align-items-center">
                        <i class="bg-light-success" data-feather="circle"></i>
                        <div>Pesanan Agen</div>
                        <span class="badge badge-light-warning text-danger badge-center ms-1">{{$countNotifPesananagen}}</span>
                      </a>
                    </li>
                    <li class="<?php if($page == "pesanancs"){echo 'active';}else{echo '';} ?> nav-item">
                      <a href="/pesanancs" class="d-flex align-items-center">
                        <i class="bg-light-success" data-feather="circle"></i>
                        <div>Pesanan Cs</div>
                        <span class="badge badge-light-warning text-danger badge-center ms-1">{{$countNotifPesanancs}}</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li style="display:<?php if($menu->pembayaran_agen == "tampil"){echo "block";}else{echo "none";} ?>" class="<?php if($page == "pembayaran"){echo 'active';}else{echo '';} ?> nav-item">
                      <a href="/pembayaran" class="d-flex align-items-center">
                        <i data-feather="credit-card"></i>
                        <div>Pembayaran Agen</div>
                        <span class="badge badge-light-success text-danger badge-center ms-1">{{$countPembayaran}}</span>
                      </a>
                </li>
                <li style="display:<?php if($menu->tagihan == "tampil"){echo "block";}else{echo "none";} ?>" class="<?php if($page == "historyreseller"){echo 'active';}else{echo '';} ?> nav-item">
                   <a href="/historyreseller" class="d-flex align-items-center">
                        <i class="menu-icon" data-feather="file-minus"></i>
                        <div>Tagihan Agen</div>
                   </a>
                </li>
                <li style="display:<?php if($menu->history_belanja == "tampil"){echo "block";}else{echo "none";} ?>" class="<?php if($page == "history"){echo 'active';}else{echo '';} ?> nav-item">
                   <a href="/historybelanja" class="d-flex align-items-center">
                    <i data-feather="clock"></i>
                        <div>History Belanja</div>
                   </a>
                </li>
                <li style="display:<?php if($menu->keuangan == "tampil"){echo "block";}else{echo "none";} ?>" class="nav-item">
                  <a class="d-flex align-items-center" href="/home">
                    <i data-feather="feather"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboards">Keuangan</span>
                    <span class="badge badge-light-success text-danger badge-center ms-1">{{$countNotifReqBayar}}</span>
                  </a>
                  <ul class="menu-sub">
                    <li class="<?php if($page == "laporan"){echo 'active';}else{echo '';} ?> nav-item">
                      <a class="d-flex align-items-center" href="/keuangan">
                        <i class="bg-light-success" data-feather="circle"></i>
                        <span class="menu-title text-truncate" data-i18n="Dashboards">Keuangan base</span>
                      </a>
                    </li>
                    <li class="<?php if($page == "penerimaan"){echo 'active';}else{echo '';} ?> nav-item">
                      <a class="d-flex align-items-center" href="/listpenerimaan">
                        <i class="bg-light-success" data-feather="circle"></i>
                        <span class="menu-title text-truncate" data-i18n="Dashboards">List Pencairan</span>
                      </a>
                    </li>
                    <li class="<?php if($page == "laporanadvance"){echo 'active';}else{echo '';} ?> nav-item">
                      <a class="d-flex align-items-center" href="/keuanganadvance">
                        <i class="bg-light-success" data-feather="circle"></i>
                        <span class="menu-title text-truncate" data-i18n="Dashboards">Keuangan Advance</span>
                      </a>
                    </li>
                    <li class="<?php if($page == "pembayaran"){echo 'active';}else{echo '';} ?> nav-item">
                      <a class="d-flex align-items-center" href="/pembayaran">
                        <i class="bg-light-success" data-feather="circle"></i>
                        <span class="menu-title text-truncate" data-i18n="Dashboards">List Pembayaran</span>
                    <span class="badge badge-light-warning text-danger badge-center ms-1">{{$countNotifReqBayar}}</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li style="display:<?php if($menu->settings == "tampil"){echo "block";}else{echo "none";} ?>" class="nav-item">
                  <a class="d-flex align-items-center" href="/home">
                    <i data-feather="settings"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboards">Settings</span>
                  </a>
                  <ul class="menu-sub">
                    <li class="<?php if($page == "User"){echo 'active';}else{echo '';} ?> nav-item">
                      <a class="d-flex align-items-center" href="/user">
                        <i class="bg-light-success" data-feather="circle"></i>
                        <span class="menu-title text-truncate" data-i18n="Dashboards">Pengguna</span>
                      </a>
                    </li>
                    <li class="<?php if($page == "role"){echo 'active';}else{echo '';} ?> nav-item">
                      <a class="d-flex align-items-center" href="/role">
                        <i class="bg-light-success" data-feather="circle"></i>
                        <span class="menu-title text-truncate" data-i18n="Dashboards">Role</span>
                      </a>
                    </li>
                    <li class="<?php if($page == "setings"){echo 'active';}else{echo '';} ?> nav-item">
                      <a class="d-flex align-items-center" href="/setingsstore">
                        <i class="bg-light-success" data-feather="circle"></i>
                        <span class="menu-title text-truncate" data-i18n="Dashboards">Toko</span>
                      </a>
                    </li>
                    <li class="<?php if($page == "marketplace"){echo 'active';}else{echo '';} ?> nav-item">
                      <a class="d-flex align-items-center" href="/listmarketplace">
                        <i class="bg-light-success" data-feather="circle"></i>
                        <span class="menu-title text-truncate" data-i18n="Dashboards">List Marketplace</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li style="display:<?php if($menu->reseller_chart == "tampil"){echo "block";}else{echo "none";} ?>" class="<?php if($page == "reseller"){echo 'active';}else{echo '';} ?> nav-item">
                  <a class="d-flex align-items-center" href="/reseller">
                    <i data-feather="users"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboards">Reseller Chart</span>
                  </a>
                </li>
                <li style="display:<?php if($menu->reseller_order == "tampil"){echo "block";}else{echo "none";} ?>" class="nav-item">
                  <a class="d-flex align-items-center" href="/home">
                    <i data-feather="box"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboards">Reseller Order</span>
                  </a>
                  <ul class="menu-sub">
                    <li class="<?php if($page == "resellerorder"){echo 'active';}else{echo '';} ?> nav-item">
                      <a class="d-flex align-items-center" href="/resellerorder">
                        <i class="bg-light-success" data-feather="circle"></i>
                        <span class="menu-title text-truncate" data-i18n="Dashboards">Daftar Pesanan</span>
                      </a>
                    </li>
                    <li class="<?php if($page == "historypembayaranreseller"){echo 'active';}else{echo '';} ?> nav-item">
                      <a class="d-flex align-items-center" href="/historypembayaranreseller">
                        <i class="bg-light-success" data-feather="circle"></i>
                        <span class="menu-title text-truncate" data-i18n="Dashboards">History Pembayaran</span>
                      </a>
                    </li>
                  </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->
            @yield('content')
            @extends('layouts.footer')