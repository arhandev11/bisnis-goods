<script type="text/javascript">
     if (document.readyState == 'loading') {
    document.addEventListener('DOMContentLoaded', ready)
} else {
    ready()
}

function ready() {
    var removeCartItemButtons = document.getElementsByClassName('btn-danger')
    for (var i = 0; i < removeCartItemButtons.length; i++) {
        var button = removeCartItemButtons[i]
        button.addEventListener('click', removeCartItem)
    }

    var quantityInputs = document.getElementsByClassName('cart-quantity-input')
    for (var i = 0; i < quantityInputs.length; i++) {
        var input = quantityInputs[i]
        input.addEventListener('change', quantityChanged)
    }

    var addToCartButtons = document.getElementsByClassName('shop-item-button')
    for (var i = 0; i < addToCartButtons.length; i++) {
        var button = addToCartButtons[i]
        button.addEventListener('click', addToCartClicked)
    }
}

function removeCartItem(event) {
    var buttonClicked = event.target
    buttonClicked.parentElement.parentElement.remove()
    updateCartTotal()
    updateCartqty()
    FunctionZeroDiskon()
    FunctionZeroKembalian()
}

function quantityChanged(event) {
    var input = event.target
    if (isNaN(input.value) || input.value <= 0) {
        input.value = 1
    }
    updateCartTotal()
    updateCartqty()
    FunctionZeroDiskon()
    FunctionZeroKembalian()
}

function addToCartClicked(event) {
    var button = event.target
    var shopItem = button.parentElement.parentElement
    var title = shopItem.getElementsByClassName('shop-item-title')[0].innerText
    var price = shopItem.getElementsByClassName('shop-item-price')[0].innerText
    var code = shopItem.getElementsByClassName('shop-item-code')[0].innerText
    addItemToCart(title, price, code)
    updateCartTotal()
    updateCartqty()
    FunctionZeroDiskon()
    FunctionZeroKembalian()
}

function addItemToCart(title, price, code) {
    var cartRow = document.createElement('tr')
    cartRow.classList.add('cart-row')
    var cartItems = document.getElementsByClassName('cart-items')[0]
    var cartItemNames = cartItems.getElementsByClassName('cart-item-title')
    for (var i = 0; i < cartItemNames.length; i++) {
        if (cartItemNames[i].value == title) {
            alert('This item is already added to the cart')
            return
        }
    }
    var cartRowContents = `
                   <tr class="cart-item">
                    <th class="text-center col-md-1 pr-1  pb-1"><input name="codeBarang[${i}]" value="${code}" type="text" class="form-control" readonly></th>
                    <th class="text-center col-md-4 pr-1  pb-1"><input name="title[${i}]" value="${title}" type="text" class="cart-item-title form-control" readonly></th>
                    <th class="text-center col-md-3 pr-1  pb-1"><input name="harga[${i}]" value="${price}" type="text" class="cart-price form-control" readonly></th>
                    <th class="text-center col-md-3 pr-1  pb-1"><input name="jumlah[${i}]" value="1" type="number" class="cart-quantity-input form-control"></th>
                    <th class="text-center col-md-1 pb-1"><button class="btn btn-danger pb-1" type="button">HAPUS</button></th>
                  </tr>
`
    cartRow.innerHTML = cartRowContents
    cartItems.append(cartRow)
    cartRow.getElementsByClassName('btn-danger')[0].addEventListener('click', removeCartItem)
    cartRow.getElementsByClassName('cart-quantity-input')[0].addEventListener('change', quantityChanged)
}

function updateCartTotal() {
    var cartItemContainer = document.getElementsByClassName('cart-items')[0]
    var cartRows = cartItemContainer.getElementsByClassName('cart-row')
    var total = 0
    for (var i = 0; i < cartRows.length; i++) {
        var cartRow = cartRows[i]
        var priceElement = cartRow.getElementsByClassName('cart-price')[0]
        var quantityElement = cartRow.getElementsByClassName('cart-quantity-input')[0]
        var price = parseFloat(priceElement.value.replace('$',''))
        var quantity = quantityElement.value
        total = total + (price * quantity)
    }
    total = Math.round(total * 100) / 100
    document.getElementsByClassName('cart-total-price')[0].value =total
    document.getElementsByClassName('cart-total-bayar')[0].value =total
}

function updateCartqty() {
    var cartItemContainer = document.getElementsByClassName('cart-items')[0]
    var cartRows = cartItemContainer.getElementsByClassName('cart-row')
    var total = 0
    for (var i = 0; i < cartRows.length; i++) {
        var cartRow = cartRows[i]
        var priceElement = cartRow.getElementsByClassName('cart-price')[0]
        var quantityElement = cartRow.getElementsByClassName('cart-quantity-input')[0]
        var qty = parseFloat(quantityElement.value.replace('$',''))
        var quantity = quantityElement.value
        total = total + (qty)
    }
    total = Math.round(total * 100) / 100
    document.getElementsByClassName('cart-total-qty')[0].value =total
}

function FunctionDiskon() {
  var tagihan = document.getElementById("tagihan").value;
  var diskon = document.getElementById("diskon").value;
  var total = parseInt(tagihan) - parseInt(diskon);
  if (!isNaN(total)){
           document.getElementById('bayar').value = total;
        }
}

function FunctionZeroDiskon() {
  var zero = 0
  if (!isNaN(zero)){
           document.getElementById('diskon').value = zero;
        }
}

function FunctionKembalian() {
  var cash = document.getElementById("cash").value;
  var bayar = document.getElementById("bayar").value;
  var total = parseInt(cash) - parseInt(bayar);
  if (!isNaN(total)){
           document.getElementById('kembalian').value = total;
        }
}

function FunctionZeroKembalian() {
  var zero = 0
  if (!isNaN(zero)){
           document.getElementById('cash').value = zero;
           document.getElementById('kembalian').value = zero;
        }
}

</script>