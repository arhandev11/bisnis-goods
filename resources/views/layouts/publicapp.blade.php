<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <script src="https://cdn.tailwindcss.com"></script>
      
      @yield('assets')
    </head>

<!-- BEGIN: Body-->
<body>
    <header class="border-b border-solid border-gray-300 shadow-sm px-4">
      <div class="mx-auto max-w-7xl flex justify-between items-center py-4">
        <a href="/" >
        <img src="../../dist/img/logo.png" class="w-40" alt="" />
        </a>
            <a href="/chartpublic" class="border border-blue-600 rounded-lg text-blue-600 font-bold text-sm py-1.5 px-3 sm:text-base sm:py-2 sm:px-5 md:hidden">
                {{$totalbarangdichart}} Item
            </a>
        <img
          src="../../dist/img/three-horizontal-lines-icon.svg"
          class="block md:hidden w-6"
          alt=""
        />

        <div class="hidden md:flex items-center gap-3">
            <a href="/chartpublic" class="border border-blue-600 rounded-lg text-blue-600 font-bold text-sm py-1.5 px-3 sm:text-base sm:py-2 sm:px-5">
                {{$totalbarangdichart}} Item
            </a>
            <a href="/login" class="border border-blue-600 rounded-lg bg-blue-600 text-white font-bold py-1.5 px-3 sm:text-base sm:py-2 sm:px-5">
                Login
            </a>
        </div>
      </div>
    </header>


                
    <!-- END: Main Menu-->
            @yield('content')
            @yield('scripts')
            @extends('layouts.footer')
</body>