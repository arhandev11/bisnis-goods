<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
</head>
<body>
    <select class="first-map-search" name="state" style="width: 300px">
    </select>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.first-map-search').select2({
                ajax: {
                    url: "{{route('first-map-search')}}",
                    delay: 250,
                    dataType: "json",
                    data: (params)=>{
                        var query = {
                            search: params.term
                        }
                        return query;
                    },
                    processResults: function(data){
                        console.log(data);
                        return {
                            results: data.data
                        }
                    }
                },
                placeholder: "Cari Lokasi",
            })
        })
    </script>
</body>
</html>