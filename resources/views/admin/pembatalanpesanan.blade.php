@extends('layouts.app')
<?php $page = "shop" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Formulir Pembatalan Pesanan</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="stok">Formulir Pembatalan Pesanan</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-xl-9 col-md-9 col-12 mb-md-0">
                  <div class="card invoice-preview-card">
                    <div class="table-responsive border-top">
                      <table class="table m-0">
                        <thead>
                          <tr>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Qty</th>
                            <th>Sub Total</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($item as $detailbarang)
                          <tr>
                            <td class="text-nowrap">{{$detailbarang->title}}</td>
                            <td>@currency($detailbarang->harga)</td>
                            <td>{{$detailbarang->jumlah}}</td>
                            <td>@currency($detailbarang->harga * $detailbarang->jumlah)</td>
                          </tr>
                        @endforeach
                          <tr>
                            <td colspan="2" class="align-top px-4 py-4">
                            </td>
                            <td class="text-end">
                              <p class="mb-2">Subtotal:</p>
                              <p class="mb-2">Discount:</p>
                              <p class="mb-0">Total:</p>
                            </td>
                        @foreach($invoice as $invoice)
                            <td >
                              <p class="fw-semibold">@currency($invoice->tagihan)</p>
                              <p class="fw-semibold">@currency($invoice->diskon)</p>
                              <p class="fw-semibold mb-0">@currency($invoice->tagihan - $invoice->diskon)</p>
                            </td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="card col-md-3">
                    <div class="card-body">
                        <form id="formpembatalan" method="POST" action="/prosespembatalanpesanan">
                            @csrf
                        @foreach($item as $index => $barang)
                            <input type="hidden" name="idbarang[{{$index}}]" value="{{$barang->id_barang}}">
                            <input type="hidden" name="codebarang[{{$index}}]" value="{{$barang->code_barang}}">
                            <input type="hidden" name="namaItem[{{$index}}]" value="{{$barang->title}}">
                            <input type="hidden" name="hargaSatuan[{{$index}}]" value="{{$barang->harga}}">
                            <input type="hidden" name="jumlah[{{$index}}]" value="{{$barang->jumlah}}" >
                            <input type="hidden" name="tanggal_batal[{{$index}}]" type="text" value="{{date('Y-m-d')}}">
                        @endforeach
                            <input type="hidden" name="id" value="{{$id}}">
                            <label>Jenis Pembatalan</label>
                            <select class="form-control mb-2" name="jenis_pembatalan" required>
                                <option></option>
                                <option>Retur Cod</option>
                                <option>Retur Expedisi</option>
                                <option>Pembatalan Pembeli</option>
                            </select>
                            <label>Alasan Pembatalan</label>
                            <textarea class="form-control" name="alasan_pembatalan" required></textarea>
                            <div class="btn-group mt-2">
                            <button type="submit" class="btn btn-primary pembatalanperif">Simpan</button>
                            </div>
                        </form>
                    </div>
                  </div>
@endsection