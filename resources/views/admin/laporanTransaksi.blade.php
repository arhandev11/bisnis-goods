@extends('layouts.app')
<?php $page = "Laporan Transaksi" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- PANJANG TABLE CARD -->
            
          <!-- PANJANG TABLE CARD -->
            <div class="col-lg-12">

              
               @if ($message = Session::get('suksesInputPenjualan'))
              <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>{{ $message }}</strong>
              </div>
              @endif
              
              <div class="form-group row col-md-12">

              <div class="form-group pr-1">
                <input class="form-control" type="date">
              </div>
              <div class="form-group pr-1">
                <input class="form-control" type="date">
              </div>
              <div class="form-grou">
                <button class="btn btn-lg btn-primary"><i class="fas fa-search"></i></button>
              </div>
            </div>
              <!-- TABLE CARD -->
            <div class="card">
             <table class="table table-striped">
                  <thead>
                  <tr>
                    <th class="text-center">Invoice</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Jumlah</th>
                    <th class="text-center">Tagihan</th>
                    <th class="text-center">Diskon</th>
                    <th class="text-center">Bayar</th>
                    <th class="text-center">Pembayaran</th>
                    <th class="text-center">Tempo</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($invoice as $inv)
                  <tr class="shop-item">
                    <td class="text-center">{{ $inv->invoice}}{{ $inv->id}}</td>
                    <td class="text-center">{{ $inv->tanggal}}</td>
                    <td class="text-center">{{ $inv->jumlah_barang}}</td>
                    <td class="text-center">{{ $inv->tagihan}}</td>
                    <td class="text-center">{{ $inv->diskon}}</td>
                    <td class="text-center">{{ $inv->bayar}}</td>
                    <td class="text-center">{{ $inv->pembayaran}}</td>
                    <td class="text-center">{{ $inv->tempo}}</td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
            </div>
            <!-- / TABLE CARD -->
          </div>
          <!-- /.col-md-12 -->
          <br>

                <div class="form-group pl-2">
                {{$invoice->links()}}
                </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<script>
$(document).ready( function () {
    $('#myTable').DataTable({
      "ordering": false,
    });
} );
</script>
@endsection
