@extends('layouts.app')
<?php $page = "role" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Edit Role</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="stok">Edit Role</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




          <!-- PANJANG TABLE CARD -->
            <div class="col-lg-12">
                <!-- Multi Column with Form Separator -->
              <div class="card mb-4">
                <h5 class="card-header">Formulir Edit Role</h5>
                <form method="get" action="/submiteditrole/{{$id}}">
                    @csrf
                <div  class="card-body">
                  <div class="row g-3">
                    
                            <div class="modal-body">
                              <div class="row">
                                <div class="col mb-1">
                                  <label for="nameLarge" class="form-label">Nama Role</label>
                                  <input name="nama_role" type="text" class="form-control" value="{{$role->role_name}}" required>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col mb-3">
                                  <label for="nameLarge" class="form-label">Home Page</label>
                                  <select name="home_menu" class="selectpicker form-control" required>
                                    <option selected value="{{$role->home_menu}}" >{{$role->home_menu}}</option>
                                    <option value="shop">Syste Transaksi</option>
                                    <option value="stok">System Inventory</option>
                                    <option value="listcabang">List Toko Cabang</option>
                                    <option value="pesananagen">Pesanan Agen</option>
                                    <option value="pembayaran">Pembayaran Agen</option>
                                    <option value="historyreseller">Tagihan Agen</option>
                                    <option value="historybelanja">History belanja</option>
                                    <option value="keuanganadvance">Keuangan</option>
                                    <option value="user">Settings</option>
                                    <option value="resellerchart">Chart Reseller</option>
                                    <option value="resellerorder">Reseller Order List</option>
                                  </select>
                                </div>
                              </div>
                              <label class="form-group">System Store Menu</label>
                              <br>
                              <label class="dropdown-divider">fasdfafadsfasasdfasdf</label>
                              <div class="row g-2 mb-1">
                                <div class="col mb-0">
                                  <label class="form-label">Dashboard</label>
                                    <select name="dashboard" class="form-control selectpicker" required>
                                        <option >{{$role->dashboard}}</option>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">System Transaksi</label>
                                    <select name="system_transaksi" class="form-control selectpicker" required>
                                        <option >{{$role->system_transaksi}}</option>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">System Inventory</label>
                                    <select name="system_inventory" class="form-control selectpicker" required>
                                      <option >{{$role->inventory}}</option>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">List Cabang</label>
                                    <select name="list_cabang" class="form-control selectpicker" required>
                                      <option >{{$role->list_cabang}}</option>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">Pesanan Agen</label>
                                    <select name="pesanan_agen" class="form-control selectpicker" required>
                                      <option >{{$role->pesanan_agen}}</option>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                              </div>
                              <div class="row g-2 mb-1">
                                <div class="col mb-0">
                                  <label class="form-label">Pembayaran Agen</label>
                                    <select name="pembayaran_agen" class="form-control selectpicker" required>
                                      <option >{{$role->pembayaran_agen}}</option>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">Tagihan Agen</label>
                                    <select name="tagihan" class="form-control selectpicker" required>
                                      <option >{{$role->tagihan}}</option>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">History Belanja</label>
                                    <select name="history_belanja" class="form-control selectpicker" required>
                                      <option >{{$role->history_belanja}}</option>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">Keuangan</label>
                                    <select name="keuangan" class="form-control selectpicker" required>
                                      <option >{{$role->keuangan}}</option>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">Settings</label>
                                  <select name="settings" class="form-control selectpicker" required>
                                      <option >{{$role->settings}}</option>
                                    <option value="tampil">Tampilkan</option>
                                    <option value="sembunyi">Sembunyikan</option>
                                  </select>
                                </div>
                              </div>

                              <label class="form-group mt-2">System Reseller Menu</label>
                              <br>
                              <label class="dropdown-divider">fasdfafadsfasasdfasdf</label>

                              <div class="row g-2 mb-1">
                                <div class="col mb-0">
                                  <label class="form-label">Chart Reseller</label>
                                    <select name="resellerchart" class="form-control selectpicker" required>
                                      <option >{{$role->reseller_chart}}</option>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">Reseller Order List</label>
                                    <select name="resellerorder" class="form-control selectpicker" required>
                                      <option >{{$role->reseller_order}}</option>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                              </div>
                            </div>
                  <div class="pt-4">
                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                  </div>
                </form>
              </div>
            </div>
    <!-- END: Content-->

@endsection