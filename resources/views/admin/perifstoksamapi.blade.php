@extends('layouts.app')
<?php $page = "stok" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Formulir Stok Sampai</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="stok">form stok sampai</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




          <!-- PANJANG TABLE CARD -->
            <div class="col-lg-12">
                <!-- Multi Column with Form Separator -->
              <div class="card mb-4">
                <form method="get" action="/stoksampai/{{$id}}">
                <div  class="card-body">
                @csrf
                @foreach($detailbelanja as $belanja)
                  <div class="row g-3">
                    <div class="col-md-2">
                      <label class="form-label" for="multicol-username">Id Belanja</label>
                      <input type="text" class="form-control" name="id_belanja" value="{{$belanja->id}}" readonly />
                    </div>
                    <div class="col-md-10">
                      <label class="form-label" for="multicol-username">Nama Barang</label>
                      <input type="text" class="form-control" name="nama_barang" value="{{$belanja->nama_barang}}" readonly/>
                    </div>
                    <div class="col-md-2">
                      <label class="form-label" for="multicol-username">Code Barang</label>
                      <input type="text" class="form-control" name="code_barang" value="{{$belanja->code_barang}}" readonly />
                    </div>
                    <div class="col-md-4">
                      <label class="form-label" for="multicol-username">Toko Cabang</label>
                      <input type="text" class="form-control" name="toko_cabang" value="{{$belanja->tipe}}" readonly />
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Stok Di Beli</label>
                      <input type="text" class="form-control" name="stok_dibeli" value="{{$belanja->stok}}" readonly />
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Stok Datang</label>
                      <input type="text" class="form-control" name="stok_datang" type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Keterangan</label>
                      <textarea type="text" class="form-control" name="keterangan" required></textarea>
                    </div>
                  </div>
                  @endforeach
                  <div class="pt-4">
                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                  </div>
                </form>
              </div>
            </div>
    <!-- END: Content-->

@endsection