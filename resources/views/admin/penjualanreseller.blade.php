@extends('layouts.app')
<?php $page = "reseller" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
    <!-- Main content -->

      <div class="container-fluid">
        <div class="row">

          <div class="col-md-4 mb-1">
            <form action="/reseller">
              <label for="select2Basic" class="form-label">Pilih Toko</label>
                      <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="hapus seluruh produk untuk pilih ulang toko">
                        <i class="text-primary" data-feather="help-circle"></i>
                      </a>


                <select name="nama_toko" id="select2Basic" class="select2 form-select form-select-lg" data-allow-clear="true" onchange="this.form.submit()" {{$clssChngToko}}>
                  
                  @foreach($listToko as $listToko)
                  <option <?php if($selectedToko == $listToko->nama_cabang){echo "selected";}else{"";} ?>  value="{{ $listToko->nama_cabang}}">{{ $listToko->nama_cabang}}</option>
                  @endforeach
                </select>
            </form>
          </div>
          <div class="col-md-5 mb-1">
            <form action="/reselleraddtochart">
              <label for="select2Basic" class="form-label">Pilih Produk</label>
                <select name="item" id="select2Basic2" class="select2 form-select form-select-lg" data-allow-clear="true" onchange="this.form.submit()" {{$disableproduklist}}>
                  <option value=""></option>
                  @foreach($pilihBarang as $st)
                  <option value="{{$st->id}}">{{ $st->nama_barang}} | stok tersedia {{$st->stok}}</option>
                  @endforeach
                </select>
            </form>
          </div>

          <!-- PANJANG TABLE CARD -->
              <div class="col-lg-9">




                <div class="table-responsive card">
                      <table class="table table-striped border-top">
                        <thead class="border-bottom">
                          <tr>
                            <th>Item</th>
                            <th style="width:50%">Qty</th>
                            <th>SubTotal</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($chart as $crt)
                          <tr>
                            <td>
                              <div class="d-flex justify-content-start align-items-center">
                                <div class="d-flex flex-column mb-4">
                                  <p class="mb-0 fw-semibold">{{$crt->nama_barang}}</p>
                                  <small class="text-muted">@currency($crt->harga_jual)</small>
                                </div>
                              </div>
                            </td>
                            <td>
                              <div class="col-md-7">
                                <form action="/Resellerchart/manual" method="get">
                                  <input name="id_barang" type="hidden" value="{{$crt->no_barang_id}}">
                                  <input type="number" class="form-control form-control mb-1" value="{{$crt->jml_barang}}" name="jml">
                                  <button class="btn btn-sm btn-primary waves-effect waves-light mb-1">update</button>
                                  <a href="/hapus/chart/public/{{$crt->no_barang_id}}" class="btn btn-sm btn-danger col-md-2 waves-effect waves-light mb-1"><i data-feather="trash" class="ti-xs"></i></a>
                                </form>
                              </div>
                            </td>
                            <td>
                              <div class="d-flex flex-column mb-5">
                                <p class="mb-0 fw-semibold">@currency($crt->total_harga)</p>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                          <tr>
                            <td></td>
                            <td>Total</td>
                            <td>@currency($subtotal)</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>

              <!-- /.card-body -->
                <div class="card">
                    <div class="card-body">
                        <p class="text-center {{$colloerAllert}} mt-3">{!!$pesanReseller!!}</p>
                    </div>
                </div>
            </div>
            <!-- /.card -->
            <div class="col-lg-3">
            <div class="card">
              <div class="card-body">
                <form id="formChart" action="/prosesReseller/chart" method="POST" enctype="multipart/form-data">
                  @csrf
                    <div class="form-group row">
                      @foreach ($chart as $index => $crt)
                        <input type="hidden" name="codeItem[{{$index}}]" value="{{$crt->id_barang}}" required>
                        <input type="hidden" name="idbarang[{{$index}}]" value="{{$crt->no_barang_id}}">
                        <input type="hidden" name="idChart[{{$index}}]" value="{{$crt->id_chart}}">
                        <input type="hidden" name="namaItem[{{$index}}]" value="{{$crt->nama_barang}}">
                        @php
                          if($crt->jml_barang >= $crt->min_qty){
                              $harga = $crt->agen_price;
                          }elseif($priceLevel->tier == "tr1"){
                              $harga = $crt->tr1;
                          }elseif($priceLevel->tier == "tr2") {
                              $harga = $crt->tr2;
                          }elseif($priceLevel->tier == "tr3") {
                              $harga = $crt->tr3;
                          };
                        @endphp
                        <input type="hidden" name="hargaSatuan[{{$index}}]" value="{{$harga}}">
                        <input type="hidden" name="jmlsbt[{{$index}}]" value="{{$crt->jml_barang}}" >
                        <input type="hidden" name="tanggal_orderar[{{$index}}]" type="datetime-local" value="{{date('Y-m-d')}}">
                      @endforeach
                      <input type="hidden" name="jumlah_barang" type="text" value="{{$totbarang}}">
                      <input type="hidden" name="tanggal_order" type="text" value="{{date('Y-m-d')}}">
                      <input type="hidden" name="tagihan" type="text" value="{{$subtotal}}">
                      <input type="hidden" name="total_modal" type="text" value="{{$submodal}}">
                      <input type="hidden" name="platform" type="text" value="Tempo">
                      <input type="hidden" name="user" type="text" value="{{ Auth::user()->id }}">
                      <input type="hidden" name="nama_pengirim" class="form-control form-control mb-1" value="{{ Auth::user()->toko}}" required>
                      <input type="hidden" name="status_tempo" value="new" class="form-control" required>
                  </div>
                  <div class="form-group row">
                    <label class="row">Pilih Marketplace</label>
                            <select name="platform" class="form-control mb-1" required>
                                <option></option>
                                @foreach($listMarketplace as $marketplace)
                                <option>{{$marketplace->nama_marketplace}}</option>
                                @endforeach
                            </select>
                    <label class="row">Upload Label</label>
                    <input name="pdf" type="file" class="form-control form-control mb-1" placeholder="upload label" required>
                    <label class="row">Inv Marketplace</label>
                    <input id="inv" name="inv_marketplace" type="text" class="form-control form-control mb-1">
                    <label class="row">No Resi</label>
                    <input name="code_booking" class="form-control form-control mb-1"></textarea>
                    <label class="row">Alamat</label>
                    <textarea name="alamat" rows="3" class="form-control form-control mb-1"></textarea>
                  </div>
                  <div class="form-group row">
                    <button class="btn btn-primary mt-1 cegah-double-klik" type="submit">Buat Pesanan</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

@endsection

