@extends('layouts.app')
<?php $page = "historypembayaranreseller" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Reseller Order</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/home">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/resellerorder">Reseller Order</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



          <!-- PANJANG TABLE CARD -->
            <div class="row" >
            <div class="col-lg-12">
                <h5 class="card-header">History Pembayaran</h5>
            <div class="card card-body">
                        <div class="table-responsive mb-3">
                        <table class="table table-striped table-responsive">
                        <thead>
                        <tr>
                          <th>Tiket</th>
                          <th>Total Bayar</th>
                          <th>Status</th>
                          <th>Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($hitoryPembayaran as $hspb)
                        <tr>
                          <td><a >{{$hspb->tiket_pembayaran}}</a></td>
                          <td>@currency($hspb->total_pembayaran)</td>
                          <td>{{ $hspb->status_pembayaran}}</td>
                            <td>
                                <a href="/detailhistorybayar/{{$hspb->tiket_pembayaran}}" class="btn btn-sm btn-primary"><i data-feather="eye"></i></a>
                            </td>
                        </tr>
                          @endforeach
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>

@endsection