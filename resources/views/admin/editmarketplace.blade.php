@extends('layouts.app')
<?php $page = "marketplace" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Form Edit Marketplace</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="stok">form edit marketplace</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

          <!-- PANJANG TABLE CARD -->
            <div class="col-lg-12">
                <!-- Multi Column with Form Separator -->
              <div class="card mb-4">
                <h5 class="card-header">Formulir Edit Marketplace</h5>
                <form method="get" action="/updatemarketplace/{{$id}}">
                  @csrf
                <div  class="card-body">
                  <div class="row g-3">
                    <div class="col-md-3">
                      <label>User Penanggung Jawab</label>
                      <select name="user" class="form-control select2"  required>
                          <option value="{{$detailmarketplace->id}}">{{$detailmarketplace->name}}</option>
                        @foreach($users as $user)
                          <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-3">
                      <label>Limit Belanja</label>
                      <input value="{{$detailmarketplace->limit_belanja}}" name="limit_belanja" class="form-control"  min="0" step="1" type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57"  required>
                    </div>
                  </div>
                  <div class="pt-4">
                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                  </div>
                </form>
              </div>
            </div>
    <!-- END: Content-->

@endsection