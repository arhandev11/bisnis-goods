@extends('layouts.app')
<?php $page = "pembayaran" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Pembayaran</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/home">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="">Pembayaran</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">

              
                <div class="col-xl-8 col-md-8 col-12 mb-md-0">
                  <div class="card invoice-preview-card">
                    <div class="table-responsive border-top">
                      <table class="table m-0">
                        <thead>
                          <tr>
                            <th>Invoice</th>
                            <th>Tanggal</th>
                            <th>Tagihan</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($listInvoice as $listInvoice)
                          <tr>
                            <td class="text-nowrap">{{$listInvoice->invoice}}</td>
                            <td>{{$listInvoice->tanggal}}</td>
                            <td>@currency($listInvoice->tagihan)</td>
                          </tr>
                        @endforeach
                          <tr>
                            <td colspan="1" class="align-top px-1 py-3">
                            </td>
                            <td class="text-end">
                              <p class="mb-0">Total:</p>
                            </td>
                            <td>@currency($totalbayar)</td>
                          </tr >
                        </tbody>
                      </table>
                    </div>
                  </div>


                  <div class="card invoice-preview-card">
                    <div class="table-responsive border-top">
                      <table class="table m-0">
                        <thead>
                          <tr>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Qty</th>
                            <th>Sub Total</th>
                          </tr>
                        </thead>
                        <tbody>
                          <form id="formditerima" method="POST" action="/pembayaranditerima/{{$id}}" enctype="multipart/form-data">
                            @foreach($item as $index => $item)
                              {{-- hidden item untuk mengurangi stok --}}
                                @csrf
                                <input type="hidden" name="idbarang[{{$index}}]" value="{{$item->id_barang}}">
                                <input type="hidden" name="codebarang[{{$index}}]" value="{{$item->code_barang}}">
                                <input type="hidden" name="namaItem[{{$index}}]" value="{{$item->title}}">
                                <input type="hidden" name="hargaSatuan[{{$index}}]" value="{{$item->harga}}">
                                <input type="hidden" name="jumlah[{{$index}}]" value="{{$item->jumlah}}" >
                                <input type="hidden" name="tanggal_batal[{{$index}}]" type="text" value="{{date('Y-m-d')}}">
                                <input type="hidden" name="id" value="{{$id}}">
                              {{--/ hidden item untuk mengurangi stok --}}
                              <tr>
                                <td class="text-nowrap">{{$item->title}}</td>
                                <td>@currency($item->harga)</td>
                                <td>{{$item->jumlah}}</td>
                                <td>@currency($item->harga * $item->jumlah)</td>
                              </tr>
                            @endforeach
                          </form>
                          <tr>
                            <td colspan="2" class="align-top px-4 py-4">
                            </td>
                            <td class="text-end">
                              <p class="">Subtotal</p>
                              <p class="">Ongkir</p>
                              <p class="text-success">Potongan</p>
                              <p class="">Total:</p>
                            </td>
                            <td >
                              <p class="fw-semibold">@currency($invoice->tagihan)</p>
                              <p class="fw-semibold">@currency($invoice->ongkir)</p>
                              <p class="fw-semibold text-success">@currency($invoice->diskon)</p>
                              <p class="fw-semibold">@currency($invoice->tagihan + $invoice->ongkir - $invoice->diskon)</p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>

                </div>


                    

                <div class="card col-md-4">
                  <div class="card-body">
                    <table class="table table-borderless">
                    <tbody>
                      <tr>
                          <td class="align-middle">
                            <label>Pembeli</label>
                          </td>
                          <td>
                            <label>: {{$dataPembayaran->nama_pembeli}}</label>
                          </td>
                      </tr>
                      <tr>
                          <td class="align-middle">
                            <label>Reseller</label>
                          </td>
                          <td>
                            <label>: {{$dataPembayaran->toko}}</label>
                          </td>
                      </tr>
                      <tr>
                          <td class="align-middle">
                            <label>Seller</label>
                          </td>
                          <td>
                            <label>: {{$dataPembayaran->pembayaran_toko}}</label>
                          </td>
                      </tr>
                      <tr>
                          <td class="align-middle">
                            <label>Status</label>
                          </td>
                          <td>
                            : <label  class="badge badge-light-success">{{$dataPembayaran->status_pembayaran}}</label>
                          </td>
                      </tr>
                      <tr>
                        <td style="display: <?php if(Auth::user()->role == 1){ echo "";}else{ echo "none";} ?>">
                          <button id="submittedformditerima" class="btn btn-primary" style="display:<?php if ($dataPembayaran->status_pembayaran == "Diterima"){ echo "none"; }else{echo "block";} ?>">Terima</button>
                        </td>
                        <td style="display: <?php if(Auth::user()->role == 1){ echo "";}else{ echo "none";} ?>">
                          <a href="/tolakpesananreseller/{{$id}}" class="btn btn-danger perifpembayaran" style="display:<?php if ($dataPembayaran->status_pembayaran == "Diterima"){ echo "none"; }else{echo "block";} ?>">Tolak</a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  </div>
                </div>


                <!-- timeline detail status pemesanan -->
                <div class="col-xl-12 mb-4 mb-xl- mb-5">
                  <div class="card">
                    <h5 class="card-header">Bukti Pembayaran</h5>
                    <div class="card-body pb-0">
                      <ul class="timeline mb-0">
                        <li class="timeline-item timeline-item-transparent mb-1 ">
                          <span class="timeline-point timeline-point-primary"></span>
                          <div class="timeline-event">
                            <div class="timeline-header border-bottom mb-1">
                              <h6 class="mb-0"></h6>
                              <span class="text-muted"></span>
                            </div>
                            <div class="d-flex justify-content-between flex-wrap">
                              <div>
                                <span></span>
                              </div>
                              <div>
                                <span class="text-muted"></span>
                              </div>
                            </div>
                            <a target="_blank" href="../../img/{{$buktiBayar->file}}">
                              <img class="round" src="../../img/{{$buktiBayar->file}}" height="200px" width="200px">
                            </a>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
@endsection