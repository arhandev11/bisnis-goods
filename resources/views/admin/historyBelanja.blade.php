@extends('layouts.app')
<?php $page = "history" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
  <!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Laporan</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/laporan">History belanja</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-body">
              <table class="table table-striped small">
                 <thead>
                 <tr>
                   <th>Nama Agen</th>
                   <th>Total Belanja</th>
                 </tr>
                 </thead>
                 <tbody>
                  @foreach ($historyprice as $hstr)
                 <tr class="shop-item">
                   <td>{{$hstr->supplier}}</td>
                   <td>@currency($hstr->total)</td>
                 </tr>
                 @endforeach
                 </tbody>
              </table>
            </div>

            <!-- Stats Horizontal Card -->
                    <div class="row">
          @foreach ($listTempo as $hstmpo)
                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <h2 class="fw-bolder mb-0">@currency($hstmpo->totaltempo)</h2>
                                        <p class="card-text"> Hutang Tempo {{$hstmpo->supplier}}</p>
                                    </div>
                                    <div class="avatar bg-light-warning p-50 m-0">
                                        <div class="avatar-content">
                                            <i data-feather="alert-octagon" class="font-medium-5"></i>
                                        </div>
                                    </div>
                                  </div>
                            </div>
                        </div>
          @endforeach
                    </div>
                    <!--/ Stats Horizontal Card -->


                    <!-- PANJANG TABLE CARD -->
          




            <div class="card card-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab" data-bs-target="#navs-tempo" aria-controls="navs-tempo" aria-selected="true">
                              History Tempo
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-lunas" aria-controls="navs-lunas" aria-selected="false" tabindex="-1">
                              History Lunas
                            </button>
                        </li>
                    </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active show" id="navs-tempo" role="tabpanel">
                     <table class="table table-striped small">
                        <thead>
                        <tr>
                          <th>Belanja</th>
                          <th>Bayar</th>
                          <th>Code</th>
                          <th>Nama</th>
                          <th>Jumlah</th>
                          <th>Modal</th>
                          <th>Total</th>
                          <th>Supplier</th>
                          <th>Status</th>
                          <th>Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($historyTempo as $stokTempo)
                        <tr class="shop-item">
                          <td>{{ $stokTempo->tgl_belanja}}</td>
                          <td>{{ $stokTempo->tgl_bayar}}</td>
                          <td>{{ $stokTempo->code_barang }}</td>
                          <td>{{ $stokTempo->nama_barang }}</td>
                          <td>{{ $stokTempo->jumlah }}</td>
                          <td>@currency($stokTempo->harga_modal)</td>
                          <td>@currency($stokTempo->total_belanja)</td>
                          <td>{{$stokTempo->supplier}}</td>
                          <td>{{$stokTempo->status_belanja}}</td>
                          <td class="text-center">
                            <button 
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                  data-submenu
                                   class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1" type="button">
                              <i  data-feather="chevron-down"></i>
                              </button>
                            <ul class="dropdown-menu">
                              <a href="/bayartempoagen/{{$stokTempo->id}}" class="dropdown-item bayartempo"><i data-feather="archive"></i> bayar Tempo</a>
                            </ul>
                          </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="form-group mt-3">
                    {{$historyTempo->links()}}
                    </div>
                </div>
                <div class="tab-pane fade" id="navs-lunas" role="tabpanel">
                     <table class="table table-striped small">
                      <thead>
                        <tr>
                          <th>Belanja</th>
                          <th>Bayar</th>
                          <th>Code</th>
                          <th>Nama</th>
                          <th>Jumlah</th>
                          <th>Modal</th>
                          <th>Total</th>
                          <th>Supplier</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($historyLunas as $stokLunas)
                        <tr class="shop-item">
                          <td>{{ $stokLunas->tgl_belanja}}</td>
                          <td>{{ $stokLunas->tgl_bayar}}</td>
                          <td>{{ $stokLunas->code_barang }}</td>
                          <td>{{ $stokLunas->nama_barang }}</td>
                          <td>{{ $stokLunas->jumlah }}</td>
                          <td>@currency($stokLunas->harga_modal)</td>
                          <td>@currency($stokLunas->total_belanja)</td>
                          <td>{{$stokLunas->supplier}}</td>
                          <td>{{$stokLunas->status_belanja}}</td>
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                    <div class="form-group mt-3">
                    {{$historyLunas->links()}}
                    </div>
                  </div>
                </div>

<script>
$(document).ready( function () {
    $('#myTable').DataTable({
      "ordering": false,
    });
} );
</script>
@endsection
