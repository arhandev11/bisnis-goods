<!DOCTYPE html>

<html
  lang="en"
  class="light-style"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="../../assets/"
  data-template="horizontal-menu-template"
>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />

    <title>Invoice (Print version) - Pages | Vuexy - Bootstrap Admin Template</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="../../assets/img/favicon/favicon.ico" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
      rel="stylesheet"
    />

    <!-- Icons -->
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/fonts/fontawesome.css')}}" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/fonts/tabler-icons.css')}}" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/fonts/flag-icons.css')}}" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/rtl/core.css')}}" class="template-customizer-core-css" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/rtl/theme-default.css')}}" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="{{ asset('app-assets/css/demo.css')}}" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/libs/perfect-scrollbar/perfect-scrollbar.css')}}" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/libs/node-waves/node-waves.css')}}" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/libs/typeahead-js/typeahead.css')}}" />

    <!-- Page CSS -->

    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/pages/app-invoice-print.css')}}" />
    <!-- Helpers -->
    <script src="{{ asset('app-assets/vendors/js/helpers.js')}}"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Template customizer: To hide customizer set displayCustomizer value false in config.js.  -->
    <script src="{{ asset('app-assets/vendor/js/template-customizer.js')}}"></script>
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="{{ asset('app-assets/js/config.js')}}"></script>
  </head>

  <body>
    <!-- Content -->

    <div class="invoice-print p-5">

      <hr />

      <div class="row d-flex justify-content-between mb-4">
        <div class="col-sm-6 w-50">
        <div>
          <h4 class="fw-bold">INVOICE #{{$invoice->invoice}}</h4>
          <div class="mb-2">
            <span class="text-muted">Tanggal:</span>
            <span class="fw-bold">{{$invoice->tanggal}}</span>
          </div>
        </div>
        </div>
        <div class="col-sm-6 w-50">
          <h6>Invoice:</h6>
          <table>
            <tbody>
              <tr>
                <td class="pe-3">Nama:</td>
                <td>{{$invoice->nama_pembeli}}</td>
              </tr>
              <tr>
                <td class="pe-3">Alamat:</td>
                <td>{{$invoice->alamat}}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div class="table-responsive">
        <table class="table m-0">
          <thead class="table-light">
            <tr>
              <th>Item</th>
              <th>Harga</th>
              <th>Qty</th>
              <th>Harga</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($item as $barang)
            <tr>
              <td>{{$barang->title}}</td>
              <td>@currency($barang->harga)</td>
              <td>{{$barang->jumlah}}</td>
              <td>@currency($barang->harga*$barang->jumlah)</td>
            </tr>
            @endforeach
              <td colspan="2" class="align-top px-4 py-3">
              </td>
              <td class="text-end px-4 py-3">
                <p class="mb-0">Ongkir:</p>
                <p class="mb-0">Total:</p>
              </td>
              <td class="px-4 py-3">
                <p class="fw-bold mb-0">@currency($invoice->ongkir)</p>
                <p class="fw-bold mb-0">@currency($invoice->tagihan)</p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="row">
        <div class="col-12">
          <span class="fw-bold">Note:</span>
          <span
            >senang brteransaksi dengan anda, kami tunggu invoice selanjutnya, terimakasih</span
          >
        </div>
      </div>
    </div>

    <!-- / Content -->

    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{ asset('app-assets/vendors/libs/jquery/jquery.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/popper/popper.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/bootstrap.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/node-waves/node-waves.js') }}"></script>

    <script src="{{ asset('app-assets/vendors/libs/hammer/hammer.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/i18n/i18n.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/typeahead-js/typeahead.js') }}"></script>

    <script src="{{ asset('app-assets/vendors/js/menu.js') }}"></script>
    <!-- endbuild -->

    <!-- Vendors JS -->

    <!-- Main JS -->
    <script src="{{ asset('app-assets/vendors/js/main.js') }}"></script>

    <!-- Page JS -->
    <script src="{{ asset('app-assets/vendors/js/app-invoice-print.js') }}"></script>
  </body>
</html>
