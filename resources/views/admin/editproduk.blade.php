@extends('layouts.app')
<?php $page = "stok" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">System Inventory</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="stok">System Inventory</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

              <div class="col-xl-12 col-md-6 col-12">
                            <div class="card card-statistics">
                                <div class="card-body statistics-body">
                                    <div class="row">
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                            <div class="d-flex flex-row">
                                                <div class="avatar bg-light-info me-2">
                                                    <div class="avatar-content">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trending-up avatar-icon"><polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline><polyline points="17 6 23 6 23 12"></polyline></svg>
                                                    </div>
                                                </div>
                                                <div class="my-auto">
                                                    <h4 class="fw-bolder mb-0">{{$allstok}} Item</h4>
                                                    <p class="card-text font-small-3 mb-0">Jumlah Stok</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                            <div class="d-flex flex-row">
                                                <div class="avatar bg-light-info me-2">
                                                    <div class="avatar-content">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-box avatar-icon"><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg>
                                                    </div>
                                                </div>
                                                <div class="my-auto">
                                                    <h4 class="fw-bolder mb-0">{{$allcategory}}</h4>
                                                    <p class="card-text font-small-3 mb-0">Jenis Barang</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
                                            <div class="d-flex flex-row">
                                                <div class="avatar bg-light-info me-2">
                                                    <div class="avatar-content">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign avatar-icon"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>
                                                    </div>
                                                </div>
                                                <div class="my-auto">
                                                    <h4 class="fw-bolder mb-0">@currency($pricestok)</h4>
                                                    <p class="card-text font-small-3 mb-0">Nilai Barang</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-sm-6 col-12">
                                            <div class="d-flex flex-row">
                                                <div class="avatar bg-light-success me-2">
                                                    <div class="avatar-content">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign avatar-icon"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>
                                                    </div>
                                                </div>
                                                <div class="my-auto">
                                                    <h4 class="fw-bolder mb-0">@currency($pricesale-$pricestok)</h4>
                                                    <p class="card-text font-small-3 mb-0">Perkiraan Margin</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




          <!-- PANJANG TABLE CARD -->
            <div class="col-lg-12">
                <!-- Multi Column with Form Separator -->

                <div class="form-group mb-1">
                  <button type="btn" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-users"><i data-feather="plus-square" class="me-1"></i> Gambar</button>
                </div>
              <div class="card mb-4">
                <h5 class="card-header">Formulir edit detail produk</h5>
                <div  class="card-body">
                <form method="get" action="/updatedetailbarang/{{$id}}">
                @csrf
                    @foreach($detailBarang as $detailBarang)
                  <div class="row g-3">
                    <div class="col-md-8">
                      <label class="form-label" for="multicol-username">Nama Produk</label>
                      <input type="text" class="form-control" name="nama_barang" value="{{$detailBarang->nama_barang}}" />
                    </div>
                    <div class="col-md-2">
                      <label class="form-label" for="multicol-username">Tampil Public</label>
                      <select class="form-control" name="tampil_public">
                        <option @php if($detailBarang->tampil_public == null){echo "selected";}else{echo"";}@endphp></option>
                        <option @php if($detailBarang->tampil_public == "tampil"){echo "selected";}else{echo"";}@endphp>tampil</option>
                        <option @php if($detailBarang->tampil_public == "tidak"){echo "selected";}else{echo"";}@endphp>tidak</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label class="form-label" for="multicol-username">Jumlah Stok</label>
                      <input type="text" class="form-control" name="stok" value="{{$detailBarang->stok}}" disabled />
                    </div>
                    <div class="col-md-12">
                      <label class="form-label" for="multicol-username">Deskripsi Produk</label>
                      <textarea type="text" class="form-control" name="produk_deskripsi" value="{{$detailBarang->produk_deskripsi}}"></textarea>
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Code Produk</label>
                      <input type="text" class="form-control" name="code_barang" value="{{$detailBarang->code_barang}}" disabled />
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Category Produk</label>
                      <input type="text" class="form-control" name="category" value="{{$detailBarang->category}}" />
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Lokasi Produk</label>
                      <input type="text" class="form-control" name="tipe" value="{{$detailBarang->tipe}}" disabled />
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Harga Modal</label>
                      <input type="text" class="form-control" name="harga_modal" value="{{$detailBarang->harga_modal}}" />
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Harga Retail</label>
                      <input type="text" class="form-control" name="harga_jual" value="{{$detailBarang->harga_jual}}"/>
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Harga Tier 1</label>
                      <input type="text" class="form-control" name="tr1" value="{{$detailBarang->tr1}}" />
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Harga tier 2</label>
                      <input type="text" class="form-control" name="tr2" value="{{$detailBarang->tr2}}" />
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Harga tier 3</label>
                      <input type="text" class="form-control" name="tr3" value="{{$detailBarang->tr3}}" />
                    </div>
                    <div class="col-md-3 mt-5">
                      <label class="form-label" for="multicol-username">Harga Agen</label>
                      <input type="text" class="form-control" name="agen_price" value="{{$detailBarang->agen_price}}" />
                    </div>
                    <div class="col-md-3 mt-5">
                      <label class="form-label" for="multicol-username">Agen Min Qty</label>
                      <input type="text" class="form-control" name="min_qty" value="{{$detailBarang->min_qty}}" />
                    </div>
                    <div class="col-md-3 mt-5" style="display:<?php if($cariIdecer == null){echo "block";}else{echo "none";} ?>">
                      <label class="form-label" for="multicol-username">Qty Sachet</label>
                      <input type="text" class="form-control" name="qty_sachet" value="{{$detailBarang->qty_sachet}}" />
                    </div>
                    <div class="col-md-3" style="display:<?php if($cariIdecer == null){echo "block";}else{echo "none";} ?>">
                        <label class="text-danger form-label" for="multicol-username">Pastikan produk box men select sachet bukan sebaliknya!!!</label>
                        <select id="select2Basic" class="select2 form-select form-select-lg" name="id_sachet" data-allow-clear="false">
                            <option value="{{$detailBarang->id_sachet}}">{{$detailBarang->id_sachet}}</option>
                            @foreach($selectIdProduk as $pilihProduk)
                            <option value="{{$pilihProduk->id}}">{{$pilihProduk->nama_barang}} - id : {{$pilihProduk->code_barang}}</option>
                            @endforeach
                        </select>
                    </div>
                  </div>
                  <div class="pt-4">
                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                  </div>
                  @endforeach
                </form>
              </div>
            </div>
    <!-- END: Content-->



  <!-- modal category -->
  <div class="modal fade" id="modal-users">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Form Tambah User</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <h5 class="card-header">Profile Details</h5>
        <!-- Account -->
        <div class="card-body">
          <div class="d-flex align-items-start align-items-sm-center gap-4">
            <img src="../../img/" alt="user-avatar" class="d-block w-px-100 h-px-100" id="uploadedAvatar" width="100" height="100">
            <form method="post" action="/uploadfotoproduk/{{$detailBarang->id}}" enctype="multipart/form-data">
            @csrf
            <div class="button-wrapper">
              <input type="file" name="file" id="upload" class="form-control mb-1" required><button type="submit" class="btn btn-primary mb-2 ">upload</button>
              <div class="text-muted">Allowed JPG, GIF or PNG. Max size of 1MB</div>
            </div>
          </form>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal --> 

@endsection