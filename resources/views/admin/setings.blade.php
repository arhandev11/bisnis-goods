@extends('layouts.app')
<?php $page = "setings" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
  <!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Setings</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/setings">Setings</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row fv-plugins-icon-container">
                <div class="col-md-12">
                  <ul class="nav nav-pills flex-column flex-md-row mb-4">
                    <li class="nav-item">
                      <a class="nav-link active" href="/setings"><i data-feather="user" class="ti-xs ti ti-users me-1"></i> Account</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="/setingspass"><i data-feather="user" class="ti-xs ti ti-lock me-1"></i> Security</a>
                    </li>
                    <li style="display:<?php if( Auth::user()->role == 1){echo "block";}else{echo "none";} ?>" class="nav-item">
                      <a class="nav-link" href="/setingsstore"><i data-feather="shopping-bag" class="ti-xs ti ti-lock me-1"></i> Store</a>
                    </li>
                  </ul>
                  <div class="card mb-4">
                    <h5 class="card-header">Profile Details</h5>
                    <!-- Account -->
                    <div class="card-body">
                      <div class="d-flex align-items-start align-items-sm-center gap-4">
                        <img src="../../img/{{$userDetail->foto}}" alt="user-avatar" class="d-block w-px-100 h-px-100" id="uploadedAvatar" width="100" height="100">
                        <form method="post" action="/uploadfotoprofile/{{$userDetail->id}}" enctype="multipart/form-data">
                        @csrf
                        <div class="button-wrapper">
                          <input type="file" name="file" id="upload" class="form-control mb-1" required><button type="submit" class="btn btn-primary mb-2 ">upload</button>
                          <div class="text-muted">Allowed JPG, GIF or PNG. Max size of 1MB</div>
                        </div>
                      </form>
                      </div>
                    </div>
                    <hr class="my-0">
                    <div class="card-body">
                        <form method="post" action="/updateprofile/{{$userDetail->id}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                          <div class="mb-3 col-md-6 fv-plugins-icon-container">
                            <label for="firstName" class="form-label">Name</label>
                            <input class="form-control" type="text" id="firstName" name="nama" value="{{$userDetail->name}}" autofocus="">
                          <div class="fv-plugins-message-container invalid-feedback"></div></div>
                          <div class="mb-3 col-md-6 fv-plugins-icon-container">
                            <label for="lastName" class="form-label">Hp</label>
                            <input class="form-control" type="text" name="hp_toko" id="hp_toko" value="{{$userDetail->hp_toko}}">
                          <div class="fv-plugins-message-container invalid-feedback"></div></div>
                          <div class="mb-3 col-md-6">
                            <label for="email" class="form-label">E-mail</label>
                            <input class="form-control" type="text" id="email" name="email" value="{{$userDetail->email}}">
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="organization" class="form-label">Toko</label>
                            <input type="text" class="form-control" id="organization" name="toko" value="{{$userDetail->toko}}" disabled>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="organization" class="form-label">Tier</label>
                            <input type="text" class="form-control" id="organization" name="tier" value="{{$userDetail->tier}}" disabled>
                          </div>
                        </div>
                        <div class="mt-2">
                          <button type="submit" class="btn btn-primary me-2 waves-effect waves-light">Simpan Perubahan</button>
                        </div>
                      </form>
                    </div>
                    <!-- /Account -->
                  </div>
                </div>
              </div>

@endsection
