@extends('layouts.app')
<?php $page = "cabang" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
  <!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">List Toko Cabang</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/laporan">User</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group mb-1">
                <button type="btn" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-users"><i data-feather="plus-square" class="me-1"></i> Cabang</button>
            </div>
              <!-- / TOMBOL INPUT STOK -->
              <!-- TABLE CARD -->
            <div class="card">
            <table class="table table-striped">
                  <thead>
                  <tr>
                    <th>Nama Toko</th>
                    <th>Alamat</th>
                    <th>Hp</th>
                    <th>Kepala Cabang</th>
                    <th>Opsi</th>
                  </tr>
                  </thead>
                  <tbody>
                      @foreach($cabang as $cbg)
                  <tr class="shop-item">
                    <td>{{$cbg->nama_cabang}}</td>
                    <td>{{$cbg->alamat_cabang}}</td>
                    <td>{{$cbg->hp_cabang}}</td>
                    <td>{{$cbg->name}}</td>
                    <td>
                      <a href="/editcabang/{{$cbg->id_cabang}}" class="btn btn-sm btn-primary"><i data-feather="edit"></i></a>
                      <a href="/hapuscabang/{{$cbg->id_cabang}}" class="btn btn-sm btn-danger hapustokocabang"><i data-feather="trash"></i></a>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
            </div>
            <!-- / TABLE CARD -->
          </div>
          <!-- /.col-md-12 -->
          <br>

                <div class="form-group pl-2">
                {{$cabang->links()}}
                </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal category -->
  <div class="modal fade" id="modal-users">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Tambah Cabang</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="post" action="/buatcabangbaru">
                @csrf
                <div class="modal-body">
                  <div class="form-group mb-1">
                    <input name="nama_cabang" type="text" class="form-control" placeholder="nama cabang" required>
                  </div>
                  <div class="form-group mb-1">
                    <textarea name="alamat_cabang" type="text" class="form-control" placeholder="alamat cabang" required></textarea>
                  </div>
                  <div class="form-group mb-1">
                    <input name="hp_cabang" type="text" class="form-control" placeholder="hp cabang" required>
                  </div>
                  <div class="form-group mb-1">
                      <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Hanya menampilkan user admin toko dan super admin yang tidak memiliki toko, jika tidak ada user yang tampil anda harus menambahkan user baru di akun user tanpa toko">
                        Info <i class="text-primary" data-feather="help-circle"></i>
                      </a>
                    <select name="kepala_cabang" class="form-control"  required>
                        <option value="">Pilih Kepala Cabag</option>
                        @foreach($userlist as $usrlst)
                        <option value="{{$usrlst->id}}">{{$usrlst->name}}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </form>
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->

<script>
$(document).ready( function () {
    $('#myTable').DataTable({
      "ordering": false,
    });
} );
</script>
@endsection
