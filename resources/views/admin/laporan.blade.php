@extends('layouts.app')
<?php $page = "dashboard" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
  <!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Laporan</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/home">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/laporan">Laporan Tahunan</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

          
          <div class="col-lg-3">
            <form  action="/yearReport" method="GET" >
            <select name="selectyear" class="form-control mb-1" onchange="this.form.submit()">
                <option>Pilih Tahun</option>
                @foreach($readyYear as $rdy)
                <option @if ($rdy->year == $scty ) selected @endif >{{$rdy->year}}</option>
                @endforeach
            </select>
            </form>
        </div>
            <div class="col-lg-12">
            <!-- TOMBOL INPUT STOK -->
              <!-- / TOMBOL INPUT STOK -->
              <!-- TABLE CARD -->
            <div class="card">
            <table class="table table-striped">
                  <thead>
                  <tr>
                    <th class="text-center">Bulan</th>
                    <th class="text-center">Omset</th>
                    <th class="text-center">Margin</th>
                    <th class="text-center">Oprasional</th>
                    <th class="text-center">Profit</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr class="shop-item">
                    <td class="text-center">Jan</td>
                    <td class="text-center">@currency($omsetJan)</td>
                    <td class="text-center">@currency($taJan-$MoJan)</td>
                    <td class="text-center text-danger">@currency($opJan)</td>
                    <td class="text-center">@currency(($taJan-$MoJan)-$opJan)</td>
                  </tr>
                  <tr class="shop-item">
                    <td class="text-center">Feb</td>
                    <td class="text-center">@currency($omsetFeb)</td>
                    <td class="text-center">@currency($taFeb-$MoFeb)</td>
                    <td class="text-center text-danger">@currency($opFeb)</td>
                    <td class="text-center">@currency(($taFeb-$MoFeb)-$opFeb)</td>
                  </tr>
                  <tr class="shop-item">
                    <td class="text-center">Mar</td>
                    <td class="text-center">@currency($omsetMar)</td>
                    <td class="text-center">@currency($taMar-$MoMar)</td>
                    <td class="text-center text-danger">@currency($opMar)</td>
                    <td class="text-center">@currency(($taMar-$MoMar)-$opMar)</td>
                  </tr>
                  <tr class="shop-item">
                    <td class="text-center">Apr</td>
                    <td class="text-center">@currency($omsetApr)</td>
                    <td class="text-center">@currency($taApr-$MoApr)</td>
                    <td class="text-center text-danger">@currency($opApr)</td>
                    <td class="text-center">@currency(($taApr-$MoApr)-$opApr)</td>
                  </tr>
                  <tr class="shop-item">
                    <td class="text-center">Mei</td>
                    <td class="text-center">@currency($omsetMei)</td>
                    <td class="text-center">@currency($taMei-$MoMei)</td>
                    <td class="text-center text-danger">@currency($opMei)</td>
                    <td class="text-center">@currency(($taMei-$MoMei)-$opMei)</td>
                  </tr>
                  <tr class="shop-item">
                    <td class="text-center">Jun</td>
                    <td class="text-center">@currency($omsetJun)</td>
                    <td class="text-center">@currency($taJun-$MoJun)</td>
                    <td class="text-center text-danger">@currency($opJun)</td>
                    <td class="text-center">@currency(($taJun-$MoJun)-$opJun)</td>
                  </tr>
                  <tr class="shop-item">
                    <td class="text-center">Jul</td>
                    <td class="text-center">@currency($omsetJul)</td>
                    <td class="text-center">@currency($taJul-$MoJul)</td>
                    <td class="text-center text-danger">@currency($opJul)</td>
                    <td class="text-center">@currency(($taJul-$MoJul)-$opJul)</td>
                  </tr>
                  <tr class="shop-item">
                    <td class="text-center">Ags</td>
                    <td class="text-center">@currency($omsetAgs)</td>
                    <td class="text-center">@currency($taAgs-$MoAgs)</td>
                    <td class="text-center text-danger">@currency($opAgs)</td>
                    <td class="text-center">@currency(($taAgs-$MoAgs)-$opAgs)</td>
                  </tr>
                  <tr class="shop-item">
                    <td class="text-center">Sep</td>
                    <td class="text-center">@currency($omsetSep)</td>
                    <td class="text-center">@currency($taSep-$MoSep)</td>
                    <td class="text-center text-danger">@currency($opSep)</td>
                    <td class="text-center">@currency(($taSep-$MoSep)-$opSep)</td>
                  </tr>
                  <tr class="shop-item">
                    <td class="text-center">Okt</td>
                    <td class="text-center">@currency($omsetOkt)</td>
                    <td class="text-center">@currency($taOkt-$MoOkt)</td>
                    <td class="text-center text-danger">@currency($opOkt)</td>
                    <td class="text-center">@currency(($taOkt-$MoOkt)-$opOkt)</td>
                  </tr>
                  <tr class="shop-item">
                    <td class="text-center">Nov</td>
                    <td class="text-center">@currency($omsetNov)</td>
                    <td class="text-center">@currency($taNov-$MoNov)</td>
                    <td class="text-center text-danger">@currency($opNov)</td>
                    <td class="text-center">@currency(($taNov-$MoNov)-$opNov)</td>
                  </tr>
                  <tr class="shop-item">
                    <td class="text-center">Des</td>
                    <td class="text-center">@currency($omsetDes)</td>
                    <td class="text-center">@currency($taDes-$MoDes)</td>
                    <td class="text-center text-danger">@currency($opDes)</td>
                    <td class="text-center">@currency(($taDes-$MoDes)-$opDes)</td>
                  </tr>
                  <tr class="shop-item">
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center text-success">@currency($totalProfit)</td>
                  </tr>
                  </tbody>
                </table>
            </div>
            <!-- / TABLE CARD -->
          </div>
          <!-- /.col-md-12 -->
          <br>

      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal category -->
  <div class="modal fade" id="modal-users">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Tambah User</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="post" action="/tambah/user">
                @csrf
                <div class="modal-body">
                  <div class="form-group">
                    <input name="nama" type="text" class="form-control" placeholder="Nama" required>
                  </div>
                  <div class="form-group">
                    <input name="email" type="text" class="form-control" placeholder="Email" required>
                  </div>
                  <div class="form-group">
                    <input name="hp" type="text" class="form-control" placeholder="hp" required>
                  </div>
                  <div class="form-group">
                    <input name="toko" type="text" class="form-control" placeholder="nama toko" required>
                  </div>
                  <div class="form-group">
                    <select name="role" class="form-control"  required>
                        <option></option>
                        <option value="1">Admin</option>
                        <option value="3">Reseller</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <input name="password" type="text" class="form-control" placeholder="password" required>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </form>
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->

<script>
$(document).ready( function () {
    $('#myTable').DataTable({
      "ordering": false,
    });
} );
</script>
@endsection
