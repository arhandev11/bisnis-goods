@extends('layouts.app')
<?php $page = "penerimaan" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
  <!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">List Pencairan</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/laporan">list pencairan</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <!-- Stats Horizontal Card -->
                    <div class="row">
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <h2 class="fw-bolder mb-0">{{$countinvpending}}</h2>
                                        <p class="card-text">inv pending</p>
                                    </div>
                                    <div class="avatar bg-light-success p-50 m-0">
                                        <div class="avatar-content" data-bs-toggle="tooltip" title="Merah Lebih Rendah Dari Kemarin, Hijau Lebih Tinggai Dari Kemarin *blm di buat" data-bs-animation="false">
                                            <i data-feather="box" class="font-medium-5" ></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <h2 class="fw-bolder mb-0">@currency($nilaiinvpending)</h2>
                                        <p class="card-text">perkiraan nilai inv pending</p>
                                    </div>
                                    <div class="avatar bg-light-success p-50 m-0">
                                        <div class="avatar-content">
                                            <i data-feather="archive" class="font-medium-5"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <h2 class="fw-bolder mb-0">{{$countinvcair}}</h2>
                                        <p class="card-text">inv cair</p>
                                    </div>
                                    <div class="avatar bg-light-success p-50 m-0">
                                        <div class="avatar-content">
                                            <i data-feather="credit-card" class="font-medium-5"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <h2 class="fw-bolder mb-0">@currency( $nilaiinvcair)</h2>
                                        <p class="card-text">nilai inv cair</p>
                                    </div>
                                    <div class="avatar bg-light-success p-50 m-0">
                                        <div class="avatar-content">
                                            <i data-feather="alert-octagon" class="font-medium-5"></i>
                                        </div>
                                    </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Stats Horizontal Card -->


          <!-- PANJANG TABLE CARD -->
            <div class="col-lg-12">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-1" style="display:<?php if(Auth::user()->role == 1){echo "block";}else{echo "none";} ?>">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="row">
                                <div class="dropdown col-md-4">
                          <form action="/uploaddatapencairan" method="post"  enctype="multipart/form-data">
                            @csrf
                                      <input name="file" type="file" class="form-control" required>
                                </div>
                                <div class="dropdown col-md-4">
                                      <button class="btn btn-primary" type="submit">Update</button>
                          </form>
                                      <a href="{{ asset('img/template_pencairan_dana.xlsx') }}" class="btn btn-primary" ><i data-feather="download"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-12 col-md-4 mb-1 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right row">
                        <div class="dropdown col-md-6">
                            <form action="/listpenerimaan" method="get">
                              <input name="cariinvoice" type="text" class="form-control" placeholder="Search..." aria-label="Search..." aria-describedby="basic-addon-search2">
                            </form>
                        </div>
                        <div class="dropdown col-md-6" style="display:<?php if(Auth::user()->role == 1){echo "block";}else{echo "none";} ?>">
                            <form action="/listpenerimaan" method="get">
                                <select name="cabang" class="form-control" onchange="this.form.submit()">>
                                    <option>{{$selectedcabang}}</option>
                                    <option value="">Tampilkan Semua</option>
                                    @foreach($listtoko as $lstk)
                                    <option>{{$lstk->nama_cabang}}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
              <!-- / TOMBOL INPUT STOK -->
              <!-- TABLE CARD -->
            <div class="card">
            <table class="table table-striped">
                  <thead>
                  <tr>
                    <th class="text-nowrap">Invoice Toko</th>
                    <th class="text-nowrap">Invoice Platform</th>
                    <th class="text-nowrap">Tanggal</th>
                    <th class="text-nowrap">Nilai Pembelian</th>
                    <th class="text-nowrap">Platform</th>
                    <th>Pencairan</th>
                    <th>Opsi</th>
                  </tr>
                  </thead>
                  <tbody>
                      @foreach($listPencairan as $index => $pencairan)
                  <tr class="shop-item">
                    <td class="text-nowrap">{{$pencairan->invoice}}</td>
                    <td>{{$pencairan->expedisi}}</td>
                    <td class="text-nowrap">{{$pencairan->tanggal}}</td>
                    <td class="text-nowrap">{{$pencairan->tagihan}}</td>
                    <td class="text-nowrap">{{$pencairan->platform}}</td>
                    <td class="text-nowrap"><form id="formpencairanmanual{{$index+1}}">@csrf<input name="id_invoice" type="hidden" value="{{$pencairan->id}}"><input id="enable{{$index+1}}" name="pencairan" class="form-control form-control-sm" min="1" step="1" type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" readonly></form></td>
                            <td class="text-center">
                            <button 
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                  data-submenu
                                   class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1" type="button">
                              <i  data-feather="chevron-down"></i>
                              </button>
                            <ul class="dropdown-menu">
                              <a class="dropdown-item yakincairkan{{$index+1}}"><i data-feather="edit"></i> Enable</a>
                              <a class="dropdown-item disabled updatepencairanmanual{{$index+1}}" id="update{{$index+1}}"><i data-feather="send"></i> Update</a>
                            </ul>
                            </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
            </div>
            <!-- / TABLE CARD -->
          </div>
          <!-- /.col-md-12 -->
          <br>

                <div class="form-group pl-2">
                {{$listPencairan->links()}}
                </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
$(document).ready( function () {
    $('#myTable').DataTable({
      "ordering": false,
    });
} );
</script>

@foreach($listPencairan as $index => $sup)
<script type="text/javascript">
    $('.yakincairkan{{$index+1}}').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "pastikan invoice nya sudah benar dan pastikan input dana cair dengan sesuai",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'proses'
    }).then((result) => {
      if (result.isConfirmed) {
          document.getElementById('enable{{$index+1}}').removeAttribute('readonly');
          $('#update{{$index+1}}').removeClass("disabled");
      }
    });
});
</script>
@endforeach

<!-- atribut menggunakan form -->
@foreach($listPencairan as $index => $sup)
<script type="text/javascript">
    $('.updatepencairanmanual{{$index+1}}').on('click', function (event) {
    event.preventDefault();
    const url = $('#formpencairanmanual{{$index+1}}').attr('action');
    Swal.fire({
      title: 'PERHATIAN!!',
      text: "kamu yakin akan memproses invoice ini??",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'yakin'
    }).then((result) => {
      if (result.isConfirmed) {
        var form = document.getElementById("formpencairanmanual{{$index+1}}");
        form.method = "POST";
        form.action = "/updatepencairanmanual";
        form.submit();
      }
    });
});
</script>
@endforeach


@endsection
