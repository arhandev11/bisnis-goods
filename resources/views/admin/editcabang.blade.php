@extends('layouts.app')
<?php $page = "cabang" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Edit Cabang</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="stok">Edit Cabang</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




          <!-- PANJANG TABLE CARD -->
            <div class="col-lg-12">
                <!-- Multi Column with Form Separator -->
              <div class="card mb-4">
                <h5 class="card-header">Formulir Edit Cabang</h5>
                <form method="get" action="/updatedetailbarang/{{$id}}">
                <div  class="card-body">
                @csrf
                    @foreach($cabang as $cbng)
                  <div class="row g-3">
                    <div class="col-md-4">
                      <label class="form-label" for="multicol-username">Nama Cabang</label>
                      <input type="text" class="form-control" name="nama_barang" value="{{$cbng->nama_cabang}}" disabled />
                    </div>
                    <div class="col-md-4">
                      <label class="form-label" for="multicol-username">Kepala Cabang</label>
                      <input type="text" class="form-control" name="stok" value="{{$cbng->name}}" disabled />
                    </div>
                    <div class="col-md-4">
                      <label class="form-label" for="multicol-username">Hp Cabang</label>
                      <input type="text" class="form-control" name="code_barang" value="{{$cbng->hp_cabang}}" disabled />
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Alamat Cabang</label>
                      <input type="text" class="form-control" name="category" value="{{$cbng->alamat_cabang}}" disabled />
                    </div>
                  </div>
                  <div class="pt-4">
                    <button type="submit" class="btn btn-primary me-sm-3 me-1 disabled">Submit</button>
                  </div>
                  @endforeach
                </form>
              </div>
            </div>
    <!-- END: Content-->

@endsection