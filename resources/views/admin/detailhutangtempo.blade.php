@extends('layouts.app')
<?php $page = "history" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Detail Hutang Tempo</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/home">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/detailorderreseller">Detail Hutang Tempo</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-body">
                <div class="col-xl-12 col-md-12 col-12 mb-md-0">
                  <div class="card invoice-preview-card">
                    <div class="table-responsive border-top">
                      <table class="table table-striped small">
                        <thead>
                        <tr>
                          <th>Tanggal belanja</th>
                          <th>Code</th>
                          <th>Nama</th>
                          <th>Jumlah</th>
                          <th>Modal</th>
                          <th>Total</th>
                          <th>Supplier</th>
                          <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($detailbarang as $stokTempo)
                        <tr class="shop-item">
                          <td>{{ $stokTempo->tgl_belanja}}</td>
                          <td>{{ $stokTempo->code_barang }}</td>
                          <td>{{ $stokTempo->nama_barang }}</td>
                          <td>{{ $stokTempo->jumlah }}</td>
                          <td>@currency($stokTempo->harga_modal)</td>
                          <td>@currency($stokTempo->total_belanja)</td>
                          <td>{{$stokTempo->supplier}}</td>
                          <td>{{$stokTempo->status_belanja}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                  </div>
                </div>

                <p class="card card-body alert-warning">
                  dengan meng klik proses maka hutang tempo akan di set menjadi lunas dan di masukan ke pengeluaran dalam menu laporan keuangan dari rekening yang di pilih
                </p>
                <form method="get" action="/prosesbayartempoagen/{{$id}}">
                <div class="mt-5 row">
                  <div class="col-3">
                    <label class="aria-label mt-2">Bayar Menggunakan Rekening</label>
                    <select name="rekening" class="form-control col-md-3">
                      <option></option>
                      @foreach($rekening as $rek)
                      <option value="{{$rek->nomor_rekening}}">{{$rek->pemilik_rekening}}</option>
                      @endforeach
                    </select>
                  </div>
                    <div class="col-2 mt-3">
                      <button class="btn btn-primary">proses</button>
                    </div>
                  </div>
                  </form>

                    <!-- Modal packing pesanan -->
                    <div class="modal fade" id="basicModal" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <form method="post" action="/uploasbuktitrf/{{$id}}" enctype="multipart/form-data">
                                @csrf
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel1">Upload Bukti Packing</h5>
                                <button
                                  type="button"
                                  class="btn-close"
                                  data-bs-dismiss="modal"
                                  aria-label="Close"
                                ></button>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col mb-3">
                                    <label for="nameBasic" class="form-label">Upload Foto Packing</label>
                                    <input name="file" type="file" class="form-control">
                                  </div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-label-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Upload</button>
                              </div>
                            </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
@endsection