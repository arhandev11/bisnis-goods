@extends('layouts.app')
<?php $page = "role" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
  <!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Role Settings</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/laporan">Role Settings</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group mb-1">
                <button type="btn" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-users"><i data-feather="plus-square" class="me-1"></i> Role</button>
            </div>
              <!-- / TOMBOL INPUT STOK -->
              <!-- TABLE CARD -->
            <div class="card">
            <table class="table table-sm table-stripped">
                  <thead>
                    <tr>
                      <th>Role</th>
                      <th>Homa Page</th>
                      <th>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="System Transaksi">
                            <i data-feather="home"></i></th>
                        </a>
                      <th>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="System Transaksi">
                            <i data-feather="shopping-cart"></i></th>
                        </a>
                      <th>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="System Inventory">
                          <i data-feather="archive"></i></th>
                        </a>
                      <th>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="List Toko Cabang">
                          <i data-feather="truck"></i></th>
                        </a>
                      <th>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Pesanan Agen">
                          <i data-feather="shopping-bag"></i></th>
                        </a>
                      <th>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Pembayaran Agen">
                          <i data-feather="credit-card"></i></th>
                        </a>
                      <th>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Tagihan Agen">
                          <i data-feather="file-minus"></i></th>
                        </a>
                      <th>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="History Beanja">
                          <i data-feather="clock"></i></th>
                        </a>
                      <th>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Keuangan">
                          <i data-feather="feather"></i></th>
                        </a>
                      <th>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Settings">
                          <i data-feather="settings"></i></th>
                        </a>
                      <th>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Reseller Chart">
                          <i data-feather="users"></i></th>
                        </a>
                      <th>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Reseller Order">
                          <i data-feather="box"></i></th>
                        </a>
                      <th class="text-center">Ops</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($role as $bagianrole)
                  <tr class="shop-item">
                    <td>{{$bagianrole->role_name}}</td>
                    <td>{{$bagianrole->home_menu}}</td>
                    <td><i data-feather="<?php if($bagianrole->dashboard == "tampil"){echo "check-circle";}else{{echo "x-octagon";}} ?>" class="<?php if($bagianrole->dashboard == "tampil"){echo "bg-light-success";}else{{echo "bg-light-danger";}} ?>"></i></td>
                    <td><i data-feather="<?php if($bagianrole->system_transaksi == "tampil"){echo "check-circle";}else{{echo "x-octagon";}} ?>" class="<?php if($bagianrole->system_transaksi == "tampil"){echo "bg-light-success";}else{{echo "bg-light-danger";}} ?>"></i></td>
                    <td><i data-feather="<?php if($bagianrole->inventory == "tampil"){echo "check-circle";}else{{echo "x-octagon";}} ?>" class="<?php if($bagianrole->inventory == "tampil"){echo "bg-light-success";}else{{echo "bg-light-danger";}} ?>"></i></td>
                    <td><i data-feather="<?php if($bagianrole->list_cabang == "tampil"){echo "check-circle";}else{{echo "x-octagon";}} ?>" class="<?php if($bagianrole->list_cabang == "tampil"){echo "bg-light-success";}else{{echo "bg-light-danger";}} ?>"></i></td>
                    <td><i data-feather="<?php if($bagianrole->pesanan_agen == "tampil"){echo "check-circle";}else{{echo "x-octagon";}} ?>" class="<?php if($bagianrole->pesanan_agen == "tampil"){echo "bg-light-success";}else{{echo "bg-light-danger";}} ?>"></i></td>
                    <td><i data-feather="<?php if($bagianrole->pembayaran_agen == "tampil"){echo "check-circle";}else{{echo "x-octagon";}} ?>" class="<?php if($bagianrole->pembayaran_agen == "tampil"){echo "bg-light-success";}else{{echo "bg-light-danger";}} ?>"></i></td>
                    <td><i data-feather="<?php if($bagianrole->tagihan == "tampil"){echo "check-circle";}else{{echo "x-octagon";}} ?>" class="<?php if($bagianrole->tagihan == "tampil"){echo "bg-light-success";}else{{echo "bg-light-danger";}} ?>"></i></td>
                    <td><i data-feather="<?php if($bagianrole->history_belanja == "tampil"){echo "check-circle";}else{{echo "x-octagon";}} ?>" class="<?php if($bagianrole->history_belanja == "tampil"){echo "bg-light-success";}else{{echo "bg-light-danger";}} ?>"></i></td>
                    <td><i data-feather="<?php if($bagianrole->keuangan == "tampil"){echo "check-circle";}else{{echo "x-octagon";}} ?>" class="<?php if($bagianrole->keuangan == "tampil"){echo "bg-light-success";}else{{echo "bg-light-danger";}} ?>"></i></td>
                    <td><i data-feather="<?php if($bagianrole->settings == "tampil"){echo "check-circle";}else{{echo "x-octagon";}} ?>" class="<?php if($bagianrole->settings == "tampil"){echo "bg-light-success";}else{{echo "bg-light-danger";}} ?>"></i></td>
                    <td><i data-feather="<?php if($bagianrole->reseller_chart == "tampil"){echo "check-circle";}else{{echo "x-octagon";}} ?>" class="<?php if($bagianrole->reseller_chart == "tampil"){echo "bg-light-success";}else{{echo "bg-light-danger";}} ?>"></i></td>
                    <td><i data-feather="<?php if($bagianrole->reseller_order == "tampil"){echo "check-circle";}else{{echo "x-octagon";}} ?>" class="<?php if($bagianrole->reseller_order == "tampil"){echo "bg-light-success";}else{{echo "bg-light-danger";}} ?>"></i></td>
                          <td class="text-center">
                            <button 
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                  data-submenu
                                   class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1" type="button">
                              <i  data-feather="chevron-down"></i>
                              </button>
                            <ul class="dropdown-menu">
                              <a href="/hapusrole/{{$bagianrole->role_id}}" class="dropdown-item bayartempo hapusdata"><i data-feather="trash"></i> Hapus</a>
                              <a href="/editrole/{{$bagianrole->role_id}}" class="dropdown-item"><i data-feather="edit"></i> Edit</a>
                            </ul>
                          </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
            </div>
            <!-- / TABLE CARD -->
          </div>
          <!-- /.col-md-12 -->
          <br>

                <div class="form-group pl-2">
                {{$role->links()}}
                </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal category -->
  <div class="modal fade" id="modal-users" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Tambah Role</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="post" action="/tambahrole">
                @csrf
                            <div class="modal-body">
                              <div class="row">
                                <div class="col mb-1">
                                  <label for="nameLarge" class="form-label">Nama Role</label>
                                  <input name="nama_role" type="text" class="form-control" required>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col mb-3">
                                  <label for="nameLarge" class="form-label">Home Page</label>
                                  <select name="home_menu" type="text" class="form-control" required>
                                    <option></option>
                                    <option value="dashboard">Dashboard</option>
                                    <option value="stok">System Inventory</option>
                                    <option value="listcabang">List Toko Cabang</option>
                                    <option value="pesananagen">Pesanan Agen</option>
                                    <option value="pembayaran">Pembayaran Agen</option>
                                    <option value="historyreseller">Tagihan Agen</option>
                                    <option value="historybelanja">History belanja</option>
                                    <option value="keuanganadvance">Keuangan</option>
                                    <option value="user">Settings</option>
                                    <option value="resellerchart">Chart Reseller</option>
                                    <option value="resellerorder">Reseller Order List</option>
                                  </select>
                                </div>
                              </div>
                              <label class="form-group">System Store Menu</label>
                              <br>
                              <label class="dropdown-divider">fasdfafadsfasasdfasdf</label>
                              <div class="row g-2 mb-1">
                                <div class="col mb-0">
                                  <label class="form-label">Dashboard</label>
                                    <select name="dashboard" class="form-control" placeholder="Nama Role" required>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">System Transaksi</label>
                                    <select name="system_transaksi" class="form-control" placeholder="Nama Role" required>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">System Inventory</label>
                                    <select name="system_inventory" class="form-control" placeholder="Nama Role" required>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">List Cabang</label>
                                    <select name="list_cabang" class="form-control" placeholder="Nama Role" required>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">Pesanan Agen</label>
                                    <select name="pesanan_agen" class="form-control" placeholder="Nama Role" required>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                              </div>
                              <div class="row g-2 mb-1">
                                <div class="col mb-0">
                                  <label class="form-label">Pembayaran Agen</label>
                                    <select name="pembayaran_agen" class="form-control" placeholder="Nama Role" required>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">Tagihan Agen</label>
                                    <select name="tagihan" class="form-control" placeholder="Nama Role" required>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">History Belanja</label>
                                    <select name="history_belanja" class="form-control" placeholder="Nama Role" required>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">Keuangan</label>
                                    <select name="keuangan" class="form-control" placeholder="Nama Role" required>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">Settings</label>
                                  <select name="settings" class="form-control" placeholder="Nama Role" required>
                                    <option value="tampil">Tampilkan</option>
                                    <option value="sembunyi">Sembunyikan</option>
                                  </select>
                                </div>
                              </div>

                              <label class="form-group mt-2">System Reseller Menu</label>
                              <br>
                              <label class="dropdown-divider">fasdfafadsfasasdfasdf</label>

                              <div class="row g-2 mb-1">
                                <div class="col mb-0">
                                  <label class="form-label">Chart Reseller</label>
                                    <select name="resellerchart" class="form-control" placeholder="Nama Role" required>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                                <div class="col mb-0">
                                  <label class="form-label">Reseller Order List</label>
                                    <select name="resellerorder" class="form-control" placeholder="Nama Role" required>
                                      <option value="tampil">Tampilkan</option>
                                      <option value="sembunyi">Sembunyikan</option>
                                    </select>
                                </div>
                              </div>
                            </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </form>
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->



                      <!-- Large Modal -->
                      <div class="modal fade" id="largeModal" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel3">Modal title</h5>
                              <button
                                type="button"
                                class="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close"
                              ></button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col mb-3">
                                  <label for="nameLarge" class="form-label">Name</label>
                                  <input type="text" id="nameLarge" class="form-control" placeholder="Enter Name" />
                                </div>
                              </div>
                              <div class="row g-2">
                                <div class="col mb-0">
                                  <label for="emailLarge" class="form-label">Email</label>
                                  <input type="email" id="emailLarge" class="form-control" placeholder="xxxx@xxx.xx" />
                                </div>
                                <div class="col mb-0">
                                  <label for="dobLarge" class="form-label">DOB</label>
                                  <input type="date" id="dobLarge" class="form-control" placeholder="DD / MM / YY" />
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-label-secondary" data-bs-dismiss="modal">
                                Close
                              </button>
                              <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                          </div>
                        </div>
                      </div>
@endsection
