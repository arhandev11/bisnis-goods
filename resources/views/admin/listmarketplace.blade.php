@extends('layouts.app')
<?php $page = "marketplace" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
  <!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">List Marketplace</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/laporan">List Marketplce</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group mb-1">
                <button type="btn" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-users"><i data-feather="plus-square" class="me-1"></i> Marketplace</button>
            </div>
              <!-- / TOMBOL INPUT STOK -->
              <!-- TABLE CARD -->
            <div class="card">
            <table class="table table-striped">
                  <thead>
                  <tr>
                    <th>Nama TOko</th>
                    <th>User</th>
                    <th>Toko Cabang</th>
                    <th>Akun Marketplace</th>
                    <th>Diatur Marketplace</th>
                    <th>Limit Belanja</th>
                    <th>Opsi</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($marketplace as $market)
                  <tr>
                    <td>{{$market->nama_marketplace}}</td>
                    <td>{{$market->name}}</td>
                    <td>{{$market->id_toko_cabang}}</td>
                    <td>{{$market->marketplace_utama}}</td>
                    <td>{{$market->diatur_marketplace}}</td>
                    <td>@currency($market->limit_belanja)</td>
                    <td>
                      <a href="/editmarketplace/{{$market->id_marketplace}}" class="btn btn-sm btn-primary"><i data-feather="edit"></i></a>
                      <a href="/hapusmarketplace/{{$market->id_marketplace}}" class="btn btn-sm btn-danger hapusdata"><i data-feather="trash"></i></a>
                    </td>
                    @endforeach
                  </tr>
                  </tbody>
                </table>
            </div>
            <!-- / TABLE CARD -->
          </div>
          <!-- /.col-md-12 -->
          <br>

                <div class="form-group pl-2">
                
                </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal category -->
  <div class="modal fade" id="modal-users">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Tambah User</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/tambahmarketplace">
                @csrf
                <div class="modal-body">
                  <div class="form-group mb-1">
                    <input name="nama_toko" type="text" class="form-control" placeholder="nama toko" required>
                  </div>
                  <div class="form-group mb-1">
                    <label>Marketplace</label>
                    <select name="marketplace" class="form-control"  required>
                        <option></option>
                        <option>Tokopedia</option>
                        <option>Shopee</option>
                        <option>Bukalapak</option>
                        <option>Bli Bli</option>
                        <option>Lazada</option>
                        <option>Tiktok</option>
                        <option>Whatsapp</option>
                    </select>
                  </div>
                  <div class="form-group mb-1">
                    <label>User</label>
                    <select name="user" class="form-control"  required>
                      @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group mb-1">
                    <label>Diatur Marketplace</label>
                    <select name="diatur_marketplace" class="form-control"  required>
                        <option>ya</option>
                        <option>tidak</option>
                    </select>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </form>
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->

<script>
$(document).ready( function () {
    $('#myTable').DataTable({
      "ordering": false,
    });
} );
</script>
@endsection
