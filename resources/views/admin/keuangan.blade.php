@extends('layouts.app')
<?php $page = "laporan" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Dashboard</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/home">Laporan keuangan</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Stats Horizontal Card -->
                    <div class="row">
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <h6 class="fw-bolder mb-0">@currency($ballance)</h6>
                                        <p class="card-text">Ballance</p>
                                    </div>
                                    <div class="avatar bg-light-success p-50 m-0">
                                        <div class="avatar-content" data-bs-toggle="tooltip" title="Merah Lebih Rendah Dari Kemarin, Hijau Lebih Tinggai Dari Kemarin *blm di buat" data-bs-animation="false">
                                            <i data-feather="alert-octagon" class="font-medium-5"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @foreach($listKantongDana as $lkd)
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">

                        @php
                            if($lkd->post == "Tokopedia"){
                                $icon = "tokopedia.png";
                            }elseif($lkd->post == "Shopee"){
                                $icon = "shopee.png";
                            }elseif($lkd->post == "BliBLi"){
                                $icon = "blibli.png";
                            }elseif($lkd->post == "Lazada"){
                                $icon = "lazada.png";
                            }else{
                                $icon = "ego.jpg";
                            };
                        @endphp
                                    <div>
                                        <h6 class="fw-bolder mb-0">@currency($lkd->total)</h6>
                                        <p class="card-text">{{$lkd->post}}</p>
                                    </div>
                                    <div class="avatar bg-light-info p-50 m-0">
                                        <div class="avatar-content">
                                            <span class="info-box-icon"><img src="../../dist/img/{{$icon}}" width="30" height="30" ></span>
                                        </div>
                                    </div>
                                  </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!--/ Stats Horizontal Card -->

                    <div class="content-header-left col-md-9 col-12 mb-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="row">
                              <div class="col-xl-12 col-md-5 col-sm-12 mb-1">
                                <button type="btn" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-debit"><i data-feather="plus-square" class="me-1"></i> Dana Masuk</button>
                                <button type="btn" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-kredit"><i data-feather="minus-square" class="me-1"></i> Dana Keluar</button>
                                <button type="btn" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-akun"><i data-feather="plus-square" class="me-1"></i> Akun Keuangan</button>
                                <button type="btn" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-rekening "><i data-feather="plus-square" class="me-1"></i> Rekening</button>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>

          <!-- PANJANG TABLE CARD -->
          <div class="col-lg-12">
        <div class="card card-primary card-outline card-outline-tabs">
              <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-four-home-tab" data-bs-toggle="pill" href="#custom-tabs-four-home" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">LAPORAN KEUANGAN</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-profile-tab" data-bs-toggle="pill" href="#custom-tabs-four-profile" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">AKUN KEUANGAN</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-profile-tab" data-bs-toggle="pill" href="#custom-tabs-four-rekening" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">REKENING BANK</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-four-tabContent">
                  <div class="tab-pane fade active show" id="custom-tabs-four-home" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                    <div class="form-group row col-md-12">
                    </div>
                     <table class="table table-striped">
                      <thead>
                      <tr>
                        <th>Tanggal</th>
                        <th>Nama Transaksi</th>
                        <th>Dana Masuk</th>
                        <th>Dana Keluar</th>
                        <th>Kantong Dana</th>
                        <th>Akun Keuangan</th>
                      </tr>
                      </thead>
                      <tbody>
                        @foreach($keuangan as $keu)
                      <tr class="shop-item">
                        <td>{{ $keu->tanggal }}</td>
                        <td>{{ $keu->nama_teransaksi }}</td>
                        <td>@currency($keu->debit)</td>
                        <td>@currency($keu->kredit)</td>
                        <td>{{ $keu->post }}</td>
                        <td>{{ $keu->post_akun }}</td>
                      </tr>
                      @endforeach
                      </tbody>
                    </table>
                    <div class="form-group mt-3">
                    {{$keuangan->links()}}
                    </div>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                    <div class="form-group row col-md-12">
                    </div>
                     <table class="table table-striped">
                      <thead>
                      <tr>
                        <th class="text-center">Nomor</th>
                        <th class="text-center">Nama Akun</th>
                        <th class="text-center">Option</th>
                      </tr>
                      </thead>
                      <tbody>
                        @foreach($akun as $index => $nuka)
                      <tr class="shop-item">
                        <td class="text-center">{{ $index+1 }}</td>
                        <td class="text-center">{{ $nuka->nama_akun }}</td>
                        <td class="text-center"><a href="/hapus/akun/{{$nuka->id_akun}}"class="btn btn-sm btn-danger hapusdata"><i data-feather="trash"></i></a></td>
                      </tr>
                      @endforeach
                      </tbody>
                    </table>
                    <div class="form-group mt-3">
                    {{$akun->links()}}
                    </div>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-four-rekening" role="tabpanel" aria-labelledby="custom-tabs-four-rekening-tab">
                    <div class="form-group row col-md-12">
                    </div>
                     <table class="table table-striped">
                      <thead>
                      <tr>
                        <th class="text-center">Pemilik Rekening</th>
                        <th class="text-center">Nama Bank</th>
                        <th class="text-center">Nomor Rekening</th>
                      </tr>
                      </thead>
                      <tbody>
                        @foreach($listrekening as $asdf)
                      <tr class="shop-item">
                        <td class="text-center">{{ $asdf->pemilik_rekening }}</td>
                        <td class="text-center">{{ $asdf->nama_bank }}</td>
                        <td class="text-center">{{ $asdf->nomor_rekening }}</td>
                      </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
          <!-- /.col-md-12 -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
          <!-- modal category -->
          <div class="modal fade" id="modal-debit">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Dana Masuk</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/dana/masuk">
                @csrf
                <div class="modal-body">
                  <div class="form-group">
                    <input name="tanggal" type="date" class="form-control mb-1" required>
                  </div>
                  <div class="form-group">
                    <input name="nama_teransaksi" type="text" class="form-control mb-1" placeholder="Nama Transaksi" required>
                  </div>
                  <div class="form-group">
                    <input name="debit" type="text" class="form-control mb-1" placeholder="Nominal" required>
                  </div>
                  <div class="form-group mb-1">
                    <Label>Masuk Ke Rekening</label>
                    <select name="post" class="form-control" required>
                      <option></option>
                        @foreach($listrekening as $lsrkng)
                      <option value="{{$lsrkng->nomor_rekening}}">{{$lsrkng->pemilik_rekening}}</option>
                        @endforeach
                      <option>Tokopedia</option>
                      <option>Bukalapak</option>
                      <option>BliBli</option>
                      <option>Shopee</option>
                      <option>Tempo</option>
                      <option>Ofline</option>
                    </select>
                  </div>
                  <div class="form-group mb-1">
                      <Label>Akun Keuangan</label>
                    <select name="post_keuangan" class="form-control" required>
                      <option></option>
                      <option>oprasional</option>
                      <option>Pendapatan</option>
                      @foreach($akun as $nuka)
                      <option>{{$nuka->nama_akun}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->
          <!-- modal category -->
          <div class="modal fade" id="modal-kredit">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Dana Keluar</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/dana/keluar">
                @csrf
                <div class="modal-body">
                  <div class="form-group mb-1">
                    <input name="tanggal" type="date" class="form-control" required>
                  </div>
                  <div class="form-group mb-1">
                    <input name="nama_teransaksi" type="text" class="form-control" placeholder="Nama Transaksi" required>
                  </div>
                  <div class="form-group mb-1">
                    <input name="debit" type="text" class="form-control" placeholder="Nominal" required>
                  </div>
                  <div class="form-group mb-1">
                    <Label>Keluar Dari Rekening</label>
                    <select name="post" class="form-control" required>
                      <option></option>
                        @foreach($listrekening as $lsrkng)
                      <option value="{{$lsrkng->nomor_rekening}}">{{$lsrkng->pemilik_rekening}}</option>
                        @endforeach
                      <option>Tokopedia</option>
                      <option>Bukalapak</option>
                      <option>BliBli</option>
                      <option>Shopee</option>
                      <option>Tempo</option>
                      <option>Ofline</option>
                    </select>
                  </div>
                  <div class="form-group mb-1">
                    <Label>Akun Keuangan</label>
                    <select name="post_keuangan" class="form-control" required>
                      <option></option>
                      <option>oprasional</option>
                      <option>pendapatan</option>
                      @foreach($akun as $nuka)
                      <option>{{$nuka->nama_akun}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->

          <!-- modal category -->
          <div class="modal fade" id="modal-akun">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Akun</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/tambah/akun">
                @csrf
                <div class="modal-body">
                  <div class="form-group">
                    <input name="akun" type="text" class="form-control" placeholder="Nama Akun" required>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->

          <!-- modal category -->
          <div class="modal fade" id="modal-rekening">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Tambah Rekening</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/tambahrekening">
                @csrf
                <div class="modal-body">
                  <div class="form-group mb-2">
                    <input name="pemilik_rekening" type="text" class="form-control" placeholder="Pemilik Rekening" required>
                  </div>
                  <div class="form-group mb-2">
                    <input name="nama_bank" type="text" class="form-control" placeholder="Nama Bank" required>
                  </div>
                  <div class="form-group mb-2">
                    <input name="nomor_rekening" type="text" class="form-control" placeholder="Nomor Rekening" required>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->

@endsection
