<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="{{ asset('dist/style.css') }}">
  <title>Invoice</title>
</head>

<body>
  <div class="ticket">
    @foreach($invoice as $inv)
    <p class="centered">{{$inv->invoice}}
    <table style="width:100%">
  
    <p class="centered">
      PENERIMA : {{$inv->nama_pembeli}}
      <br>
      HP : {{$inv->hp_pembeli}}
      <br>
      ALAMAT : {{$inv->alamat}}
    </p>
    @endforeach
      <thead>
        <tr style="background-color: #f4f4f4;">
          <th class="description">Deskripsi</th>
          <th class="quantity">Qty</th>
        </tr>
      </thead>
      <tbody>
      @foreach ($item as $barang)
        <tr>
          <td class="description">{{$barang->title}}</td>
          <td class="quantity">{{$barang->jumlah}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
    <p class="centered">
      Terima kasih atas pembeliannya! Di tunggu pesanan selanjutnya
    </p>
  </div>
  <button id="btnPrint" class="hidden-print">Print</button>
  <script src="{{ asset('dist/script.js') }}"></script>
</body>

</html>










