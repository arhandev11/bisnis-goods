@extends('layouts.app')
<?php $page = "resellerorder" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Reseller Order</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/home">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/resellerorder">Reseller Order</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

              

            <div  class="col-xl-12 col-md-6 col-12">
                <div class="card card-statistics">
                    <div class="card-body statistics-body">
                        <div class="row">
                            <div class="col-xl-3 col-sm-6 col-12">
                                <div class="d-flex flex-row">
                                    <div class="avatar bg-light-success me-2">
                                        <div class="avatar-content">
                                            <i data-feather="box"></i>
                                        </div>
                                    </div>
                                    <div class="my-auto">
                                        <h4 class="fw-bolder mb-0">{{$totaljmltrx}}</h4>
                                        <p class="card-text font-small-3 mb-0">Total Transaksi</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
                                <div class="d-flex flex-row">
                                    <div class="avatar bg-light-info me-2">
                                        <div class="avatar-content">
                                            <i data-feather="trending-up"></i>
                                        </div>
                                    </div>
                                    <div class="my-auto">
                                        <h4 class="fw-bolder mb-0">@currency($totaltrx)</h4>
                                        <p class="card-text font-small-3 mb-0">Total Belanja</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                <div class="d-flex flex-row">
                                    <div class="avatar bg-light-info me-2">
                                        <div class="avatar-content">
                                            <i data-feather="trending-up"></i>
                                        </div>
                                    </div>
                                    <div class="my-auto">
                                        <h4 class="fw-bolder mb-0">@currency($totalbayar)</h4>
                                        <p class="card-text font-small-3 mb-0">Sudah Dibayar</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                <div class="d-flex flex-row">
                                    <div class="avatar bg-light-info me-2">
                                        <div class="avatar-content">
                                            <i data-feather="trending-down"></i>
                                        </div>
                                    </div>
                                    <div class="my-auto">
                                        <h4 class="fw-bolder mb-0">@currency($tagihan)</h4>
                                        <p class="card-text font-small-3 mb-0">Hutang Tempo Kamu</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




          <!-- PANJANG TABLE CARD -->
            <div class="row" >
            <div class="col-lg-6">
            <div class="card card-body">
                        <div class="table-responsive mb-5">
                            <label class="mb-1">Daftar Rencana Pembayaran</label>
                        <table class="table table-striped table-responsive">
                        <thead>
                        <tr>
                          <th>Invoice</th>
                          <th>Tanggal</th>
                          <th>Total Bayar</th>
                          <th>Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($dataRcb as $rcb)
                        <tr>
                          <td><a target="_blank" href="/invoice/thermal/{{ $rcb->id}}">{{ $rcb->invoice}}</a></td>
                          <td>{{ $rcb->tanggal}}</td>
                          <td>@currency($rcb->tagihan)</td>
                            <td>
                                <a href="/hapusrencanabayar/{{$rcb->id_rencana}}" class="btn btn-sm btn-danger"><i data-feather="trash"></i></a>
                            </td>
                        </tr>
                          @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td>@currency($dataRcbtotal)</td>
                            <td></td>
                        </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
                </div>
            <div class="col-lg-6">
            <div class="card card-body">
                        
                <form action="/buatpembayaran" method="post"  enctype="multipart/form-data">
                    @csrf
                        <div class="row">
                            <label class="mb-1">Upload Bukti Transfer</label>
                            <div class="col-6">
                                <input name="file" type="file" class="form-control" required>
                            @foreach($dataRcb as $index => $rcb)
                                <input type="hidden" name="id_inv[{{$index+0}}]" value="{{$rcb->id_inv}}" >
                            @endforeach
                            </div>
                            <div class="col-6">
                                <button class="btn btn-primary" type="submit">Kirim</button>
                            </div>
                        </div>
                </form>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="content-header-left col-md-12 col-12 mb-1" style="display:block">
                    <div class="row breadcrumbs-top">
                        <div class="col-3">
                            <div class="row">
                              <div class="col-xl-12 col-md-12 col-sm-12 mb-1">
                                    <form method="get">
                                        <select name="dari" class="form-control col-6" aria-describedby="basic-addon-search2">
                                            <option>Invoice</option>
                                            <option>Resi</option>
                                        </select>
                              </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="row">
                              <div class="col-xl-12 col-md-12 col-sm-12 mb-1">
                                        <input name="cari" type="text" class="form-control col-6" placeholder="Search..." aria-label="Search..." aria-describedby="basic-addon-search2">
                              </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="row">
                              <div class="col-xl-12 col-md-12 col-sm-12 mb-1">
                                        <button class="btn btn-primary">Cari</button>
                                    </form>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="card card-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab" data-bs-target="#navs-stok-barang" aria-controls="navs-stok-barang" aria-selected="true">
                              Transaksi Tempo
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-category-produk" aria-controls="navs-category-produk" aria-selected="false" tabindex="-1">
                              Transaksi Lunas
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-batal" aria-controls="navs-batal" aria-selected="false" tabindex="-1">
                              Transaksi Batal
                            </button>
                        </li>
                    </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active show" id="navs-stok-barang" role="tabpanel">
                        <div class="table-responsive">
                        <table class="table table-striped table-responsive">
                        <thead>
                        <tr>
                          <th>Invoice</th>
                          <th>Resi</th>
                          <th>Tanggal</th>
                          <th>Jumlah</th>
                          <th>Total Bayar</th>
                          <th>Platform</th>
                          <th>Status Tempo</th>
                          <th>Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($hutang as $ht)
                        <tr class="shop-item">
                          <td><a target="_blank" href="/invoice/thermal/{{ $ht->id}}">{{ $ht->expedisi}}</a></td>
                          <td>{{ $ht->code_booking}}</td>
                          <td>{{ $ht->tanggal}}</td>
                          <td>{{ $ht->jumlah_barang}}</td>
                          <td>@currency($ht->tagihan)</td>
                          <td>{{ $ht->platform}}</td>
                          <td>{{ $ht->tempo}}</td>
                          @php
                            if($ht->check_bayar == "check"){
                                $icon  = "check-circle";
                                $dis   = "disabled";
                                $class = "btn-dark";
                            }else{
                                $icon  = "plus";
                                $dis   = "";
                                $class = "btn-success";
                            }
                          @endphp
                            <td>
                                <a href="/detailorderreseller/{{$ht->id}}" class="btn btn-sm btn-primary"><i data-feather="eye"></i></a>
                                <a href="/rencanabayar/{{$ht->id}}" class="btn btn-sm {{$class}} {{$dis}}"><i data-feather="{{$icon}}"></i></a>
                            </td>
                          </tr>
                          @endforeach
                          </tbody>
                        </table>
                        <div class="form-group mt-2">
                        {{$hutang->links()}}
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane fade" id="navs-category-produk" role="tabpanel">
                        <div class="table-responsive">
                        <table class="table table-striped table-responsive">
                        <thead>
                        <tr>
                          <th>Invoice</th>
                          <th>Tanggal</th>
                          <th>Jumlah</th>
                          <th>Total Bayar</th>
                          <th>Platform</th>
                          <th>Status Pesanan</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($lunas as $ls)
                        <tr class="shop-item">
                          <td width="2%"><a target="_blank" href="/invoice/thermal/{{ $ls->id}}">{{ $ls->invoice}}</a></td>
                          <td>{{ $ls->tanggal}}</td>
                          <td>{{ $ls->jumlah_barang}}</td>
                          <td>@currency($ls->tagihan)</td>
                          <td>{{ $ls->platform}}</td>
                          <td>{{ $ls->tempo}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                      </table>
                        <div class="form-group pl-2">
                        {{$lunas->links()}}
                        </div>   
                    </div> 
                    </div>
                    <div class="tab-pane fade" id="navs-batal" role="tabpanel">
                        <div class="table-responsive">
                        <table class="table table-striped">
                        <thead>
                        <tr>
                          <th>Invoice</th>
                          <th>Tanggal</th>
                          <th>Jumlah</th>
                          <th>Total Bayar</th>
                          <th>Jenis</th>
                          <th>Alasan</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($cancelpesanan as $cancel)
                        <tr class="shop-item">
                          <td><a target="_blank" href="/invoice/thermalReseller/{{ $cancel->id_invoice}}">{{ $cancel->invoice}}</a></td>
                          <td>{{ $cancel->tanggal}}</td>
                          <td>{{ $cancel->jumlah_barang}}</td>
                          <td>@currency($cancel->tagihan)</td>
                          <td>{{ $cancel->jenis_pembatalan}}</td>
                          <td>{{ $cancel->alasan_pembatalan}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                      </table>
                        <div class="form-group mt-2">
                        {{$cancelpesanan->links()}}
                        </div> 
                    </div> 
                    </div>
                </div>
            </div>
@endsection