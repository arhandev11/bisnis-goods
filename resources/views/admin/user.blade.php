@extends('layouts.app')
<?php $page = "User" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
  <!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Laporan</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/laporan">User</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group mb-1">
                <button type="btn" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-users"><i data-feather="plus-square" class="me-1"></i> User</button>
            </div>
              <!-- / TOMBOL INPUT STOK -->
              <!-- TABLE CARD -->
            <div class="card">
            <table class="table table-striped">
                  <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Tier</th>
                    <th>Nama Toko</th>
                    <th>Nomor Hp</th>
                    <th>Nomor Hp</th>
                  </tr>
                  </thead>
                  <tbody>
                      @foreach($user as $pengguna)
                  <tr class="shop-item">
                    <td>{{$pengguna->name}}</td>
                    <td>{{$pengguna->email}}</td>
                    <td>{{$pengguna->role}}</td>
                    <td>{{$pengguna->tier}}</td>
                    <td>{{$pengguna->toko}}</td>
                    <td>{{$pengguna->hp_toko}}</td>
                    <td>
                      <a href="/detailuser/{{$pengguna->id}}" class="btn btn-sm btn-primary"><i data-feather="edit"></i></a>
                      <a href="/hapususer/{{$pengguna->id}}" class="btn btn-sm btn-danger hapusdata"><i data-feather="trash"></i></a>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
            </div>
            <!-- / TABLE CARD -->
          </div>
          <!-- /.col-md-12 -->
          <br>

                <div class="form-group pl-2">
                {{$user->links()}}
                </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal category -->
  <div class="modal fade" id="modal-users">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Tambah User</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="post" action="/tambah/user">
                @csrf
                <div class="modal-body">
                  <div class="form-group mb-1">
                    <input name="nama" type="text" class="form-control" placeholder="Nama" required>
                  </div>
                  <div class="form-group mb-1">
                    <input name="email" type="text" class="form-control" placeholder="Email" required>
                  </div>
                  <div class="form-group mb-1">
                    <input name="hp" type="text" class="form-control" placeholder="hp" required>
                  </div>
                  <div class="form-group mb-1">
                    <input name="toko" type="text" class="form-control" placeholder="Nama Toko">
                  </div>
                  @php
                    if($sadmVal->role == 1){
                      $disabled = "disabled";
                    }else{
                      $disabled = "";
                    };
                  @endphp
                  <div class="form-group mb-1">
                    <select name="role" class="form-control"  required>
                        <option value=""></option>
                      @foreach($selectrole as $role)
                        <option value="{{$role->id_role}}">{{$role->role_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group mb-1">
                    <input name="password" type="text" class="form-control" placeholder="password" required>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </form>
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->

<script>
$(document).ready( function () {
    $('#myTable').DataTable({
      "ordering": false,
    });
} );
</script>
@endsection
