@extends('layouts.app')
<?php $page = "pesanancs" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Pesanan Agen</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/home">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/resellerorder">Pesanan Agen</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
                
                
            <div class="content-header-left col-md-12 col-12 mb-1" style="display:block">
              <div class="row breadcrumbs-top">
                  <div class="col-3">
                      <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 mb-1">
                              <form method="get">
                                  <select name="dari" class="form-control col-6" aria-describedby="basic-addon-search2">
                                      <option>Invoice</option>
                                      <option>Resi</option>
                                  </select>
                        </div>
                      </div>
                  </div>
                  <div class="col-3">
                      <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 mb-1">
                                  <input name="cari" type="text" class="form-control col-6" placeholder="Search..." aria-label="Search..." aria-describedby="basic-addon-search2" required>
                        </div>
                      </div>
                  </div>
                  <div class="col-3">
                      <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 mb-1">
                                  <button class="btn btn-primary">Cari</button>
                              </form>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
                <div class="table-responsive card card-body" style="display: <?php if(Auth::user()->role != 3){ echo "block";}else{ echo "none";} ?>">
                  <div class="nav-align-top nav-tabs-shadow mb-4">
                    <ul class="nav nav-tabs nav-fill" role="tablist">
                      <li class="nav-item">
                        <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab" data-bs-target="#navs-pesananbaru" aria-controls="navs-pesananbaru" aria-selected="true">
                          <i data-feather="home"></i> Pesanan Baru 
                          <a class="badge badge-light-success ms-1">{{$counttrxBaru}}</a>
                        </button>
                      </li>
                      <li class="nav-item">
                        <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-diprosses" aria-controls="navs-diprosses" aria-selected="false">
                          <i data-feather="printer"></i> Pesanan Diproses
                          <a class="badge badge-light-success ms-1">{{$counttrxproses}}</a>
                        </button>
                      </li>
                      <li class="nav-item">
                        <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-dipacking" aria-controls="navs-dipacking" aria-selected="false">
                          <i data-feather="gift"></i> Pesanan Dipacking
                          <a class="badge badge-light-success ms-1">{{$counttrxpacking}}</a>
                        </button>
                      </li>
                      <li class="nav-item">
                        <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-dikirim" aria-controls="navs-dikirim" aria-selected="false">
                          <i data-feather="send"></i> Pesanan Dikirim
                          <a class="badge badge-light-success ms-1">{{$counttrxkirim}}</a>
                        </button>
                      </li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane fade show active" id="navs-pesananbaru" role="tabpanel">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                  <th>Invoice</th>
                                  <th>Resi</th>
                                  <th>Tanggal</th>
                                  <th>Jumlah</th>
                                  <th>Total Bayar</th>
                                  <th>Platform</th>
                                  <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @foreach($trxBaru as $new)
                                <tr class="shop-item">
                                  <td width="2%">{{ $new->expedisi}}</td>
                                  <td>{{ $new->code_booking}}</td>
                                  <td>{{ $new->tanggal}}</td>
                                  <td>{{ $new->jumlah_barang}}</td>
                                  <td>@currency($new->tagihan + $new->ongkir - $new->diskon)</td>
                                  <td>{{ $new->platform}}</td>
                                    <td>
                                        <a href="/detailorderreseller/{{$new->id}}" class="btn btn-sm btn-primary">Detail</a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="form-group pl-2">
                            {{$trxBaru->links()}}
                            </div>   
                        </div>  
                      </div>
                      <div class="tab-pane fade" id="navs-diprosses" role="tabpanel">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                  <th>Invoice</th>
                                  <th>Tanggal</th>
                                  <th>Jumlah</th>
                                  <th>Total Bayar</th>
                                  <th>Platform</th>
                                  <th>Resi</th>
                                  <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @foreach($trxproses as $proses)
                                <tr class="shop-item">
                                  <td width="2%">{{ $proses->expedisi}}</td>
                                  <td>{{ $proses->tanggal}}</td>
                                  <td>{{ $proses->jumlah_barang}}</td>
                                  <td>@currency($proses->tagihan + $proses->ongkir - $proses->diskon)</td>
                                  <td>{{ $proses->platform}}</td>
                                  <td>{{ $proses->code_booking}}</td>
                                    <td>
                                        <a href="/detailorderreseller/{{$proses->id}}" class="btn btn-sm btn-primary">Detail</a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="form-group pl-2">
                            {{$trxproses->links()}}
                            </div>   
                        </div>  
                      </div>
                      <div class="tab-pane fade" id="navs-dipacking" role="tabpanel">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                  <th>Invoice</th>
                                  <th>Tanggal</th>
                                  <th>Jumlah</th>
                                  <th>Total Bayar</th>
                                  <th>Platform</th>
                                  <th>Status Pesanan</th>
                                  <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @foreach($trxpacking as $packing)
                                <tr class="shop-item">
                                  <td width="2%"><a target="_blank" href="/invoicethermal/{{ $packing->id}}">{{ $packing->invoice}}</a></td>
                                  <td>{{ $packing->tanggal}}</td>
                                  <td>{{ $packing->jumlah_barang}}</td>
                                  <td>@currency($packing->tagihan + $packing->ongkir - $packing->diskon)</td>
                                  <td>{{ $packing->platform}}</td>
                                  <td>{{ $packing->tempo}}</td>
                                    <td>
                                        <a href="/detailorderreseller/{{$packing->id}}" class="btn btn-sm btn-primary">Detail</a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="form-group pl-2">
                            {{$trxpacking->links()}}
                            </div>   
                        </div>  
                      </div>
                      <div class="tab-pane fade" id="navs-dikirim" role="tabpanel">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                  <th>Invoice</th>
                                  <th>Tanggal</th>
                                  <th>Jumlah</th>
                                  <th>Total Bayar</th>
                                  <th>Platform</th>
                                  <th>Status Pesanan</th>
                                  <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @foreach($trxkirim as $kirim)
                                <tr class="shop-item">
                                  <td width="2%"><a target="_blank" href="/invoicethermal/{{ $kirim->id}}">{{ $kirim->invoice}}</a></td>
                                  <td>{{ $kirim->tanggal}}</td>
                                  <td>{{ $kirim->jumlah_barang}}</td>
                                  <td>@currency($kirim->tagihan + $kirim->ongkir - $kirim->diskon)</td>
                                  <td>{{ $kirim->platform}}</td>
                                  <td>{{ $kirim->tempo}}</td>
                                    <td>
                                        <a href="/detailorderreseller/{{$kirim->id}}" class="btn btn-sm btn-primary">Detail</a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="form-group pl-2">
                            {{$trxkirim->links()}}
                            </div>   
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>


@endsection