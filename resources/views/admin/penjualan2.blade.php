@extends('layouts.app')
<?php $page = "shop" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">System Transaksi</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="stok">System Transaksi</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

          <!-- PANJANG TABLE CARD -->
            <div class="col-lg-12">
               @if ($message = Session::get('warning'))
              <div class="alert alert-warning text-center">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <span class="text-center">{{ $message }}</span>
              </div>
              @endif
            </div>



            <div class="row">
              <div class="col-lg-9">
              <div class="card">
              <div class="card-body">
                <div class="row mb-5">
                  <form action="/tambah/chart" method="get"><input name="user" type="hidden" value="{{ Auth::user()->id }}"><input placeholder="Code Barang" name="id_barang" class="form-control" autofocus><input type="hidden" name="jml" class="form-control" value="1"></form>
                </div>
                <div class="table-responsive">
                <table class="table table-stripped small">
                  <thead>
                  <tr>
                    <th width="10%">Code</th>
                    <th width="20%">Nama</th>
                    <th width="15%">Tersedia</th>
                    <th width="15%">Pesanan</th>
                    <th width="15%">Satuan</th>
                    <th width="15%">Sub Total</th>
                    <th width="15%">option</th>
                  </tr>
                  </thead>
                  @foreach ($chart as $crt)
                  <tbody>
                    <td >{{$crt->id_barang}}</td>
                    <td>{{$crt->nama_barang}}</td>
                    <td>{{$crt->stok}}</td>
                    <td><form action="/tambah/chart/manual" method="get"><input name="id_barang" type="hidden" value="{{$crt->id_barang}}"><input type="number" class="form-control form-control-sm col-md-10" value="{{$crt->jml_barang}}" name="jml"></form></td>
                    <td>@currency($crt->harga_jual)</td>
                    <td>@currency($crt->total_harga)</td>
                    <td><a href="/hapus/chart/{{$crt->id_barang}}" class="btn btn-sm btn-danger"><i data-feather="trash"></i></a></td>
                  </tbody>
                  @endforeach
                  <tbody>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>@currency($subtotal)</td>
                    <td></td>
                  </tbody>
                </table>
              </div>
            </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <div class="col-lg-3">
            <div class="card">
              <div class="card-body">
            <form action="/proses/chart" method="POST">
              @csrf
                <div class="form-group mb-1">
                  @foreach ($chart as $index => $crt)
                    <input type="hidden" name="codeItem[{{$index}}]" value="{{$crt->id_barang}}">
                    <input type="hidden" name="idChart[{{$index}}]" value="{{$crt->id_chart}}">
                    <input type="hidden" name="namaItem[{{$index}}]" value="{{$crt->nama_barang}}">
                    <input type="hidden" name="hargaSatuan[{{$index}}]" value="{{$crt->harga_jual}}">
                    <input type="hidden" name="jmlsbt[{{$index}}]" value="{{$crt->jml_barang}}" >
                    <input type="hidden" name="tanggal_orderar[{{$index}}]" type="text" value="{{date('Y-m-d')}}">
                  @endforeach
                  <input type="hidden" name="jumlah_barang" type="text" value="{{$totbarang}}">
                  <input type="hidden" name="tanggal_order" type="text" value="{{date('Y-m-d')}}">
                  <input type="hidden" name="tagihan" type="text" value="{{$subtotal}}">
                  <input type="hidden" name="total_modal" type="text" value="{{$submodal}}">
                <select id="platform" name="platform" class="form-control" onchange="tampil()">
                  <option value="Tokopedia">Tokopedia</option>
                  <option value="Shopee">Shopee</option>
                  <option value="Lazada">Lazada</option>
                  <option value="BliBLi">Bli Bli</option>
                  <option value="Ofline">Ofline</option>
                </select>
              </div>
              <div class="form-group mb-1">
                <input style="display:none;" id="nama_pembeli" name="nama_pembeli" type="text" class="form-control form-control" placeholder="Nama Pembeli">
              </div>
              <div style="display:none;" id="kembalian" class="form-group mb-1">
                <input type="text" class="form-control" placeholder="Kembalian">
              </div>
              <div style="display:none;" id="uang_cs" class="form-group mb-1">
                <input type="text" class="form-control" placeholder="Uang Customer">
              </div>
              <div id="tanggal_tempo" style="display:none;" class="form-group mb-1">
                <input  name="tanggal_tempo" type="date" class="form-control">
              </div>
              <button class="btn btn-primary float-right" type="submit">Simpan</button>
            </form>
              </div>
            </div>
          </div>








           <div class="">
                <div class="card card-body col-xl-12">
                  <div class="nav-align-top nav-tabs-shadow mb-4">
                    <ul class="nav nav-tabs" role="tablist">
                      <li class="nav-item">
                        <button
                          type="button"
                          class="nav-link active"
                          role="tab"
                          data-bs-toggle="tab"
                          data-bs-target="#navs-marketplate"
                          aria-controls="navs-marketplate"
                          aria-selected="true"
                        >
                          Marketplace
                        </button>
                      </li>
                      <li class="nav-item">
                        <button
                          type="button"
                          class="nav-link"
                          role="tab"
                          data-bs-toggle="tab"
                          data-bs-target="#navs-ofline"
                          aria-controls="navs-ofline"
                          aria-selected="false"
                        >
                          Ofline
                        </button>
                      </li>
                      <li class="nav-item">
                        <button
                          type="button"
                          class="nav-link"
                          role="tab"
                          data-bs-toggle="tab"
                          data-bs-target="#navs-tempo-lunas"
                          aria-controls="navs-tempo-lunas"
                          aria-selected="false"
                        >
                          Tempo Lunas
                        </button>
                      </li>
                      <li class="nav-item">
                        <button
                          type="button"
                          class="nav-link"
                          role="tab"
                          data-bs-toggle="tab"
                          data-bs-target="#navs-tempo-hutang"
                          aria-controls="navs-tempo-hutang"
                          aria-selected="false"
                        >
                          Tempo Hutang
                        </button>
                      </li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane fade show active" id="navs-marketplate" role="tabpanel">
                        <table class="table table-striped">
                        <thead>
                        <tr>
                          <th>Invoice</th>
                          <th>Tanggal</th>
                          <th>Jumlah</th>
                          <th>Total Bayar</th>
                          <th>Platform</th>
                          <th class="text-center">option</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($invoice as $inv)
                        <tr class="shop-item">
                          <td><a target="_blank" href="/invoice/thermal/{{ $inv->id}}">{{ $inv->invoice}}</a></td>
                          <td>{{ $inv->tanggal}}</td>
                          <td>{{ $inv->jumlah_barang}}</td>
                          <td>@currency($inv->tagihan)</td>
                          <td>{{ $inv->platform}}</td>
                          <td class="text-center">
                            <button 
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                  data-submenu
                                   class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1" type="button">
                              <i  data-feather="chevron-down"></i>
                              </button>
                            <ul class="dropdown-menu">
                              <a href="/returcod" onclick="myFunctionCancel()" class="dropdown-item"><i data-feather="edit"></i> Retur Cod</a>
                              <a href="/returexpedisi" onclick="myFunctionCancel()" class="dropdown-item"><i data-feather="move"></i> Retur Expedisi</a>
                            </ul>
                          </td>
                        </tr>
                        @endforeach
                        </tbody>
                      </table>
                        <div class="form-group mt-2">
                        {{$invoice->links()}}
                        </div>
                      </div>
                      <div class="tab-pane fade" id="navs-ofline" role="tabpanel">
                        <table class="table table-striped">
                        <thead>
                        <tr>
                          <th>Invoice</th>
                          <th>Tanggal</th>
                          <th>Pembeli</th>
                          <th>Jumlah</th>
                          <th>Total Bayar</th>
                          <th>Platform</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($ofline as $of)
                        <tr class="shop-item">
                          <td><a target="_blank" href="/invoice/thermal/{{ $of->id}}">{{ $of->invoice}}</a></td>
                          <td>{{ $of->tanggal}}</td>
                          <td>{{ $of->nama_pembeli}}</td>
                          <td>{{ $of->jumlah_barang}}</td>
                          <td>@currency($of->tagihan)</td>
                          <td>{{ $of->platform}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                      </table>
                        <div class="pagination mt-2">
                        {{$ofline->links()}}
                        </div>  
                      </div>
                      <div class="tab-pane fade" id="navs-tempo-lunas" role="tabpanel">
                        <table class="table table-striped">
                        <thead>
                        <tr>
                          <th>Invoice</th>
                          <th>Tanggal</th>
                          <th>Pembeli</th>
                          <th>Jumlah</th>
                          <th>Total Bayar</th>
                          <th>Platform</th>
                          <th>Jatuh Tempo</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($lunas as $ls)
                        <tr class="shop-item">
                          <td><a target="_blank" href="/invoice/thermalReseller/{{ $ls->id}}">{{ $ls->invoice}}</a></td>
                          <td>{{ $ls->tanggal}}</td>
                          <td>{{ $ls->nama_pembeli}}</td>
                          <td>{{ $ls->jumlah_barang}}</td>
                          <td>@currency($ls->tagihan)</td>
                          <td>{{ $ls->platform}}</td>
                          <td>{{ $ls->tempo}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                      </table>
                        <div class="form-group mt-2">
                        {{$lunas->links()}}
                        </div>   
                      </div>
                      <div class="tab-pane fade" id="navs-tempo-hutang" role="tabpanel">
                        <table class="table table-striped">
                        <thead>
                        <tr>
                          <th>Invoice</th>
                          <th>Tanggal</th>
                          <th>Pembeli</th>
                          <th>Jumlah</th>
                          <th>Total Bayar</th>
                          <th>Platform</th>
                          <th>Jatuh Tempo</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($hutang as $ht)
                        <tr class="shop-item">
                          <td><a target="_blank" href="/invoice/thermalReseller/{{ $ht->id}}">{{ $ht->invoice}}</a></td>
                          <td>{{ $ht->tanggal}}</td>
                          <td>{{ $ht->nama_pembeli}}</td>
                          <td>{{ $ht->jumlah_barang}}</td>
                          <td>@currency($ht->tagihan)</td>
                          <td>{{ $ht->platform}}</td>
                          <td>{{ $ht->tempo}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                      </table>
                        <div class="form-group mt-2">
                        {{$hutang->links()}}
                        </div> 
                      </div>
                    </div>
                  </div>
                </div>
              <!-- Tabs -->

            </div>
              </div>
              <!-- /.card-body -->
            </section>
                <!-- Dashboard Analytics end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

<script>
  function tampil(){
    var s = document.getElementById('platform').value;
    if(s=="Ofline"){
      document.getElementById("nama_pembeli").style.display = "block";
      document.getElementById("uang_cs").style.display = "block";
      document.getElementById("kembalian").style.display = "block";
      document.getElementById("tanggal_tempo").style.display = "none";
    }else if(s=="Tempo"){
      document.getElementById("nama_pembeli").style.display = "block";
      document.getElementById("tanggal_tempo").style.display = "block";
      document.getElementById("uang_cs").style.display = "none";
      document.getElementById("kembalian").style.display = "none";
    }else{
      document.getElementById("nama_pembeli").style.display = "none";
      document.getElementById("uang_cs").style.display = "none";
      document.getElementById("kembalian").style.display = "none";
      document.getElementById("tanggal_tempo").style.display = "none";
    }
  }
</script>
@endsection
