@extends('layouts.app')
<?php $page = "dashboardcrm" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">

            <!-- Stats Horizontal Card -->
                    <div class="row">
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <h2 class="fw-bolder mb-0">@currency($brutoHariIni)</h2>
                                        <p class="card-text">Year Deal</p>
                                    </div>
                                    <div class="avatar bg-light-success p-50 m-0">
                                        <div class="avatar-content" data-bs-toggle="tooltip" title="Merah Lebih Rendah Dari Kemarin, Hijau Lebih Tinggai Dari Kemarin *blm di buat" data-bs-animation="false">
                                            <i data-feather="alert-octagon" class="font-medium-5" ></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <h2 class="fw-bolder mb-0">@currency($marginHariIni)</h2>
                                        <p class="card-text">Year Sales</p>
                                    </div>
                                    <div class="avatar bg-light-warning p-50 m-0">
                                        <div class="avatar-content">
                                            <i data-feather="alert-octagon" class="font-medium-5"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <h2 class="fw-bolder mb-0">@currency($expensHariIni)</h2>
                                        <p class="card-text">Year Expense</p>
                                    </div>
                                    <div class="avatar bg-light-warning p-50 m-0">
                                        <div class="avatar-content">
                                            <i data-feather="alert-octagon" class="font-medium-5"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <h2 class="fw-bolder mb-0">{{$pesananHariIni}} Orderan</h2>
                                        <p class="card-text">sales yang bersangkutan</p>
                                    </div>
                                    <div class="avatar bg-light-warning p-50 m-0">
                                        <div class="avatar-content">
                                            <i data-feather="alert-octagon" class="font-medium-5"></i>
                                        </div>
                                    </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Stats Horizontal Card -->




                    <div class="row match-height">
                        <div class="col-lg-4 col-12">
                            <div class="row match-height">
                                <!-- Earnings Card -->
                                <div class="col-lg-12 col-md-6 col-12">
                                    <div class="card earnings-card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-6">
                                                    <h6 class="card-title mb-1">Statistik Bulan Ini</>
                                                    <div class="font-small-2">Omset</div>
                                                    <h5 class="mb-1">@currency($brutoBulanIni)</h5>
                                                    <div class="font-small-2">Margin</div>
                                                    <h5 class="mb-1">@currency($marginBulanIni)</h5>
                                                    <div class="font-small-2">Expense</div>
                                                    <h5 class="mb-1">@currency($expensBulanIni)</h5>
                                                    <p class="card-text text-muted font-small-2">
                                                        <span class="fw-bolder <?php if($presResult > 0){echo "text-success";}else{echo "text-danger";}?>">{{round($presResult)}}%</span><span> <?php if($presResult > 0){echo "Lebih Besar";}else{echo "Lebih Kecil";}?> dari bulan lalu.</span>
                                                    </p>
                                                </div>
                                                <div class="col-6 mt-3">
                                                    <div id="earnings-donut-chart"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ Earnings Card -->
                                <!-- Bar Chart - Orders -->
                                <div class="col-lg-6 col-md-3 col-6">
                                    <div class="card">
                                        <div class="card-body pb-50">
                                            <h6>Total Pesanan (chart belum works)</h6>
                                            <h2 class="fw-bolder mb-1">{{$PesananTahunIni}}</h2>
                                            <div id="statistics-bar-chart"></div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ Bar Chart - Orders -->

                                <!-- Line Chart - Profit -->
                                <div class="col-lg-6 col-md-3 col-6">
                                    <div class="card card-tiny-line-stats">
                                        <div class="card-body pb-50">
                                            <h6>Profit (chart belum works)</h6>
                                            <h2 class="fw-bolder mb-1">6,24k</h2>
                                            <div id="statistics-line-chart"></div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ Line Chart - Profit -->
                            </div>
                        </div>

                        <!-- Revenue Report Card -->
                        <div class="col-lg-8 col-12">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                                    <div class="header-left">
                                        <h4 class="card-title">Omset Tahunan @currency($omsetTahunan)</h4>
                                        <a href="/laporan">Lihat Laporan</a>
                                    </div>
                                    <div class="d-flex align-items-center">
                                                <div class="d-flex align-items-center me-75 mb-2">
                                                    <span class="bullet bullet-primary font-small-3 me-50 cursor-pointer"></span>
                                                    <span>Omset</span>
                                                </div>
                                                <div class="d-flex align-items-center ms-75 mb-2">
                                                    <span class="bullet bullet-success font-small-3 me-50 cursor-pointer"></span>
                                                    <span>Margin</span>
                                                </div>
                                                <div class="d-flex align-items-center ms-75 mb-2">
                                                    <span class="bullet bullet-warning font-small-3 me-50 cursor-pointer"></span>
                                                    <span>Expense</span>
                                                </div>
                                            </div>
                                </div>
                                <div class="card-body">
                                    <canvas id="bar-chart-ex" data-height="300"></canvas>
                                </div>
                            </div>
                        </div>
                        <!--/ Revenue Report Card -->

                        <div class="col-lg-4 col-12">
                            <div class="row match-height">
                                <!-- Earnings Card -->
                                <div class="col-lg-12 col-md-6 col-12">
                                    <div class="card earnings-card">
                                        <div class="card-body">
                                            <div class="card-header">
                                            <h5 class="card-title">Stok Alert</h5>
                                            <div class="card-tools">
                                            </div>
                                          </div>
                                          <div class="card-body table-responsive p-0">
                                            <table class="table table-striped table-valign-middle small">
                                              <thead>
                                              <tr>
                                                <th>Product</th>
                                                <th>Stok</th>
                                              </tr>
                                              </thead>
                                              <tbody>
                                                @foreach($stokAlert as $alers)
                                              <tr>
                                                <td>{{$alers->nama_barang}}</td>
                                                <td><a class="badge badge-danger">{{$alers->stok}}</a></td>
                                              </tr>
                                              @endforeach
                                              </tbody>
                                            </table>
                                            <div class="pt-3 pl-1 small">
                                            {{$stokAlert->links()}}
                                          </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ Earnings Card -->
                            </div>
                        </div>

                        <div class="col-lg-8 col-12">
                            <div class="row match-height">
                                <!-- Earnings Card -->
                                <div class="col-lg-12 col-md-6 col-12">
                                    <div class="card earnings-card">
                                        <div class="card-body">
                                            <div class="card-header">
                                            <h5 class="card-title">Item Terlaris</h5>
                                            <div class="card-tools">
                                            </div>
                                          </div>
                                          <div class="card-body table-responsive p-0">
                                            <table class="table table-striped table-valign-middle small">
                                              <thead>
                                              <tr>
                                                <th>Product</th>
                                                <th>Terjual</th>
                                                <th>Total Sales</th>
                                              </tr>
                                              </thead>
                                              <tbody>
                                                @foreach($terlaris as $trlrs)
                                              <tr>
                                                <td>{{$trlrs->title}}</td>
                                                <td>{{$trlrs->totalTerjual}} Pcs</td>
                                                <td>@currency($trlrs->totalHarga)</td>
                                              </tr>
                                              @endforeach
                                              </tbody>
                                            </table>
                                            <div class="pt-3 pl-1 small">
                                            {{$terlaris->links()}}
                                          </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ Earnings Card -->
                            </div>
                        </div>
                    </div>


          <div class="col-lg-12">

                  <div class="input-group-prepend pb-3">
                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                      Settings
                    </button>
                    <ul class="dropdown-menu">
                      <li data-toggle="modal" data-target="#modal-akun" class="dropdown-item">Stok Alert</li>
                      <li class="dropdown-divider"></li>
                      <li data-toggle="modal" data-target="#modal-pass" class="dropdown-item" class="dropdown-item">Rubah Password</li>
                    </ul>
                  </div>



        <!-- modal category -->
          <div class="modal fade" id="modal-akun">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Rubah Minimum Stok</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/rubah/minimum">
                @csrf
                <div class="modal-body">
                  <div class="form-group">
                    <input name="minimum" type="text" class="form-control" value="{{$minimumStok}}"required>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->
          <!-- modal category -->
          <div class="modal fade" id="modal-pass">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Rubah Password</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="POST" action="/rubah/password" onsubmit="myFunction()">
                @csrf
                <div class="modal-body">
                  <div class="form-group">
                    <input type="password" id="pass1" name="pass1" type="text" class="form-control" placeholder="Masukan Password" required>
                  </div>
                  <div class="form-group">
                    <input type="password" id="pass2" name="pass2" type="text" class="form-control" placeholder="Masukan Ulang" required>
                    <input type="hidden" name="id" value="{{ Auth::user()->id }}"  required>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->
<script>
  function myFunction() {
    var s = document.getElementById('pass1').value;
    var x = document.getElementById('pass2').value;
    if(s != x){
      alert("Password yang kamu ketik belum sesuai");
      event.preventDefault();
    }
  }
</script>

  
    <script >
      $(window).on('load', function () {
  'use strict';

  var chartWrapper = $('.chartjs'),
    flatPicker = $('.flat-picker'),
    barChartEx = $('#bar-chart-ex'),
    lineAreaChartEx = $('.line-area-chart-ex');

  // Color Variables
  var primaryColorShade = '#836AF9',
    yellowColor = '#ffe800',
    successColorShade = '#28dac6',
    warningColorShade = '#ffe802',
    warningLightColor = '#FDAC34',
    infoColorShade = '#299AFF',
    greyColor = '#4F5D70',
    blueColor = '#2c9aff',
    blueLightColor = '#84D0FF',
    greyLightColor = '#EDF1F4',
    tooltipShadow = 'rgba(0, 0, 0, 0.25)',
    lineChartPrimary = '#666ee8',
    lineChartDanger = '#ff4961',
    labelColor = '#6e6b7b',
    grid_line_color = 'rgba(200, 200, 200, 0.2)'; // RGBA color helps in dark layout

  

  // Detect Dark Layout
  if ($('html').hasClass('dark-layout')) {
    labelColor = '#b4b7bd';
  }

  // Wrap charts with div of height according to their data-height
  if (chartWrapper.length) {
    chartWrapper.each(function () {
      $(this).wrap($('<div style="height:' + this.getAttribute('data-height') + 'px"></div>'));
    });
  }

  // Init flatpicker
  if (flatPicker.length) {
    var date = new Date();
    flatPicker.each(function () {
      $(this).flatpickr({
        mode: 'range',
        defaultDate: ['2019-05-01', '2019-05-10']
      });
    });
  }

  // Bar Chart
  // --------------------------------------------------------------------
  if (barChartEx.length) {
    var barChartExample = new Chart(barChartEx, {
      type: 'bar',
      options: {
        elements: {
          rectangle: {
            borderWidth: 2,
            borderSkipped: 'bottom'
          }
        },
        responsive: true,
        maintainAspectRatio: false,
        responsiveAnimationDuration: 100,
        legend: {
          display: false
        },
        tooltips: {
          // Updated default tooltip UI
          shadowOffsetX: 1,
          shadowOffsetY: 1,
          shadowBlur: 8,
          shadowColor: tooltipShadow,
          backgroundColor: window.colors.solid.white,
          titleFontColor: window.colors.solid.black,
          bodyFontColor: window.colors.solid.black
        },
        scales: {
          xAxes: [
            {
              display: true,
              gridLines: {
                display: true,
                color: grid_line_color,
                zeroLineColor: grid_line_color
              },
              scaleLabel: {
                display: false
              },
              ticks: {
                fontColor: labelColor
              }
            }
          ],
          yAxes: [
            {
              display: true,
              gridLines: {
                color: grid_line_color,
                zeroLineColor: grid_line_color
              },
              ticks: {
                stepSize: 10000000,
                fontColor: labelColor
              }
            }
          ]
        }
      },
      data: {
        labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
        datasets: [
          {
            data: [{{$janTagihan}}, {{$febTagihan}}, {{$marTagihan}}, {{$aprTagihan}}, {{$meiTagihan}}, {{$junTagihan}}, {{$julTagihan}}, {{$agsTagihan}}, {{$sepTagihan}}, {{$oktTagihan}}, {{$novTagihan}}, {{$desTagihan}}],
            barThickness: 15,
            backgroundColor: primaryColorShade,
            borderColor: 'transparent'
          },
          {
            data: [{{$janTagihan-$janModal}}, {{$febTagihan-$febModal}}, {{$marTagihan-$marModal}}, {{$aprTagihan-$aprModal}}, {{$meiTagihan-$meiModal}}, {{$junTagihan-$junModal}}, {{$julTagihan-$julModal}}, {{$agsTagihan-$agsModal}}, {{$sepTagihan-$sepModal}}, {{$oktTagihan-$oktModal}}, {{$novTagihan-$novModal}}, {{$desTagihan-$desModal}}],
            barThickness: 15,
            backgroundColor: successColorShade,
            borderColor: 'transparent'
          },
          {
            data: [{{$janexpans}}, {{$febexpans}}, {{$marexpans}}, {{$aprexpans}}, {{$meiexpans}}, {{$junexpans}}, {{$julexpans}}, {{$agsexpans}}, {{$sepexpans}}, {{$oktexpans}}, {{$novexpans}}, {{$desexpans}}],
            barThickness: 15, 
            backgroundColor: warningColorShade,
            borderColor: 'transparent'
          }
        ]
      }
    });
  }

  //Draw rectangle Bar charts with rounded border
  Chart.elements.Rectangle.prototype.draw = function () {
    var ctx = this._chart.ctx;
    var viewVar = this._view;
    var left, right, top, bottom, signX, signY, borderSkipped, radius;
    var borderWidth = viewVar.borderWidth;
    var cornerRadius = 20;
    if (!viewVar.horizontal) {
      left = viewVar.x - viewVar.width / 2;
      right = viewVar.x + viewVar.width / 2;
      top = viewVar.y;
      bottom = viewVar.base;
      signX = 1;
      signY = top > bottom ? 1 : -1;
      borderSkipped = viewVar.borderSkipped || 'bottom';
    } else {
      left = viewVar.base;
      right = viewVar.x;
      top = viewVar.y - viewVar.height / 2;
      bottom = viewVar.y + viewVar.height / 2;
      signX = right > left ? 1 : -1;
      signY = 1;
      borderSkipped = viewVar.borderSkipped || 'left';
    }

    if (borderWidth) {
      var barSize = Math.min(Math.abs(left - right), Math.abs(top - bottom));
      borderWidth = borderWidth > barSize ? barSize : borderWidth;
      var halfStroke = borderWidth / 2;
      var borderLeft = left + (borderSkipped !== 'left' ? halfStroke * signX : 0);
      var borderRight = right + (borderSkipped !== 'right' ? -halfStroke * signX : 0);
      var borderTop = top + (borderSkipped !== 'top' ? halfStroke * signY : 0);
      var borderBottom = bottom + (borderSkipped !== 'bottom' ? -halfStroke * signY : 0);
      if (borderLeft !== borderRight) {
        top = borderTop;
        bottom = borderBottom;
      }
      if (borderTop !== borderBottom) {
        left = borderLeft;
        right = borderRight;
      }
    }

    ctx.beginPath();
    ctx.fillStyle = viewVar.backgroundColor;
    ctx.strokeStyle = viewVar.borderColor;
    ctx.lineWidth = borderWidth;
    var corners = [
      [left, bottom],
      [left, top],
      [right, top],
      [right, bottom]
    ];

    var borders = ['bottom', 'left', 'top', 'right'];
    var startCorner = borders.indexOf(borderSkipped, 0);
    if (startCorner === -1) {
      startCorner = 0;
    }

    function cornerAt(index) {
      return corners[(startCorner + index) % 4];
    }

    var corner = cornerAt(0);
    ctx.moveTo(corner[0], corner[1]);

    for (var i = 1; i < 4; i++) {
      corner = cornerAt(i);
      var nextCornerId = i + 1;
      if (nextCornerId == 4) {
        nextCornerId = 0;
      }

      var nextCorner = cornerAt(nextCornerId);

      var width = corners[2][0] - corners[1][0],
        height = corners[0][1] - corners[1][1],
        x = corners[1][0],
        y = corners[1][1];

      var radius = cornerRadius;

      if (radius > height / 2) {
        radius = height / 2;
      }
      if (radius > width / 2) {
        radius = width / 2;
      }

      if (!viewVar.horizontal) {
        ctx.moveTo(x + radius, y);
        ctx.lineTo(x + width - radius, y);
        ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
        ctx.lineTo(x + width, y + height - radius);
        ctx.quadraticCurveTo(x + width, y + height, x + width, y + height);
        ctx.lineTo(x + radius, y + height);
        ctx.quadraticCurveTo(x, y + height, x, y + height);
        ctx.lineTo(x, y + radius);
        ctx.quadraticCurveTo(x, y, x + radius, y);
      } else {
        ctx.moveTo(x + radius, y);
        ctx.lineTo(x + width - radius, y);
        ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
        ctx.lineTo(x + width, y + height - radius);
        ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
        ctx.lineTo(x + radius, y + height);
        ctx.quadraticCurveTo(x, y + height, x, y + height);
        ctx.lineTo(x, y + radius);
        ctx.quadraticCurveTo(x, y, x, y);
      }
    }

    ctx.fill();
    if (borderWidth) {
      ctx.stroke();
    }
  };

  
  // Line AreaChart
  // --------------------------------------------------------------------
  if (lineAreaChartEx.length) {
    new Chart(lineAreaChartEx, {
      type: 'line',
      plugins: [
        // to add spacing between legends and chart
        {
          beforeInit: function (chart) {
            chart.legend.afterFit = function () {
              this.height += 20;
            };
          }
        }
      ],
      options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          position: 'top',
          align: 'start',
          labels: {
            usePointStyle: true,
            padding: 25,
            boxWidth: 9
          }
        },
        layout: {
          padding: {
            top: -20,
            bottom: -20,
            left: -20
          }
        },
        tooltips: {
          // Updated default tooltip UI
          shadowOffsetX: 1,
          shadowOffsetY: 1,
          shadowBlur: 8,
          shadowColor: tooltipShadow,
          backgroundColor: window.colors.solid.white,
          titleFontColor: window.colors.solid.black,
          bodyFontColor: window.colors.solid.black
        },
        scales: {
          xAxes: [
            {
              display: true,
              gridLines: {
                color: 'transparent',
                zeroLineColor: grid_line_color
              },
              scaleLabel: {
                display: true
              },
              ticks: {
                fontColor: labelColor
              }
            }
          ],
          yAxes: [
            {
              display: true,
              gridLines: {
                color: 'transparent',
                zeroLineColor: grid_line_color
              },
              ticks: {
                stepSize: 100,
                min: 0,
                max: 400,
                fontColor: labelColor
              },
              scaleLabel: {
                display: true
              }
            }
          ]
        }
      },
      data: {
        labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
        datasets: [
          {
            label: 'Africa',
            data: [40, 55, 45, 75, 65, 55, 70, 60, 100, 98, 90, 120, 125, 140, 155],
            lineTension: 0,
            backgroundColor: blueColor,
            pointStyle: 'circle',
            borderColor: 'transparent',
            pointRadius: 0.5,
            pointHoverRadius: 5,
            pointHoverBorderWidth: 5,
            pointBorderColor: 'transparent',
            pointHoverBackgroundColor: blueColor,
            pointHoverBorderColor: window.colors.solid.white
          },
          {
            label: 'Asia',
            data: [70, 85, 75, 150, 100, 140, 110, 105, 160, 150, 125, 190, 200, 240, 275],
            lineTension: 0,
            backgroundColor: blueLightColor,
            pointStyle: 'circle',
            borderColor: 'transparent',
            pointRadius: 0.5,
            pointHoverRadius: 5,
            pointHoverBorderWidth: 5,
            pointBorderColor: 'transparent',
            pointHoverBackgroundColor: blueLightColor,
            pointHoverBorderColor: window.colors.solid.white
          },
          {
            label: 'Europe',
            data: [240, 195, 160, 215, 185, 215, 185, 200, 250, 210, 195, 250, 235, 300, 315],
            lineTension: 0,
            backgroundColor: greyLightColor,
            pointStyle: 'circle',
            borderColor: 'transparent',
            pointRadius: 0.5,
            pointHoverRadius: 5,
            pointHoverBorderWidth: 5,
            pointBorderColor: 'transparent',
            pointHoverBackgroundColor: greyLightColor,
            pointHoverBorderColor: window.colors.solid.white
          }
        ]
      }
    });
  }
});
    </script>

<script>
  $(window).on('load', function () {
  'use strict';

  var $textHeadingColor = '#5e5873';
  var $strokeColor = '#ebe9f1';
  var $labelColor = '#e7eef7';
  var $avgSessionStrokeColor2 = '#ebf0f7';
  var $budgetStrokeColor2 = '#dcdae3';
  var $goalStrokeColor2 = '#51e5a8';
  var $revenueStrokeColor2 = '#d0ccff';
  var $textMutedColor = '#b9b9c3';
  var $salesStrokeColor2 = '#df87f2';
  var $white = '#fff';
  var $earningsStrokeColor2 = '#28c76f66';
  var $earningsStrokeColor3 = '#28c76f33';

  var supportChartOptions;
  var avgSessionChartOptions;
  var revenueReportChartOptions;
  var budgetChartOptions;
  var goalChartOptions;
  var revenueChartOptions;
  var salesChartOptions;
  var salesLineChartOptions;
  var sessionChartOptions;
  var customerChartOptions;
  var orderChartOptions;
  var earningsChartOptions;

  var supportChart;
  var avgSessionChart;
  var revenueReportChart;
  var budgetChart;
  var goalChart;
  var revenueChart;
  var salesChart;
  var salesLineChart;
  var sessionChart;
  var customerChart;
  var orderChart;
  var earningsChart;

  var $supportTrackerChart = document.querySelector('#support-tracker-chart');
  var $avgSessionChart = document.querySelector('#avg-session-chart');
  var $revenueReportChart = document.querySelector('#revenue-report-chart');
  var $budgetChart = document.querySelector('#budget-chart');
  var $goalOverviewChart = document.querySelector('#goal-overview-chart');
  var $revenueChart = document.querySelector('#revenue-chart');
  var $salesChart = document.querySelector('#sales-chart');
  var $salesLineChart = document.querySelector('#sales-line-chart');
  var $sessionChart = document.querySelector('#session-chart');
  var $customerChart = document.querySelector('#customer-chart');
  var $productOrderChart = document.querySelector('#product-order-chart');
  var $earningsChart = document.querySelector('#earnings-donut-chart');

  // Support Tracker Chart
  // -----------------------------
  supportChartOptions = {
    chart: {
      height: 270,
      type: 'radialBar'
    },
    plotOptions: {
      radialBar: {
        size: 150,
        offsetY: 20,
        startAngle: -150,
        endAngle: 150,
        hollow: {
          size: '65%'
        },
        track: {
          background: $white,
          strokeWidth: '100%'
        },
        dataLabels: {
          name: {
            offsetY: -5,
            color: $textHeadingColor,
            fontSize: '1rem'
          },
          value: {
            offsetY: 15,
            color: $textHeadingColor,
            fontSize: '1.714rem'
          }
        }
      }
    },
    colors: [window.colors.solid.danger],
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        type: 'horizontal',
        shadeIntensity: 0.5,
        gradientToColors: [window.colors.solid.primary],
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100]
      }
    },
    stroke: {
      dashArray: 8
    },
    series: [83],
    labels: ['Completed Tickets']
  };
  supportChart = new ApexCharts($supportTrackerChart, supportChartOptions);
  supportChart.render();

  // Average Session Chart
  // ----------------------------------
  avgSessionChartOptions = {
    chart: {
      type: 'bar',
      height: 200,
      sparkline: { enabled: true },
      toolbar: { show: false }
    },
    states: {
      hover: {
        filter: 'none'
      }
    },
    colors: [
      $avgSessionStrokeColor2,
      $avgSessionStrokeColor2,
      window.colors.solid.primary,
      $avgSessionStrokeColor2,
      $avgSessionStrokeColor2,
      $avgSessionStrokeColor2
    ],
    series: [
      {
        name: 'Sessions',
        data: [75, 125, 225, 175, 125, 75, 25]
      }
    ],
    grid: {
      show: false,
      padding: {
        left: 0,
        right: 0
      }
    },
    plotOptions: {
      bar: {
        columnWidth: '45%',
        distributed: true,
        endingShape: 'rounded'
      }
    },
    tooltip: {
      x: { show: false }
    },
    xaxis: {
      type: 'numeric'
    }
  };
  avgSessionChart = new ApexCharts($avgSessionChart, avgSessionChartOptions);
  avgSessionChart.render();

  // Revenue Report Chart
  // ----------------------------------
  revenueReportChartOptions = {
    chart: {
      height: 230,
      stacked: true,
      type: 'bar',
      toolbar: { show: false }
    },
    plotOptions: {
      bar: {
        columnWidth: '17%',
        endingShape: 'rounded'
      },
      distributed: true
    },
    colors: [window.colors.solid.primary, window.colors.solid.warning],
    series: [
      {
        name: 'Earning',
        data: [95, 177, 284, 256, 105, 63, 168, 218, 72]
      },
      {
        name: 'Expense',
        data: [-145, -80, -60, -180, -100, -60, -85, -75, -100]
      }
    ],
    dataLabels: {
      enabled: false
    },
    legend: {
      show: false
    },
    grid: {
      padding: {
        top: -20,
        bottom: -10
      },
      yaxis: {
        lines: { show: false }
      }
    },
    xaxis: {
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
      labels: {
        style: {
          colors: $textMutedColor,
          fontSize: '0.86rem'
        }
      },
      axisTicks: {
        show: false
      },
      axisBorder: {
        show: false
      }
    },
    yaxis: {
      labels: {
        style: {
          colors: $textMutedColor,
          fontSize: '0.86rem'
        }
      }
    }
  };
  revenueReportChart = new ApexCharts($revenueReportChart, revenueReportChartOptions);
  revenueReportChart.render();

  // Budget Chart
  // ----------------------------------
  budgetChartOptions = {
    chart: {
      height: 80,
      toolbar: { show: false },
      zoom: { enabled: false },
      type: 'line',
      sparkline: { enabled: true }
    },
    stroke: {
      curve: 'smooth',
      dashArray: [0, 5],
      width: [2]
    },
    colors: [window.colors.solid.primary, $budgetStrokeColor2],
    series: [
      {
        data: [61, 48, 69, 52, 60, 40, 79, 60, 59, 43, 62]
      },
      {
        data: [20, 10, 30, 15, 23, 0, 25, 15, 20, 5, 27]
      }
    ],
    tooltip: {
      enabled: false
    }
  };
  budgetChart = new ApexCharts($budgetChart, budgetChartOptions);
  budgetChart.render();

  // Goal Overview  Chart
  // -----------------------------
  goalChartOptions = {
    chart: {
      height: 245,
      type: 'radialBar',
      sparkline: {
        enabled: true
      },
      dropShadow: {
        enabled: true,
        blur: 3,
        left: 1,
        top: 1,
        opacity: 0.1
      }
    },
    colors: [$goalStrokeColor2],
    plotOptions: {
      radialBar: {
        offsetY: -10,
        startAngle: -150,
        endAngle: 150,
        hollow: {
          size: '77%'
        },
        track: {
          background: $strokeColor,
          strokeWidth: '50%'
        },
        dataLabels: {
          name: {
            show: false
          },
          value: {
            color: $textHeadingColor,
            fontSize: '2.86rem',
            fontWeight: '600'
          }
        }
      }
    },
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        type: 'horizontal',
        shadeIntensity: 0.5,
        gradientToColors: [window.colors.solid.success],
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100]
      }
    },
    series: [83],
    stroke: {
      lineCap: 'round'
    },
    grid: {
      padding: {
        bottom: 30
      }
    }
  };
  goalChart = new ApexCharts($goalOverviewChart, goalChartOptions);
  goalChart.render();

  // Revenue  Chart
  // -----------------------------
  revenueChartOptions = {
    chart: {
      height: 240,
      toolbar: { show: false },
      zoom: { enabled: false },
      type: 'line',
      offsetX: -10
    },
    stroke: {
      curve: 'smooth',
      dashArray: [0, 12],
      width: [4, 3]
    },
    grid: {
      borderColor: $labelColor
    },
    legend: {
      show: false
    },
    colors: [$revenueStrokeColor2, $strokeColor],
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        inverseColors: false,
        gradientToColors: [window.colors.solid.primary, $strokeColor],
        shadeIntensity: 1,
        type: 'horizontal',
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100, 100, 100]
      }
    },
    markers: {
      size: 0,
      hover: {
        size: 5
      }
    },
    xaxis: {
      labels: {
        style: {
          colors: $textMutedColor,
          fontSize: '1rem'
        }
      },
      axisTicks: {
        show: false
      },
      categories: ['01', '05', '09', '13', '17', '21', '26', '31'],
      axisBorder: {
        show: false
      },
      tickPlacement: 'on'
    },
    yaxis: {
      tickAmount: 5,
      labels: {
        style: {
          colors: $textMutedColor,
          fontSize: '1rem'
        },
        formatter: function (val) {
          return val > 999 ? (val / 1000).toFixed(0) + 'k' : val;
        }
      }
    },
    grid: {
      padding: {
        top: -20,
        bottom: -10,
        left: 20
      }
    },
    tooltip: {
      x: { show: false }
    },
    series: [
      {
        name: 'This Month',
        data: [45000, 47000, 44800, 47500, 45500, 48000, 46500, 48600]
      },
      {
        name: 'Last Month',
        data: [46000, 48000, 45500, 46600, 44500, 46500, 45000, 47000]
      }
    ]
  };
  revenueChart = new ApexCharts($revenueChart, revenueChartOptions);
  revenueChart.render();

  // Sales Chart
  // -----------------------------
  salesChartOptions = {
    chart: {
      height: 300,
      type: 'radar',
      dropShadow: {
        enabled: true,
        blur: 8,
        left: 1,
        top: 1,
        opacity: 0.2
      },
      toolbar: {
        show: false
      },
      offsetY: 5
    },
    series: [
      {
        name: 'Sales',
        data: [90, 50, 86, 40, 100, 20]
      },
      {
        name: 'Visit',
        data: [70, 75, 70, 76, 20, 85]
      }
    ],
    stroke: {
      width: 0
    },
    colors: [window.colors.solid.primary, window.colors.solid.info],
    plotOptions: {
      radar: {
        polygons: {
          strokeColors: [$strokeColor, 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
          connectorColors: 'transparent'
        }
      }
    },
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        gradientToColors: [window.colors.solid.primary, window.colors.solid.info],
        shadeIntensity: 1,
        type: 'horizontal',
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100, 100, 100]
      }
    },
    markers: {
      size: 0
    },
    legend: {
      show: false
    },
    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
    dataLabels: {
      background: {
        foreColor: [$strokeColor, $strokeColor, $strokeColor, $strokeColor, $strokeColor, $strokeColor]
      }
    },
    yaxis: {
      show: false
    },
    grid: {
      show: false,
      padding: {
        bottom: -27
      }
    }
  };
  salesChart = new ApexCharts($salesChart, salesChartOptions);
  salesChart.render();

  // Sales Line Chart
  // -----------------------------
  salesLineChartOptions = {
    chart: {
      height: 240,
      toolbar: { show: false },
      zoom: { enabled: false },
      type: 'line',
      dropShadow: {
        enabled: true,
        top: 18,
        left: 2,
        blur: 5,
        opacity: 0.2
      },
      offsetX: -10
    },
    stroke: {
      curve: 'smooth',
      width: 4
    },
    grid: {
      borderColor: $strokeColor,
      padding: {
        top: -20,
        bottom: 5,
        left: 20
      }
    },
    legend: {
      show: false
    },
    colors: [$salesStrokeColor2],
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        inverseColors: false,
        gradientToColors: [window.colors.solid.primary],
        shadeIntensity: 1,
        type: 'horizontal',
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100, 100, 100]
      }
    },
    markers: {
      size: 0,
      hover: {
        size: 5
      }
    },
    xaxis: {
      labels: {
        offsetY: 5,
        style: {
          colors: $textMutedColor,
          fontSize: '0.857rem'
        }
      },
      axisTicks: {
        show: false
      },
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      axisBorder: {
        show: false
      },
      tickPlacement: 'on'
    },
    yaxis: {
      tickAmount: 5,
      labels: {
        style: {
          colors: $textMutedColor,
          fontSize: '0.857rem'
        },
        formatter: function (val) {
          return val > 999 ? (val / 1000).toFixed(1) + 'k' : val;
        }
      }
    },
    tooltip: {
      x: { show: false }
    },
    series: [
      {
        name: 'Sales',
        data: [140, 180, 150, 205, 160, 295, 125, 255, 205, 305, 240, 295]
      }
    ]
  };
  salesLineChart = new ApexCharts($salesLineChart, salesLineChartOptions);
  salesLineChart.render();

  // Session Chart
  // ----------------------------------
  sessionChartOptions = {
    chart: {
      type: 'donut',
      height: 300,
      toolbar: {
        show: false
      }
    },
    dataLabels: {
      enabled: false
    },
    series: [58.6, 34.9, 6.5],
    legend: { show: false },
    comparedResult: [2, -3, 8],
    labels: ['Desktop', 'Mobile', 'Tablet'],
    stroke: { width: 0 },
    colors: [window.colors.solid.primary, window.colors.solid.warning, window.colors.solid.danger]
  };
  sessionChart = new ApexCharts($sessionChart, sessionChartOptions);
  sessionChart.render();

  // Customer Chart
  // -----------------------------
  customerChartOptions = {
    chart: {
      type: 'pie',
      height: 325,
      toolbar: {
        show: false
      }
    },
    labels: ['New', 'Returning', 'Referrals'],
    series: [690, 258, 149],
    dataLabels: {
      enabled: false
    },
    legend: { show: false },
    stroke: {
      width: 4
    },
    colors: [window.colors.solid.primary, window.colors.solid.warning, window.colors.solid.danger]
  };
  customerChart = new ApexCharts($customerChart, customerChartOptions);
  customerChart.render();

  // Product Order Chart
  // -----------------------------
  orderChartOptions = {
    chart: {
      height: 325,
      type: 'radialBar'
    },
    colors: [window.colors.solid.primary, window.colors.solid.warning, window.colors.solid.danger],
    stroke: {
      lineCap: 'round'
    },
    plotOptions: {
      radialBar: {
        size: 150,
        hollow: {
          size: '20%'
        },
        track: {
          strokeWidth: '100%',
          margin: 15
        },
        dataLabels: {
          value: {
            fontSize: '1rem',
            colors: $textHeadingColor,
            fontWeight: '500',
            offsetY: 5
          },
          total: {
            show: true,
            label: 'Total',
            fontSize: '1.286rem',
            colors: $textHeadingColor,
            fontWeight: '500',

            formatter: function (w) {
              // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
              return 42459;
            }
          }
        }
      }
    },
    series: [70, 52, 26],
    labels: ['Finished', 'Pending', 'Rejected']
  };
  orderChart = new ApexCharts($productOrderChart, orderChartOptions);
  orderChart.render();

  // Earnings Chart
  // -----------------------------
  earningsChartOptions = {
    chart: {
      type: 'donut',
      height: 120,
      toolbar: {
        show: false
      }
    },
    dataLabels: {
      enabled: false
    },
    series: [ {{round($presMarginBlnIni)}}, {{round($presExpenseBlnIni)}}, {{round($presProfitBlnIni)}}],
    legend: { show: false },
    comparedResult: [2, -3, 8],
    labels: ['Margin', 'Expense', 'Profit'],
    stroke: { width: 0 },
    colors: [$earningsStrokeColor2, $earningsStrokeColor3, window.colors.solid.success],
    grid: {
      padding: {
        right: -20,
        bottom: -8,
        left: -20
      }
    },
    plotOptions: {
      pie: {
        startAngle: -10,
        donut: {
          labels: {
            show: true,
            name: {
              offsetY: 15
            },
            value: {
              offsetY: -15,
              formatter: function (val) {
                return parseInt(val) + '%';
              }
            },
            total: {
              show: true,
              offsetY: 15,
              label: 'Margin',
              formatter: function (w) {
                return '{{round($presMarginBlnIni)}}%';
              }
            }
          }
        }
      }
    },
    responsive: [
      {
        breakpoint: 1325,
        options: {
          chart: {
            height: 100
          }
        }
      },
      {
        breakpoint: 1200,
        options: {
          chart: {
            height: 120
          }
        }
      },
      {
        breakpoint: 1065,
        options: {
          chart: {
            height: 100
          }
        }
      },
      {
        breakpoint: 992,
        options: {
          chart: {
            height: 120
          }
        }
      }
    ]
  };
  earningsChart = new ApexCharts($earningsChart, earningsChartOptions);
  earningsChart.render();
});

</script>

<script>
$(window).on('load', function () {
  'use strict';

  var $barColor = '#f3f3f3';
  var $trackBgColor = '#EBEBEB';
  var $primary_light = '#A9A2F6';
  var $success_light = '#55DD92';
  var $warning_light = '#ffc085';

  var statisticsBarChartOptions;
  var statisticsLineChartOptions;
  var gainedChartOptions;
  var revenueChartOptions;
  var salesChartOptions;
  var orderChartOptions;
  var trafficChartOptions;
  var userChartOptions;
  var newsletterChartOptions;

  var statisticsBarChart = document.querySelector('#statistics-bar-chart');
  var statisticsLineChart = document.querySelector('#statistics-line-chart');
  var lineAreaChart1 = document.querySelector('#line-area-chart-1');
  var lineAreaChart2 = document.querySelector('#line-area-chart-2');
  var lineAreaChart3 = document.querySelector('#line-area-chart-3');
  var lineAreaChart4 = document.querySelector('#line-area-chart-4');
  var lineAreaChart5 = document.querySelector('#line-area-chart-5');
  var lineAreaChart6 = document.querySelector('#line-area-chart-6');
  var lineAreaChart7 = document.querySelector('#line-area-chart-7');

  var statisticsBar;
  var statisticsLine;
  var gainedChart;
  var revenueChart;
  var salesChart;
  var orderChart;
  var trafficChart;
  var userChart;
  var newsletterChart;

  //------------ Statistics Bar Chart ------------
  //----------------------------------------------

  statisticsBarChartOptions = {
    chart: {
      height: 70,
      type: 'bar',
      stacked: true,
      toolbar: {
        show: false
      }
    },
    grid: {
      show: false,
      padding: {
        left: 0,
        right: 0,
        top: -15,
        bottom: -15
      }
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: '20%',
        startingShape: 'rounded',
        colors: {
          backgroundBarColors: [$barColor, $barColor, $barColor, $barColor, $barColor],
          backgroundBarRadius: 5
        }
      }
    },
    legend: {
      show: false
    },
    dataLabels: {
      enabled: false
    },
    colors: [window.colors.solid.warning],
    series: [
      {
        name: '2020',
        data: [0, 85, 65, 45, 65, 65, 65, 65, 65, 65, 65, 65]
      }
    ],
    xaxis: {
      labels: {
        show: false
      },
      axisBorder: {
        show: false
      },
      axisTicks: {
        show: false
      }
    },
    yaxis: {
      show: false
    },
    tooltip: {
      x: {
        show: false
      }
    }
  };

  statisticsBar = new ApexCharts(statisticsBarChart, statisticsBarChartOptions);
  statisticsBar.render();

  //------------ Statistics Line Chart ------------
  //-----------------------------------------------

  statisticsLineChartOptions = {
    chart: {
      height: 70,
      type: 'line',
      toolbar: {
        show: false
      },
      zoom: {
        enabled: false
      }
    },
    grid: {
      borderColor: $trackBgColor,
      strokeDashArray: 5,
      xaxis: {
        lines: {
          show: true
        }
      },
      yaxis: {
        lines: {
          show: false
        }
      },
      padding: {
        top: -30,
        bottom: -10
      }
    },
    stroke: {
      width: 3
    },
    colors: [window.colors.solid.info],
    series: [
      {
        data: [0, 20, 5, 30, 15, 45, 65, 65, 65, 65, 65, 65]
      }
    ],
    markers: {
      size: 2,
      colors: window.colors.solid.info,
      strokeColors: window.colors.solid.info,
      strokeWidth: 2,
      strokeOpacity: 1,
      strokeDashArray: 0,
      fillOpacity: 1,
      discrete: [
        {
          seriesIndex: 0,
          dataPointIndex: 5,
          fillColor: '#ffffff',
          strokeColor: window.colors.solid.info,
          size: 5
        }
      ],
      shape: 'circle',
      radius: 2,
      hover: {
        size: 3
      }
    },
    xaxis: {
      labels: {
        show: true,
        style: {
          fontSize: '0px'
        }
      },
      axisBorder: {
        show: false
      },
      axisTicks: {
        show: false
      }
    },
    yaxis: {
      show: false
    },
    tooltip: {
      x: {
        show: false
      }
    }
  };

  statisticsLine = new ApexCharts(statisticsLineChart, statisticsLineChartOptions);
  statisticsLine.render();

  // Subscribed Gained Chart
  // ----------------------------------

  gainedChartOptions = {
    chart: {
      height: 100,
      type: 'area',
      toolbar: {
        show: false
      },
      sparkline: {
        enabled: true
      },
      grid: {
        show: false,
        padding: {
          left: 0,
          right: 0
        }
      }
    },
    colors: [window.colors.solid.primary],
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth',
      width: 2.5
    },
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 0.9,
        opacityFrom: 0.7,
        opacityTo: 0.5,
        stops: [0, 80, 100]
      }
    },
    series: [
      {
        name: 'Subscribers',
        data: [28, 40, 36, 52, 38, 60, 55]
      }
    ],
    xaxis: {
      labels: {
        show: false
      },
      axisBorder: {
        show: false
      }
    },
    yaxis: [
      {
        y: 0,
        offsetX: 0,
        offsetY: 0,
        padding: { left: 0, right: 0 }
      }
    ],
    tooltip: {
      x: { show: false }
    }
  };

  gainedChart = new ApexCharts(lineAreaChart1, gainedChartOptions);
  gainedChart.render();

  // Revenue Generated Chart
  // ----------------------------------

  revenueChartOptions = {
    chart: {
      height: 100,
      type: 'area',
      toolbar: {
        show: false
      },
      sparkline: {
        enabled: true
      },
      grid: {
        show: false,
        padding: {
          left: 0,
          right: 0
        }
      }
    },
    colors: [window.colors.solid.success],
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth',
      width: 2.5
    },
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 0.9,
        opacityFrom: 0.7,
        opacityTo: 0.5,
        stops: [0, 80, 100]
      }
    },
    series: [
      {
        name: 'Revenue',
        data: [350, 275, 400, 300, 350, 300, 450]
      }
    ],
    xaxis: {
      labels: {
        show: false
      },
      axisBorder: {
        show: false
      }
    },
    yaxis: [
      {
        y: 0,
        offsetX: 0,
        offsetY: 0,
        padding: { left: 0, right: 0 }
      }
    ],
    tooltip: {
      x: { show: false }
    }
  };

  revenueChart = new ApexCharts(lineAreaChart2, revenueChartOptions);
  revenueChart.render();

  // Quaterly Sales Chart
  // ----------------------------------

  salesChartOptions = {
    chart: {
      height: 100,
      type: 'area',
      toolbar: {
        show: false
      },
      sparkline: {
        enabled: true
      },
      grid: {
        show: false,
        padding: {
          left: 0,
          right: 0
        }
      }
    },
    colors: [window.colors.solid.danger],
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth',
      width: 2.5
    },
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 0.9,
        opacityFrom: 0.7,
        opacityTo: 0.5,
        stops: [0, 80, 100]
      }
    },
    series: [
      {
        name: 'Sales',
        data: [10, 15, 7, 12, 3, 16]
      }
    ],
    xaxis: {
      labels: {
        show: false
      },
      axisBorder: {
        show: false
      }
    },
    yaxis: [
      {
        y: 0,
        offsetX: 0,
        offsetY: 0,
        padding: { left: 0, right: 0 }
      }
    ],
    tooltip: {
      x: { show: false }
    }
  };

  salesChart = new ApexCharts(lineAreaChart3, salesChartOptions);
  salesChart.render();

  // Order Received Chart
  // ----------------------------------

  orderChartOptions = {
    chart: {
      height: 100,
      type: 'area',
      toolbar: {
        show: false
      },
      sparkline: {
        enabled: true
      },
      grid: {
        show: false,
        padding: {
          left: 0,
          right: 0
        }
      }
    },
    colors: [window.colors.solid.warning],
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth',
      width: 2.5
    },
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 0.9,
        opacityFrom: 0.7,
        opacityTo: 0.5,
        stops: [0, 80, 100]
      }
    },
    series: [
      {
        name: 'Orders',
        data: [10, 15, 8, 15, 7, 12, 8]
      }
    ],
    xaxis: {
      labels: {
        show: false
      },
      axisBorder: {
        show: false
      }
    },
    yaxis: [
      {
        y: 0,
        offsetX: 0,
        offsetY: 0,
        padding: { left: 0, right: 0 }
      }
    ],
    tooltip: {
      x: { show: false }
    }
  };

  orderChart = new ApexCharts(lineAreaChart4, orderChartOptions);
  orderChart.render();

  // Site Traffic Chart
  // ----------------------------------

  trafficChartOptions = {
    chart: {
      height: 100,
      type: 'line',
      dropShadow: {
        enabled: true,
        top: 5,
        left: 0,
        blur: 4,
        opacity: 0.1
      },
      toolbar: {
        show: false
      },
      sparkline: {
        enabled: true
      },
      grid: {
        show: false,
        padding: {
          left: 0,
          right: 0
        }
      }
    },
    colors: [window.colors.solid.primary],
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth',
      width: 5
    },
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 1,
        gradientToColors: [$primary_light],
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100, 100, 100]
      }
    },
    series: [
      {
        name: 'Traffic Rate',
        data: [150, 200, 125, 225, 200, 250]
      }
    ],
    xaxis: {
      labels: {
        show: false
      },
      axisBorder: {
        show: false
      }
    },
    yaxis: [
      {
        y: 0,
        offsetX: 0,
        offsetY: 0,
        padding: { left: 0, right: 0 }
      }
    ],
    tooltip: {
      x: { show: false }
    }
  };

  trafficChart = new ApexCharts(lineAreaChart5, trafficChartOptions);
  trafficChart.render();

  // Active Users Chart
  // ----------------------------------

  userChartOptions = {
    chart: {
      height: 100,
      type: 'line',
      dropShadow: {
        enabled: true,
        top: 5,
        left: 0,
        blur: 4,
        opacity: 0.1
      },
      toolbar: {
        show: false
      },
      sparkline: {
        enabled: true
      },
      grid: {
        show: false,
        padding: {
          left: 0,
          right: 0
        }
      }
    },
    colors: [window.colors.solid.success],
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth',
      width: 5
    },
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 1,
        gradientToColors: [$success_light],
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100, 100, 100]
      }
    },
    series: [
      {
        name: 'Active Users',
        data: [750, 1000, 900, 1250, 1000, 1200, 1100]
      }
    ],
    xaxis: {
      labels: {
        show: false
      },
      axisBorder: {
        show: false
      }
    },
    yaxis: [
      {
        y: 0,
        offsetX: 0,
        offsetY: 0,
        padding: { left: 0, right: 0 }
      }
    ],
    tooltip: {
      x: { show: false }
    }
  };

  userChart = new ApexCharts(lineAreaChart6, userChartOptions);
  userChart.render();

  // News Letter Chart
  // ----------------------------------

  newsletterChartOptions = {
    chart: {
      height: 100,
      type: 'line',
      dropShadow: {
        enabled: true,
        top: 5,
        left: 0,
        blur: 4,
        opacity: 0.1
      },
      toolbar: {
        show: false
      },
      sparkline: {
        enabled: true
      },
      grid: {
        show: false,
        padding: {
          left: 0,
          right: 0
        }
      }
    },
    colors: [window.colors.solid.warning],
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth',
      width: 5
    },
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 1,
        gradientToColors: [$warning_light],
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100, 100, 100]
      }
    },
    series: [
      {
        name: 'Newsletter',
        data: [365, 390, 365, 400, 375, 400]
      }
    ],
    xaxis: {
      labels: {
        show: false
      },
      axisBorder: {
        show: false
      }
    },
    yaxis: [
      {
        y: 0,
        offsetX: 0,
        offsetY: 0,
        padding: { left: 0, right: 0 }
      }
    ],
    tooltip: {
      x: { show: false }
    }
  };

  newsletterChart = new ApexCharts(lineAreaChart7, newsletterChartOptions);
  newsletterChart.render();
});

</script>

@endsection