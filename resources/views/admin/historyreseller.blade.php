@extends('layouts.app')
<?php $page = "historyreseller" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
  <!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Laporan</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/laporan">History belanja</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="form-group mb-1">
              <a class="btn btn-primary" href="/pembayaran"> Ada <badge class="text-success">{{$jmlpembayaranagen}}</badge> Pengajuan Pembayaran</a>
            </div>

            <div class="card card-body">
              <table class="table table-striped small">
                 <thead>
                 <tr>
                   <th>Nama Reseller</th>
                   <th>Hutang</th>
                 </tr>
                 </thead>
                 <tbody>
                  @foreach ($listTempo as $hstmpo)
                 <tr class="shop-item">
                   <td>{{$hstmpo->name}}</td>
                   <td>@currency($hstmpo->tagihanTempo)</td>
                 </tr>
                 @endforeach
                 </tbody>
              </table>
            </div>

                  <!-- PANJANG TABLE CARD -->
            <div class="card card-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab" data-bs-target="#navs-tempo" aria-controls="navs-tempo" aria-selected="true">
                              History Tempo
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-lunas" aria-controls="navs-lunas" aria-selected="false" tabindex="-1">
                              History Lunas
                            </button>
                        </li>
                    </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active show" id="navs-tempo" role="tabpanel">
                     <table class="table table-striped small">
                        <thead>
                        <tr>
                          <th>Belanja</th>
                          <th>Invoice</th>
                          <th>Jumlah</th>
                          <th>Total</th>
                          <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($historyTempo as $hstrytempo)
                        <tr class="shop-item">
                          <td>{{ $hstrytempo->tanggal}}</td>
                          <td>{{ $hstrytempo->invoice }}</td>
                          <td>{{ $hstrytempo->jumlah_barang }}</td>
                          <td>@currency($hstrytempo->tagihan)</td>
                          <td>{{$hstrytempo->platform}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="form-group mt-3">
                    {{$historyTempo->links()}}
                    </div>
                </div>
                <div class="tab-pane fade" id="navs-lunas" role="tabpanel">
                     <table class="table table-striped small">
                        <thead>
                        <tr>
                          <th>Belanja</th>
                          <th>Invoice</th>
                          <th>Jumlah</th>
                          <th>Total</th>
                          <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($historyLunas as $hstrylunas)
                        <tr class="shop-item">
                          <td>{{ $hstrylunas->tanggal}}</td>
                          <td>{{ $hstrylunas->invoice }}</td>
                          <td>{{ $hstrylunas->jumlah_barang }}</td>
                          <td>@currency($hstrylunas->tagihan)</td>
                          <td>{{$hstrylunas->platform}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="form-group mt-3">
                    {{$historyLunas->links()}}
                    </div>
                  </div>
                </div>

<script>
$(document).ready( function () {
    $('#myTable').DataTable({
      "ordering": false,
    });
} );
</script>
@endsection
