<!DOCTYPE html>

<html
  lang="en"
  class="light-style"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="../../assets/"
  data-template="horizontal-menu-template"
>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=2.0, maximum-scale=1"
    />

    <title>Invoice (Print version) - Pages | Vuexy - Bootstrap Admin Template</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="../../assets/img/favicon/favicon.ico" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
      rel="stylesheet"
    />

    <!-- Icons -->
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/fonts/fontawesome.css')}}" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/fonts/tabler-icons.css')}}" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/fonts/flag-icons.css')}}" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/rtl/core.css')}}" class="template-customizer-core-css" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/rtl/theme-default.css')}}" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="{{ asset('app-assets/css/demo.css')}}" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/libs/perfect-scrollbar/perfect-scrollbar.css')}}" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/libs/node-waves/node-waves.css')}}" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/libs/typeahead-js/typeahead.css')}}" />

    <!-- Page CSS -->

    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/pages/app-invoice-print.css')}}" />
    <!-- Helpers -->
    <script src="{{ asset('app-assets/vendors/js/helpers.js')}}"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Template customizer: To hide customizer set displayCustomizer value false in config.js.  -->
    <script src="{{ asset('app-assets/vendor/js/template-customizer.js')}}"></script>
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="{{ asset('app-assets/js/config.js')}}"></script>
  </head>

  <body>
    <!-- Content -->

                                        <div class="invoice-print p-5">
                                    
                                          <hr />
                                    

                                          <div class="table-responsive">
                                            <table class="table m-0">
                                              <thead class="table-light">
                                              <tr>
                                                <th>Product</th>
                                                <th>14 hari terjual</th>
                                                <th>Sisa Stok</th>
                                                <th>Perlu Stok</th>
                                                <th>Gudang</th>
                                              </tr>
                                              </thead>
                                              <tbody>
                                                @foreach($stokAlert as $alers)
                                                  @if($alers->total_terjual - $alers->stok > 0)
                                                    <tr>
                                                      <td>{{$alers->title}}</td>
                                                      <td>{{$alers->total_terjual}}</td>
                                                      <td>{{$alers->stok}}</td>
                                                      <td>{{$alers->total_terjual - $alers->stok}}
                                                      </td>
                                                      <td>{{$alers->toko}}</td>
                                                    </tr>
                                                @else
                                                @endif
                                              @endforeach
                                              </tbody>
                                            </table>
                                          </div>
                                        </div>
                                    
                                          <hr />
                                          
                                        <div class="invoice-print p-5">

    <!-- / Content -->

    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{ asset('app-assets/vendors/libs/jquery/jquery.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/popper/popper.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/bootstrap.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/node-waves/node-waves.js') }}"></script>

    <script src="{{ asset('app-assets/vendors/libs/hammer/hammer.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/i18n/i18n.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/typeahead-js/typeahead.js') }}"></script>

    <script src="{{ asset('app-assets/vendors/js/menu.js') }}"></script>
    <!-- endbuild -->

    <!-- Vendors JS -->

    <!-- Main JS -->
    <script src="{{ asset('app-assets/vendors/js/main.js') }}"></script>

    <!-- Page JS -->
    <script src="{{ asset('app-assets/vendors/js/app-invoice-print.js') }}"></script>
  </body>
</html>
