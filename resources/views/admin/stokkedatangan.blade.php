@extends('layouts.app')
<?php $page = "stok" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">System Inventory</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="stok">System Inventory</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <!-- Stats Horizontal Card -->
                    <div class="row">
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <h2 class="fw-bolder mb-0">{{$allstok}}</h2>
                                        <p class="card-text">jumlah stok</p>
                                    </div>
                                    <div class="avatar bg-light-success p-50 m-0">
                                        <div class="avatar-content" data-bs-toggle="tooltip" title="Merah Lebih Rendah Dari Kemarin, Hijau Lebih Tinggai Dari Kemarin *blm di buat" data-bs-animation="false">
                                            <i data-feather="box" class="font-medium-5" ></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <h2 class="fw-bolder mb-0">{{$allcategory}}</h2>
                                        <p class="card-text">jenis barang</p>
                                    </div>
                                    <div class="avatar bg-light-success p-50 m-0">
                                        <div class="avatar-content">
                                            <i data-feather="archive" class="font-medium-5"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <h2 class="fw-bolder mb-0">@currency($pricestok)</h2>
                                        <p class="card-text">nilai barang</p>
                                    </div>
                                    <div class="avatar bg-light-success p-50 m-0">
                                        <div class="avatar-content">
                                            <i data-feather="credit-card" class="font-medium-5"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div>
                                        <h2 class="fw-bolder mb-0">@currency($pricesale-$pricestok)</h2>
                                        <p class="card-text">perkiraan margin</p>
                                    </div>
                                    <div class="avatar bg-light-success p-50 m-0">
                                        <div class="avatar-content">
                                            <i data-feather="alert-octagon" class="font-medium-5"></i>
                                        </div>
                                    </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Stats Horizontal Card -->


          <!-- PANJANG TABLE CARD -->
            <div class="col-lg-12">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-1" style="display:<?php if(Auth::user()->role == 1){echo "block";}elseif(Auth::user()->role == 8){echo "block";}else{echo "none";} ?>">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="row">
                              <div class="col-xl-12 col-md-5 col-sm-12 mb-1">
                                <button type="button" class="btn btn-primary mb-1" data-bs-toggle="modal" data-bs-target="#modal-barang-baru"><i data-feather="plus-square" class="me-1"></i> Item</button>
                                <button type="button" class="btn btn-primary mb-1" data-bs-toggle="modal" data-bs-target="#modal-belanja"><i data-feather="shopping-bag" class="me-1"></i> Belanja</button>
                                <button type="button" class="btn btn-primary mb-1" data-bs-toggle="modal" data-bs-target="#modal-supplier"><i data-feather="plus-square" class="me-1"></i> Suplier</button>
                                <button type="button" class="btn btn-primary mb-1" data-bs-toggle="modal" data-bs-target="#modal-category"><i data-feather="plus-square" class="me-1"></i> Category</button>
                                <a type="button" class="btn btn-primary mb-1" href="/printstokopname?cabang={{$selectedcabang}}" target="_blank"><i data-feather="printer" class="me-1"></i> print Stok</a>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right row">
                        <div class="dropdown col-md-6">
                            <form action="/caristok" method="get">
                              <input name="stok" type="text" class="form-control" placeholder="Search..." aria-label="Search..." aria-describedby="basic-addon-search2">
                            </form>
                        </div>
                        <div class="dropdown col-md-6" style="display:<?php if(Auth::user()->role == 1){echo "block";}else{echo "none";} ?>">
                            <form action="/stok" method="get">
                                <select name="cabang" class="form-control" onchange="this.form.submit()">>
                                    <option>{{$selectedcabang}}</option>
                                    <option value="">Tampilkan Semua</option>
                                    @foreach($listtoko as $lstk)
                                    <option>{{$lstk->nama_cabang}}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                    </div>
                </div>
            </div>




            <div class="card card-body">
                    <ul class="nav nav-tabs nav-fill" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a type="button" class="nav-link" href="/stok">
                              Stok Barang
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a type="button" class="nav-link" href="/stokcategory">
                              Category Produk
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a type="button" class="nav-link" href="/stoksupplier">
                              Supplier
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a type="button" class="nav-link" href="/stokpending">
                                Stok Pending 
                            <span class="badge badge-light-success rounded-pill badge-center h-px-20 w-px-20 ms-1">{{ $count_p_stok }}</span>
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a type="button" class="nav-link active" href="/stokkedatangan">
                                History Kedatangan
                            </a>
                        </li>
                    </ul>
                <div class="tab-content">
                    <div class="tab-pane table-responsive fade" id="navs-stok-barang" role="tabpanel">
                        <table class="table table-sm table-responsive small">
                          <thead>
                          <tr>
                            <th>Code</th>
                            <th>Nama</th>
                            <th>Stok</th>
                            <th>Modal</th>
                            <th>Retail</th>
                            <th>Tr1</th>
                            <th>Tr2</th>
                            <th>Tr3</th>
                            <th>Lokasi</th>
                            <th>Opt</th>
                          </tr>
                          </thead>
                          <tbody>
                            @foreach($stok as $index => $stoks)
                          <tr>
                            <td>{{ $stoks->code_barang }}</td>
                            <td class="text-nowrap">{{$stoks->nama_barang}}</td>
                            <td>{{$stoks->stok}}</td>
                            <td class="text-nowrap">@currency($stoks->harga_modal)</td>
                            <td class="text-nowrap">@currency($stoks->harga_jual)</td>
                            <td class="text-nowrap">@currency($stoks->tr1)</td>
                            <td class="text-nowrap">@currency($stoks->tr2)</td>
                            <td class="text-nowrap">@currency($stoks->tr3)</td>
                            <td class="text-nowrap">{{ $stoks->tipe }}</td>
                            <td>
                            <button 
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                  data-submenu
                                   class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1" type="button">
                              <i  data-feather="chevron-down"></i>
                              </button>
                            <ul class="dropdown-menu">
                              <a href="/jadikanecer/{{$stoks->id}}" class="dropdown-item <?php if($stoks->id_sachet == null){echo "jadikanecer";}else{echo "";} ?>"><i data-feather="pocket"></i> Jadikan Sachet</a>
                              <a href="/pindahkanstok/{{$stoks->id}}" class="dropdown-item"><i data-feather="move"></i> Pindahkan Stok</a>
                              <a href="/editproduk/{{$stoks->id}}" class="dropdown-item"><i data-feather="edit"></i> Edit</a>
                              <a  href="/print/qrcode/{{$stoks->id}}" target="_blank" class="dropdown-item"><i data-feather="printer"></i> Print QR Code</a>
                              <li class="dropdown-divider"></li>
                              <a href="/hapus/barang/{{$stoks->id}}" class="dropdown-item hapusdata"><i class="fas fa-trash-alt"></i> Hapus</a>
                            </ul>
                            </td>
                          </tr>
                          @endforeach
                          </tbody>
                        </table>
                        <div class="form-group mt-5">
                        {{$stok->links()}}
                        </div>
                    </div>
                    <div class="tab-pane fade" id="navs-category-produk" role="tabpanel">
                        <table class="table table-sm table-striped">
                          <thead>
                          <tr>
                            <th class="text-center">Nomor</th>
                            <th class="text-center">Category</th>
                            <th class="text-center">Option</th>
                          </tr>
                          </thead>
                          <tbody>
                            @foreach($categrory as $index => $ctg)
                          <tr>
                            <td class="text-center">{{ $index+1 }}</td>
                            <td class="text-center">{{ $ctg->nama_category }}</td>
                            <td class="text-center">
                            <button 
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                  data-submenu
                                   class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1" type="button">
                              <i  data-feather="chevron-down"></i>
                              </button>
                            <ul class="dropdown-menu">
                              <a  href="/hapus/category/{{$ctg->id_category}}" class="dropdown-item"><i class="fas fa-trash-alt"></i> Hapus</a>
                            </ul>
                            </td>
                          </tr>
                          @endforeach
                          </tbody>
                        </table>
                        <div class="form-group pl-2">
                        {{$categrory->links()}}
                        </div>
                    </div>
                    <div class="tab-pane fade" id="navs-supplier" role="tabpanel">
                        <table class="table table-striped">
                          <thead>
                          <tr>
                            <th class="text-center">Nomor</th>
                            <th class="text-center">Nama Supplier</th>
                            <th class="text-center">Alamat Supplier</th>
                            <th class="text-center">No Hp</th>
                            <th class="text-center">Option</th>
                          </tr>
                          </thead>
                          <tbody>
                            @foreach($supplier as $index => $sup)
                          <tr>
                            <td class="text-center">{{ $index+1 }}</td>
                            <td class="text-center"><form action="/ganti/namasup/{{$sup->id_supplier}}" method="get"><input name="nama_supplier" id="namasup[{{ $index+1 }}]" class="form-control" value="{{ $sup->nama_supplier }}" readonly></form></td>
                            <td class="text-center"><form action="/ganti/alamatsup/{{$sup->id_supplier}}" method="get"><input name="alamat_supplier" id="alamatsup[{{ $index+1 }}]" class="form-control" value="{{ $sup->alamat_supplier }}" readonly></form></td>
                            <td class="text-center"><form action="/ganti/hpsup/{{$sup->id_supplier}}" method="get"><input name="hp_supplier" id="hpsup[{{ $index+1 }}]" class="form-control" value="{{ $sup->hp_supplier }}" readonly></form></td>
                            <td class="text-center">
                            <button 
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                  data-submenu
                                   class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1" type="button">
                              <i  data-feather="chevron-down"></i>
                              </button>
                            <ul class="dropdown-menu">
                              <a  id="editsup[{{ $index+1 }}]" class="dropdown-item"><i class="far fa-edit"></i> Edit</a>
                              <a  href="/hapus/sup/{{$sup->id_supplier}}" class="dropdown-item"><i class="fas fa-trash-alt"></i> Hapus</a>
                            </ul>
                            </td>
                          </tr>
                          @endforeach
                          </tbody>
                        </table>
                        <div class="form-group pl-2">
                        </div>
                    </div>
                    <div class="tab-pane fade" id="navs-stok-pending" role="tabpanel">
                        <table class="table table-sm table-striped">
                          <thead>
                          <tr>
                            <th class="text-nowrap">Id Belanja</th>
                            <th class="text-nowrap">Code Barang</th>
                            <th class="text-nowrap">Nama</th>
                            <th class="text-nowrap">Category</th>
                            <th class="text-nowrap">Stok Akan Datang</th>
                            <th class="text-nowrap">Cabang Tujuan</th>
                            <th class="text-nowrap">Ketarangan</th>
                            <th class="text-nowrap">option</th>
                          </tr>
                          </thead>
                          <tbody>
                            @foreach($p_stok as $index => $P_stoks)
                          <tr>
                            <td>#{{ $P_stoks->id }}</td>
                            <td>{{ $P_stoks->code_barang }}</td>
                            <td class="text-nowrap">{{ $P_stoks->nama_barang }}</td>
                            <td class="text-center">{{ $P_stoks->category}}</td>
                            <td class="text-center ">{{ $P_stoks->stok }}</td>
                            <td class="text-nowrap">{{ $P_stoks->tipe }}</td>
                            <td class="text-center">{{ $P_stoks->keterangan }}</td>
                            <td class="text-center">
                            <button 
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                  data-submenu
                                   class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1" type="button">
                              <i  data-feather="chevron-down"></i>
                              </button>
                            <ul class="dropdown-menu">
                              <a href="/perifstoksamapi/{{ $P_stoks->id }}" class="dropdown-item yakindatang"><i class="fas fa-check-square"></i> Stok Sampai</a>
                              <a style="display:<?php if(Auth::user()->role == 1){echo "block";}else{echo "none";} ?>" href="/hapusstokpending/{{ $P_stoks->id }}" class="dropdown-item tanpafiturkeuangan"><i class="fas fa-check-square"></i> Hapus</a>
                            </ul>
                            </td>
                          </tr>
                          @endforeach
                          </tbody>
                        </table>
                        <div class="form-group mt-2">
                        {{$p_stok->links()}}
                        </div>
                    </div>
                    <div class="tab-pane fade active show" id="navs-stok-history" role="tabpanel">
                        <table class="table table-striped">
                          <thead>
                          <tr>
                            <th>Tanggal</th>
                            <th>Id Belanja</th>
                            <th>Nama Barang</th>
                            <th class="text-center">Code Barang</th>
                            <th class="text-center">Toko Cabang</th>
                            <th class="text-center">Stok Dibeli</th>
                            <th class="text-center">Stok Datang</th>
                            <th class="text-center">Keterangan</th>
                          </tr>
                          </thead>
                          <tbody>
                            @foreach($historybelanja as $hstryblnja)
                          <tr>
                            <td>{{ $hstryblnja->tanggal}}</td>
                            <td>{{ $hstryblnja->id_tertahan}}</td>
                            <td>{{ $hstryblnja->nama_barang}}</td>
                            <td class="text-center">{{ $hstryblnja->code_barang}}</td>
                            <td class="text-center">{{ $hstryblnja->toko_cabang }}</td>
                            <td class="text-center">{{ $hstryblnja->stok_dibeli }}</td>
                            <td class="text-center">{{ $hstryblnja->stok_datang }}</td>
                            <td class="text-center">{{ $hstryblnja->keterangan }}</td>
                          </tr>
                          @endforeach
                          </tbody>
                        </table>
                        <div class="form-group mt-2">
                        {{$historybelanja->links()}}
                        </div>
                    </div>
                </div>
            </div>


          <!-- SEMUA MODAL DI DALAM SINI -->
          <div class="modal fade" id="modal-belanja">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Belanja Barang</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/tambah/barang">
                @csrf
                <div class="modal-body">
                  <div class="form-group mb-1">
                    <span>Pilih Barang</span>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="buat terlebih dulu barang baru sebelum bisa menggunakan fitur belanja">
                            <i class="text-primary" data-feather="help-circle"></i>
                        </a>
                    <select name="code_barang" type="text" class="form-control select2" required>
                      <option></option>
                      @foreach($selectstok as $scts)
                      <option value="{{$scts->code_barang}}">{{$scts->code_barang}} | {{$scts->tipe}} | {{$scts->nama_barang}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group mb-1">
                    <span>Lokasi Store</span>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Tambahkan toko di menu list toko cabang - lokasi store harus sama dengan code lokasi barang">
                            <i class="text-primary" data-feather="help-circle"></i>
                        </a>
                    <select name="tipe" type="text" class="form-control" title="Code Barang / Biarkan kosong untuk barang baru">
                      <option></option>
                      @foreach($listtoko as $lst)
                      <option>{{$lst->nama_cabang}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group mb-1">
                    <span>Tanggal belanja</span>
                    <input name="tanggal" type="date" class="form-control" required>
                  </div>
                    <div class="form-group mb-1">
                        <span>Qty</span>
                        <input name="jumlah" type="number" class="form-control" placeholder="Jumlah" required>
                    </div>
                  <div class="form-group mb-1">
                    <span>Pilih Suplier</span>
                    <select name="supplier" class="form-control select2">
                      <option></option>
                      @foreach($supplier as $sup)
                      <option>{{$sup->nama_supplier}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group mb-1">
                    <span>Bayar Menggunakan</span>
                    <select name="akun_keuangan" class="form-control" required>
                      <option></option>
                        @foreach($listrekening as $lsrkng)
                      <option value="{{$lsrkng->nomor_rekening}}">{{$lsrkng->pemilik_rekening}}</option>
                        @endforeach
                      <option>Tokopedia</option>
                      <option>Bukalapak</option>
                      <option>BliBli</option>
                      <option>Shopee</option>
                      <option>Belanja Tempo</option>
                      <option>Ofline</option>
                    </select>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </form>
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->




          <div class="modal fade" id="modal-barang-baru">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Belanja Barang</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/tambah/barang">
                @csrf
                <div class="modal-body">
                  <div class="form-group mb-1">
                    <span>Lokasi Store</span>
                        <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Tambahkan toko di menu list toko cabang">
                            <i class="text-primary" data-feather="help-circle"></i>
                        </a>
                    <select name="tipe" type="text" class="form-control" title="Code Barang / Biarkan kosong untuk barang baru">
                      <option></option>
                      @foreach($listtoko as $lst)
                      <option>{{$lst->nama_cabang}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group mb-1">
                    <span>Tanggal belanja</span>
                    <input name="tanggal" type="date" class="form-control" required>
                  </div>
                  <div class="form-group mb-1">
                    <span>Nama Barang</span>
                    <input id="namaBarang" name="nama_barang" type="text" class="form-control" placeholder="Nama Barang" required>
                  </div>
                <div class="form-group mb-1">
                    <span>Qty</span>
                    <input onkeyup="FunctionTotal()" id="jumlah" name="jumlah" type="number" class="form-control" placeholder="Jumlah" required>
                  </div>
                <div class="form-group mb-1">
                    <span>Harga Modal</span>
                    <input onkeyup="FunctionTotal()" id="harga_modal" name="harga_modal" type="number" class="form-control" placeholder="Harga Modal" required>
                  </div>
                <div class="form-group mb-1">
                    <span>Harga Total</span>
                    <input id="total_belanja" name="total_belanja" type="number" class="form-control" readonly required>
                  </div>
                <div class="form-group mb-1">
                    <span>Harga</span>
                    <input name="harga_jual" type="number" class="form-control" placeholder="Harga Jual" required>
                    <input name="post" type="hidden" class="form-control" value="Belanja Barang" required>
                  </div>
                  <div class="form-group">
                    <div class="form-group mb-1">
                      <span>Pilih Category</span>
                    <select name="category" class="form-control">
                      <option></option>
                      @foreach($categrory as $ctg)
                      <option>{{$ctg->nama_category}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group mb-1">
                    <span>Pilih Suplier</span>
                    <select name="supplier" class="form-control select2">
                      <option></option>
                      @foreach($supplier as $sup)
                      <option>{{$sup->nama_supplier}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group mb-1">
                    <span>Bayar Menggunakan</span>
                    <select name="akun_keuangan" class="form-control" required>
                      <option></option>
                        @foreach($listrekening as $lsrkng)
                      <option value="{{$lsrkng->nomor_rekening}}">{{$lsrkng->pemilik_rekening}}</option>
                        @endforeach
                      <option>Tokopedia</option>
                      <option>Bukalapak</option>
                      <option>BliBli</option>
                      <option>Shopee</option>
                      <option>Belanja Tempo</option>
                      <option>Ofline</option>
                    </select>
                  </div>
                </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </form>
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->





          <!-- modal category -->
          <div class="modal fade" id="modal-category">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Tambah Category</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/tambah/category">
                @csrf
                <div class="modal-body">
                  <div class="form-group mb-1">
                    <input name="category" type="text" class="form-control" placeholder="Nama Category" required>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </form>
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->
          <!-- modal supplier -->
          <div class="modal fade" id="modal-supplier">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Tambah Supplier</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/tambah/supplier">
                @csrf
                <div class="modal-body">
                  <div class="form-group mb-1">
                    <input name="nama_supplier" type="text" class="form-control" placeholder="Nama Supplier" required>
                  </div>
                  <div class="form-group mb-1">
                    <input name="alamat_supplier" type="text" class="form-control" placeholder="Alamat Supplier" required>
                  </div>
                  <div class="form-group mb-1">
                    <input name="hp_supplier" type="text" class="form-control" placeholder="No Hp Supplier" required>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </form>
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->
          <!-- / SEMUA MODAL DI DALAM SINI -->

          
                  </section>
                <!-- Dashboard Analytics end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

<script>
function FunctionTotal() {
  var harga = document.getElementById("harga_modal").value;
  var jumlah = document.getElementById("jumlah").value;
  var total = parseInt(harga) * parseInt(jumlah);
  if (!isNaN(total)){
           document.getElementById('total_belanja').value = total;
        }
}
</script>
<script>
  @foreach($supplier as $index => $sup)
    document.getElementById('editsup[{{$index+1}}]').onclick = function() {
    document.getElementById('namasup[{{$index+1}}]').removeAttribute('readonly');
    document.getElementById('alamatsup[{{$index+1}}]').removeAttribute('readonly');
    document.getElementById('hpsup[{{$index+1}}]').removeAttribute('readonly');
  }
  @endforeach

</script>
<script>
 @foreach($stok as $index => $stoks)
    document.getElementById('editnamstok[{{$index+1}}]').onclick = function() {
    document.getElementById('namastok[{{$index+1}}]').removeAttribute('readonly');
  }
  @endforeach

</script>
<script>
 @foreach($stok as $index => $stoks)
    document.getElementById('editcatstok[{{$index+1}}]').onclick = function() {
    document.getElementById('catstok[{{$index+1}}]').removeAttribute('disabled');
  }
  @endforeach

</script>
<script>
 @foreach($stok as $index => $stoks)
    document.getElementById('editstok[{{$index+1}}]').onclick = function() {
    document.getElementById('stok[{{$index+1}}]').removeAttribute('readonly');
  }
  @endforeach

</script>

@endsection