@extends('layouts.app')
<?php $page = "laporanadvance" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Dashboard</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/home">Laporan keuangan</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="card-body row  col-md-12">
                  <div class="card col-md-6">
                    <div class="card-body">
                      <form action="/keuanganadvance">
                        <div class="form-group row mb-3">
                            <div class="col-md-4">
                              <input class="form-control" name="awal" type="date" value="{{$dmyAwal}}" required>
                            </div>
                            <div class="col-md-4">
                              <input class="form-control" name="akhir" type="date" value="{{$dmyAkhir}}" required>
                            </div>
                            <div class="col-md-4">
                              <button class="btn btn-primary" type="submit">Tampilkan</button>
                            </div>
                        </div>
                      </form>
                    <table class="table table-sm table-striped">
                      <thead>
                        <tr>
                          <th class="text-nowrap">Nama Produk</th>
                          <th class="text-nowrap">Total Terjual</th>
                          <th class="text-nowrap">Total Modal</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($penjualancair as $penjualancair)
                          <tr>
                            <td class="text-nowrap">{{$penjualancair->nama_barang}}</td>
                            <td class="text-nowrap">{{$penjualancair->total_terjual}}</td>
                            <td class="text-nowrap">@currency($penjualancair->total_modal)</td>
                          </tr>
                        @endforeach
                        <tr>
                          <td class="text-nowrap"></td>
                          <td class="text-nowrap"></td>
                          <td class="text-nowrap">@currency($sumpenjualancair->total_modal)</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="card">
                   <table class="table table-sm table-striped">
                    <thead>
                    <tr>
                      <th>Platform</th>
                      <th>Nama</th>
                      <th>Saldo</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($listKantongDana as $lkd)
                      @php
                        if($lkd->post == "Tokopedia"){
                            $icon = "tokopedia.png";
                        }elseif($lkd->post == "Shopee"){
                            $icon = "shopee.png";
                        }elseif($lkd->post == "BliBLi"){
                            $icon = "blibli.png";
                        }elseif($lkd->post == "Lazada"){
                            $icon = "lazada.png";
                        }else{
                            $icon = "ego.jpg";
                        };
                      @endphp
                      <tr>
                        <td class="text-nowrap"><span class="info-box-icon"><img src="../../dist/img/{{$icon}}" width="30" height="30" ></span></td>
                        <td class="text-nowrap">{{$lkd->post}}</td>
                        <td class="text-nowrap">@currency($lkd->total)</td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                  </div>
                </div>
            <!-- /.card -->

            <div class="content-header-left col-md-12 col-12 mb-1">
              <div class="row breadcrumbs-top">
                <div class="col-12">
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 mb-1">
                      <button type="btn" class="btn btn-primary mt-1" data-bs-toggle="modal" data-bs-target="#modal-debit"><i data-feather="plus-square" class="me-1"></i> Dana Masuk</button>
                      <button type="btn" class="btn btn-primary mt-1" data-bs-toggle="modal" data-bs-target="#modal-kredit"><i data-feather="minus-square" class="me-1"></i> Dana Keluar</button>
                      <button type="btn" class="btn btn-primary mt-1" data-bs-toggle="modal" data-bs-target="#modal-pencairan"><i data-feather="plus-square" class="me-1"></i> Antar Kantong Dana</button>
                      <button type="btn" class="btn btn-primary mt-1" data-bs-toggle="modal" data-bs-target="#modal-akun"><i data-feather="plus-square" class="me-1"></i> Akun Keuangan</button>
                      <button type="btn" class="btn btn-primary mt-1" data-bs-toggle="modal" data-bs-target="#modal-rekening "><i data-feather="plus-square" class="me-1"></i> Rekening</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          <!-- PANJANG TABLE CARD -->
          <div class="col-lg-12 mb-5">
            <div class="card card-primary card-outline card-outline-tabs">
              <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-four-home-tab" data-bs-toggle="pill" href="#custom-tabs-four-home" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">LAPORAN KEUANGAN</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-profile-tab" data-bs-toggle="pill" href="#custom-tabs-four-profile" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">AKUN KEUANGAN</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-profile-tab" data-bs-toggle="pill" href="#custom-tabs-four-rekening" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">REKENING BANK</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-four-tabContent">
                  <div class="tab-pane fade active show" id="custom-tabs-four-home" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                    <div class="table-responsivetable">
                      <table class="table table-responsive table-sm table-striped">
                        <thead>
                        <tr>
                          <th class="text-nowrap">Tanggal</th>
                          <th class="text-nowrap">Nama Transaksi</th>
                          <th class="text-nowrap">Dana Masuk</th>
                          <th class="text-nowrap">Dana Keluar</th>
                          <th class="text-nowrap">Kantong Dana</th>
                          <th class="text-nowrap">Akun Keuangan</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($keuangan as $keu)
                        <tr class="shop-item">
                          <td class="text-nowrap">{{ $keu->tanggal }}</td>
                          <td>{{ $keu->nama_teransaksi }}</td>
                          <td class="text-nowrap">@currency($keu->debit)</td>
                          <td class="text-nowrap">@currency($keu->kredit)</td>
                          <td class="text-nowrap">{{ $keu->post }}</td>
                          <td class="text-nowrap">{{ $keu->post_akun }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                      </table>
                      <div class="form-group mt-3">
                      {{$keuangan->links()}}
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                    <div class="form-group row col-md-12">
                    </div>
                     <table class="table table-striped">
                      <thead>
                      <tr>
                        <th class="text-center">Nomor</th>
                        <th class="text-center">Nama Akun</th>
                        <th class="text-center">Option</th>
                      </tr>
                      </thead>
                      <tbody>
                        @foreach($akun as $index => $nuka)
                      <tr class="shop-item">
                        <td class="text-center">{{ $index+1 }}</td>
                        <td class="text-center">{{ $nuka->nama_akun }}</td>
                        <td class="text-center"><a href="/hapus/akun/{{$nuka->id_akun}}"class="btn btn-sm btn-danger hapusdata"><i data-feather="trash"></i></a></td>
                      </tr>
                      @endforeach
                      </tbody>
                    </table>
                    <div class="form-group mt-3">
                    {{$akun->links()}}
                    </div>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-four-rekening" role="tabpanel" aria-labelledby="custom-tabs-four-rekening-tab">
                    <div class="form-group row col-md-12">
                    </div>
                     <table class="table table-striped">
                      <thead>
                      <tr>
                        <th class="text-center">Pemilik Rekening</th>
                        <th class="text-center">Nama Bank</th>
                        <th class="text-center">Nomor Rekening</th>
                      </tr>
                      </thead>
                      <tbody>
                        @foreach($listrekening as $asdf)
                      <tr class="shop-item">
                        <td class="text-center">{{ $asdf->pemilik_rekening }}</td>
                        <td class="text-center">{{ $asdf->nama_bank }}</td>
                        <td class="text-center">{{ $asdf->nomor_rekening }}</td>
                      </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
          <!-- /.col-md-12 -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

          <!-- modal category -->
          <div class="modal fade" id="modal-debit">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Dana Masuk</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/dana/masukadvance">
                @csrf
                <div class="modal-body">
                  <div class="form-group">
                    <input name="tanggal" type="date" class="form-control mb-1" required>
                  </div>
                  <div class="form-group">
                    <input name="nama_teransaksi" type="text" class="form-control mb-1" placeholder="Nama Transaksi" required>
                  </div>
                  <div class="form-group">
                    <input name="debit" type="text" class="form-control mb-1" placeholder="Nominal" required>
                  </div>
                  <div class="form-group mb-1">
                    <Label>Masuk Ke Kantong</label>
                    <select name="post" class="form-control" required>
                      <option></option>
                        @foreach($listrekening as $lsrkng)
                      <option value="{{$lsrkng->nomor_rekening}}">{{$lsrkng->pemilik_rekening}}</option>
                        @endforeach
                      <option>Ofline</option>
                    </select>
                  </div>
                  <div class="form-group mb-1">
                      <Label>Post Keuangan</label>
                    <select name="post_keuangan" class="form-control" required>
                      <option></option>
                      <option>oprasional</option>
                      <option>Pendapatan</option>
                      @foreach($akun as $nuka)
                      <option>{{$nuka->nama_akun}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->

          <!-- modal category -->
          <div class="modal fade" id="modal-kredit">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Dana Keluar</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/dana/keluaradvance">
                @csrf
                <div class="modal-body">
                  <div class="form-group mb-1">
                    <input name="tanggal" type="date" class="form-control" required>
                  </div>
                  <div class="form-group mb-1">
                    <input name="nama_teransaksi" type="text" class="form-control" placeholder="Nama Transaksi" required>
                  </div>
                  <div class="form-group mb-1">
                    <input name="debit" type="text" class="form-control" placeholder="Nominal" required>
                  </div>
                  <div class="form-group mb-1">
                    <Label>Keluar Dari Kantong</label>
                    <select name="post" class="form-control" required>
                      <option></option>
                        @foreach($listrekening as $lsrkng)
                          <option value="{{$lsrkng->nomor_rekening}}">{{$lsrkng->pemilik_rekening}}</option>
                        @endforeach
                        @foreach($listMarketplace as $marketplace)
                          <option>{{$marketplace->nama_marketplace}}</option>
                        @endforeach
                      <option>Ofline</option>
                    </select>
                  </div>
                  <div class="form-group mb-1">
                    <Label>Post Keuangan</label>
                    <select name="post_keuangan" class="form-control" required>
                      <option></option>
                      <option>oprasional</option>
                      <option>pendapatan</option>
                      @foreach($akun as $nuka)
                      <option>{{$nuka->nama_akun}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->
          
          <!-- modal category -->
          <div class="modal fade" id="modal-pencairan">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Pencairan Marketplace</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/danaantarrekening">
                @csrf
                <div class="modal-body">
                  <div class="form-group mb-1">
                    <input name="tanggal" type="date" class="form-control" required>
                  </div>
                  <div class="form-group mb-1">
                    <input name="nama_teransaksi" type="text" class="form-control" placeholder="Nama Transaksi" required>
                  </div>
                  <div class="form-group mb-1">
                    <input name="nilai_teransaksi" type="text" class="form-control" placeholder="Nominal" required>
                  </div>
                  <div class="form-group mb-1">
                    <Label>Keluar Dari Rekening</label>
                    <select name="dana_keluar" class="form-control" required>
                      <option></option>
                        @foreach($listrekening as $lsrkng)
                          <option value="{{$lsrkng->nomor_rekening}}">{{$lsrkng->pemilik_rekening}}</option>
                        @endforeach
                        @foreach($listMarketplace as $marketplace)
                          <option>{{$marketplace->nama_marketplace}}</option>
                        @endforeach
                      <option>Ofline</option>
                    </select>
                  </div>
                  <div class="form-group mb-1">
                    <Label>Masuk Ke Rekening</label>
                    <select name="dana_masuk" class="form-control" required>
                      <option></option>
                        @foreach($listrekening as $lsrkng)
                          <option value="{{$lsrkng->nomor_rekening}}">{{$lsrkng->pemilik_rekening}}</option>
                        @endforeach
                        @foreach($listMarketplace as $marketplace)
                          <option>{{$marketplace->nama_marketplace}}</option>
                        @endforeach
                      <option>Ofline</option>
                    </select>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->

          <!-- modal category -->
          <div class="modal fade" id="modal-akun">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Akun</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/tambah/akun">
                @csrf
                <div class="modal-body">
                  <div class="form-group">
                    <input name="akun" type="text" class="form-control" placeholder="Nama Akun" required>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->

          <!-- modal category -->
          <div class="modal fade" id="modal-rekening">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Form Tambah Rekening</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="get" action="/tambahrekening">
                @csrf
                <div class="modal-body">
                  <div class="form-group mb-2">
                    <input name="pemilik_rekening" type="text" class="form-control" placeholder="Pemilik Rekening" required>
                  </div>
                  <div class="form-group mb-2">
                    <input name="nama_bank" type="text" class="form-control" placeholder="Nama Bank" required>
                  </div>
                  <div class="form-group mb-2">
                    <input name="nomor_rekening" type="text" class="form-control" placeholder="Nomor Rekening" required>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->

@endsection
