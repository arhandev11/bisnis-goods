@extends('layouts.app')

<?php
if ($detailInvoice->platform == "ChartPublic") {
  $page = "pesanancs";
}else {
  $page = "pesananagen" ;
}
?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Detail Order Reseller</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/home">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/detailorderreseller">Detail Order Reseller</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-9 col-md-9 col-12 mb-md-0">
                  <div class="card invoice-preview-card">
                    <div class="table-responsive border-top">
                      <table class="table m-0">
                        <thead>
                          <tr>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Qty</th>
                            <th>Sub Total</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($item as $item)
                          <tr>
                            <td class="text-nowrap">{{$item->title}}</td>
                            <td>@currency($item->harga)</td>
                            <td>{{$item->jumlah}}</td>
                            <td>@currency($item->harga * $item->jumlah)</td>
                          </tr>
                        @endforeach
                          <tr>
                            <td colspan="2" class="align-top px-4 py-4">
                            </td>
                            <td class="text-end">
                              <p class="">Subtotal:</p>
                              <p class="">Ongkir:</p>
                              <p class="text-success">Potongan:</p>
                              <p class="">Total:</p>
                            </td>
                            <td >
                              <p class="fw-semibold">@currency($invoice->tagihan)</p>
                              <p class="fw-semibold">@currency($invoice->ongkir)</p>
                              <p class="fw-semibold text-success">@currency($invoice->diskon)</p>
                              <p class="fw-semibold">@currency($invoice->tagihan + $invoice->ongkir - $invoice->diskon)</p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="card col-md-3">
                  <div class="card-body">
                    <table class="table-borderless mb-2">
                    <tbody>
                      <tr>
                          <td class="align-middle">
                            <label>Pembyaran</label>
                          </td>
                          <td>
                            <label>: {{$detailInvoice->bayar_id }}</label>
                          </td>
                      </tr>
                      <tr style="display: @php if($detailInvoice->platform != "ChartPublic"){echo"none";} @endphp">
                          <td class="align-middle">
                            <label>Pembeli</label>
                          </td>
                          <td>
                            <label>: {{$detailInvoice->nama_pembeli}}</label>
                          </td>
                      </tr>
                      <tr style="display: @php if($detailInvoice->platform == "ChartPublic"){echo"none";} @endphp">
                          <td class="align-middle">
                            <label>Reseller</label>
                          </td>
                          <td>
                            <label>:  {{$detailInvoice->platform}}</label>
                          </td>
                      </tr>
                      <tr>
                          <td class="align-middle">
                            <label>Seller</label>
                          </td>
                          <td>
                            <label>: {{$detailInvoice->invoice_toko}} </label>
                          </td>
                      </tr>
                      <tr>
                          <td class="align-middle">
                            <label>Status</label>
                          </td>
                          <td>
                            : <label  class="badge badge-light-success"> {{$detailInvoice->tempo}}</label>
                          </td>
                      </tr>
                    </tbody>
                  </table>
                            @php
                                if($detailInvoice->tempo == "new"){
                                    $diproses = "";
                                    $dipacking = "";
                                    $dikirim = "";
                                }elseif($detailInvoice->tempo == "diproses"){
                                    $diproses = "disabled";
                                    $dipacking = "";
                                    $dikirim = "";
                                }elseif($detailInvoice->tempo == "dipacking"){
                                    $diproses = "disabled";
                                    $dipacking = "disabled";
                                    $dikirim = "";
                                }elseif($detailInvoice->tempo == "dikirim"){
                                    $diproses = "disabled";
                                    $dipacking = "disabled";
                                    $dikirim = "disabled";
                                }
                            @endphp
                          <button type="button" class="btn btn-dark disabled">Order Settings</button>
                          <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                            <span class="visually-hidden">Toggle Dropdown</span>
                          </button>
                          @php
                            if($detailInvoice->platform == "ChartPublic" && $detailInvoice->tempo == "new"){
                              $disablechrtpublic = "disabled";
                            }else {
                              $disablechrtpublic = "";
                            }
                          @endphp
                          <ul class="dropdown-menu">
                            <li style="display: <?php if(Auth::user()->role == 8){ echo "";}if(Auth::user()->role == 1){ echo "";}else{ echo "none";} ?>"><a class="dropdown-item akanProsesPesanan {{$disablechrtpublic}} {{$diproses}}" href="/updateprosesreseller/{{$id}}">Proses Pesanan</a></li>
                            <li style="display: <?php if(Auth::user()->role == 8){ echo "";}if(Auth::user()->role == 1){ echo "";}else{ echo "none";} ?>"><a class="dropdown-item {{$disablechrtpublic}} {{$dipacking}}" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#basicModal" >Packing Pesanan</a></li>
                            <li style="display: <?php if(Auth::user()->role == 8){ echo "";}if(Auth::user()->role == 1){ echo "";}else{ echo "none";} ?>"><a class="dropdown-item akanKirimPesanan {{$disablechrtpublic}} {{$dikirim}}" href="/updatekirimreseller/{{$id}}">Kirim Pesanan</a></li>
                            <li class="dropdown-divider"> style="display: <?php if(Auth::user()->role != 3){ echo "";}else{ echo "none";} ?>"></li>
                            <li style="display: <?php if(Auth::user()->role == 8){ echo "";}if(Auth::user()->role == 1){ echo "";}else{ echo "none";} ?>"><a class="dropdown-item tolakpesanan text-danger" href="/tolakpesananreseller/{{$id}}">Tolak Pesanan</a></li>
                            <li style="display: <?php if(Auth::user()->role == 8){ echo "";}else{ echo "none";} ?>"><a style="display: <?php if($detailInvoice->tempo == "dikirim"){ echo "";}else{ echo "none";} ?>" class="dropdown-item pembatalanwarning text-danger" href="/tolakpesananreseller/{{$id}}">Batalkan Pesanan</a></li> <!-- milik gudang -->
                            <li style="display: <?php if(Auth::user()->role == 9){ echo "";}else{ echo "none";} ?>"><a style="display: <?php if($detailInvoice->tempo != "new"){ echo "none";}else{ echo "";} ?>" class="dropdown-item pembatalanwarning text-danger" href="/tolakpesananreseller/{{$id}}">Batalkan Pesanan</a></li> <!-- milik reseller -->
                            <li><a class="dropdown-item" href="/invoice/thermalReseller/{{$id}}" target="_blank">Print Reseller</a></li>
                            <li><a class="dropdown-item" href="/invoicethermal/{{$id}}"  target="_blank">Print Buyer</a></li>
                            <li><a class="dropdown-item" href="https://api.whatsapp.com/send/?phone={{$detailInvoice->hp_pembeli}}&text=Hello%2C+{{$detailInvoice->nama_pembeli}}+Silahkan lakukan konfirmasi pembayaran di link berikut+%0A+{{$domain}}/publicpayment?id={{$detailInvoice->id}}{{$detailInvoice->invoice}}" target="_blank">Kirim Wa</a></li>
                            
                          </ul>
                        </div>
                    </div>
                  </div>

                <!-- timeline detail status pemesanan -->
                <div class="col-xl-12 mb-4 mb-xl- mb-5">
                  <div class="card">
                    <h5 class="card-header">Timeline Pesanan</h5>
                    <div class="card-body pb-0">
                      <ul class="timeline mb-0">
                        @foreach($timeline as $timeline)
                        <li class="timeline-item timeline-item-transparent mb-1 ">
                          <span class="timeline-point timeline-point-primary"></span>
                          <div class="timeline-event">
                            <div class="timeline-header border-bottom mb-1">
                              <h6 class="mb-0">{{$timeline->judul_timeline}}</h6>
                              <span class="text-muted">{{$timeline->tanggal_timeline}}</span>
                            </div>
                            <div class="d-flex justify-content-between flex-wrap">
                              <div>
                                <span>{{$timeline->deskripsi_timeline}}</span>
                              </div>
                              <div>
                                <span class="text-muted">{{$timeline->jam_timeline}}</span>
                              </div>
                            </div>
                            <a href="../../img/{{$timeline->file_timeline}}" target="_blank" style="display:<?php if($timeline->file_timeline == null){echo "none";}else{echo "block";} ?>">
                              <img class="round" src="../../img/{{$timeline->file_timeline}}" height="200px" width="200px">
                            </a>
                          </div>
                        </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>

                    <!-- Modal packing pesanan -->
                    <div class="modal fade" id="basicModal" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <form method="post" action="/uploasbuktitrf/{{$id}}" enctype="multipart/form-data">
                                @csrf
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel1">Upload Bukti Packing</h5>
                                <button
                                  type="button"
                                  class="btn-close"
                                  data-bs-dismiss="modal"
                                  aria-label="Close"
                                ></button>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col mb-3">
                                    <label for="nameBasic" class="form-label">Upload Foto Packing</label>
                                    <input name="file" type="file" class="form-control">
                                  </div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-label-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Upload</button>
                              </div>
                            </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
@endsection