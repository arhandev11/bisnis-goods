@extends('layouts.app')
<?php $page = "User" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Detail User</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="stok">Detail User</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

          <!-- PANJANG TABLE CARD -->
            <div class="col-lg-12">
                <!-- Multi Column with Form Separator -->
              <div class="card mb-4">
                <h5 class="card-header">Formulir edit detail produk</h5>
                <form method="get" action="/updateuserdetail/{{$id}}">
                <div  class="card-body">
                @csrf
                  <div class="row g-3">
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Nama</label>
                      <input type="text" class="form-control" name="nama" value="{{$userDetail->name}}" />
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Nomor Handphone</label>
                      <input type="text" class="form-control" name="hp" value="{{$userDetail->hp_toko}}" />
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Email</label>
                      <input type="text" class="form-control" name="email" value="{{$userDetail->email}}" />
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Role</label>
                        <select name="role" class="form-control"  required>
                            <option value="{{$idRole}}">{{$roleSelected}}</option>
                          @foreach($role as $roleakun)
                            <option value="{{$roleakun->id_role}}">{{$roleakun->role_name}}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="col-md-3" style="display:">
                      <label class="form-label" for="multicol-username">Divisi</label>
                      <input type="text" class="form-control" name="tipe" value="" disabled />
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Tier</label>
                        <select name="tier" class="form-control"  required>
                            <option>{{$userDetail->tier}}</option>
                            <option value="tr1">tr 1</option>
                            <option value="tr2">tr 2</option>
                            <option value="tr3">tr 3</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Manager</label>
                        @php
                          if($userDetail->manager == 1){
                            $managerStatus = "yes";
                          }else{
                            $managerStatus = "no";
                          };
                        @endphp
                        <select name="manager" class="form-control"  required>
                            <option>{{$managerStatus}}</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Toko</label>
                        <select name="toko" class="form-control"  required>
                            <option>{{$userDetail->toko}}</option>
                            <option></option>
                            @foreach($listToko as $toko)
                            <option>{{$toko->nama_cabang}}</option>
                            @endforeach
                        </select>
                    </div>
                  </div>
                  <div class="pt-4">
                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                  </div>
                </form>
              </div>
            </div>
    <!-- END: Content-->

@endsection