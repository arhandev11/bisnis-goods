@extends('layouts.app')
<?php $page = "setings" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
  <!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Store Setings</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/setingsstore">Store Setings</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

           <div class="row">
                <div class="col-md-12">
                  <ul class="nav nav-pills flex-column flex-md-row mb-4">
                    <li class="nav-item">
                      <a class="nav-link" href="/setings"><i data-feather="user" class="ti-xs ti ti-users me-1"></i> Account</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="/setingspass"><i data-feather="user" class="ti-xs ti ti-lock me-1"></i> Security</a>
                    </li>
                    <li  style="display:<?php if( Auth::user()->role == 1){echo "block";}else{echo "none";} ?>" class="nav-item">
                      <a class="nav-link active" href="/setingsstore"><i data-feather="shopping-bag" class="ti-xs ti ti-lock me-1"></i> Store</a>
                    </li>
                  </ul>
                  <!-- Change Password -->
                  <div class="card mb-4">
                    <h5 class="card-header">Store Setings</h5>
                    <div class="card-body">
                      <form action="/updatetokodetail" method="POST">
                        @csrf
                        <div class="row">
                          <div class="mb-3 col-md-6 form-password-toggle fv-plugins-icon-container">
                            <label class="form-label" for="currentPassword">Rekening Tempo Reseller</label>
                            <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="rekening yang di pilih akan menjadi rekening tujuan ketika reseller dan agen membayar tagihan tempo mereka">
                              <i class="text-primary" data-feather="help-circle"></i>
                            </a>
                            <div class="input-group input-group-merge has-validation">
                              <select class="form-control" name="rekeningreseller" required>
                                <option value="{{$norekreseller}}">{{$namerekreseller}} - {{$norekreseller}}</option>
                                @foreach($rekening as $rekening1)
                                <option value="{{$rekening1->nomor_rekening}}">{{$rekening1->pemilik_rekening}}</option>
                                @endforeach
                              </select>
                            </div>
                            <div class="fv-plugins-message-container invalid-feedback"></div>
                          </div>
                          <div class="mb-3 col-md-6 form-password-toggle fv-plugins-icon-container">
                            <label class="form-label" for="currentPassword">Type Invoice</label>
                            <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="pilih jenis invoice print able yang di butuhkan">
                              <i class="text-primary" data-feather="help-circle"></i>
                            </a>
                            <div class="input-group input-group-merge has-validation">
                              <select class="form-control" name="invoice_type" required>
                                <option {{$selectedpaper}} value="invoicepaper">Invoice Paper</option>
                                <option {{$selectedthermal}} value="invoicethermal">Invoice Thermal</option>
                              </select>
                            </div>
                            <div class="fv-plugins-message-container invalid-feedback"></div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="mb-3 col-md-6 form-password-toggle fv-plugins-icon-container">
                            <label class="form-label" for="currentPassword">Type Chart</label>
                            <a data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="pilih jenis chart yang akan di gunakan toko">
                              <i class="text-primary" data-feather="help-circle"></i>
                            </a>
                            <div class="input-group input-group-merge has-validation">
                              <select class="form-control" name="jenis_chart" required>
                                <option {{$selectchart}} value="select">Select Chart</option>
                                <option {{$barcodechart}} value="shop">Barcode Chart</option>
                              </select>
                            </div>
                            <div class="fv-plugins-message-container invalid-feedback"></div>
                          </div>
                        </div>
                        <div class="mt-2">
                          <button type="submit" class="btn btn-primary me-2 waves-effect waves-light">Simpan Perubahan</button>
                        </div>
                      </form>
                    </div>
                  </div>
                  <!--/ Change Password -->
                </div>
              </div>

@endsection
