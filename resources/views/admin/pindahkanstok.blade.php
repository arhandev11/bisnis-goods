@extends('layouts.app')
<?php $page = "stok" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Form Pemindahan Stok</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="stok">Form Pemindahan Stok</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




          <!-- PANJANG TABLE CARD -->
            <div class="col-lg-12">
                <!-- Multi Column with Form Separator -->
              <div class="card mb-4">
                <div  class="card-body">
                <form method="get" action="/prosespindahkanstok/{{$id}}">
                @csrf
                    @foreach($detailBarang as $detailBarang)
                  <div class="row g-1">
                    <div class="col-md-2">
                      <label class="form-label" for="multicol-username">Code Produk</label>
                      <input type="text" class="form-control" name="code_barang" value="{{$detailBarang->code_barang}}" readonly />
                    </div>
                    <div class="col-md-3">
                      <label class="form-label" for="multicol-username">Lokasi Produk</label>
                      <input type="text" class="form-control" name="tipe" value="{{$detailBarang->tipe}}" readonly />
                    </div>
                    <div class="col-md-5">
                      <label class="form-label" for="multicol-username">Nama Produk</label>
                      <input type="text" class="form-control" name="nama_barang" value="{{$detailBarang->nama_barang}}" readonly />
                    </div>
                    <div class="col-md-2 mb-5">
                      <label class="form-label" for="multicol-username">Jumlah Stok</label>
                      <input type="text" class="form-control" name="jumlah_stok_asal" value="{{$detailBarang->stok}}" readonly />
                    </div>
                    <div class="col-md-3">
                        <label class="form-label" for="multicol-username">Pindahkan Ke</label>
                        <select id="select2Basic" class="select2 form-select form-select-lg" name="tipe_tujuan" data-allow-clear="false">
                            @foreach($tokoCabang as $cabang)
                            <option>{{$cabang->nama_cabang}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2 mb-5">
                      <label class="form-label" for="multicol-username">Pindahkan Sejumlah</label>
                      <input type="text" class="form-control" name="jumlah_dipindah"/>
                    </div>
                  </div>
                  <div class="pt-4">
                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                  </div>
                  @endforeach
                </form>
              </div>
            </div>
    <!-- END: Content-->

@endsection