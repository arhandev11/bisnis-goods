@extends('layouts.app')
<?php $page = "pembayaran" ?>
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Pembayaran Reseller</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/home">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/pembayaran">Pembayaran Reseller</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




                <div class="table-responsive card card-body" style="display: <?php if(Auth::user()->role != 3){ echo "block";}else{ echo "none";} ?>">
                  <div class="nav-align-top nav-tabs-shadow mb-4">
                    <ul class="nav nav-tabs nav-fill" role="tablist">
                      <li class="nav-item">
                        <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab" data-bs-target="#navs-pesananbaru" aria-controls="navs-pesananbaru" aria-selected="true">
                          <i data-feather="user-check"></i> Perlu Di Tinjau 
                          <a class="badge badge-light-success ms-1"></a>
                        </button>
                      </li>
                      <li class="nav-item">
                        <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-diprosses" aria-controls="navs-diprosses" aria-selected="false">
                          <i data-feather="check-square"></i> Pembayaran Di Terima
                          <a class="badge badge-light-success ms-1"></a>
                        </button>
                      </li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane fade show active" id="navs-pesananbaru" role="tabpanel">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                  <th>tiket</th>
                                  <th>Tanggal</th>
                                  <th>Total Bayar</th>
                                  <th>Status</th>
                                  <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @foreach($listpembayaran as $lspb)
                                <tr class="shop-item">
                                    <td>{{ $lspb->tiket_pembayaran}}</td>
                                    <td>{{$lspb->tanggal_pembayaran}}</td>
                                    <td>@currency($lspb->total_pembayaran)</td>
                                    <td>{{$lspb->status_pembayaran}}</td>
                                    <td>
                                        <a href="/detailhistorybayar/{{$lspb->tiket_pembayaran}}" class="btn btn-sm btn-primary"><i data-feather="eye"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="form-group pl-2">
                            {{$listpembayaran->links()}}
                            </div>   
                        </div>  
                      </div>
                      <div class="tab-pane fade" id="navs-diprosses" role="tabpanel">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                  <th>tiket</th>
                                  <th>Tanggal</th>
                                  <th>Total Bayar</th>
                                  <th>Status</th>
                                  <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @foreach($listlunas as $lslns)
                                <tr class="shop-item">
                                    <td><a target="_blank" href="/invoice/thermal/{{ $lslns->tiket_pembayaran}}">{{ $lslns->tiket_pembayaran}}</a></td>
                                    <td>{{$lslns->tanggal_pembayaran}}</td>
                                    <td>@currency($lslns->total_pembayaran)</td>
                                    <td>{{$lslns->status_pembayaran}}</td>
                                    <td>
                                        <a href="/detailhistorybayar/{{$lslns->tiket_pembayaran}}" class="btn btn-sm btn-primary"><i data-feather="eye"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="form-group pl-2">
                            {{$listlunas->links()}}
                            </div>   
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>


@endsection