@extends('layouts.app')
<?php $page = "setings" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
  <!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                  <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Security</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/setingspass">Security</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

           <div class="row">
                <div class="col-md-12">
                  <ul class="nav nav-pills flex-column flex-md-row mb-4">
                    <li class="nav-item">
                      <a class="nav-link" href="/setings"><i data-feather="user" class="ti-xs ti ti-users me-1"></i> Account</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link active" href="/setingspass"><i data-feather="user" class="ti-xs ti ti-lock me-1"></i> Security</a>
                    </li>
                    <li class="nav-item">
                      <a style="display:<?php if( Auth::user()->role == 1){echo "block";}else{echo "none";} ?>" class="nav-link" href="/setingsstore"><i data-feather="shopping-bag" class="ti-xs ti ti-lock me-1"></i> Store</a>
                    </li>
                  </ul>
                  <!-- Change Password -->
                  <div class="card mb-4">
                    <h5 class="card-header">Change Password</h5>
                    <div class="card-body">
                      <form action="/gantipassword/{{$userDetail->id}}" method="POST">
                        @csrf
                        <div class="row">
                          <div class="mb-3 col-md-6 form-password-toggle fv-plugins-icon-container">
                            <label class="form-label" for="currentPassword">Password Saat Ini</label>
                            <div class="input-group input-group-merge has-validation">
                              <input class="form-control" type="" name="password_saat_ini" id="currentPassword" placeholder="············" required>
                            </div><div class="fv-plugins-message-container invalid-feedback"></div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="mb-3 col-md-6 form-password-toggle fv-plugins-icon-container">
                            <label class="form-label" for="newPassword">Password Baru</label>
                            <div class="input-group input-group-merge has-validation">
                              <input class="form-control" type="" id="newPassword" name="password_baru" placeholder="············" required>
                            </div><div class="fv-plugins-message-container invalid-feedback"></div>
                          </div>

                          <div class="mb-3 col-md-6 form-password-toggle fv-plugins-icon-container">
                            <label class="form-label" for="confirmPassword">Confirm Password Baru</label>
                            <div class="input-group input-group-merge has-validation">
                              <input class="form-control" type="" name="confirm_password_saat_ini" id="confirm_password_saat_ini" placeholder="············" required>
                            </div><div class="fv-plugins-message-container invalid-feedback"></div>
                          </div>
                          <div class="col-12 mb-4">
                            <h6>Tips Password Yang Kuat</h6>
                            <ul class="ps-3 mb-0">
                              <li class="mb-1">Minimum 8 karakter</li>
                              <li class="mb-1">Mengandung minimal 1 huruf capital</li>
                              <li>Paling tidak mengandung 1 angka</li>
                            </ul>
                          </div>
                          <div>
                            <button type="submit" class="btn btn-primary me-2 waves-effect waves-light">Simpan Perubahan</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <!--/ Change Password -->
                </div>
              </div>

@endsection
