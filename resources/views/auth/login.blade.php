<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />
  <title>login</title>

  <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="apple-touch-icon" href="{{ asset('app-assets/images/ico/ego.jpg') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('app-assets/images/ico/ego.jpg') }}">

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
      rel="stylesheet"
    />

    <!-- Icons -->
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/fonts/fontawesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/fonts/tabler-icons.css') }}" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/fonts/flag-icons.css') }}" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/rtl/core.css') }}" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/rtl/theme-default.css') }}" />
    <link rel="stylesheet" href="{{ asset('app-assets/css/demo.css') }}" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/libs/perfect-scrollbar/perfect-scrollbar.css') }}" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/libs/node-waves/node-waves.css') }}" />
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/libs/typeahead-js/typeahead.css') }}" />
    <!-- Vendor -->
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/libs/formvalidation/dist/css/formValidation.min.css') }}" />

    <!-- Page CSS -->
    <!-- Page -->
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/pages/page-auth.css') }}" />
    <!-- Helpers -->
    <script src="{{ asset('app-assets/vendors/js/helpers.js') }}"></script>
</head>

  <body>
    <!-- Content -->

    <div class="authentication-wrapper authentication-cover authentication-bg">
      <div class="authentication-inner row">
        <!-- /Left Text -->
        <div class="d-none d-lg-flex col-lg-7 p-0">
          <div class="auth-cover-bg auth-cover-bg-color d-flex justify-content-center align-items-center">
            <img
              src="{{ asset('app-assets/images/ico/Bisnisgood.png') }}"
              alt="auth-login-cover"
              class="img-fluid my-5 auth-illustration"
              data-app-light-img="illustrations/auth-login-illustration-light.png"
              data-app-dark-img="illustrations/auth-login-illustration-dark.png"
            />

            <img
              src="{{ asset('app-assets/images/ico/Bisnisgood1.png') }}"
              alt="auth-login-cover"
              width="200"
              class="img-fluid my-5 auth-illustration"
            />
          </div>
        </div>
        <!-- /Left Text -->

        <!-- Login -->
        <div class="d-flex col-12 col-lg-5 align-items-center p-sm-5 p-4">
          <div class="w-px-400 mx-auto">
            <!-- Logo -->
            <div class="app-brand mb-4">
              <a class="app-brand-link gap-2">
                <span class="app-brand-logo ">
                  <img
                    src="{{ asset('app-assets/images/ico/Bisnisgood1.png') }}"
                    alt="auth-login-cover"
                    data-app-light-img="illustrations/auth-login-illustration-light.png"
                    data-app-dark-img="illustrations/auth-login-illustration-dark.png"
                    width="100"
                  />
                </span>
              </a>
            </div>
            <!-- /Logo -->
            <p class="mb-4">Create A Good Bisnis Star From A Bisnisgood</p>

            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="mb-3">
                  <label for="email" class="form-label">Email</label>
                  <input placeholder="Email" id="email" type="" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                </div>
                <div class="mb-3 form-password-toggle">
                  <div class="d-flex justify-content-between">
                    <label class="form-label" for="password">Password</label>
                    <a href="auth-forgot-password-basic.html">
                      <small>Forgot Password?</small>
                    </a>
                  </div>
                  <div class="input-group input-group-merge">
                  <input placeholder="******" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                  </div>
                </div>
                <div class="mb-3">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="remember-me" />
                    <label class="form-check-label" for="remember-me"> Remember Me </label>
                  </div>
                </div>
                <div class="mb-3">
                  <button class="btn btn-primary d-grid w-100" type="submit">Sign in</button>
                </div>
              </form>

            <p class="text-center">
              <span>New on our platform?</span>
              <a href="auth-register-cover.html">
                <span>Create an account</span>
              </a>
            </p>
            </div>
          </div>
        </div>
        <!-- /Login -->
      </div>
    </div>
    <!-- / Content -->
    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{ asset('app-assets/vendors/libs/jquery/jquery.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/popper/popper.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/bootstrap.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/node-waves/node-waves.js') }}"></script>

    <script src="{{ asset('app-assets/vendors/libs/hammer/hammer.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/i18n/i18n.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/typeahead-js/typeahead.js') }}"></script>

    <script src="{{ asset('app-assets/vendors/js/menu.js') }}"></script>
    <!-- endbuild -->

    <!-- Vendors JS -->
    <script src="{{ asset('app-assets/vendors/libs/formvalidation/dist/js/FormValidation.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/formvalidation/dist/js/plugins/Bootstrap5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/libs/formvalidation/dist/js/plugins/AutoFocus.min.js') }}"></script>

    <!-- Main JS -->
    <script src="{{ asset('app-assets/js/main.js') }}"></script>

    <!-- Page JS -->
    <script src="{{ asset('app-assets/js/pages-auth.js') }}"></script>
  </body>
</html>
