
@extends('layouts.publicapp')
<?php $page = "checkout" ?>
@section('assets')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endsection


@section('content')
<body class=" layout-top-nav">

 
    <div class="bg-gray-50">
      <main class="py-8 mx-auto max-w-7xl px-8">
        <section class="grid md:grid-cols-10 gap-16">
          <div class="md:col-span-7">
            <div>
              <h1 class="font-bold text-2xl mb-4">Produk</h1>
              <div class="flex flex-col gap-4">
                @foreach ($chart as $crt)
                  <div class="border border-gray-300 bg-white shadow-md rounded-lg p-4 flex items-center gap-4">
                    <div class="w-20 h-20 md:w-40 md:h-40 bg-black rounded-lg shrink-0">
                      <img src="../../img/{{$crt->produk_picture}}" alt="tutor image 1"/>
                    </div>
                    <div class="flex flex-col grow">
                      <h1 class="tex-lg md:text-2xl font-bold mb-1">
                        {{$crt->nama_barang}}
                      </h1>
                      <h1 class="text-xl md:text-4xl font-semibold">@currency($crt->harga_jual)</h1>
                      <div class="flex gap-2 items-start mb-1">
                        <h1 class="text-sm md:text-lg text-gray-400 line-through">
                          Rp. 30.000
                        </h1>
                        <h2
                          class="text-green-500 text-sm md:text-lg font-bold bg-green-100 text-green-500 p-0.5"
                        >
                          50%
                        </h2>
                      </div>
                      <h1 class="text-sm md:text-lg text-gray-600">
                        Jumlah Pesanan: 
                        <form action="/tambahchartmanualPublic" method="get">
                          <input name="id_barang" type="hidden" value="{{$crt->no_barang_id}}">
                          <input type="number" class="border border-gray-400 shadow-sm px-3 py-1 mb-1 rounded-md" value="{{$crt->jml_barang}}" name="jml" min="1" step="1" type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                          <button class="border border-blue-600 py-1 text-sm px-1 rounded-md bg-blue-600 text-white font-bold">update</button>
                          <a href="/hapus/chart/public/{{$crt->no_barang_id}}" class="btn btn-sm btn-danger waves-effect waves-light text-red-500 mb-1">hapus</a>
                        </form>
                      </h1>
                    </div>
                  </div>
                @endforeach
                
              </div>
            </div>
            <div class="border-t-2 border-gray-300 my-8"></div>
          </div>
          <div class="relative md:col-span-3">
            <div
              class="border border-gray-300 bg-white shadow-md rounded-lg overflow-hidden flex flex-col self-start p-3 sticky top-16"
            >
              <h1 class="text-center font-bold text-2xl">Form Pemesanan</h1>
              <div class="flex items-center justify-between my-3">
                <h3>Jumlah Pesanan</h3>
                <div class="flex items-center gap-3">
                  <p>{{$totbarang}} Barang</p>
                </div>
              </div>
              <div class="border-t-2 border-gray-300 mb-2"></div>
              <div class="flex items-center justify-between mt-2">
                <h3 class="font-bold">Sub Total</h3>
                <h3 class="font-bold">@currency($subtotal)</h3>
              </div>
              <div class="flex items-center justify-between">
                <h3 class="font-bold">Ongkir</h3>
                <h3 class="font-bold" id="ongkir-price">-</h3>
              </div>
              <div class="flex items-center justify-between">
                <h3 class="font-bold">Potongan</h3>
                <h3 class="font-bold text-green-500">- @currency($nilaivocher)</h3>
              </div>
              <div class="flex items-center justify-between mb-4">
                <h3 class="font-bold">Total</h3>
                <h3 class="font-bold">@currency($subtotal + 10000 - $nilaivocher)</h3>
              </div>

              <div class="flex flex-col gap-2">
                <form action="/prosespublicchart" method="POST" enctype="multipart/form-data">
                  @csrf
                    <div class="form-group row">
                      @foreach ($chart as $index => $crt)
                      <div class="hidden chart-value">
                        <input type="hidden" id="codeItem-{{$index}}" name="codeItem[{{$index}}]" value="{{$crt->id_barang}}" required>
                        <input type="hidden" id="idbarang-{{$index}}" name="idbarang[{{$index}}]" value="{{$crt->no_barang_id}}">
                        <input type="hidden" id="idChart-{{$index}}" name="idChart[{{$index}}]" value="{{$crt->id_chart}}">
                        <input type="hidden" id="namaItem-{{$index}}" name="namaItem[{{$index}}]" value="{{$crt->nama_barang}}">
                        @php
                              $harga = $crt->harga_jual;
                        @endphp
                        <input type="hidden" id="hargaSatuan-{{$index}}" name="hargaSatuan[{{$index}}]" value="{{$harga}}">
                        <input type="hidden" id="jmlsbt-{{$index}}" name="jmlsbt[{{$index}}]" value="{{$crt->jml_barang}}" >
                        <input type="hidden" id="tanggal_orderar-{{$index}}" name="tanggal_orderar[{{$index}}]" type="datetime-local" value="{{date('Y-m-d')}}">
                      </div>
                      @endforeach
                      <input type="hidden" name="jumlah_barang" type="text" value="{{$totbarang}}">
                      <input type="hidden" name="tanggal_order" type="text" value="{{date('Y-m-d')}}">
                      <input type="hidden" name="tagihan" type="text" value="{{$subtotal}}">
                      <input type="hidden" name="vocher" type="text" value="{{$nilaivocher}}">
                      <input type="hidden" name="total_modal" type="text" value="{{$submodal}}">
                      <input type="hidden" name="platform" type="text" value="ChartPublic">
                      <input type="hidden" name="user" type="text" value="{{$userActive}}">
                      <input type="hidden" name="ongkir" type="text" value="0" id="ongkir-input">
                      <input type="hidden" name="status_tempo" value="new" class="form-control" required>
                  </div>
                  <div class="flex flex-col gap-2">
                    <input name="nama_pembeli" type="text" class="border border-gray-400 shadow-sm px-3 py-1 rounded-md w-full" placeholder="Nama" required>
                    <input name="hp_pembeli" type="text" class="border border-gray-400 shadow-sm px-3 py-1 rounded-md w-full" placeholder="No Hp" min="1" step="1" type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>
                    <select class="first-map-search" name="first-search" style="width: 100%;" >
                    </select>
                    <div class="second-map-search-container" style="display: none">
                      <select class="second-map-search" name="second-search" style="width: 100%;" >
                      </select>
                    </div>
                    <div class="couriers-search-container" style="display: none">
                      <select class="couriers-search" name="" style="width: 100%;" >
                      </select>
                    </div>
                    <div class="rates-search-container" style="display: none">
                      <select class="rates-search" name="rates" style="width: 100%;" >
                      </select>
                    </div>
                    <input type="hidden" name="ongkir" />
                    <textarea name="alamat" rows="5" class="border border-gray-400 shadow-sm px-3 py-1 rounded-md w-full"  placeholder="Alamat Lengkap" required></textarea>
                  </div>
                  <div class="form-group row">
                    <button class="border border-blue-600 py-1.5 text-sm px-5 rounded-md bg-blue-600 text-white font-bold w-full" type="submit">Buat Pesanan</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
@endsection
    
@section('scripts')


  <script>
    let rates = [];
    $(document).ready(function(){
      $('.first-map-search').select2({
          ajax: {
              cache: true,
              url: "{{route('first-map-search')}}",
              delay: 250,
              dataType: "json",
              data: (params)=>{
                  var query = {
                      search: params.term,
                  }
                  return query;
              },
              processResults: function(data){
                  return {
                      results: data.data
                  }
              }
          },
          placeholder: "Cari Lokasi",
      })
      $('.second-map-search').select2({
          ajax: {
              cache: true,
              url: "{{route('second-map-search')}}",
              delay: 250,
              dataType: "json",
              data: (params)=>{
                  var query = {
                      area_id: $('.first-map-search').val()
                  }
                  return query;
              },
              processResults: function(data){
                  return {
                      results: data.data
                  }
              }
          },
          placeholder: "Pilih Kode Pos",
          minimumResultsForSearch: -1
      })
      $('.couriers-search').select2({
          ajax: {
              cache: true,
              url: "{{route('get-couriers')}}",
              delay: 250,
              dataType: "json",
              data: (params)=>{
                  var query = {
                  }
                  return query;
              },
              processResults: function(data){
                  return {
                      results: data.data
                  }
              }
          },
          placeholder: "Pilih Kurir",
          minimumResultsForSearch: -1
      })
      $('.rates-search').select2({
          ajax: {
              cache: true,
              url: "{{route('get-rates')}}",
              delay: 250,
              dataType: "json",
              data: (params)=>{
                let items = []
                $('.chart-value').each(function(index){
                  let obj = {
                    nama: $(`#namaItem-${index}`).val(),
                    harga: $(`#hargaSatuan-${index}`).val(),
                    jumlah: $(`#jmlsbt-${index}`).val(),
                  }
                  items.push(obj)
                })
                var query = {
                  'destination_area_id': $('.second-map-search').val(),
                  'couriers': $('.couriers-search').val(),
                  items
                }
                return query;
              },
              processResults: function(data){
                rates = [];
                let lists = [];
                data.data.forEach((item)=>{
                  rates.push(item);
                  let price = 
                    lists.push({
                      id: item.courier_service_code,
                      text:`${item.courier_service_name} (${item.shipment_duration_range} ${item.shipment_duration_unit}) - Rp. ${item.price}`
                    })
                })
                  return {
                      results: lists
                  }
              }
          },
          placeholder: "Pilih Service",
          minimumResultsForSearch: -1
      });
      
      $('.first-map-search').on('change', function(){
        if($('.first-map-search').val()){
          $('.second-map-search-container').show()
          }else{
          $('.second-map-search-container').hide()
        }
      })

      $('.second-map-search').on('change', function(){
        if($('.second-map-search').val()){
          $('.couriers-search-container').show()
          }else{
          $('.couriers-search-container').hide()
        }
      })
      $('.couriers-search').on('change', function(){
        if($('.couriers-search').val()){
          $('.rates-search-container').show()
          }else{
          $('.rates-search-container').hide()
        }
      })
      $('.rates-search').on('change', function(){
        if($('.rates-search').val()){
          let result = rates.find((item)=>item.courier_service_code == $('.rates-search').val())
          $("#ongkir-price").text(`Rp. ${result.price}`)
          $("#ongkir-input").val(result.price)
        } else {
          $("#ongkir-price").text(`-`)
          $("#ongkir-input").val(0)
        }
      })
  })
  </script>
@endsection