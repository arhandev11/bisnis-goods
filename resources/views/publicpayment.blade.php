
@extends('layouts.publicapp')
<?php $page = "checkout" ?>
<body class=" layout-top-nav">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    
<nav class="navbar bg-primary card-body">
  <div class="navbar-brand">Pusat Harmoni</div>
  <div class="navbar-links bg-primary" id="navbarLinks">
      <ul>
          <li><a href="/produk">Home</a></li>
      </ul>
  <ul>
    <a href="/chartpublic" type="button" class="btn btn-outline-success me-1">
      <i class="avatar-icon" data-feather="shopping-cart"></i>
      <span class="ti-xs ti ti-bell me-1"></span>{{$totalbarangdichart}}
    </a>
  </ul>
  <ul>
      <li><button class="btn bg-white">login</button></li>
  </ul>
  </div>
  <div class="burger-menu" id="burgerMenu">
      <div class="burger-line"></div>
      <div class="burger-line"></div>
      <div class="burger-line"></div>
  </div>
</nav>


    <!-- Main content -->
    <div class="card-body">
      <div class="md-12 mt-5">


<div class="row">

<div class="row">
    <div class="col-xl-9 col-md-9 col-12 mb-md-0">
      <div class="card invoice-preview-card">
        <div class="table-responsive">
            <table class="table">
            <thead>
              <tr>
                <th>Nama</th>
                <th width="50%">Harga</th>
                <th>Qty</th>
                <th>Sub Total</th>
              </tr>
            </thead>
            <tbody>
            @foreach($item as $item)
              <tr>
                <td class="text-nowrap">{{$item->title}}</td>
                <td class="text-nowrap">@currency($item->harga)</td>
                <td class="text-nowrap">{{$item->jumlah}}</td>
                <td class="text-nowrap">@currency($item->harga * $item->jumlah)</td>
              </tr>
            @endforeach
              <tr>
                <td colspan="2" class="align-top px-4 py-4">
                </td>
                <td class="text-end">
                  <p class="">Subtotal</p>
                  <p class="">Ongkir</p>
                  <p class="">Potongan</p>
                  <p class="mb-2">Total</p>
                </td>
            @foreach($invoice as $invoice)
                <td >
                  <p class="fw-semibold">@currency($invoice->tagihan)</p>
                  <p class="fw-semibold">@currency($invoice->ongkir) <a class="text-success">FLAT</a></p>
                  <p class="fw-semibold text-success">@currency($invoice->diskon)</p>
                  <p class="fw-semibold">@currency($invoice->tagihan + $invoice->ongkir - $invoice->diskon)</p>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="card col-md-3">
        <form action="/publicuploasbuktitrf/{{$invoiceno}}" method="post"  enctype="multipart/form-data">
            @csrf
        <div class="card-body">
            <label>Upload Bukti Trf</label>
            </br>
            <input name="file" type="file" class="" required>
            <div class="btn-group mt-2">
                    <button class="btn btn-primary">Kirim</button>
            </div>
        </div>
        </form>
      </div>

      



@if(Session::has('fileoversize'))
<script type="text/javascript">

function massge() {
Swal.fire(
            'WARNING!!',
            'file terlalu besar maksimal 10mb',
            'warning'
        );
}

window.onload = massge;
</script>
@endif