
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Home | Ego Store Official</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      <a href="/" class="navbar-brand">
        <img src="../../dist/img/ego.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Ego Store Official</span>
      </a>
      
      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="/" class="nav-link">Home</a>
          </li>
          <li class="nav-item">
            <a href="/login" class="nav-link">Login</a>
          </li>
          <li class="nav-item">
            <a href="https://www.tokopedia.com/egostoreofficial" target="_blank" class="nav-link text-danger">Mulai Belanja</a>
          </li>
        </ul>
      </div>


    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">



@if(Session::has('tidakterdaftar'))
        <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>code yang kamu gunakan tidak terdaftar, silahkan belanja di ego store untuk mendapatkan code, klik <a href="https://www.tokopedia.com/egostoreofficial">SI SINI</a> untuk belanja</strong>
                </div>
@endif

@if(Session::has('sudahdigunakan'))
        <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>code yang kamu masukan sudah di gunakan, silahkan belanja di ego store untuk mendapatkan code baru, klik <a href="https://www.tokopedia.com/egostoreofficial">SI SINI</a> untuk belanja</strong>
                </div>
@endif

@if(Session::has('berhasilinputcode'))
        <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>poin kamu berhasil di tambahkan, ayo belanja lagi untuk menangkan hadiah utama, klik <a href="https://www.tokopedia.com/egostoreofficial">SI SINI</a> untuk belanja</strong>
                </div>
@endif


        <div class="row">
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title m-0">Daftar Layanan Exclusif</h5>
              </div>
                <form class="form-horizontal" action="https://app.sicuba.in/api/store/10745-opening-ego-store" method="post">
                    <div class="card-body">
                      <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                          <input class="form-control" name="nama" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">No Hp</label>
                        <div class="col-sm-9">
                          <input class="form-control" placeholder="Gunakan 0 contoh : 0857***" name="phone" required>
                        </div>
                      </div>
                    </div>
                    <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Kirim Data</button>
                  <a href="https://api.whatsapp.com/send/?phone=6281233337682&text&app_absent=0" target="_blank" class="float-right btn btn-success">Bingung? Tanya Cs</a>
                </div>
                </form>
                <!-- /.card-footer -->
            </div>
          </div>
          <!-- /.col-md-6 -->
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title m-0">Formulir Code Poin Belanja</h5>
              </div>
              <form class="form-horizontal" action="kirimpoin" method="post">
                @csrf
                <div class="card-body">
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 col-form-label">No Hp</label>
                    <div class="col-sm-9">
                      <input class="form-control" placeholder="gunakan 0 contoh : 0857***" name="no_hp" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">Code</label>
                    <div class="col-sm-9">
                      <input class="form-control" placeholder="dapatkan code dengan belanja" name="code" required>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Kirim Data</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->

        <div class="row">
          <div class="col-lg-6">
            <div class="card">
              <button type="button" data-toggle="modal" data-target="#modal-poin" class="btn btn-info">TATA CARA IKUT GEBYAR POIN</button>
              </div>
          </div>
          <!-- /.col-md-6 -->
          <div class="col-lg-6">
            <div class="card">
              <a type="button" href="/leaderboard" class="btn btn-info">CEK PEMENANG & LIHAT POIN MU DI SINI</a>
              </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body" style="display: block;">
                <img class="img-fluid pad" src="dist/img/doorprize.jpg" alt="Photo">
              </div>
            </div>





        <div class="modal fade" id="modal-poin">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tata Tata Cara Mengikuti Gebyar Poin</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <h4><center>BACA LENGKAP YA AGAR TIDAK ADA YANG TERLEWAT</center></h4>
                </br>
            <p>
                <H6>PERATURAN GEBYAR POIN EGO STORE OFFICIAL</H6>
                1. Ini bukanlah undian.
                <br>
                2. Orang yang memiliki poin tertinggi per minggu maka akan menjadi pemenang mingguan
                <br>
                3. Orang yang memiliki poin tertinggi per bulan maka akan menjadi pemenang bulanan
                <br>
                4. Yang sudah memenangkan hadiah mingguan tidak bisa menang hadiah mingguan lagi. 
                <br>
                5. Begitupun yang sudah menang hadiah bulanan tidak dapat menang hadiah bulanan lagi.
                <br>
                6. Akan tetapi masi berhak mendapatkan hadiah dorprize berupa ANDORID TV 40 INCH
                <br>
                7. Setiap kali belanja di <a href="https://www.tokopedia.com/egostoreofficial" target="_blank">EGO STORE OFFICIAL</a> anda akan mendapatkan CODE yang dapat di input ke FORMULIR CODE POIN BELANJA
                <br>
                8. Jadi satu satunya cara untuk menang adalah belanja sebanyak banyak nya dan kumpulkan poin nya.
                <br>
                9. Jika pada hari H ada lebih dari satu orang yang poin terbesar nya sama, maka akan di pilih tergantung kebijakan toko, tidak bisa di ganggu gugat.
                <br>
                10. Pemenang mingguan di umumkan setiap hari sabtu, Pemenang bulanan akan di umumkan setiap tanggal terakhir pada bulan tersebut, dan pemenang dorprize akan di umumkan pada tanggal terakhir di bulan desember.
                <br>
            </p>
                <br>

                <H6>SYARAT MEMENANGKAN HADIAH GEBYAR POIN EGO STORE OFFICIAL</H6>
                1. Jika anda terpilih sebagai pemenang anda harus memenuhui syarat sebagai berikut
                <br>
                2. Selalu memberikan bintang 5 pada ulasan di marketplace
                <br>
                3. Nomor yang anda ikuti gebyar poin harus sudah mengikuti LAYANAN EXLUSIF, jadi pastikan anda sudah daftar layanan exlusif ya
                <br>
                4. Sudah follow instagram <a href="https://www.instagram.com/egostorecoffee/" target="_blank">@egostorecoffee</a> 
                <br>
                5. Sudah follow instagram <a href="https://www.instagram.com/egoistic_id/" target="_blank">@EGOISTIC_ID</a>
                <br>
                6. Jika ketika hari pengumuman poin poin di atas belum terpenuhi, pemenang akan di ganti ke orang dengan poin di bawahnya.
            </p>
            <H6>PEMENANG AKAN DI JAPRI OLEH ADMIN DAN AKAN DI UMUMKAN LIVE DI INSTAGRAM <a href="https://www.instagram.com/egostorecoffee/" target="_blank">@egostorecoffee</a>  DAN <a href="https://www.instagram.com/egoistic_id/" target="_blank">@EGOISTIC_ID</a></H6>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <div class="modal fade" id="modal-poincek">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Lederboard Pemenang </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="col-12 col-sm-12">
            <div class="card card-primary card-outline card-outline-tabs">
              <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill" href="#custom-tabs-four-home" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-profile" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">Leaderboard Mingguan</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-messages-tab" data-toggle="pill" href="#custom-tabs-four-messages" role="tab" aria-controls="custom-tabs-four-messages" aria-selected="false">Leaderboard Bulanan</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-settings-tab" data-toggle="pill" href="#custom-tabs-four-settings" role="tab" aria-controls="custom-tabs-four-settings" aria-selected="false">Leaderboard Dorprize</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-four-tabContent">
                  <div class="tab-pane fade active show" id="custom-tabs-four-home" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                     <h4 class="text-danger">AYO BELANJA SEBANYAK BANYAKNYA DAN MENANGKAN HADIAHNYA</h4> 
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                     Mauris tincidunt mi at erat gravida, eget tristique urna bibendum. Mauris pharetra purus ut ligula tempor, et vulputate metus facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas sollicitudin, nisi a luctus interdum, nisl ligula placerat mi, quis posuere purus ligula eu lectus. Donec nunc tellus, elementum sit amet ultricies at, posuere nec nunc. Nunc euismod pellentesque diam. 
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-four-messages" role="tabpanel" aria-labelledby="custom-tabs-four-messages-tab">
                     HADIAH BULANAN DI MULAI BULAN JULI. 
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-four-settings" role="tabpanel" aria-labelledby="custom-tabs-four-settings-tab">
                     Pellentesque vestibulum commodo nibh nec blandit. Maecenas neque magna, iaculis tempus turpis ac, ornare sodales tellus. Mauris eget blandit dolor. Quisque tincidunt venenatis vulputate. Morbi euismod molestie tristique. Vestibulum consectetur dolor a vestibulum pharetra. Donec interdum placerat urna nec pharetra. Etiam eget dapibus orci, eget aliquet urna. Nunc at consequat diam. Nunc et felis ut nisl commodo dignissim. In hac habitasse platea dictumst. Praesent imperdiet accumsan ex sit amet facilisis. 
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->




        
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Ego Store Official Sahabat Kesehatan Anda
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2020-{{date('Y')}} <a href="https://egogayub.com/">Ego Store Official</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<script src="https://unpkg.com/sweetalert@2.1.2/dist/sweetalert.min.js"></script>
</body>
</html>
