@extends('layouts.app')
<?php $page = "contentplanning" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">


          </div>
          <!-- Title and Top Buttons End -->
          <div class="row">
            <div class="col-12 col-lg-8">
              <!-- Inventory Start -->
                <div class="row">
                  <div class="col-12 col-lg-7">
                        <a class="btn btn-outline-primary btn-icon" data-bs-toggle="modal" data-bs-target="#buattask" href="/buattask/">
                            <i  data-cs-icon="check"></i>  Task Baru
                        </a>
                  </div>
                  <div class="col-12 col-lg-2">
                      <form action="/kontenplaning" method="get">
                          <select class="mb-1 form-control" name="tahun">
                              <option>{{$selectedTahun}}</option>
                              <option value="2022">2022</option>
                              <option value="2023">2023</option>
                              <option value="2024">2024</option>
                              <option value="3025">3025</option>
                          </select>
                    </div>
                    @php
                      if($selectedBulan == '01'){
                        $selected = "januari";
                      }if($selectedBulan == '02'){
                        $selected = "Februari";
                      }if($selectedBulan == '03'){
                        $selected = "Maret";
                      }if($selectedBulan == '04'){
                        $selected = "April";
                      }if($selectedBulan == '05'){
                        $selected = "Mei";
                      }if($selectedBulan == '06'){
                        $selected = "Juni";
                      }if($selectedBulan == '07'){
                        $selected = "Juli";
                      }if($selectedBulan == '08'){
                        $selected = "Agustus";
                      }if($selectedBulan == '09'){
                        $selected = "September";
                      }if($selectedBulan == '10'){
                        $selected = "Oktober";
                      }if($selectedBulan == 11){
                        $selected = "November";
                      }if($selectedBulan == 12){
                        $selected = "Desember";
                      };
                    @endphp
                  <div class="col-12 col-lg-3">
                          <select class="mb-1 col-md-12 form-control" name="bulan" onchange="this.form.submit()">
                              <option>{{$selected}}</option>
                              <option value="01">januri</option>
                              <option value="02">Februari</option>
                              <option value="03">Maret</option>
                              <option value="04">April</option>
                              <option value="05">Mei</option>
                              <option value="06">Juni</option>
                              <option value="07">Juli</option>
                              <option value="08">Agustus</option>
                              <option value="09">September</option>
                              <option value="10">Oktober</option>
                              <option value="11">November</option>
                              <option value="12">Desember</option>
                          </select>
                      </form>
                    </div>
                </div>
              <div class="mb-1">
                <div class="row">

                  <!-- kotak tanggal start -->
                  <div class="col-12 col-sm-6 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-01');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="card {{$active}}">
                      <div class="h-100 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-12 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-01" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-01</a>
                            </div>
                          </div>
                            @php
                            if($instagram1 == ''){
                                $ig1 = "x-square";
                                $classig1 = "text-danger"; 
                            }else{
                                $ig1 = "check-square"; 
                                $classig1 = "text-success";
                            };
                            if($displayCheckIg1 == "x-square"){
                                $classdisplayig1 = "text-danger";
                            }else{
                                $classdisplayig1 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig1}}" class="{{$classig1}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg1}}" class="{{$classdisplayig1}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube1 == ''){
                                $youtube1 = "x-square";
                                $classyt1 = "text-danger";
                            }else{
                                $youtube1 = "check-square"; 
                                $classyt1 = "text-success"; 
                            };
                            if($displayCheckYt1 == "x-square"){
                                $classdisplayyt1 = "text-danger";
                            }else{
                                $classdisplayyt1 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube1}}" class="{{$classyt1}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt1}}" class="{{$classdisplayyt1}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok1 == ''){
                                $tiktok1 = "x-square";
                                $classtk1 = "text-danger";
                            }else{
                                $tiktok1 = "check-square"; 
                                $classtk1 = "text-success"; 
                            };
                            if($displayCheckTk1 == "x-square"){
                                $classdisplaytk1 = "text-danger";
                            }else{
                                $classdisplaytk1 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok1}}" class="{{$classtk1}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk1}}" class="{{$classdisplaytk1}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-12 col-sm-6 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-02');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-100 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-12 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-02" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-02</a>
                            </div>
                          </div>
                            @php
                            if($instagram2 == ''){
                                $ig2 = "x-square";
                                $classig2 = "text-danger"; 
                            }else{
                                $ig2 = "check-square"; 
                                $classig2 = "text-success";
                            };
                            if($displayCheckIg2 == "x-square"){
                                $classdisplayig2 = "text-danger";
                            }else{
                                $classdisplayig2 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig2}}" class="{{$classig2}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg2}}" class="{{$classdisplayig2}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube2 == ''){
                                $youtube2 = "x-square";
                                $classyt2 = "text-danger";
                            }else{
                                $youtube2 = "check-square"; 
                                $classyt2 = "text-success"; 
                            };
                            if($displayCheckYt2 == "x-square"){
                                $classdisplayyt2 = "text-danger";
                            }else{
                                $classdisplayyt2 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube2}}" class="{{$classyt2}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt2}}" class="{{$classdisplayyt2}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok2 == ''){
                                $tiktok2 = "x-square";
                                $classtk2 = "text-danger";
                            }else{
                                $tiktok2 = "check-square"; 
                                $classtk2 = "text-success"; 
                            };
                            if($displayCheckTk2 == "x-square"){
                                $classdisplaytk2 = "text-danger";
                            }else{
                                $classdisplaytk2 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok2}}" class="{{$classtk2}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk2}}" class="{{$classdisplaytk2}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-12 col-sm-6 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-03');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-100 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-12 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-03" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-03</a>
                            </div>
                          </div>
                            @php
                            if($instagram3 == ''){
                                $ig3 = "x-square";
                                $classig3 = "text-danger"; 
                            }else{
                                $ig3 = "check-square"; 
                                $classig3 = "text-success";
                            };
                            if($displayCheckIg3 == "x-square"){
                                $classdisplayig3 = "text-danger";
                            }else{
                                $classdisplayig3 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig3}}" class="{{$classig3}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg3}}" class="{{$classdisplayig3}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube3 == ''){
                                $youtube3 = "x-square";
                                $classyt3 = "text-danger";
                            }else{
                                $youtube3 = "check-square"; 
                                $classyt3 = "text-success"; 
                            };
                            if($displayCheckYt3 == "x-square"){
                                $classdisplayyt3 = "text-danger";
                            }else{
                                $classdisplayyt3 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube3}}" class="{{$classyt3}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt3}}" class="{{$classdisplayyt3}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok3 == ''){
                                $tiktok3 = "x-square";
                                $classtk3 = "text-danger";
                            }else{
                                $tiktok3 = "check-square"; 
                                $classtk3 = "text-success"; 
                            };
                            if($displayCheckTk3 == "x-square"){
                                $classdisplaytk3 = "text-danger";
                            }else{
                                $classdisplaytk3 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok3}}" class="{{$classtk3}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk3}}" class="{{$classdisplaytk3}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-12 col-sm-6 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-04');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-100 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-12 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-04" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-04</a>
                            </div>
                          </div>
                            @php
                            if($instagram4 == ''){
                                $ig4 = "x-square";
                                $classig4 = "text-danger"; 
                            }else{
                                $ig4 = "check-square"; 
                                $classig4 = "text-success";
                            };
                            if($displayCheckIg4 == "x-square"){
                                $classdisplayig4 = "text-danger";
                            }else{
                                $classdisplayig4 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig4}}" class="{{$classig4}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg4}}" class="{{$classdisplayig4}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube4 == ''){
                                $youtube4 = "x-square";
                                $classyt4 = "text-danger";
                            }else{
                                $youtube4 = "check-square"; 
                                $classyt4 = "text-success"; 
                            };
                            if($displayCheckYt4 == "x-square"){
                                $classdisplayyt4 = "text-danger";
                            }else{
                                $classdisplayyt4 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube4}}" class="{{$classyt4}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt4}}" class="{{$classdisplayyt4}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok4 == ''){
                                $tiktok4 = "x-square";
                                $classtk4 = "text-danger";
                            }else{
                                $tiktok4 = "check-square"; 
                                $classtk4 = "text-success"; 
                            };
                            if($displayCheckTk4 == "x-square"){
                                $classdisplaytk4 = "text-danger";
                            }else{
                                $classdisplaytk4 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok4}}" class="{{$classtk4}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk4}}" class="{{$classdisplaytk4}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-12 col-sm-6 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-05');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-100 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-12 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-05" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-05</a>
                            </div>
                          </div>
                            @php
                            if($instagram5 == ''){
                                $ig5 = "x-square";
                                $classig5 = "text-danger"; 
                            }else{
                                $ig5 = "check-square"; 
                                $classig5 = "text-success";
                            };
                            if($displayCheckIg5 == "x-square"){
                                $classdisplayig5 = "text-danger";
                            }else{
                                $classdisplayig5 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig5}}" class="{{$classig5}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg5}}" class="{{$classdisplayig5}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube5 == ''){
                                $youtube5 = "x-square";
                                $classyt5 = "text-danger";
                            }else{
                                $youtube5 = "check-square"; 
                                $classyt5 = "text-success"; 
                            };
                            if($displayCheckYt5 == "x-square"){
                                $classdisplayyt5 = "text-danger";
                            }else{
                                $classdisplayyt5 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube5}}" class="{{$classyt5}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt5}}" class="{{$classdisplayyt5}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok5 == ''){
                                $tiktok5 = "x-square";
                                $classtk5 = "text-danger";
                            }else{
                                $tiktok5 = "check-square"; 
                                $classtk5 = "text-success"; 
                            };
                            if($displayCheckTk5 == "x-square"){
                                $classdisplaytk5 = "text-danger";
                            }else{
                                $classdisplaytk5 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok5}}" class="{{$classtk5}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk5}}" class="{{$classdisplaytk5}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-12 col-sm-6 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-06');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-100 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-12 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-06" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-06</a>
                            </div>
                          </div>
                            @php
                            if($instagram6 == ''){
                                $ig6 = "x-square";
                                $classig6 = "text-danger"; 
                            }else{
                                $ig6 = "check-square"; 
                                $classig6 = "text-success";
                            };
                            if($displayCheckIg6 == "x-square"){
                                $classdisplayig6 = "text-danger";
                            }else{
                                $classdisplayig6 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig6}}" class="{{$classig6}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg6}}" class="{{$classdisplayig6}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube6 == ''){
                                $youtube6 = "x-square";
                                $classyt6 = "text-danger";
                            }else{
                                $youtube6 = "check-square"; 
                                $classyt6 = "text-success"; 
                            };
                            if($displayCheckYt6 == "x-square"){
                                $classdisplayyt6 = "text-danger";
                            }else{
                                $classdisplayyt6 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube6}}" class="{{$classyt6}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt6}}" class="{{$classdisplayyt6}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok6 == ''){
                                $tiktok6 = "x-square";
                                $classtk6 = "text-danger";
                            }else{
                                $tiktok6 = "check-square"; 
                                $classtk6 = "text-success"; 
                            };
                            if($displayCheckTk6 == "x-square"){
                                $classdisplaytk6 = "text-danger";
                            }else{
                                $classdisplaytk6 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok6}}" class="{{$classtk6}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk6}}" class="{{$classdisplaytk6}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-12 col-sm-7 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-07');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-100 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-12 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-07" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-07</a>
                            </div>
                          </div>
                            @php
                            if($instagram7 == ''){
                                $ig7 = "x-square";
                                $classig7 = "text-danger"; 
                            }else{
                                $ig7 = "check-square"; 
                                $classig7 = "text-success";
                            };
                            if($displayCheckIg7 == "x-square"){
                                $classdisplayig7 = "text-danger";
                            }else{
                                $classdisplayig7 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig7}}" class="{{$classig7}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg7}}" class="{{$classdisplayig7}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube7 == ''){
                                $youtube7 = "x-square";
                                $classyt7 = "text-danger";
                            }else{
                                $youtube7 = "check-square"; 
                                $classyt7 = "text-success"; 
                            };
                            if($displayCheckYt7 == "x-square"){
                                $classdisplayyt7 = "text-danger";
                            }else{
                                $classdisplayyt7 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube7}}" class="{{$classyt7}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt7}}" class="{{$classdisplayyt7}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok7 == ''){
                                $tiktok7 = "x-square";
                                $classtk7 = "text-danger";
                            }else{
                                $tiktok7 = "check-square"; 
                                $classtk7 = "text-success"; 
                            };
                            if($displayCheckTk7 == "x-square"){
                                $classdisplaytk7 = "text-danger";
                            }else{
                                $classdisplaytk7 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok7}}" class="{{$classtk7}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk7}}" class="{{$classdisplaytk7}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-12 col-sm-8 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-08');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-100 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-12 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-08" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-08</a>
                            </div>
                          </div>
                            @php
                            if($instagram8 == ''){
                                $ig8 = "x-square";
                                $classig8 = "text-danger"; 
                            }else{
                                $ig8 = "check-square"; 
                                $classig8 = "text-success";
                            };
                            if($displayCheckIg8 == "x-square"){
                                $classdisplayig8 = "text-danger";
                            }else{
                                $classdisplayig8 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig8}}" class="{{$classig8}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg8}}" class="{{$classdisplayig8}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube8 == ''){
                                $youtube8 = "x-square";
                                $classyt8 = "text-danger";
                            }else{
                                $youtube8 = "check-square"; 
                                $classyt8 = "text-success"; 
                            };
                            if($displayCheckYt8 == "x-square"){
                                $classdisplayyt8 = "text-danger";
                            }else{
                                $classdisplayyt8 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube8}}" class="{{$classyt8}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt8}}" class="{{$classdisplayyt8}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok8 == ''){
                                $tiktok8 = "x-square";
                                $classtk8 = "text-danger";
                            }else{
                                $tiktok8 = "check-square"; 
                                $classtk8 = "text-success"; 
                            };
                            if($displayCheckTk8 == "x-square"){
                                $classdisplaytk8 = "text-danger";
                            }else{
                                $classdisplaytk8 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok8}}" class="{{$classtk8}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk8}}" class="{{$classdisplaytk8}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-12 col-sm-9 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-09');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-100 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-12 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-09" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-09</a>
                            </div>
                          </div>
                            @php
                            if($instagram9 == ''){
                                $ig9 = "x-square";
                                $classig9 = "text-danger"; 
                            }else{
                                $ig9 = "check-square"; 
                                $classig9 = "text-success";
                            };
                            if($displayCheckIg9 == "x-square"){
                                $classdisplayig9 = "text-danger";
                            }else{
                                $classdisplayig9 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig9}}" class="{{$classig9}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg9}}" class="{{$classdisplayig9}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube9 == ''){
                                $youtube9 = "x-square";
                                $classyt9 = "text-danger";
                            }else{
                                $youtube9 = "check-square"; 
                                $classyt9 = "text-success"; 
                            };
                            if($displayCheckYt9 == "x-square"){
                                $classdisplayyt9 = "text-danger";
                            }else{
                                $classdisplayyt9 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube9}}" class="{{$classyt9}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt9}}" class="{{$classdisplayyt9}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok9 == ''){
                                $tiktok9 = "x-square";
                                $classtk9 = "text-danger";
                            }else{
                                $tiktok9 = "check-square"; 
                                $classtk9 = "text-success"; 
                            };
                            if($displayCheckTk9 == "x-square"){
                                $classdisplaytk9 = "text-danger";
                            }else{
                                $classdisplaytk9 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok9}}" class="{{$classtk9}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk9}}" class="{{$classdisplaytk9}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-12 col-sm-10 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-10');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-100 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-12 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-10" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-10</a>
                            </div>
                          </div>
                            @php
                            if($instagram10 == ''){
                                $ig10 = "x-square";
                                $classig10 = "text-danger"; 
                            }else{
                                $ig10 = "check-square"; 
                                $classig10 = "text-success";
                            };
                            if($displayCheckIg10 == "x-square"){
                                $classdisplayig10 = "text-danger";
                            }else{
                                $classdisplayig10 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig10}}" class="{{$classig10}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg10}}" class="{{$classdisplayig10}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube10 == ''){
                                $youtube10 = "x-square";
                                $classyt10 = "text-danger";
                            }else{
                                $youtube10 = "check-square"; 
                                $classyt10 = "text-success"; 
                            };
                            if($displayCheckYt10 == "x-square"){
                                $classdisplayyt10 = "text-danger";
                            }else{
                                $classdisplayyt10 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube10}}" class="{{$classyt10}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt10}}" class="{{$classdisplayyt10}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok10 == ''){
                                $tiktok10 = "x-square";
                                $classtk10 = "text-danger";
                            }else{
                                $tiktok10 = "check-square"; 
                                $classtk10 = "text-success"; 
                            };
                            if($displayCheckTk10 == "x-square"){
                                $classdisplaytk10 = "text-danger";
                            }else{
                                $classdisplaytk10 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok10}}" class="{{$classtk10}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk10}}" class="{{$classdisplaytk10}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-12 col-sm-11 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-11');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-110 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-12 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-11" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-11</a>
                            </div>
                          </div>
                            @php
                            if($instagram11 == ''){
                                $ig11 = "x-square";
                                $classig11 = "text-danger"; 
                            }else{
                                $ig11 = "check-square"; 
                                $classig11 = "text-success";
                            };
                            if($displayCheckIg11 == "x-square"){
                                $classdisplayig11 = "text-danger";
                            }else{
                                $classdisplayig11 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig11}}" class="{{$classig11}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg11}}" class="{{$classdisplayig11}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube11 == ''){
                                $youtube11 = "x-square";
                                $classyt11 = "text-danger";
                            }else{
                                $youtube11 = "check-square"; 
                                $classyt11 = "text-success"; 
                            };
                            if($displayCheckYt11 == "x-square"){
                                $classdisplayyt11 = "text-danger";
                            }else{
                                $classdisplayyt11 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube11}}" class="{{$classyt11}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt11}}" class="{{$classdisplayyt11}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok11 == ''){
                                $tiktok11 = "x-square";
                                $classtk11 = "text-danger";
                            }else{
                                $tiktok11 = "check-square"; 
                                $classtk11 = "text-success"; 
                            };
                            if($displayCheckTk11 == "x-square"){
                                $classdisplaytk11 = "text-danger";
                            }else{
                                $classdisplaytk11 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok11}}" class="{{$classtk11}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk11}}" class="{{$classdisplaytk11}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-12 col-sm-12 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-12');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-120 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-12 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-12" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-12</a>
                            </div>
                          </div>
                            @php
                            if($instagram12 == ''){
                                $ig12 = "x-square";
                                $classig12 = "text-danger"; 
                            }else{
                                $ig12 = "check-square"; 
                                $classig12 = "text-success";
                            };
                            if($displayCheckIg12 == "x-square"){
                                $classdisplayig12 = "text-danger";
                            }else{
                                $classdisplayig12 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig12}}" class="{{$classig12}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg12}}" class="{{$classdisplayig12}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube12 == ''){
                                $youtube12 = "x-square";
                                $classyt12 = "text-danger";
                            }else{
                                $youtube12 = "check-square"; 
                                $classyt12 = "text-success"; 
                            };
                            if($displayCheckYt12 == "x-square"){
                                $classdisplayyt12 = "text-danger";
                            }else{
                                $classdisplayyt12 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube12}}" class="{{$classyt12}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt12}}" class="{{$classdisplayyt12}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok12 == ''){
                                $tiktok12 = "x-square";
                                $classtk12 = "text-danger";
                            }else{
                                $tiktok12 = "check-square"; 
                                $classtk12 = "text-success"; 
                            };
                            if($displayCheckTk12 == "x-square"){
                                $classdisplaytk12 = "text-danger";
                            }else{
                                $classdisplaytk12 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok12}}" class="{{$classtk12}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk12}}" class="{{$classdisplaytk12}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-13 col-sm-13 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-13');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-130 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-13 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-13" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-13</a>
                            </div>
                          </div>
                            @php
                            if($instagram13 == ''){
                                $ig13 = "x-square";
                                $classig13 = "text-danger"; 
                            }else{
                                $ig13 = "check-square"; 
                                $classig13 = "text-success";
                            };
                            if($displayCheckIg13 == "x-square"){
                                $classdisplayig13 = "text-danger";
                            }else{
                                $classdisplayig13 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig13}}" class="{{$classig13}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg13}}" class="{{$classdisplayig13}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube13 == ''){
                                $youtube13 = "x-square";
                                $classyt13 = "text-danger";
                            }else{
                                $youtube13 = "check-square"; 
                                $classyt13 = "text-success"; 
                            };
                            if($displayCheckYt13 == "x-square"){
                                $classdisplayyt13 = "text-danger";
                            }else{
                                $classdisplayyt13 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube13}}" class="{{$classyt13}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt13}}" class="{{$classdisplayyt13}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok13 == ''){
                                $tiktok13 = "x-square";
                                $classtk13 = "text-danger";
                            }else{
                                $tiktok13 = "check-square"; 
                                $classtk13 = "text-success"; 
                            };
                            if($displayCheckTk13 == "x-square"){
                                $classdisplaytk13 = "text-danger";
                            }else{
                                $classdisplaytk13 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok13}}" class="{{$classtk13}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk13}}" class="{{$classdisplaytk13}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-14 col-sm-14 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-14');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-140 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-14 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-14" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-14</a>
                            </div>
                          </div>
                            @php
                            if($instagram14 == ''){
                                $ig14 = "x-square";
                                $classig14 = "text-danger"; 
                            }else{
                                $ig14 = "check-square"; 
                                $classig14 = "text-success";
                            };
                            if($displayCheckIg14 == "x-square"){
                                $classdisplayig14 = "text-danger";
                            }else{
                                $classdisplayig14 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig14}}" class="{{$classig14}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg14}}" class="{{$classdisplayig14}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube14 == ''){
                                $youtube14 = "x-square";
                                $classyt14 = "text-danger";
                            }else{
                                $youtube14 = "check-square"; 
                                $classyt14 = "text-success"; 
                            };
                            if($displayCheckYt14 == "x-square"){
                                $classdisplayyt14 = "text-danger";
                            }else{
                                $classdisplayyt14 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube14}}" class="{{$classyt14}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt14}}" class="{{$classdisplayyt14}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok14 == ''){
                                $tiktok14 = "x-square";
                                $classtk14 = "text-danger";
                            }else{
                                $tiktok14 = "check-square"; 
                                $classtk14 = "text-success"; 
                            };
                            if($displayCheckTk14 == "x-square"){
                                $classdisplaytk14 = "text-danger";
                            }else{
                                $classdisplaytk14 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok14}}" class="{{$classtk14}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk14}}" class="{{$classdisplaytk14}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-15 col-sm-15 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-15');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-150 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-15 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-15" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-15</a>
                            </div>
                          </div>
                            @php
                            if($instagram15 == ''){
                                $ig15 = "x-square";
                                $classig15 = "text-danger"; 
                            }else{
                                $ig15 = "check-square"; 
                                $classig15 = "text-success";
                            };
                            if($displayCheckIg15 == "x-square"){
                                $classdisplayig15 = "text-danger";
                            }else{
                                $classdisplayig15 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig15}}" class="{{$classig15}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg15}}" class="{{$classdisplayig15}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube15 == ''){
                                $youtube15 = "x-square";
                                $classyt15 = "text-danger";
                            }else{
                                $youtube15 = "check-square"; 
                                $classyt15 = "text-success"; 
                            };
                            if($displayCheckYt15 == "x-square"){
                                $classdisplayyt15 = "text-danger";
                            }else{
                                $classdisplayyt15 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube15}}" class="{{$classyt15}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt15}}" class="{{$classdisplayyt15}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok15 == ''){
                                $tiktok15 = "x-square";
                                $classtk15 = "text-danger";
                            }else{
                                $tiktok15 = "check-square"; 
                                $classtk15 = "text-success"; 
                            };
                            if($displayCheckTk15 == "x-square"){
                                $classdisplaytk15 = "text-danger";
                            }else{
                                $classdisplaytk15 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok15}}" class="{{$classtk15}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk15}}" class="{{$classdisplaytk15}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-16 col-sm-16 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-16');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-160 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-16 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-16" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-16</a>
                            </div>
                          </div>
                            @php
                            if($instagram16 == ''){
                                $ig16 = "x-square";
                                $classig16 = "text-danger"; 
                            }else{
                                $ig16 = "check-square"; 
                                $classig16 = "text-success";
                            };
                            if($displayCheckIg16 == "x-square"){
                                $classdisplayig16 = "text-danger";
                            }else{
                                $classdisplayig16 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig16}}" class="{{$classig16}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg16}}" class="{{$classdisplayig16}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube16 == ''){
                                $youtube16 = "x-square";
                                $classyt16 = "text-danger";
                            }else{
                                $youtube16 = "check-square"; 
                                $classyt16 = "text-success"; 
                            };
                            if($displayCheckYt16 == "x-square"){
                                $classdisplayyt16 = "text-danger";
                            }else{
                                $classdisplayyt16 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube16}}" class="{{$classyt16}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt16}}" class="{{$classdisplayyt16}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok16 == ''){
                                $tiktok16 = "x-square";
                                $classtk16 = "text-danger";
                            }else{
                                $tiktok16 = "check-square"; 
                                $classtk16 = "text-success"; 
                            };
                            if($displayCheckTk16 == "x-square"){
                                $classdisplaytk16 = "text-danger";
                            }else{
                                $classdisplaytk16 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok16}}" class="{{$classtk16}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk16}}" class="{{$classdisplaytk16}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-17 col-sm-17 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-17');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-170 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-17 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-17" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-17</a>
                            </div>
                          </div>
                            @php
                            if($instagram17 == ''){
                                $ig17 = "x-square";
                                $classig17 = "text-danger"; 
                            }else{
                                $ig17 = "check-square"; 
                                $classig17 = "text-success";
                            };
                            if($displayCheckIg17 == "x-square"){
                                $classdisplayig17 = "text-danger";
                            }else{
                                $classdisplayig17 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig17}}" class="{{$classig17}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg17}}" class="{{$classdisplayig17}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube17 == ''){
                                $youtube17 = "x-square";
                                $classyt17 = "text-danger";
                            }else{
                                $youtube17 = "check-square"; 
                                $classyt17 = "text-success"; 
                            };
                            if($displayCheckYt17 == "x-square"){
                                $classdisplayyt17 = "text-danger";
                            }else{
                                $classdisplayyt17 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube17}}" class="{{$classyt17}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt17}}" class="{{$classdisplayyt17}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok17 == ''){
                                $tiktok17 = "x-square";
                                $classtk17 = "text-danger";
                            }else{
                                $tiktok17 = "check-square"; 
                                $classtk17 = "text-success"; 
                            };
                            if($displayCheckTk17 == "x-square"){
                                $classdisplaytk17 = "text-danger";
                            }else{
                                $classdisplaytk17 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok17}}" class="{{$classtk17}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk17}}" class="{{$classdisplaytk17}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-18 col-sm-18 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-18');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-180 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-18 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-18" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-18</a>
                            </div>
                          </div>
                            @php
                            if($instagram18 == ''){
                                $ig18 = "x-square";
                                $classig18 = "text-danger"; 
                            }else{
                                $ig18 = "check-square"; 
                                $classig18 = "text-success";
                            };
                            if($displayCheckIg18 == "x-square"){
                                $classdisplayig18 = "text-danger";
                            }else{
                                $classdisplayig18 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig18}}" class="{{$classig18}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg18}}" class="{{$classdisplayig18}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube18 == ''){
                                $youtube18 = "x-square";
                                $classyt18 = "text-danger";
                            }else{
                                $youtube18 = "check-square"; 
                                $classyt18 = "text-success"; 
                            };
                            if($displayCheckYt18 == "x-square"){
                                $classdisplayyt18 = "text-danger";
                            }else{
                                $classdisplayyt18 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube18}}" class="{{$classyt18}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt18}}" class="{{$classdisplayyt18}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok18 == ''){
                                $tiktok18 = "x-square";
                                $classtk18 = "text-danger";
                            }else{
                                $tiktok18 = "check-square"; 
                                $classtk18 = "text-success"; 
                            };
                            if($displayCheckTk18 == "x-square"){
                                $classdisplaytk18 = "text-danger";
                            }else{
                                $classdisplaytk18 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok18}}" class="{{$classtk18}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk18}}" class="{{$classdisplaytk18}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-19 col-sm-19 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-19');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-190 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-19 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-19" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-19</a>
                            </div>
                          </div>
                            @php
                            if($instagram19 == ''){
                                $ig19 = "x-square";
                                $classig19 = "text-danger"; 
                            }else{
                                $ig19 = "check-square"; 
                                $classig19 = "text-success";
                            };
                            if($displayCheckIg19 == "x-square"){
                                $classdisplayig19 = "text-danger";
                            }else{
                                $classdisplayig19 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig19}}" class="{{$classig19}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg19}}" class="{{$classdisplayig19}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube19 == ''){
                                $youtube19 = "x-square";
                                $classyt19 = "text-danger";
                            }else{
                                $youtube19 = "check-square"; 
                                $classyt19 = "text-success"; 
                            };
                            if($displayCheckYt19 == "x-square"){
                                $classdisplayyt19 = "text-danger";
                            }else{
                                $classdisplayyt19 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube19}}" class="{{$classyt19}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt19}}" class="{{$classdisplayyt19}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok19 == ''){
                                $tiktok19 = "x-square";
                                $classtk19 = "text-danger";
                            }else{
                                $tiktok19 = "check-square"; 
                                $classtk19 = "text-success"; 
                            };
                            if($displayCheckTk19 == "x-square"){
                                $classdisplaytk19 = "text-danger";
                            }else{
                                $classdisplaytk19 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok19}}" class="{{$classtk19}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk19}}" class="{{$classdisplaytk19}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-20 col-sm-20 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-20');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-200 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-20 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-20" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-20</a>
                            </div>
                          </div>
                            @php
                            if($instagram20 == ''){
                                $ig20 = "x-square";
                                $classig20 = "text-danger"; 
                            }else{
                                $ig20 = "check-square"; 
                                $classig20 = "text-success";
                            };
                            if($displayCheckIg20 == "x-square"){
                                $classdisplayig20 = "text-danger";
                            }else{
                                $classdisplayig20 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig20}}" class="{{$classig20}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg20}}" class="{{$classdisplayig20}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube20 == ''){
                                $youtube20 = "x-square";
                                $classyt20 = "text-danger";
                            }else{
                                $youtube20 = "check-square"; 
                                $classyt20 = "text-success"; 
                            };
                            if($displayCheckYt20 == "x-square"){
                                $classdisplayyt20 = "text-danger";
                            }else{
                                $classdisplayyt20 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube20}}" class="{{$classyt20}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt20}}" class="{{$classdisplayyt20}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok20 == ''){
                                $tiktok20 = "x-square";
                                $classtk20 = "text-danger";
                            }else{
                                $tiktok20 = "check-square"; 
                                $classtk20 = "text-success"; 
                            };
                            if($displayCheckTk20 == "x-square"){
                                $classdisplaytk20 = "text-danger";
                            }else{
                                $classdisplaytk20 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok20}}" class="{{$classtk20}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk20}}" class="{{$classdisplaytk20}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-21 col-sm-21 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-21');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-210 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-21 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-21" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-21</a>
                            </div>
                          </div>
                            @php
                            if($instagram21 == ''){
                                $ig21 = "x-square";
                                $classig21 = "text-danger"; 
                            }else{
                                $ig21 = "check-square"; 
                                $classig21 = "text-success";
                            };
                            if($displayCheckIg21 == "x-square"){
                                $classdisplayig21 = "text-danger";
                            }else{
                                $classdisplayig21 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig21}}" class="{{$classig21}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg21}}" class="{{$classdisplayig21}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube21 == ''){
                                $youtube21 = "x-square";
                                $classyt21 = "text-danger";
                            }else{
                                $youtube21 = "check-square"; 
                                $classyt21 = "text-success"; 
                            };
                            if($displayCheckYt21 == "x-square"){
                                $classdisplayyt21 = "text-danger";
                            }else{
                                $classdisplayyt21 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube21}}" class="{{$classyt21}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt21}}" class="{{$classdisplayyt21}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok21 == ''){
                                $tiktok21 = "x-square";
                                $classtk21 = "text-danger";
                            }else{
                                $tiktok21 = "check-square"; 
                                $classtk21 = "text-success"; 
                            };
                            if($displayCheckTk21 == "x-square"){
                                $classdisplaytk21 = "text-danger";
                            }else{
                                $classdisplaytk21 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok21}}" class="{{$classtk21}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk21}}" class="{{$classdisplaytk21}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-22 col-sm-22 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-22');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-220 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-22 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-22" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-22</a>
                            </div>
                          </div>
                            @php
                            if($instagram22 == ''){
                                $ig22 = "x-square";
                                $classig22 = "text-danger"; 
                            }else{
                                $ig22 = "check-square"; 
                                $classig22 = "text-success";
                            };
                            if($displayCheckIg22 == "x-square"){
                                $classdisplayig22 = "text-danger";
                            }else{
                                $classdisplayig22 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig22}}" class="{{$classig22}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg22}}" class="{{$classdisplayig22}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube22 == ''){
                                $youtube22 = "x-square";
                                $classyt22 = "text-danger";
                            }else{
                                $youtube22 = "check-square"; 
                                $classyt22 = "text-success"; 
                            };
                            if($displayCheckYt22 == "x-square"){
                                $classdisplayyt22 = "text-danger";
                            }else{
                                $classdisplayyt22 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube22}}" class="{{$classyt22}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt22}}" class="{{$classdisplayyt22}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok22 == ''){
                                $tiktok22 = "x-square";
                                $classtk22 = "text-danger";
                            }else{
                                $tiktok22 = "check-square"; 
                                $classtk22 = "text-success"; 
                            };
                            if($displayCheckTk22 == "x-square"){
                                $classdisplaytk22 = "text-danger";
                            }else{
                                $classdisplaytk22 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok22}}" class="{{$classtk22}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk22}}" class="{{$classdisplaytk22}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-23 col-sm-23 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-23');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-230 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-23 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-23" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-23</a>
                            </div>
                          </div>
                            @php
                            if($instagram23 == ''){
                                $ig23 = "x-square";
                                $classig23 = "text-danger"; 
                            }else{
                                $ig23 = "check-square"; 
                                $classig23 = "text-success";
                            };
                            if($displayCheckIg23 == "x-square"){
                                $classdisplayig23 = "text-danger";
                            }else{
                                $classdisplayig23 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig23}}" class="{{$classig23}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg23}}" class="{{$classdisplayig23}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube23 == ''){
                                $youtube23 = "x-square";
                                $classyt23 = "text-danger";
                            }else{
                                $youtube23 = "check-square"; 
                                $classyt23 = "text-success"; 
                            };
                            if($displayCheckYt23 == "x-square"){
                                $classdisplayyt23 = "text-danger";
                            }else{
                                $classdisplayyt23 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube23}}" class="{{$classyt23}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt23}}" class="{{$classdisplayyt23}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok23 == ''){
                                $tiktok23 = "x-square";
                                $classtk23 = "text-danger";
                            }else{
                                $tiktok23 = "check-square"; 
                                $classtk23 = "text-success"; 
                            };
                            if($displayCheckTk23 == "x-square"){
                                $classdisplaytk23 = "text-danger";
                            }else{
                                $classdisplaytk23 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok23}}" class="{{$classtk23}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk23}}" class="{{$classdisplaytk23}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-24 col-sm-24 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-24');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-240 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-24 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-24" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-24</a>
                            </div>
                          </div>
                            @php
                            if($instagram24 == ''){
                                $ig24 = "x-square";
                                $classig24 = "text-danger"; 
                            }else{
                                $ig24 = "check-square"; 
                                $classig24 = "text-success";
                            };
                            if($displayCheckIg24 == "x-square"){
                                $classdisplayig24 = "text-danger";
                            }else{
                                $classdisplayig24 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig24}}" class="{{$classig24}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg24}}" class="{{$classdisplayig24}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube24 == ''){
                                $youtube24 = "x-square";
                                $classyt24 = "text-danger";
                            }else{
                                $youtube24 = "check-square"; 
                                $classyt24 = "text-success"; 
                            };
                            if($displayCheckYt24 == "x-square"){
                                $classdisplayyt24 = "text-danger";
                            }else{
                                $classdisplayyt24 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube24}}" class="{{$classyt24}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt24}}" class="{{$classdisplayyt24}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok24 == ''){
                                $tiktok24 = "x-square";
                                $classtk24 = "text-danger";
                            }else{
                                $tiktok24 = "check-square"; 
                                $classtk24 = "text-success"; 
                            };
                            if($displayCheckTk24 == "x-square"){
                                $classdisplaytk24 = "text-danger";
                            }else{
                                $classdisplaytk24 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok24}}" class="{{$classtk24}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk24}}" class="{{$classdisplaytk24}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-25 col-sm-25 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-25');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-250 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-25 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-25" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-25</a>
                            </div>
                          </div>
                            @php
                            if($instagram25 == ''){
                                $ig25 = "x-square";
                                $classig25 = "text-danger"; 
                            }else{
                                $ig25 = "check-square"; 
                                $classig25 = "text-success";
                            };
                            if($displayCheckIg25 == "x-square"){
                                $classdisplayig25 = "text-danger";
                            }else{
                                $classdisplayig25 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig25}}" class="{{$classig25}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg25}}" class="{{$classdisplayig25}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube25 == ''){
                                $youtube25 = "x-square";
                                $classyt25 = "text-danger";
                            }else{
                                $youtube25 = "check-square"; 
                                $classyt25 = "text-success"; 
                            };
                            if($displayCheckYt25 == "x-square"){
                                $classdisplayyt25 = "text-danger";
                            }else{
                                $classdisplayyt25 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube25}}" class="{{$classyt25}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt25}}" class="{{$classdisplayyt25}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok25 == ''){
                                $tiktok25 = "x-square";
                                $classtk25 = "text-danger";
                            }else{
                                $tiktok25 = "check-square"; 
                                $classtk25 = "text-success"; 
                            };
                            if($displayCheckTk25 == "x-square"){
                                $classdisplaytk25 = "text-danger";
                            }else{
                                $classdisplaytk25 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok25}}" class="{{$classtk25}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk25}}" class="{{$classdisplaytk25}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-26 col-sm-26 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-26');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-260 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-26 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-26" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-26</a>
                            </div>
                          </div>
                            @php
                            if($instagram26 == ''){
                                $ig26 = "x-square";
                                $classig26 = "text-danger"; 
                            }else{
                                $ig26 = "check-square"; 
                                $classig26 = "text-success";
                            };
                            if($displayCheckIg26 == "x-square"){
                                $classdisplayig26 = "text-danger";
                            }else{
                                $classdisplayig26 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig26}}" class="{{$classig26}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg26}}" class="{{$classdisplayig26}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube26 == ''){
                                $youtube26 = "x-square";
                                $classyt26 = "text-danger";
                            }else{
                                $youtube26 = "check-square"; 
                                $classyt26 = "text-success"; 
                            };
                            if($displayCheckYt26 == "x-square"){
                                $classdisplayyt26 = "text-danger";
                            }else{
                                $classdisplayyt26 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube26}}" class="{{$classyt26}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt26}}" class="{{$classdisplayyt26}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok26 == ''){
                                $tiktok26 = "x-square";
                                $classtk26 = "text-danger";
                            }else{
                                $tiktok26 = "check-square"; 
                                $classtk26 = "text-success"; 
                            };
                            if($displayCheckTk26 == "x-square"){
                                $classdisplaytk26 = "text-danger";
                            }else{
                                $classdisplaytk26 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok26}}" class="{{$classtk26}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk26}}" class="{{$classdisplaytk26}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-27 col-sm-27 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-27');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-270 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-27 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-27" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-27</a>
                            </div>
                          </div>
                            @php
                            if($instagram27 == ''){
                                $ig27 = "x-square";
                                $classig27 = "text-danger"; 
                            }else{
                                $ig27 = "check-square"; 
                                $classig27 = "text-success";
                            };
                            if($displayCheckIg27 == "x-square"){
                                $classdisplayig27 = "text-danger";
                            }else{
                                $classdisplayig27 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig27}}" class="{{$classig27}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg27}}" class="{{$classdisplayig27}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube27 == ''){
                                $youtube27 = "x-square";
                                $classyt27 = "text-danger";
                            }else{
                                $youtube27 = "check-square"; 
                                $classyt27 = "text-success"; 
                            };
                            if($displayCheckYt27 == "x-square"){
                                $classdisplayyt27 = "text-danger";
                            }else{
                                $classdisplayyt27 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube27}}" class="{{$classyt27}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt27}}" class="{{$classdisplayyt27}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok27 == ''){
                                $tiktok27 = "x-square";
                                $classtk27 = "text-danger";
                            }else{
                                $tiktok27 = "check-square"; 
                                $classtk27 = "text-success"; 
                            };
                            if($displayCheckTk27 == "x-square"){
                                $classdisplaytk27 = "text-danger";
                            }else{
                                $classdisplaytk27 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok27}}" class="{{$classtk27}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk27}}" class="{{$classdisplaytk27}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-28 col-sm-28 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-28');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-280 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-28 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-28" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-28</a>
                            </div>
                          </div>
                            @php
                            if($instagram28 == ''){
                                $ig28 = "x-square";
                                $classig28 = "text-danger"; 
                            }else{
                                $ig28 = "check-square"; 
                                $classig28 = "text-success";
                            };
                            if($displayCheckIg28 == "x-square"){
                                $classdisplayig28 = "text-danger";
                            }else{
                                $classdisplayig28 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig28}}" class="{{$classig28}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg28}}" class="{{$classdisplayig28}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube28 == ''){
                                $youtube28 = "x-square";
                                $classyt28 = "text-danger";
                            }else{
                                $youtube28 = "check-square"; 
                                $classyt28 = "text-success"; 
                            };
                            if($displayCheckYt28 == "x-square"){
                                $classdisplayyt28 = "text-danger";
                            }else{
                                $classdisplayyt28 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube28}}" class="{{$classyt28}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt28}}" class="{{$classdisplayyt28}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok28 == ''){
                                $tiktok28 = "x-square";
                                $classtk28 = "text-danger";
                            }else{
                                $tiktok28 = "check-square"; 
                                $classtk28 = "text-success"; 
                            };
                            if($displayCheckTk28 == "x-square"){
                                $classdisplaytk28 = "text-danger";
                            }else{
                                $classdisplaytk28 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok28}}" class="{{$classtk28}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk28}}" class="{{$classdisplaytk28}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-29 col-sm-29 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-29');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-290 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-29 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-29" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-29</a>
                            </div>
                          </div>
                            @php
                            if($instagram29 == ''){
                                $ig29 = "x-square";
                                $classig29 = "text-danger"; 
                            }else{
                                $ig29 = "check-square"; 
                                $classig29 = "text-success";
                            };
                            if($displayCheckIg29 == "x-square"){
                                $classdisplayig29 = "text-danger";
                            }else{
                                $classdisplayig29 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig29}}" class="{{$classig29}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg29}}" class="{{$classdisplayig29}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube29 == ''){
                                $youtube29 = "x-square";
                                $classyt29 = "text-danger";
                            }else{
                                $youtube29 = "check-square"; 
                                $classyt29 = "text-success"; 
                            };
                            if($displayCheckYt29 == "x-square"){
                                $classdisplayyt29 = "text-danger";
                            }else{
                                $classdisplayyt29 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube29}}" class="{{$classyt29}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt29}}" class="{{$classdisplayyt29}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok29 == ''){
                                $tiktok29 = "x-square";
                                $classtk29 = "text-danger";
                            }else{
                                $tiktok29 = "check-square"; 
                                $classtk29 = "text-success"; 
                            };
                            if($displayCheckTk29 == "x-square"){
                                $classdisplaytk29 = "text-danger";
                            }else{
                                $classdisplaytk29 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok29}}" class="{{$classtk29}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk29}}" class="{{$classdisplaytk29}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-30 col-sm-30 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-30');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-300 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-30 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-30" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-30</a>
                            </div>
                          </div>
                            @php
                            if($instagram30 == ''){
                                $ig30 = "x-square";
                                $classig30 = "text-danger"; 
                            }else{
                                $ig30 = "check-square"; 
                                $classig30 = "text-success";
                            };
                            if($displayCheckIg30 == "x-square"){
                                $classdisplayig30 = "text-danger";
                            }else{
                                $classdisplayig30 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig30}}" class="{{$classig30}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg30}}" class="{{$classdisplayig30}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube30 == ''){
                                $youtube30 = "x-square";
                                $classyt30 = "text-danger";
                            }else{
                                $youtube30 = "check-square"; 
                                $classyt30 = "text-success"; 
                            };
                            if($displayCheckYt30 == "x-square"){
                                $classdisplayyt30 = "text-danger";
                            }else{
                                $classdisplayyt30 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube30}}" class="{{$classyt30}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt30}}" class="{{$classdisplayyt30}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok30 == ''){
                                $tiktok30 = "x-square";
                                $classtk30 = "text-danger";
                            }else{
                                $tiktok30 = "check-square"; 
                                $classtk30 = "text-success"; 
                            };
                            if($displayCheckTk30 == "x-square"){
                                $classdisplaytk30 = "text-danger";
                            }else{
                                $classdisplaytk30 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok30}}" class="{{$classtk30}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk30}}" class="{{$classdisplaytk30}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                  <!-- kotak tanggal start -->
                  <div class="col-31 col-sm-31 col-lg-3">
                            @php
                            $today = ($tahun.'-'.$bulan.'-31');
                            if($selectedDate == $today){
                              $active = "border-info";
                            }else{
                              $active = "";
                            };
                            @endphp
                    <div class="{{$active}} card">
                      <div class="h-310 d-flex flex-column justify-content-between card-body">
                          <div class="row">
                            <div class="col-sm-31 mb-1">
                              <a href="/contentplanningday/{{$tahun}}-{{$bulan}}-31" class="btn btn-sm bg-primary text-light">{{$tahun}}-{{$bulan}}-31</a>
                            </div>
                          </div>
                            @php
                            if($instagram31 == ''){
                                $ig31 = "x-square";
                                $classig31 = "text-danger"; 
                            }else{
                                $ig31 = "check-square"; 
                                $classig31 = "text-success";
                            };
                            if($displayCheckIg31 == "x-square"){
                                $classdisplayig31 = "text-danger";
                            }else{
                                $classdisplayig31 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Insta
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$ig31}}" class="{{$classig31}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckIg31}}" class="{{$classdisplayig31}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($youtube31 == ''){
                                $youtube31 = "x-square";
                                $classyt31 = "text-danger";
                            }else{
                                $youtube31 = "check-square"; 
                                $classyt31 = "text-success"; 
                            };
                            if($displayCheckYt31 == "x-square"){
                                $classdisplayyt31 = "text-danger";
                            }else{
                                $classdisplayyt31 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Youtube
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$youtube31}}" class="{{$classyt31}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckYt31}}" class="{{$classdisplayyt31}} font-medium-5"></i>
                            </div>
                          </div>
                            @php
                            if($tiktok31 == ''){
                                $tiktok31 = "x-square";
                                $classtk31 = "text-danger";
                            }else{
                                $tiktok31 = "check-square"; 
                                $classtk31 = "text-success"; 
                            };
                            if($displayCheckTk31 == "x-square"){
                                $classdisplaytk31 = "text-danger";
                            }else{
                                $classdisplaytk31 = "text-success";
                            };
                            @endphp
                          <div class="row">
                            <div class="col-sm-6">
                              Tiktok
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$tiktok31}}" class="{{$classtk31}} font-medium-5"></i>
                            </div>
                            <div class="col-sm-3">
                              <i data-feather="{{$displayCheckTk31}}" class="{{$classdisplaytk31}} font-medium-5"></i>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- kotak tanggal end -->

                </div>
              </div>
              <!-- Inventory End -->
            </div>
            <div class="col-12 col-lg-4">
              <!-- Today's Orders Start -->
              <h2 class="small-title mb-2">Detail Content {{$selectedDate}}</h2>
              <div class="card w-100 sh-50 mb-5">
                      <div class="h-100 d-flex flex-column justify-content-between card-body">
                <div class="card justify-content-between bg-transparent">
                  <div class="d-flex flex-column h-100 justify-content-between align-items-start">
                    <div>
                    </div>
                    <div>
                      <div class="cta-1 text-primary mb-1">INSTAGRAM</div>
                      @php
                      if($igtaskcount == '0'){
                        $style1 = "block";
                        $style2 = "none";
                      }else{
                        $style1 = "none";
                        $style2 = "block";
                      }
                      @endphp
                        <div style="display:{{$style1}}" class="text-danger lh-1-25 mb-0">Tidak ada Konten Terjadwal</div>
                      @foreach($detailtaskIg as $index => $igtask)
                        <a href="/detailtodo/{{$igtask->id_task}}" style="display:{{$style2}}" class="text-dark lh-1-25 mb-0">{{$index+1}}. {{$igtask->judul}}</a>
                      @endforeach
                    </div>
                    <div>
                      <div class="mt-5 cta-1 text-primary mb-1">YOUTUBE</div>
                      @php
                      if($yttaskcount == '0'){
                        $style1 = "block";
                        $style2 = "none";
                      }else{
                        $style1 = "none";
                        $style2 = "block";
                      }
                      @endphp
                        <div style="display:{{$style1}}" class="text-danger lh-1-25 mb-0">Tidak ada Konten Terjadwal</div>
                      @foreach($detailtaskyt as $index => $yttask)
                        <a href="/detailtodo/{{$yttask->id_task}}" style="display:{{$style2}}" class="text-dark lh-1-25 mb-0">{{$index+1}}. {{$yttask->judul}}</a>
                      @endforeach
                    </div>
                    <div>
                      <div class="mt-5 cta-1 text-primary mb-1">TIKTOK</div>
                      @php
                      if($tktaskcount == '0'){
                        $style1 = "block";
                        $style2 = "none";
                      }else{
                        $style1 = "none";
                        $style2 = "block";
                      }
                      @endphp
                        <div style="display:{{$style1}}" class="text-danger lh-1-25 mb-0">Tidak ada Konten Terjadwal</div>
                      @foreach($detailtasktk as $index => $tktask)
                        <a href="/detailtodo/{{$tktask->id_task}}" style="display:{{$style2}}" class="text-dark lh-1-25 mb-0">{{$index+1}}. {{$tktask->judul}}</a>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
              <!-- Today's Orders End -->
            </div>
          </div>

          
        <!-- Right Modal detail team -->
        <div class="modal modal-right large fade" id="buattask" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">FORMULIR TASK BARU</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                                <form method="post" action="/taskkontenbaru" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        {{csrf_field()}}
                                        <label class="form-label mt-1">Task</label>
                                        <input type="text" placeholder="" class="form-control" name="task" required>
                                        <label class="form-label mt-1">Media</label>
                                        <select class="form-control" name="media" required>
                                            <option></option>
                                            <option value="tiktok">tiktok</option>
                                            <option value="youtube">youtube</option>
                                            <option value="instagram">instagram</option>
                                        </select>
                                        <label class="form-label mt-1">Tanggal Mulai</label>
                                        <input type="datetime-local" placeholder="" class="form-control" name="tanggal_mulai" required>
                                        <label class="form-label mt-1">Tanggal Deadline</label>
                                        <input type="datetime-local" placeholder="" class="form-control" name="tanggal_selesai" required>
                                        <label class="form-label mt-1">File Reference</label>
                                        <input type="file" class="form-control" name="file[]" multiple>
                                        <label class="form-label mt-1">Man In Charge</label>
                                        <select class="form-control" name="manincharge" required>
                                            <option></option>
                                              @foreach($manincharge as $mic )
                                            <option value="{{$mic->id}}">{{$mic->name}}</option>
                                              @endforeach
                                        </select>
                                        <label class="form-label mt-1">Content Purpose</label>
                                        <input type="text" placeholder="" class="form-control" name="tujuan_konten" required>
                                        <label class="form-label mt-1">Detail Task</label>
                                        <textarea placeholder="" class="form-control" rows="5" name="detail" required></textarea>
                                        <input type="text" value="New Task" class="form-control" hidden name="status" required>
                                        <input type="text" value="refrensi" class="form-control" hidden name="tag" required>
                                        <input type="text" value="{{ Auth::user()->id }}" class="form-control" hidden name="user" required>
                                    </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                  </div>
                                </form>
                        </div>
                        </div>
                      </div>
        <!-- end Right Modal Start -->

        This page took {{ (microtime(true) - LARAVEL_START) }} seconds to render



@endsection