@extends('layouts.app')
<?php $page = "todolist" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">





              <!-- Top Buttons Start -->
              <div class="col-12 col-md-5 d-flex align-items-start justify-content-end">
              </div>
              <!-- Top Buttons End -->
            </div>
          </div>
          <!-- Title and Top Buttons End -->
          @php
            if(Auth::user()->role == 1){
              $selectedbtn = "block";
              $namepanel = "none";
            }else{
              $selectedbtn = "none";
              $namepanel = "block";
            };
            if($getfilter == null){
              $dap = "/todolist";
            }else{
              $dap = $getfilter;
            };
          @endphp
          <div class="row">
            <div class="col-12 col-lg-4">
              <!-- selected -->
              <div class="d-flex">
                <div class="group row col-md-12">
                  <div style="display:{{$selectedbtn}}" class="col-6">
                      <form action="{{$dap}}" method="get">
                          <select class="col-md-12 form-control mb-1" name="id" onchange="this.form.submit()">
                              <option>{{$selectedManager->name}}</option>
                              @foreach($managerTeam as $manager)
                              <option value="{{$manager->id}}">{{$manager->name}}</option>
                              @endforeach
                          </select>
                      </form>
                    </div>
                  <div class="col-md-6 mb-1">
                        <a class="btn btn-outline-primary btn-icon" data-bs-toggle="modal" data-bs-target="#buattask">
                            <i  data-cs-icon="check"></i>  Task Baru
                        </a>
                  </div>
                </div>
              </div>
              <!-- end selected -->
              <!-- Stats Start -->
              <div class="d-flex">
                <h2 style="display:{{$namepanel}}"  class="small-title">Stats</h2>
              </div>
              <!-- end stats -->
              @php
              $active1 = "";
              $active2 = "";
              $active3 = "";
              $active4 = "";
              $active5 = "";
              $active6 = "";
              $active7 = "";
              $active8 = "";
              $active9 = "";
                if($getfilter == null){
                  $active1 = "border-primary";
                }if($getfilter == "hariini"){
                  $active2 ="border-primary";
                }if($getfilter == "selesai"){
                  $active3 ="border-primary";
                }if($getfilter == "pending"){
                  $active4 ="border-primary";
                }if($getfilter == "deadline"){
                  $active5 ="border-primary";
                }if($getfilter == "review"){
                  $active6 ="border-primary";
                }if($getfilter == "reviewed"){
                  $active7 ="border-primary";
                }if($getfilter == "cancel"){
                  $active8 ="border-primary";
                }if($getfilter == "selanjutnya"){
                  $active9 ="border-primary";
                }
              @endphp
              
              @php
                if($activeuser == null){
                    $linksec = "";
                }else{
                    $inksec = $activeuser;
                };
              @endphp

                <div class="">
                    <div class="row">
                        <a  href="/todolist?id={{$inksec}}" class="col-lg-6">
                            <div>
                              <div class="card {{$active1}}">
                                <div class="card-body d-flex justify-content-between align-items-center">
                                  <div class="card-title mb-0">
                                    <h5 class="mb-0">{{$taskonprogresscount}} Task</h5>
                                    <small class="text-dark">On Progress</small>
                                  </div>
                                    <div class="avatar bg-light-success p-50">
                                        <div class="avatar-content" data-bs-toggle="tooltip" title="Merah Lebih Rendah Dari Kemarin, Hijau Lebih Tinggai Dari Kemarin *blm di buat" data-bs-animation="false">
                                            <i data-feather="compass" class="font-medium-5" ></i>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </a>

                        <a  href="/todolist/hariini?id={{$inksec}}" class="col-lg-6">
                            <div>
                              <div class="card {{$active2}}">
                                <div class="card-body d-flex justify-content-between align-items-center">
                                  <div class="card-title mb-0">
                                    <h5 class="mb-0">{{$taskHariIniCount}} Task</h5>
                                    <small class="text-dark">Today</small>
                                  </div>
                                    <div class="avatar bg-light-success p-50">
                                        <div class="avatar-content" data-bs-toggle="tooltip" title="Merah Lebih Rendah Dari Kemarin, Hijau Lebih Tinggai Dari Kemarin *blm di buat" data-bs-animation="false">
                                            <i data-feather="sunrise" class="font-medium-5" ></i>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </a>
                        
                        <a  href="/todolist/selanjutnya?id={{$inksec}}" class="col-lg-6">
                            <div>
                              <div class="card {{$active9}}">
                                <div class="card-body d-flex justify-content-between align-items-center">
                                  <div class="card-title mb-0">
                                    <h5 class="mb-0">{{$taskNextcount}} Task</h5>
                                    <small class="text-dark">Next Task</small>
                                  </div>
                                    <div class="avatar bg-light-success p-50">
                                        <div class="avatar-content" data-bs-toggle="tooltip" title="Merah Lebih Rendah Dari Kemarin, Hijau Lebih Tinggai Dari Kemarin *blm di buat" data-bs-animation="false">
                                            <i data-feather="wind" class="font-medium-5" ></i>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </a>
                        
                        <a  href="/todolist/selesai?id={{$inksec}}" class="col-lg-6">
                            <div>
                              <div class="card {{$active3}}">
                                <div class="card-body d-flex justify-content-between align-items-center">
                                  <div class="card-title mb-0">
                                    <h5 class="mb-0">{{$selesaiCount}} Task</h5>
                                    <small class="text-dark">Done</small>
                                  </div>
                                    <div class="avatar bg-light-success p-50">
                                        <div class="avatar-content" data-bs-toggle="tooltip" title="Merah Lebih Rendah Dari Kemarin, Hijau Lebih Tinggai Dari Kemarin *blm di buat" data-bs-animation="false">
                                            <i data-feather="check-circle" class="font-medium-5" ></i>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </a>
                        
                        <a  href="/todolist/pending?id={{$inksec}}" class="col-lg-6">
                            <div>
                              <div class="card {{$active4}}">
                                <div class="card-body d-flex justify-content-between align-items-center">
                                  <div class="card-title mb-0">
                                    <h5 class="mb-0">{{$pendingCount}} Task</h5>
                                    <small class="text-dark">Pending</small>
                                  </div>
                                    <div class="avatar bg-light-success p-50">
                                        <div class="avatar-content" data-bs-toggle="tooltip" title="Merah Lebih Rendah Dari Kemarin, Hijau Lebih Tinggai Dari Kemarin *blm di buat" data-bs-animation="false">
                                            <i data-feather="clock" class="font-medium-5" ></i>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </a>
                        
                        <a  href="/todolist/deadline?id={{$inksec}}" class="col-lg-6">
                            <div>
                              <div class="card {{$active5}}">
                                <div class="card-body d-flex justify-content-between align-items-center">
                                  <div class="card-title mb-0">
                                    <h5 class="mb-0">{{$taskDeadlineCount}} Task</h5>
                                    <small class="text-dark">Overdue</small>
                                  </div>
                                    <div class="avatar bg-light-dark p-50">
                                        <div class="avatar-content" data-bs-toggle="tooltip" title="Merah Lebih Rendah Dari Kemarin, Hijau Lebih Tinggai Dari Kemarin *blm di buat" data-bs-animation="false">
                                            <i data-feather="sunset" class="font-medium-5" ></i>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </a>

                      @php
                        if(Auth::user()->manager == 1){
                          $rev1 = "block";
                          $rev2 = "none";
                        }else{
                          $rev1 = "none";
                          $rev2 = "block";
                        }
                      @endphp
                      <a style="display:{{$rev1}}" href="/todolist/review?id={{$inksec}}" class="col-lg-6">
                            <div>
                              <div class="card {{$active6}}">
                                <div class="card-body d-flex justify-content-between align-items-center">
                                  <div class="card-title mb-0">
                                    <h5 class="mb-0">{{$Reviewcount}} Task</h5>
                                    <small class="text-dark">Need Review</small>
                                  </div>
                                    <div class="avatar bg-light-warning p-50">
                                        <div class="avatar-content" data-bs-toggle="tooltip" title="Merah Lebih Rendah Dari Kemarin, Hijau Lebih Tinggai Dari Kemarin *blm di buat" data-bs-animation="false">
                                            <i data-feather="alert-octagon" class="font-medium-5" ></i>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </a>

                      <a style="display:{{$rev2}}" href="/todolist/reviewed?id={{$inksec}}" class="col-lg-6">
                            <div>
                              <div class="card {{$active7}}">
                                <div class="card-body d-flex justify-content-between align-items-center">
                                  <div class="card-title mb-0">
                                    <h5 class="mb-0">{{$ReviewEdcount}} Task</h5>
                                    <small class="text-dark">Need Revision</small>
                                  </div>
                                    <div class="avatar bg-light-warning p-50">
                                        <div class="avatar-content" data-bs-toggle="tooltip" title="Merah Lebih Rendah Dari Kemarin, Hijau Lebih Tinggai Dari Kemarin *blm di buat" data-bs-animation="false">
                                            <i data-feather="alert-circle" class="font-medium-5" ></i>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </a>

                      <a href="/todolist/cancel?id={{$inksec}}" class="col-lg-6">
                            <div>
                              <div class="card {{$active8}}">
                                <div class="card-body d-flex justify-content-between align-items-center">
                                  <div class="card-title mb-0">
                                    <h5 class="mb-0">{{$cancelcount}} Task</h5>
                                    <small class="text-dark">Cancel</small>
                                  </div>
                                    <div class="avatar bg-light-danger p-50">
                                        <div class="avatar-content" data-bs-toggle="tooltip" title="Merah Lebih Rendah Dari Kemarin, Hijau Lebih Tinggai Dari Kemarin *blm di buat" data-bs-animation="false">
                                            <i data-feather="alert-triangle" class="font-medium-5" ></i>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </a>
                    </div>
                </div>
              <!-- Stats End -->

              <!-- Sales Start -->
              <h2 class="small-title">Your Performance</h2>
              

                        <!-- Revenue Report Card -->
                        <div class="col-lg-12 col-12">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                                </div>
                                <div class="card-body">
                                    <canvas id="bar-chart-extodo" data-height="300"></canvas>
                                </div>
                            </div>
                        </div>
                        <!--/ Revenue Report Card -->

              <!-- Sales End -->
            </div>

            <!-- task list Start -->
            <div class="col-12 col-lg-8 mb-5">
              <div class="d-flex justify-content-between">
                <h2 class="small-title">Your Task List</h2>
              </div>
              <!-- Basic Bootstrap Table -->
              <div class="card mt-1">
                <div class="table-responsive text-nowrap">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Task Name</th>
                        <th>Start</th>
                        <th>Deadline</th>
                        <th>Status</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody class="table-border-bottom-0">
                    @foreach($dataTask as $dts)
                        @php 
                      if($dts->tanggal_selesai <= $kemarin){
                        $bgcard = "bg-dark";
                        $text = "text-white";
                        $bgicon = "bg-warning";
                        $btnHariIni = "";
                        $icon = "alarm";
                        $btn = "btn-danger";
                      }else{
                        $bgcard = "";
                        $text = "text-dark";
                        $bgicon = "bg-primary";
                        $btnHariIni = "";
                        $icon = "dashboard-1";
                        $btn = "btn-outline-primary";
                      };

                      if($dts->status == "cancel"){
                        $bgcard1 = "bg-dark";
                        $text = "text-white";
                        $bgicon = "bg-danger";
                        $icon = "activity";
                        $button3 = "disabled";
                        $btnHariIni = "";
                        $button4 = "#";
                      }else{
                        $bgcard1 = "";
                        $button3 = "";
                        $btnHariIni = "";
                        $button4 = "/detailtodo/$dts->id_task";
                      };
                      if($getfilter == "hariini"){
                        $btnHariIni = "disabled";
                      }else{
                        $btnHariIni = "";
                      };
                      if($getfilter == "selesai"){
                        $btnHariIni = "disabled";
                      }else{
                        $btnHariIni = "";
                      };
                      @endphp
                      <tr class="{{$bgcard}} {{$bgcard1}} text-info">
                        <td><a class="text-info" href="{{$button4}}">{{$dts->judul}}</a></td>
                        <td>{{$dts->tanggal_mulai}}</td>
                        <td>{{$dts->tanggal_selesai}}</td>
                        <td>{{$dts->status}}</td>
                        <td>
                              <button 
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                  data-submenu
                                   class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1" type="button">
                              <i  data-feather="chevron-down"></i>
                              </button>
                              <div class="dropdown-menu dropdown-menu-end">
                                <a class="dropdown-item" href="">Make Today Task</a>
                                <a class="dropdown-item" href="{{$button4}}">Detail Task</a>
                              </div>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              <!--/ Basic Bootstrap Table -->



            @php
                if(Auth::user()->manager == 0){
                    $style1 = "none";
                }else{
                    $style1 = "block";
                }
            @endphp
            <!-- task list Start -->
            <div class="col-12 col-lg-12 mt-5">
              <div class="d-flex justify-content-between">
                <h2 class="small-title">Your Team Task List</h2>
              </div>
              <!-- Basic Bootstrap Table -->
              <div class="card ">
                <div class="table-responsive text-nowrap">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Task Name</th>
                        <th>Start</th>
                        <th>Deadline</th>
                        <th>Status</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody class="table-border-bottom-0">
                  @foreach($teamDataTask as $tdts)
                  @php 
                  if($tdts->tanggal_selesai <= $kemarin){
                    $bgcard = "bg-dark";
                    $text = "text-white";
                    $bgicon = "bg-warning";
                    $icon = "alarm";
                    $btn = "btn-danger";
                  }else{
                    $bgcard = "";
                    $text = "text-dark";
                    $bgicon = "bg-primary";
                    $icon = "dashboard-1";
                    $btn = "btn-outline-primary";
                  };

                  if($tdts->status == "cancel"){
                    $bgcard1 = "bg-dark";
                    $text = "text-white";
                    $bgicon = "bg-danger";
                    $icon = "activity";
                    $button3 = "disabled";
                    $button4 = "#";
                  }else{
                    $bgcard1 = "";
                    $button3 = "";
                    $button4 = "/detailtodo/$tdts->id_task";
                  };
                  if($getfilter == "hariini"){
                    $btnHariIni = "disabled";
                  }else{
                    $btnHariIni = "";
                  };
                  if($getfilter == "selesai"){
                    $btnHariIni = "disabled";
                  }else{
                    $btnHariIni = "";
                  };
                  @endphp
                    @php
                    if($tdts->status == "cancel"){
                          $button5 = "#";
                        }else{
                          $button5 = "/detailtodo/$tdts->id_task";
                        };
                    @endphp
                      <tr class="{{$bgcard}} {{$bgcard1}} text-info">
                        <td><a class="text-info" href="{{$button4}}">{{$tdts->judul}}</a></td>
                        <td>{{$tdts->tanggal_mulai}}</td>
                        <td>{{$tdts->tanggal_selesai}}</td>
                        <td>{{$tdts->status}}</td>
                        <td>
                              <button 
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                  data-submenu
                                   class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1" type="button">
                              <i  data-feather="chevron-down"></i>
                              </button>
                              <div class="dropdown-menu dropdown-menu-end">
                                <a class="dropdown-item" href="">Make Today Task</a>
                                <a class="dropdown-item" href="{{$button4}}">Detail Task</a>
                              </div>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              <!--/ Basic Bootstrap Table -->
                </div>
              </div>

      
        <!-- Right Modal detail team -->
        <div class="modal modal-right large fade" id="buattask" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">FORMULIR TASK BARU</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                <form method="post" action="/taskdapbaru"  enctype="multipart/form-data">
                              </div>
                                  <div class="modal-body">
                                      {{csrf_field()}}
                                      <label class="form-label">Task</label>
                                        <input type="text" placeholder="" class="form-control" name="task" required>
                                      <label class="form-label mt-1">Tanggal Mulai</label>
                                        <input type="datetime-local" placeholder="" class="form-control" name="tanggal_mulai" required>
                                      <label class="form-label mt-1">Tanggal Deadline</label>
                                        <input type="datetime-local" placeholder="" class="form-control" name="tanggal_selesai" required>
                                      <label class="form-label mt-1">Man In Charge</label>
                                      <select class="form-control" name="manincharge" required>
                                          <option></option>
                                            @foreach($manincharge as $mic )
                                          <option value="{{$mic->id}}">{{$mic->name}}</option>
                                            @endforeach
                                      </select>
                                      <label class="form-label mt-1">Detail Task</label>
                                      <textarea placeholder="" class="form-control" rows="10" name="detail" required></textarea>
                                      <input type="text" value="New Task" placeholder="" class="form-control" name="status" hidden required>
                                      <input type="text" value="{{ Auth::user()->id }}" placeholder="" class="form-control" name="user" hidden required>
                                  </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                                </form>
                      </div>
                        </div>
                      </div>
        <!-- end Right Modal Start -->
        
    <script>
      $(window).on('load', function () {
  'use strict';

  var chartWrapper = $('.chartjs'),
    flatPicker = $('.flat-picker'),
    barChartEx = $('#bar-chart-extodo'),
    lineAreaChartEx = $('.line-area-chart-ex');

  // Color Variables
  var primaryColorShade = '#836AF9',
    yellowColor = '#ffe800',
    successColorShade = '#28dac6',
    warningColorShade = '#ffe802',
    warningLightColor = '#FDAC34',
    infoColorShade = '#299AFF',
    greyColor = '#4F5D70',
    blueColor = '#2c9aff',
    blueLightColor = '#84D0FF',
    greyLightColor = '#EDF1F4',
    tooltipShadow = 'rgba(0, 0, 0, 0.25)',
    lineChartPrimary = '#666ee8',
    lineChartDanger = '#ff4961',
    labelColor = '#6e6b7b',
    grid_line_color = 'rgba(200, 200, 200, 0.2)'; // RGBA color helps in dark layout

  

  // Detect Dark Layout
  if ($('html').hasClass('dark-layout')) {
    labelColor = '#b4b7bd';
  }

  // Wrap charts with div of height according to their data-height
  if (chartWrapper.length) {
    chartWrapper.each(function () {
      $(this).wrap($('<div style="height:' + this.getAttribute('data-height') + 'px"></div>'));
    });
  }

  // Init flatpicker
  if (flatPicker.length) {
    var date = new Date();
    flatPicker.each(function () {
      $(this).flatpickr({
        mode: 'range',
        defaultDate: ['2019-05-01', '2019-05-10']
      });
    });
  }

  // Bar Chart
  // --------------------------------------------------------------------
  if (barChartEx.length) {
    var barChartExample = new Chart(barChartEx, {
      type: 'bar',
      options: {
        elements: {
          rectangle: {
            borderWidth: 2,
            borderSkipped: 'bottom'
          }
        },
        responsive: true,
        maintainAspectRatio: false,
        responsiveAnimationDuration: 100,
        legend: {
          display: false
        },
        tooltips: {
          // Updated default tooltip UI
          shadowOffsetX: 1,
          shadowOffsetY: 1,
          shadowBlur: 8,
          shadowColor: tooltipShadow,
          backgroundColor: window.colors.solid.white,
          titleFontColor: window.colors.solid.black,
          bodyFontColor: window.colors.solid.black
        },
        scales: {
          xAxes: [
            {
              display: true,
              gridLines: {
                display: true,
                color: grid_line_color,
                zeroLineColor: grid_line_color
              },
              scaleLabel: {
                display: false
              },
              ticks: {
                fontColor: labelColor
              }
            }
          ],
          yAxes: [
            {
              display: true,
              gridLines: {
                color: grid_line_color,
                zeroLineColor: grid_line_color
              },
              ticks: {
                stepSize: 5,
                fontColor: labelColor
              }
            }
          ]
        }
      },
      data: {
        labels: ['{{$sixDl}}', '{{$fiveDl}}', '{{$fourDl}}', '{{$treeDl}}', '{{$twoDl}}', '{{$kemarin}}', '{{$hariIni}}'],
        datasets: [
          {
            data: [{{$task7count}}, {{$task6count}}, {{$task5count}}, {{$task4count}}, {{$task3count}}, {{$task2count}}, {{$task1count}}],
            barThickness: 15,
            backgroundColor: primaryColorShade,
            borderColor: 'transparent'
          }
        ]
      }
    });
  }

  //Draw rectangle Bar charts with rounded border
  Chart.elements.Rectangle.prototype.draw = function () {
    var ctx = this._chart.ctx;
    var viewVar = this._view;
    var left, right, top, bottom, signX, signY, borderSkipped, radius;
    var borderWidth = viewVar.borderWidth;
    var cornerRadius = 20;
    if (!viewVar.horizontal) {
      left = viewVar.x - viewVar.width / 2;
      right = viewVar.x + viewVar.width / 2;
      top = viewVar.y;
      bottom = viewVar.base;
      signX = 1;
      signY = top > bottom ? 1 : -1;
      borderSkipped = viewVar.borderSkipped || 'bottom';
    } else {
      left = viewVar.base;
      right = viewVar.x;
      top = viewVar.y - viewVar.height / 2;
      bottom = viewVar.y + viewVar.height / 2;
      signX = right > left ? 1 : -1;
      signY = 1;
      borderSkipped = viewVar.borderSkipped || 'left';
    }

    if (borderWidth) {
      var barSize = Math.min(Math.abs(left - right), Math.abs(top - bottom));
      borderWidth = borderWidth > barSize ? barSize : borderWidth;
      var halfStroke = borderWidth / 2;
      var borderLeft = left + (borderSkipped !== 'left' ? halfStroke * signX : 0);
      var borderRight = right + (borderSkipped !== 'right' ? -halfStroke * signX : 0);
      var borderTop = top + (borderSkipped !== 'top' ? halfStroke * signY : 0);
      var borderBottom = bottom + (borderSkipped !== 'bottom' ? -halfStroke * signY : 0);
      if (borderLeft !== borderRight) {
        top = borderTop;
        bottom = borderBottom;
      }
      if (borderTop !== borderBottom) {
        left = borderLeft;
        right = borderRight;
      }
    }

    ctx.beginPath();
    ctx.fillStyle = viewVar.backgroundColor;
    ctx.strokeStyle = viewVar.borderColor;
    ctx.lineWidth = borderWidth;
    var corners = [
      [left, bottom],
      [left, top],
      [right, top],
      [right, bottom]
    ];

    var borders = ['bottom', 'left', 'top', 'right'];
    var startCorner = borders.indexOf(borderSkipped, 0);
    if (startCorner === -1) {
      startCorner = 0;
    }

    function cornerAt(index) {
      return corners[(startCorner + index) % 4];
    }

    var corner = cornerAt(0);
    ctx.moveTo(corner[0], corner[1]);

    for (var i = 1; i < 4; i++) {
      corner = cornerAt(i);
      var nextCornerId = i + 1;
      if (nextCornerId == 4) {
        nextCornerId = 0;
      }

      var nextCorner = cornerAt(nextCornerId);

      var width = corners[2][0] - corners[1][0],
        height = corners[0][1] - corners[1][1],
        x = corners[1][0],
        y = corners[1][1];

      var radius = cornerRadius;

      if (radius > height / 2) {
        radius = height / 2;
      }
      if (radius > width / 2) {
        radius = width / 2;
      }

      if (!viewVar.horizontal) {
        ctx.moveTo(x + radius, y);
        ctx.lineTo(x + width - radius, y);
        ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
        ctx.lineTo(x + width, y + height - radius);
        ctx.quadraticCurveTo(x + width, y + height, x + width, y + height);
        ctx.lineTo(x + radius, y + height);
        ctx.quadraticCurveTo(x, y + height, x, y + height);
        ctx.lineTo(x, y + radius);
        ctx.quadraticCurveTo(x, y, x + radius, y);
      } else {
        ctx.moveTo(x + radius, y);
        ctx.lineTo(x + width - radius, y);
        ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
        ctx.lineTo(x + width, y + height - radius);
        ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
        ctx.lineTo(x + radius, y + height);
        ctx.quadraticCurveTo(x, y + height, x, y + height);
        ctx.lineTo(x, y + radius);
        ctx.quadraticCurveTo(x, y, x, y);
      }
    }

    ctx.fill();
    if (borderWidth) {
      ctx.stroke();
    }
  };

  
  // Line AreaChart
  // --------------------------------------------------------------------
  if (lineAreaChartEx.length) {
    new Chart(lineAreaChartEx, {
      type: 'line',
      plugins: [
        // to add spacing between legends and chart
        {
          beforeInit: function (chart) {
            chart.legend.afterFit = function () {
              this.height += 20;
            };
          }
        }
      ],
      options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          position: 'top',
          align: 'start',
          labels: {
            usePointStyle: true,
            padding: 25,
            boxWidth: 9
          }
        },
        layout: {
          padding: {
            top: -20,
            bottom: -20,
            left: -20
          }
        },
        tooltips: {
          // Updated default tooltip UI
          shadowOffsetX: 1,
          shadowOffsetY: 1,
          shadowBlur: 8,
          shadowColor: tooltipShadow,
          backgroundColor: window.colors.solid.white,
          titleFontColor: window.colors.solid.black,
          bodyFontColor: window.colors.solid.black
        },
        scales: {
          xAxes: [
            {
              display: true,
              gridLines: {
                color: 'transparent',
                zeroLineColor: grid_line_color
              },
              scaleLabel: {
                display: true
              },
              ticks: {
                fontColor: labelColor
              }
            }
          ],
          yAxes: [
            {
              display: true,
              gridLines: {
                color: 'transparent',
                zeroLineColor: grid_line_color
              },
              ticks: {
                stepSize: 100,
                min: 0,
                max: 400,
                fontColor: labelColor
              },
              scaleLabel: {
                display: true
              }
            }
          ]
        }
      },
      data: {
        labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
        datasets: [
          {
            label: 'Africa',
            data: [40, 55, 45, 75, 65, 55, 70, 60, 100, 98, 90, 120, 125, 140, 155],
            lineTension: 0,
            backgroundColor: blueColor,
            pointStyle: 'circle',
            borderColor: 'transparent',
            pointRadius: 0.5,
            pointHoverRadius: 5,
            pointHoverBorderWidth: 5,
            pointBorderColor: 'transparent',
            pointHoverBackgroundColor: blueColor,
            pointHoverBorderColor: window.colors.solid.white
          },
          {
            label: 'Asia',
            data: [70, 85, 75, 150, 100, 140, 110, 105, 160, 150, 125, 190, 200, 240, 275],
            lineTension: 0,
            backgroundColor: blueLightColor,
            pointStyle: 'circle',
            borderColor: 'transparent',
            pointRadius: 0.5,
            pointHoverRadius: 5,
            pointHoverBorderWidth: 5,
            pointBorderColor: 'transparent',
            pointHoverBackgroundColor: blueLightColor,
            pointHoverBorderColor: window.colors.solid.white
          },
          {
            label: 'Europe',
            data: [240, 195, 160, 215, 185, 215, 185, 200, 250, 210, 195, 250, 235, 300, 315],
            lineTension: 0,
            backgroundColor: greyLightColor,
            pointStyle: 'circle',
            borderColor: 'transparent',
            pointRadius: 0.5,
            pointHoverRadius: 5,
            pointHoverBorderWidth: 5,
            pointBorderColor: 'transparent',
            pointHoverBackgroundColor: greyLightColor,
            pointHoverBorderColor: window.colors.solid.white
          }
        ]
      }
    });
  }
});
</script>
@endsection