@extends('layouts.app')
<?php $page = "todolist" ?>
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">





@foreach($detailTask as $detail)
              @php
                if($detail->tanggal_dimulai == null){
                  $tombolSelesai = "none";
                }else{
                  $tombolSelesai = "block";
                };
                if($detail->status == "New Task"){
                  $tombolKerjakan = "block";
                  $tombolReview = "none";
                  $tombolLanjutkan = "none";
                  $tombolRevisi = "none";
                  $tombol3titik = "blocl";
                }
                if($detail->status == "on progress"){
                  $tombolKerjakan = "none";
                  $tombolReview = "block";
                  $tombolLanjutkan = "none";
                  $tombolRevisi = "none";
                  $tombol3titik = "blocl";
                };
                if($detail->status == "review"){
                  $tombolKerjakan = "none";
                  $tombolReview = "none";
                  $tombolLanjutkan = "none";
                  $tombolRevisi = "block";
                  $tombol3titik = "blocl";
                };
                if($detail->status == "reviewed"){
                  $tombolKerjakan = "none";
                  $tombolReview = "block";
                  $tombolLanjutkan = "none";
                  $tombolRevisi = "none";
                  $tombol3titik = "blocl";
                };
                if($detail->status == "pending"){
                  $tombolKerjakan = "none";
                  $tombolReview = "none";
                  $tombolLanjutkan = "block";
                  $tombolRevisi = "none";
                  $tombol3titik = "blocl";
                };
                if($detail->status == "Selesai"){
                  $tombolKerjakan = "none";
                  $tombolReview = "none";
                  $tombolLanjutkan = "none";
                  $tombolRevisi = "none";
                  $tombol3titik = "none";
                };
                if($detail->status == "cancel"){
                  $tombolKerjakan = "none";
                  $tombolReview = "none";
                  $tombolLanjutkan = "none";
                  $tombolRevisi = "none";
                  $tombol3titik = "none";
                };
                if(Auth::user()->manager == '1'){
                  $tombolReview = "none";
                }else{
                  $tombolRevisi = "none";
                };
                if($valTask->konten_link == ''){
                  $btnUpload = "";
                }else{
                  $btnUpload = "hidden";
                }
              @endphp

              <!-- Top Buttons Start -->
              <div class="col-12 col-md-5 d-flex align-items-start justify-content-end">
                <!-- Contact Button Start -->
                <button style="display:{{$tombolKerjakan}}" class="btn btn-outline-primary btn-icon kerjakan_task" href="/mulaiTask/{{$detail->id_task}}">
                  <i  data-cs-icon="check"></i> Kerjakan Task
                </button>
                <div class="ms-1">
                <button style="display:{{$tombolReview}}" data-bs-toggle="modal" data-bs-target="#modalReview"  class="btn btn-outline-info btn-icon" >
                  <i  data-cs-icon="check"></i> Ajukan Review
                </button>
              </div>
                <div class="ms-1">
                <button style="display:{{$tombolLanjutkan}}"  class="btn btn-outline-primary btn-icon lanjutkan_task" href="/lanjutkanTask/{{$detail->id_task}}">
                  <i  data-cs-icon="check"></i> Lanjutkan Task
                </button>
              </div>
                <div class="ms-1">
                <button style="display:{{$tombolRevisi}}"  data-bs-toggle="modal" data-bs-target="#modalRevisi"  class="btn btn-outline-primary btn-icon" href="/revisiTask/{{$detail->id_task}}">
                  <i  data-cs-icon="check"></i> Revisi Task
                </button>
              </div>
                <div class="ms-1">
                <button {{$btnUpload}} style="display:{{$btnSocialUpload}}"  data-bs-toggle="modal" data-bs-target="#modalUploadTask"  class="btn btn-outline-primary btn-icon" href="/revisiTask/{{$detail->id_task}}">
                  <i  data-cs-icon="check"></i> Upload Task
                </button>
              </div>
                <!-- Contact Button End -->

                <!-- Dropdown Button Start -->
                <div class="ms-1">
                  <button style="display:{{$tombol3titik}}"
                    type="button"
                    class="btn btn-outline-primary btn-icon btn-icon-only"
                    data-bs-offset="0,3"
                    data-bs-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    data-submenu
                  >
                    <i data-cs-icon="more-horizontal"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-end">
                    <a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#modalUpdateMic"> Update MIC</a>
                    <a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#modalupdatebrief"> Update Detail Task</a>
                    <a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#modalrefrensi"> Update Foto Task</a>
                    <a style="display:{{$tombolSelesai}}" class="dropdown-item selesaikan_task" href="/taskSelesai/{{$detail->id_task}}">Selesaikan Task</a>
                    <a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#modalpending">Pending Task</a>
                    <a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#modalcancel">Cancel Task</a>
                    <a class="dropdown-item" href="/downloadtask/{{$detail->id_task}}"> Download</a>
                  </div>
                </div>
                <!-- Dropdown Button End -->
              </div>
              <!-- Top Buttons End -->
            </div>
          </div>

      
        <!-- Right Modal detail team -->
        <div class="modal modal-right large fade" id="modalupdatebrief" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">FORMULIR UPDATE DETAIL TASK</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              <form method="get" action="/updatedetaildap/{{$detail->id_task}}">
                            </div>
                            <div class="modal-body">
                                {{csrf_field()}}
                                <label class="form-label">Brieff Client</label>
                                <textarea placeholder="" class="form-control bg-dark" rows="25" name="detail">{!! $detail->detail !!}</textarea>
                            </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                              </div>
                          </div>
                              </form>
                        </div>
                      </div>
        <!-- end Right Modal Start -->
        <!-- Right Modal detail team -->
        <div class="modal modal-right large fade" id="modalUpdateMic" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">FORMULIR UPDATE MAN IN CHARGE</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              <form method="get" action="/updatemic/{{$detail->id_task}}">
                            </div>
                            <div class="modal-body">
                                {{csrf_field()}}
                                <label class="form-label">Brieff Client</label>
                                    <select name="mic" class="form-control">
                                        <option></option>
                                @foreach($selecMic as $selectmic)
                                        <option value="{{$selectmic->id}}">{{$selectmic->name}}</option>
                                @endforeach
                                    </select>
                            </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                              </div>
                          </div>
                              </form>
                        </div>
                      </div>
        <!-- end Right Modal Start -->
        <!-- Right Modal detail team -->
        <div class="modal modal-right large fade" id="modalRevisi" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">FORMULIR DETAIL REVISI TASK</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              <form method="get" action="/revisiTask/{{$detail->id_task}}">
                            </div>
                            <div class="modal-body">
                                {{csrf_field()}}
                                <label class="form-label">Brieff Client</label>
                                <textarea placeholder="" class="form-control bg-dark" rows="25" name="alasan_revisi"></textarea>
                            </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                              </div>
                          </div>
                              </form>
                        </div>
                      </div>
        <!-- end Right Modal Start -->
        <!-- Right Modal detail team -->
        <div class="modal modal-right large fade" id="modalUploadTask" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">FORMULIR UPLOAD TASK KE SOSMED</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              <form method="get" action="/uploadTask/{{$detail->id_task}}">
                            </div>
                            <div class="modal-body">
                                {{csrf_field()}}
                                <label class="form-label">Link Hasil Upload Social Media</label>
                                <textarea placeholder="" class="form-control bg-dark" rows="25" name="link_socialmedia"></textarea>
                            </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                              </div>
                          </div>
                              </form>
                        </div>
                      </div>
        <!-- end Right Modal Start -->
        <!-- Right Modal detail team -->
        <div class="modal modal-right large fade" id="modalChangeNameTime" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">FORMULIR DETAIL REVISI TASK</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              <form method="get" action="/changeNameTime/{{$detail->id_task}}">
                            </div>
                            <div class="modal-body">
                                {{csrf_field()}}
                                <label class="form-label">Judul</label>
                                <input placeholder="" class="form-control bg-dark mb-3" name="ganti_nama" value="{{$detail->judul}}"></input>
                                <label class="form-label mt-3">Tanggal Mulai</label>
                                        <input type="date" placeholder="" class="form-control" name="tanggal_mulai" required>
                                      <label class="form-label mt-3">Tanggal Deadline</label>
                                        <input type="date" placeholder="" class="form-control" name="tanggal_selesai" required>
                            </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                              </div>
                          </div>
                              </form>
                        </div>
                      </div>
        <!-- end Right Modal Start -->
        <!-- Right Modal detail team -->
        <div class="modal modal-right large fade" id="modalReview" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">FORMULIR REQUEST REVIEW</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                              <form method="post" action="/reviewTask/{{$id}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                              <label class="form-label">Upload File</label>
                              <input type="file" class="form-control" name="file[]" multiple required>
                            </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                              </div>
                          </div>
                              </form>
                        </div>
                      </div>
        <!-- end Right Modal Start -->
        <!-- Right Modal detail team -->
        <div class="modal modal-right large fade" id="modalpending" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">FORMULIR UPDATE TASK</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              <form method="get" action="/taskpending/{{$detail->id_task}}">
                            </div>
                            <div class="modal-body">
                                {{csrf_field()}}
                                <label class="form-label">Alasan Mem Panding Task Ini</label>
                                <textarea placeholder="" class="form-control bg-dark" rows="25" name="alasan_pending"></textarea>
                            </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                              </div>
                          </div>
                              </form>
                        </div>
                      </div>
        <!-- end Right Modal Start -->
        <!-- Right Modal detail team -->
        <div class="modal modal-right large fade" id="modalcancel" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">FORMULIR UPDATE TASK</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              <form method="get" action="/taskcancel/{{$detail->id_task}}">
                            </div>
                            <div class="modal-body">
                                {{csrf_field()}}
                                <label class="form-label">Alasan Meng Cancel Task Ini</label>
                                <textarea placeholder="" class="form-control bg-dark" rows="25" name="alasan_cancel"></textarea>
                            </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                              </div>
                          </div>
                              </form>
                        </div>
                      </div>
        <!-- end Right Modal Start -->
        <!-- Right Modal detail team -->
        <div class="modal modal-right large fade" id="modalrefrensi" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">FORMULIR TAMBAH FOTO TASK</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                            <form method="post" action="/tambahrefrensi/{{$id}}"  enctype="multipart/form-data">
                              {{csrf_field()}}
                              <label class="form-label">Upload File</label>
                              <input type="file" class="form-control" name="file[]" multiple required>
                              <!-- Basic Start -->
                                  <div class="card mb-5">
                                    <div class="card-body">
                                      <table class="table">
                                        <thead>
                                          <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">gambar</th>
                                            <th scope="col">Action</th>
                                          </tr>
                                        </thead>
                                                @foreach($img_ref as $index => $refthumb)
                                        <tbody>
                                          <tr>
                                            <th>{{$index+1}}</th>
                                            <td>
                                              <div class="sh-10 me-1 mb-1 d-inline-blockflex-column">
                                                  <img alt="thumb" src="/img/{{$refthumb->file}}" class="img-fluid-height rounded-md" />
                                              </div>
                                            </td>
                                            <td>hapus</td>
                                          </tr>
                                        </tbody>
                                                @endforeach
                                      </table>
                                    </div>
                                  </div>
                                </section>
                                <!-- Basic End -->
                          </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                            </form>
                        </div>
                      </div>
        <!-- end Right Modal Start -->
                      @php
                      if($count_img == '0'){
                        $style1 = "none";
                        $style2 = "block";
                      }else{
                        $style1 = "block";
                        $style2 = "none";
                      }
                      @endphp

          <!-- Title and Top Buttons End -->
            <div class="row">
            <div class="col-12 col-xl-8 col-xxl-9 mb-5">
              <div class="card mb-5">
                <!-- Carousel Start -->
                <div class="card-body p-0">
                  <div class="glide glide-gallery" id="glidePortfolioDetail">
                    <!-- Large Images Start -->
                    <div class="glide glide-large">
                      <div class="glide__track" data-glide-el="track">
                        <ul class="glide__slides gallery-glide-custom">
                          @foreach($img_ref as $imglarge)
                          <li style="display:{{$style1}}" class="glide__slide p-0">
                            <a href="/img/{{$imglarge->file}}">
                              <img
                                alt="detail"
                                src="/img/{{$imglarge->file}}"
                                class="responsive border-0 rounded-top-end rounded-top-start img-fluid mb-3 sh-35 sh-md-60 w-100"
                              />
                            </a>
                          </li>
                          @endforeach
                          <li style="display:{{$style2}}" class="glide__slide p-0">
                            <a href="/img/default_project.jpg">
                              <img
                                alt="detail"
                                src="/img/default_project.jpg"
                                class="responsive border-0 rounded-top-end rounded-top-start img-fluid mb-3 sh-35 sh-md-60 w-100"
                              />
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <!-- Large Images End -->
                    <!-- Thumbs Start -->
                    <div class="glide glide-thumb mb-3">
                      <div class="glide__track" data-glide-el="track">
                        <ul class="glide__slides">
                          @foreach($img_ref as $refthumb)
                          <li style="display:{{$style1}}"  class="glide__slide p-0">
                            <img alt="thumb" src="/img/{{$refthumb->file}}" class="responsive rounded-md img-fluid" />
                          </li>
                          @endforeach
                          <li style="display:{{$style2}}"  class="glide__slide p-0">
                            <img alt="thumb" src="/img/default_project.jpg" class="responsive rounded-md img-fluid" />
                          </li>
                        </ul>
                      </div>
                      <div class="glide__arrows" data-glide-el="controls">
                        <button class="btn btn-icon btn-icon-only btn-foreground hover-outline left-arrow" data-glide-dir="<">
                          <i data-cs-icon="chevron-left"></i>
                        </button>
                        <button class="btn btn-icon btn-icon-only btn-foreground hover-outline right-arrow" data-glide-dir=">">
                          <i data-cs-icon="chevron-right"></i>
                        </button>
                      </div>
                    </div>
                    <!-- Thumbs End -->
                  </div>
                </div>
                <!-- Carousel End -->
              </div>
                <!-- Logs Start -->
                <h2 class="small-title">Logs</h2>
                <div class="card">
              <div class="card">
                <div class="card-body">
                  @foreach($logdata as $log)
                  <div class="d-flex align-items-center border-bottom border-separator-light pb-3 mt-3">
                    <div class="row g-0 w-100">
                      <div class="col-auto">
                        <div class="sw-5 me-3">
                          <img src="/img/{{$log->foto}}" class="img-fluid rounded-sm" alt="thumb" />
                        </div>
                      </div>
                      <div class="col-12 col-xl-9 col-xxl-9">
                        <a href="#">{{$log->name}}</a>
                        <div class="text-muted text-small mb-2">{{$log->nama_log}}</div>
                        <div class="text-medium text-alternate lh-1-25">{{$log->detail_log}} 
                          </br>
                          @php
                            if($log->link_log == null){
                              $btnDonwloadFile = "hidden";
                            }else{
                              $btnDonwloadFile = "";
                            };
                          @endphp
                        <a {{$btnDonwloadFile}} href="/downloadDataReview/{{$log->link_log}}" class="btn btn-sm btn-icon btn-icon-only btn-primary mb-1">
                          <i data-cs-icon="download" class="icon" data-cs-size="18"></i>
                        </a>
                      </div>
                      </div>
                      <div class="col-auto justify-self-end">
                        <div>
                          <span class="text-muted">{{$log->tanggal_log}}{{$log->jam_log}}</span>
                          <i data-cs-icon="{{$log->icon_log}}" class="{{$log->icon_collor}} align-top"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
                </div>
                <!-- Logs End -->

                
            </div>

            <!-- Right Side Start -->
            <div class="col-12 col-xl-4 col-xxl-3">
              <div class="row">
                <!-- Hire Start -->
                <div class="col-12">
              <!-- Biography Start -->
              @php
              if($detail->status == "Selesai"){
                $warnaStatus = "btn-success";
              }else{
                $warnaStatus = "btn-warning";
              }
              @endphp

              <div>
                <div class="card mb-3">
                  <button class="btn {{$warnaStatus}}">
                      <p class="h5 card-text text-white">{{$detail->status}}</p>       
                  </button>
                </div>
                <div class="card mb-5">
                    <div class="card-body row g-0">
                      <div class="col-12">
                        <div class="cta-3">{{$detail->judul}}</div>
                        <div class="mb-3 cta-3 text-primary">{{$detail->jam_mulai}} - {{$detail->jam_selesai}}
                        <i data-bs-toggle="modal" data-bs-target="#modalChangeNameTime" class="mb-1 text-dark" data-cs-icon="edit" data-cs-size="15"></i>
                        </div>

                        <div class="row g-0 sh-6 mb-1">
                          <div class="col-auto">
                            <img src="/img/{{$detail->foto}}" class="card-img rounded-xl sh-6 sw-6" alt="thumb" />
                          </div>
                          <div class="col">
                            <div class="card-body d-flex flex-row pt-0 pb-0 ps-3 pe-0 h-100 align-items-center justify-content-between">
                              <div class="d-flex flex-column">
                                <div>{{$detail->name}}</div>
                                <div class="text-small text-muted">{{$detail->divisi}} Manager</div>
                              </div>
                              <div class="d-flex">
                                <button type="button" class="btn btn-outline-primary btn-sm ms-1">Level 1</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row g-0 sh-6 mb-3">
                          <div class="col-auto">
                            <img src="/img/{{$detailTeam->foto}}" class="card-img rounded-xl sh-6 sw-6" alt="thumb" />
                          </div>
                          <div class="col">
                            <div class="card-body d-flex flex-row pt-0 pb-0 ps-3 pe-0 h-100 align-items-center justify-content-between">
                              <div class="d-flex flex-column">
                                <div>{{$detailTeam->name}}</div>
                                <div class="text-small text-muted">{{$detailTeam->divisi}} In Charge</div>
                              </div>
                              <div class="d-flex">
                                <button type="button" class="btn btn-outline-primary btn-sm ms-1">Level 2</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="mb-5">
                            <p class="text-small text-muted mb-2">DETAIL</p>
                            <textarea class="bg-dark form-control" rows="10">{!! $detail->detail !!}</textarea>
                        </div>
                        <div class="mb-5">
                    <div class="row g-0 align-items-center mb-2">
                      <div class="col-auto">
                        <div class="border border-primary sw-5 sh-5 rounded-xl d-flex justify-content-center align-items-center">
                          <i data-cs-icon="calendar" class="text-primary"></i>
                        </div>
                      </div>
                      <div class="col ps-3">
                        <div class="row g-0">
                          <div class="col">
                            <div class="sh-5 d-flex align-items-center lh-1-25">Task Dimulai</div>
                          </div>
                          <div class="col-auto">
                            <div class="sh-5 d-flex align-items-center">{{$detail->tanggal_mulai}}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row g-0 align-items-center mb-2">
                      <div class="col-auto">
                        <div class="border border-primary sw-5 sh-5 rounded-xl d-flex justify-content-center align-items-center">
                          <i data-cs-icon="calendar" class="text-primary"></i>
                        </div>
                      </div>
                      <div class="col ps-3">
                        <div class="row g-0">
                          <div class="col">
                            <div class="sh-5 d-flex align-items-center lh-1-25">Task Deadline</div>
                          </div>
                          <div class="col-auto">
                            <div class="sh-5 d-flex align-items-center">{{$detail->tanggal_selesai}}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row g-0 align-items-center mb-2">
                      <div class="col-auto">
                        <div class="border border-primary sw-5 sh-5 rounded-xl d-flex justify-content-center align-items-center">
                          <i data-cs-icon="calendar" class="text-primary"></i>
                        </div>
                      </div>
                      <div class="col ps-3">
                        <div class="row g-0">
                          <div class="col">
                            <div class="sh-5 d-flex align-items-center lh-1-25">Task Dikerjakan</div>
                          </div>
                          <div class="col-auto">
                            <div class="sh-5 d-flex align-items-center">{{$detail->tanggal_dimulai}}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row g-0 align-items-center mb-2">
                      <div class="col-auto">
                        <div class="border border-primary sw-5 sh-5 rounded-xl d-flex justify-content-center align-items-center">
                          <i data-cs-icon="calendar" class="text-primary"></i>
                        </div>
                      </div>
                      <div class="col ps-3">
                        <div class="row g-0">
                          <div class="col">
                            <div class="sh-5 d-flex align-items-center lh-1-25">Task Diselesaikan</div>
                          </div>
                          <div class="col-auto">
                            <div class="sh-5 d-flex align-items-center">{{$detail->tanggal_penyelesaian}}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

                @endforeach
                    
                              </tbody>
                            </table>
                          </div>
                        <!-- /.card-body -->
                        </div>
                        </div>
                            <div class="tab-pane fade" id="basicThird" role="tabpanel">
                                <div class="card">
                                    <div class="card-body">
                                        <table id="post" class="table table-bordered table-striped">
                                  
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card --> 
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <!-- Basic End -->
          <!-- Banners End -->

          

        <!-- Right Modal kredit -->
                      <div class="modal modal-right fade" id="rightModalkerjaan" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">Formulir Tambah kerjaan</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                            <form method="post" action="/input/progress">
                            {{csrf_field()}}
                                <div class="form-group mb-3">
                                    <input type="hidden" class="form-control" name="id_project" value="" readonly>
                                </div>
                                <div class="form-group mb-3">
                                    <span class="text-bold">Nama Pekerjaan</span>
                                    <input type="text" class="form-control" name="nama_pekerjaan" required>
                                </div>
                                <div class="form-group mb-3">
                                    <span class="text-bold">Detail Pengerjaan</span>
                                    <textarea class="form-control" name="detail_pengerjaan" required></textarea>
                                </div>
                                <div class="form-group mb-3">
                                    <span class="text-bold">Penanggung Jawab</span>
                                    <input type="text" class="form-control" name="penanggungjawab" required>
                                </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Buat</button>
                            </div>
                            </form>
                          </div>
                        </div>
                      </div>
        <!-- end Right Modal Start -->


        <!-- Right Modal kredit -->
        <div class="modal modal-right fade" id="modalprogressupdate" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">Formulir Update Progress</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                            <form method="post" action="/progress/update">
                            {{csrf_field()}}
                                <div class="form-group mb-3">
                                    <input type="hidden" class="form-control" name="id_project" value="" readonly>
                                </div>
                                <div class="form-group mb-3">
                                    <span class="text-bold">Input Progress</span>
                                    <input name="progress" class="form-control" type="text" onkeyup="this.value = this.value.replace(/,/g, '.')">
                                </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                            </form>
                          </div>
                        </div>
                      </div>
        <!-- end Right Modal Start -->
</script>



@endsection