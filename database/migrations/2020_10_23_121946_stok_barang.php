<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StokBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('stok_barang', function (Blueprint $table) {
            $table->id();
            $table->string('code_barang');
            $table->string('nama_barang');
            $table->string('stok');
            $table->string('harga_modal');
            $table->string('harga-jual');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
