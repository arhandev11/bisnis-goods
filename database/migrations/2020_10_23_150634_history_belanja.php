<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HistoryBelanja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_belanja', function (Blueprint $table) {
            $table->id();
            $table->string('code_barang');
            $table->string('nama_barang');
            $table->string('jumlah');
            $table->string('harga_modal');
            $table->string('total_belanja');
            $table->string('supplier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
